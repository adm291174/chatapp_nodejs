var path = require('path')
var webpack = require('webpack')


module.exports = {
    name: 'express-config',
    mode: 'development',
    // devtool: 'cheap-module-eval-source-map',
    devtool: 'inline-source-map',
    entry: [
        'webpack-hot-middleware/client',
        'core-js/modules/es6.promise',  // added for promise support in IE
        'core-js/modules/es6.array.iterator', // added for promise support in IE
        '@babel/polyfill', // needed here because of default useBuiltIns = false in @babel/preset-env babel.rc preset option
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/js/',
        sourceMapFilename: '[file].map'
    },
    // Add module to global browser scope 
    /* resolve: {
        extensions: ['.js'],
        alias: {
            'core': path.resolve(__dirname, './src/core/core_functions')
        }   
    }, */
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
                $: 'jquery',
                jquery: 'jquery',
                jQuery: 'jquery',
                // 'window.jquery': 'jquery',
                'window.jQuery': 'jquery',
                // 'core': 'core'
       })
        // new webpack.NoEmitOnErrorsPlugin()
    ],
    module: { 
        rules: [
            {
                loader: ['babel-loader'],
                include: [
                            path.resolve(__dirname, 'src'),
                        ],
                test: /\.(js|jsx)$/,
                
            },
            {
                loader: ['eslint-loader'],
                enforce: 'pre',
                include: [
                            path.resolve(__dirname, 'src'),
                        ],
                test: /\.js$/,                
            },
            {
                test: require.resolve('jquery'),
                use: [{
                          loader: 'expose-loader',
                          options: '$'
                      },
                      { 
                          loader: 'expose-loader',
                          options: 'jQuery'
                     },

                ]
            },
            /* {
                test: require.resolve('./src/core/core_functions'),
                use: [{
                          loader: 'expose-loader',
                          options: 'core'
                      }
                ]
            }, */
               
        ]    
    }
}
