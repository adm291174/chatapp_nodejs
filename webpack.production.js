var path = require('path')
var webpack = require('webpack')
module.exports = {
    mode: 'production',
    entry: [
        '@babel/polyfill',        
        './src/index'
    ],
    output: {
        path: 'D:\\andrey\\develop\\a-project\\chatapp\\static\\js',
        // path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/js/'
    },
    // Add module to global browser scope 
    /* resolve: {
        extensions: ['.js'],
        alias: {
            'core': path.resolve(__dirname, './src/core/core_functions')
        }   
    }, */
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jquery: 'jquery',
            jQuery: 'jquery',
            // 'window.jquery': 'jquery',
            'window.jQuery': 'jquery',
            // 'core': 'core'
        }),
        // new webpack.NoEmitOnErrorsPlugin()
    ],
    module: { 
        rules: [
            {
                loader: ['babel-loader'],
                include: [
                            path.resolve(__dirname, 'src'),
                        ],
                test: /\.js$/,
                
            },
            {
                loader: ['eslint-loader'],
                enforce: 'pre',
                include: [
                            path.resolve(__dirname, 'src'),
                        ],
                test: /\.js$/,                
            },
            {
                test: require.resolve('jquery'),
                use: [{
                          loader: 'expose-loader',
                          options: '$'
                      },
                      { 
                          loader: 'expose-loader',
                          options: 'jQuery'
                     },

                ]
            },
            /* {
                test: require.resolve('./src/core/core_functions'),
                use: [{
                          loader: 'expose-loader',
                          options: 'core'
                      }
                ]
            }, */
               
        ]    
    }
}
