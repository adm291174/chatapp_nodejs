var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.config');
var express = require('express');
var app = new express();
var port = 3000;
var compiler = webpack(config);

var bodyParser = require('body-parser');
var multer = require('multer'); // v1.0.5
var upload = multer(); // for parsing multipart/form-data

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


var { apiCallsSrv } = require('./src/server_api/srv_test_api');
var { servRoot } = require('./src/server_api/srv_serv_root');
var { Sessions, mainCallsSrv } = require('./src/server_api/srv_test_main');
var { servStatic } = require('./src/server_api/srv_serv_static');
var { test_data_users } = require('./src/test_data/test_data_users');

// sessions object for sessions management 
var sessions = new Sessions(test_data_users);

// cookie parser for development use 
var cookieParser = require('cookie-parser');

app.use(webpackDevMiddleware(compiler, { noInfo: true, writeToDisk: true, publicPath: config.output.publicPath }));
app.use(webpackHotMiddleware(compiler));
app.use(cookieParser())

// serve static files 
servStatic(express, app);

// serve JSON api replies 
app.get('/test', function(req, res) {
    res.json({ user: 'andrey'});
    res.send('test ok');
});

// call API calls handler setup 
mainCallsSrv(app, sessions, __dirname); // serv auth and download urls 
apiCallsSrv(app, sessions);  // serv all api calls 
servRoot(app, sessions, __dirname, );  // serv root url 

app.listen(port, function(error) {
if (error) {
    console.error(error);
} else {
    console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', 
                 port, 
                 port);
}
})
