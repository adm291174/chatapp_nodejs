var path = require('path');
var webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: [
        '@babel/polyfill',
        './src/index'
    ],
    output: {
        // path: 'D:\\andrey\\develop\\a-project\\chatapp\\static\\js',
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/',
        sourceMapFilename: '[file].map'
    },
    optimization: {
        minimize: true,
        occurrenceOrder: true,
        usedExports: true,
        mangleWasmImports: true

        /* minimizer: [new UglifyJsPlugin({ 
            sourceMap: false, 
            uglifyOptions: {
                warnings: false,
                parse: {},
                // compress: {},
                mangle: true, // Note `mangle.properties` is `false` by default.
                output: {
                    comments: false,
                },
                toplevel: false,
                nameCache: null,
                ie8: false,
                keep_fnames: false,
            },
        })], */
    },
    
    // Add module to global browser scope 
    /* resolve: {
        extensions: ['.js'],
        alias: {
            'core': path.resolve(__dirname, './src/core/core_functions')
        }   
    }, */
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
                $: 'jquery',
                jquery: 'jquery',
                jQuery: 'jquery',
                // 'window.jquery': 'jquery',
                'window.jQuery': 'jquery',
                // 'core': 'core'
       })
        // new webpack.NoEmitOnErrorsPlugin()
    ],
    module: { 
        rules: [
            {
                loader: ['babel-loader'],
                include: [
                            path.resolve(__dirname, 'src'),
                        ],
                test: /\.js$/,
                
            },
            {
                loader: ['eslint-loader'],
                enforce: 'pre',
                include: [
                            path.resolve(__dirname, 'src'),
                        ],
                test: /\.js$/,                
            },
            {
                test: require.resolve('jquery'),
                use: [{
                          loader: 'expose-loader',
                          options: '$'
                      },
                      { 
                          loader: 'expose-loader',
                          options: 'jQuery'
                     },

                ]
            },
            /* {
                test: require.resolve('./src/core/core_functions'),
                use: [{
                          loader: 'expose-loader',
                          options: 'core'
                      }
                ]
            }, */
               
        ]    
    }
}
