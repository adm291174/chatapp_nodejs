var serveIndex = require('serve-index');
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.config');
var express = require('express');
var app = new express();
var port = 3000;
var compiler = webpack(config);

var bodyParser = require('body-parser');
var multer = require('multer'); // v1.0.5
var upload = multer(); // for parsing multipart/form-data

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


var { apiCallsSrv } = require('./src/server_api/srv_test_api');

// cookie parser for development use 
var cookieParser = require('cookie-parser');

app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
app.use(webpackHotMiddleware(compiler));
app.use(cookieParser())

// serve static files 
app.use('/static', express.static('D:\\Andrey\\develop\\a-project\\chatapp\\chat\\markup\\chat_v2\\static'), 
                   serveIndex('D:\\Andrey\\develop\\a-project\\chatapp\\chat\\markup\\chat_v2\\static'));

app.get('/', function(req, res) {
    console.log('Cookies:', req.cookies);
    res.cookie('session_id', 'sess-6a99657a-5e18-484f-ba21-f2762dd16435');
    res.cookie('csrftoken', 'csrf-6a4b1fcf-5fe4-42ad-a746-c09c7f40da12');
    res.sendFile(__dirname + '/index.html');
});

// serve JSON api replies 
app.get('/test', function(req, res) {
    res.json({ user: 'andrey'});
    res.send('test ok');
});

// call API calls handler setup 
apiCallsSrv(app);

app.listen(port, function(error) {
if (error) {
    console.error(error);
} else {
    console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', 
                 port, 
                 port);
}
})
