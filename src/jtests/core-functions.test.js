var core_functions = require('../core/core_functions');

var floatPrettyPrintForInput = core_functions.floatPrettyPrintForInput,
    roundAlt = core_functions.roundAlt;


describe('Core function testing', () => {
    test('floatPrettyPrintForInput', () => {
        expect(floatPrettyPrintForInput(2.22)).toBe('2.22');
        expect(floatPrettyPrintForInput(-2)).toBe('-2');
        expect(floatPrettyPrintForInput(-2.52, 1)).toBe('-2.5');
        expect(floatPrettyPrintForInput(129.25245, 3)).toBe( '129.252');
    });
    test('should return correct values on function call', () => {
        expect(roundAlt(2.225, 2)).toBe(2.23);
        expect(roundAlt(122.78271, 4)).toBe(122.7827);
        expect(roundAlt(0, 0)).toBe(0);
        expect(roundAlt(-0.06, 1)).toBe(-0.1);
        expect(roundAlt(-10.562, 3)).toBe(-10.562);
        expect(roundAlt(1092881092.56239012, 5)).toBe(1092881092.56239);
    });
});



