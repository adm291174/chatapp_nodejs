import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import DatePickerInput from '../elements/DatePickerInput';
import Datepicker from 'react-datepicker';

import { getDateFromStr } from '../core/core_functions';

import moment from 'moment';

const refreshValidityTimeout = 330;
const refreshBlurTimeout = 250;

class DatePickerCustom extends Component {
    constructor(props) {
        super(props);
        this.datepickerRef = React.createRef();
        this.state = {
            isValid: undefined    // validity of element, for element border rendering 
        }
    }

    setFocus() {
        const element = document.getElementById(this.props.id);
        element && element.focus();
    }

    handleValueChange(value) {        
        const { onChangeHandle, id } = this.props;
        // validate data here 
        this.setState({ isValid: !(value === null) }); 
        const dateValue = value ?  value.toDate() : null;
        onChangeHandle(id, dateValue); // pass elementId and value - for proper dispatching 
    }

    /* legacy - fixed with tab key press handling 
    handleOnFocus(e) {
        if (e.target.classList.contains('react-datepicker__navigation')) {
            const inputElementId = this.props.id;
            document.getElementById(inputElementId).focus();
        }
        // console.log(e.target.className);
    } */

    handleKeyDown(e) {
    /* handle Tab & Shift-Tab press here */ 
        if (e.keyCode == 9) {
            const focusingElement = document.getElementById(
                e.shiftKey ? this.props.prevElementId : this.props.nextElementId
            );
            const validityState = this.checkAndSetValidity();
            if (validityState) {
                if (focusingElement) {
                    focusingElement.focus();
                    this.datepickerRef.current.setOpen(false);                   
                }
            }
            e.preventDefault();
        }
    }  

    handleBlur() {
        // Fix react-datepicker invalid input values handling 
        const calledFunction = this.checkAndSetValidity.bind(this);
        setTimeout(calledFunction, refreshBlurTimeout);
    }

    checkAndSetValidity() {
        const element = document.getElementById(this.props.id);
        if (element) {
            const inputElementValue = element.value;
            const validityState = this.validateInput(inputElementValue);
            this.setState({ isValid: validityState}); 
            return validityState;
        }
        else return false;        
    }

    validateInput(inputElementValue) {
        return (getDateFromStr(inputElementValue, 'DD.MM.YYYY') !== null);
    }
    
    componentDidMount() { 
        // Fix react-datepicker invalid class manipulation 
        this.setState( { 
            timerId: setInterval( 
                        this.setInputElementValidityState.bind(this), 
                        refreshValidityTimeout)
        });
    } 

    componentWillUnmount() {
        clearInterval(this.state.timerId);
    }

    setInputElementValidityState() {
        const { isValid } = this.state;
        const { disabled, id } = this.props;
        const showInvalidClass = (isValid !== undefined 
                                  && (!disabled) 
                                  && (isValid == false)),
                element = document.getElementById(id);
        const { showValidClass } = this.props;
        if (showInvalidClass) {
                element.classList.add('is-invalid');
                showValidClass && element.classList.remove('is-valid');
            }
            else {
                element.classList.remove('is-invalid');
                showValidClass && element.classList.add('is-valid');
            }
    } 

    componentDidUpdate(prevProps) {
        const { disabled } = this.props;
        if (prevProps.disabled && !disabled) {
            this.checkAndSetValidity(); 
        }
    }
    

    render () {
        const { title, id, name, value, disabled, 
                blockElementClass, headerElementClass,
                bodyElementGroup, useStub } = this.props;
        const pickerValue = value instanceof Date ? moment(value) : null;
        
        return (
            
            <div className={ blockElementClass } >
                <div className={ headerElementClass } >{ title }</div>
                <div className={ bodyElementGroup }>
                    <Datepicker 
                        customInput = { <DatePickerInput /> }
                        ref = { this.datepickerRef }
                        onChange={ this.handleValueChange.bind(this) }
                        id={ id }
                        name={ name }
                        selected={ pickerValue }
                        dateFormat='DD.MM.YYYY'
                        onKeyDown = { this.handleKeyDown.bind(this) }
                        disabled = { disabled }
                        onBlur = { this.handleBlur.bind(this) }
                        className = 'form-control form-control__datepicker'
                        key = { disabled ? 'disabled' : 'enabled' }
                        locale='ru-ru'
                    />
                    { useStub ? <div className='fi-stub'></div> : null }
                </div>
            </div>
        );
    }

}

DatePickerCustom.propTypes = {
    nextElementId: PropTypes.string,
    prevElementId: PropTypes.string,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.instanceOf(Date),
    onChangeHandle: PropTypes.func.isRequired,
    blockElementClass: PropTypes.string.isRequired,
    headerElementClass: PropTypes.string.isRequired, 
    bodyElementGroup: PropTypes.string.isRequired,
    showValidClass: PropTypes.bool,
    useStub: PropTypes.bool
}

// export default DatePickerCustom;
export default React.forwardRef(({forwardedRef, ...props}, ref) => 
    <DatePickerCustom {...props} ref={forwardedRef} />
); 

