import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { getDateStr, floatPrettyPrint } from '../core/core_functions';


class OrdersListItem extends Component { 

    renderClientName(isSmall, orderClientName) {
        return !isSmall ? <td className='align-text-left'>{ orderClientName }</td> : null;
    }

    renderNumber(isSmall, orderId, orderNumber, orderDate) {
        const { onOrderEditClick } = this.props;
        return (isSmall ? <td>
                    <a href='#' onClick={ onOrderEditClick } 
                       data-order-id={ orderId.toString() }>
                          { orderNumber }</a><br/>
                          { getDateStr(orderDate) }
                    </td>
               : <td><a href='#' onClick= { onOrderEditClick } 
                    data-order-id={ orderId.toString() } >{ orderNumber }</a></td>
        );
    }
    
    renderDate(isSmall, orderDate) {
        return !isSmall ? <td>{ getDateStr(orderDate) }</td> : null;
                    

    }


    render() {
        const { orderId, orderDate, orderStatusDescription, 
                orderClientName, isSmall, orderSum, orderCurrencyCode, orderTypeDescription,
                orderNumber } = this.props;

        const sumDescription = floatPrettyPrint(orderSum) + ' ' + orderCurrencyCode;
        return (
        <tr>
            { this.renderNumber(isSmall, orderId, orderNumber, orderDate) }
            { this.renderDate(isSmall, orderDate) }
            <td>{ orderStatusDescription }</td>
            { this.renderClientName(isSmall, orderClientName) }
            <td className='align-text-right'>{ sumDescription }</td>
            <td>{ orderTypeDescription }</td>                        
        </tr>
        );
    }
}

OrdersListItem.propTypes = {
    orderId: PropTypes.number.isRequired,
    orderDate: PropTypes.instanceOf(Date).isRequired,
    orderNumber: PropTypes.string.isRequired,
    orderStatusDescription: PropTypes.string.isRequired,
    orderClientName: PropTypes.string.isRequired,
    orderSum: PropTypes.number.isRequired,
    orderCurrencyCode: PropTypes.string.isRequired,
    orderTypeDescription: PropTypes.string.isRequired,
    onOrderEditClick: PropTypes.func.isRequired,
    isSmall: PropTypes.bool.isRequired, 
}

export default OrdersListItem; 

