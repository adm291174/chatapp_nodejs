import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

class DatePickerInput extends Component {
    
    render () {
        const { onClick, onChange, onKeyDown, value, 
                name, id, disabled, className } = this.props;

        return (
            <div>
                <input type='text' className={ className } placeholder='' 
                    name={ name } 
                    id={ id }
                    onChange={ onChange }
                    onClick = { onClick }
                    onKeyDown = { onKeyDown }
                    value={ value }
                    disabled={ disabled }
                    onBlur = { this.props.onBlur }
                    />
                    
            </div>
        )
    }
}

DatePickerInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  name: PropTypes.string,
  id: PropTypes.string,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
};


/* const ClickableInput = (props) => {
    const { onClick } = props;
    const localProps = props;
    console.log('ClickableInput props: ', Object.keys(localProps));
    return (<div onClick={ onClick }>
        <DatePickerInput { ...localProps } />
    </div>);
}  */  


export default DatePickerInput;