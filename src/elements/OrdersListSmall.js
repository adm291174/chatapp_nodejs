import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import OrdersListItem from './OrdersListItem';

import { List } from 'immutable';

class OrdersListSmall extends Component {

    renderTableValues(clientsOrders) {
        const onOrderEditClick = this.props.onOrderEditClick;
        const contents = clientsOrders.map( orderItem => {
                const orderId = orderItem.get('orderId');            
                return (<OrdersListItem 
                    key={ orderId }
                    orderId={ orderId }
                    orderNumber={ orderItem.get('orderNumber') }
                    orderDate={ orderItem.get('orderDate') }
                    orderStatusDescription={ orderItem.get('orderStatusDescription') }
                    orderClientName={ orderItem.get('orderClientName') }
                    orderSum={ orderItem.get('orderSum') }
                    orderCurrencyCode={ orderItem.get('orderCurrencyCode') }
                    orderTypeDescription={ orderItem.get('orderTypeDescription') }
                    onOrderEditClick={ onOrderEditClick }
                    isSmall = { true }
                />)
            })

        return contents;
    }

    renderListItem(clientListItem) {
        const clientName = clientListItem.get('clientName'), 
              clientId = clientListItem.get('clientId'), 
              clientOrders = clientListItem.get('clientOrders');

        return (
            <div className='orders-list__company-wrapper' key={ clientId }>
                <div className='orders-list__company-name'>{ clientName } </div>
                <table>
                    <thead>
                        <tr>
                            <td>Номер<br/>Дата</td>
                            <td>Статус</td>
                            <td>Сумма</td>
                            <td>Тип ордера</td>
                        </tr>
                    </thead>
                    <tbody>
                        { this.renderTableValues(clientOrders) }
                    </tbody>
                </table>
            </div>
        );
    }


    render() {
        const { clientsList } = this.props;
        const contents = clientsList.map( value => { return this.renderListItem(value) });
        return contents;
    }
}

OrdersListSmall.propTypes = {
    clientsList: PropTypes.instanceOf(List).isRequired,
    onOrderEditClick: PropTypes.func.isRequired
}

export default OrdersListSmall;