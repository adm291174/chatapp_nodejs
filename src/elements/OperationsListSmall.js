import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import OperationsListItem from './OperationsListItem';

import { List } from 'immutable';

class OperationsListSmall extends Component {

    renderTableValues(clientsOrders) {
        const onOperationEditClick = this.props.onOperationEditClick;
        const contents = clientsOrders.map( operationItem => {
                const operationId = operationItem.get('operationId');            
                return (<OperationsListItem 
                    key={ operationId }
                    operationId={ operationId }
                    operationNumber={ operationItem.get('operationNumber') }
                    operationDate={ operationItem.get('operationDate') }
                    operationStatusDescription={ operationItem.get('operationStatusDescription') }
                    clients={ operationItem.get('clients') }
                    operationSum={ operationItem.get('operationSum') }
                    operationCurrencyCode={ operationItem.get('operationCurrencyCode') }
                    operationTypeDescription={ operationItem.get('operationTypeDescription') }
                    onOperationEditClick={ onOperationEditClick }
                    isSmall = { true }
                />)
            })

        return contents;
    }

    renderListItem(clientListItem) {
        const clientName = clientListItem.get('clientName'), 
              clientId = clientListItem.get('clientId'), 
              clientOrders = clientListItem.get('clientOperations');

        return (
            <div className='operations-list__company-wrapper' key={ clientId }>
                <div className='operations-list__company-name'>{ clientName } </div>
                <table>
                    <thead>
                        <tr>
                            <td>Номер<br/>Дата</td>
                            <td>Тип</td>
                            <td>Статус</td>
                            <td>Сумма</td>
                        </tr>
                    </thead>
                    <tbody>
                        { this.renderTableValues(clientOrders) }
                    </tbody>
                </table>
            </div>
        );
    }


    render() {
        const { clientsList } = this.props;
        const contents = clientsList.map( value => { return this.renderListItem(value) });
        return contents;
    }
}

OperationsListSmall.propTypes = {
    clientsList: PropTypes.instanceOf(List).isRequired,
    onOperationEditClick: PropTypes.func.isRequired
}

export default OperationsListSmall;