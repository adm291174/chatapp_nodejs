import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { Map } from 'immutable';
import { getDateStr, getCurrentDate } from '../core/core_functions';
import { showLabelValidityState } from '../core/validation_functions';
import { operationPickerAccsFilterFieldsFunc, 
         operationPickerAccsListColumns,
         operationPickerLOFilterFieldsFunc, 
         operationPickerLOListColumns } from '../constants/picker_constants';

import Picker from './picker/Picker';
import { supplyOperationType } from '../constants/operations_constants';

class OperationPartedCommon extends Component {
    
    constructor(props) {
        super(props);
        this.state={
            pickAccountElementCollapsed: true,
            pickLinkedOrdersElementCollapsed: true,
            filterValues: Map({
                'id_fi-pf-supplier-account-picker-filter': '',
                'id_fi-pf-supplier-account-picker-client-ro': 'Поставщик',
                'id_fi-pf-buyer-account-picker-filter': '',
                'id_fi-pf-buyer-account-picker-client-ro': 'Покупатель',
                'id_fi-pf-supplier-lo-picker-order-type-ro': 'Поставка',
                'id_fi-pf-supplier-lo-picker-date': {
                    startDate: getCurrentDate(),
                    endDate: getCurrentDate()
                },
                'id_fi-pf-buyer-lo-picker-order-type-ro': 'Покупка',
                'id_fi-pf-buyer-lo-picker-date': {
                    startDate: getCurrentDate(),
                    endDate: getCurrentDate()
                }
            })
        }
    }

    validate(needToSetFocus) {
        /* validate function for component. If needToSetFocus is true - focus on first invalid element 
           don't pass needToSetFocus == true, if precedent OperationTransferPart component is invalid  */  
        const [ accountElementState, accountElement ] = this.validateAccount(),
              [ linkedOrderElementState, linkedOrderElement] = this.validateLinkedOrder(),
              validationState = (accountElementState && linkedOrderElementState);

        if (needToSetFocus && !validationState) {
            if (!accountElementState) accountElement.focus()
            else if (!linkedOrderElementState) linkedOrderElement.focus()
        }
        return validationState;
    }

    validateAccount() {
        /* validate account data and returns state and element reference 
           set element invalid class if validation failed  
           return button element for focus station 
        */
        const { operationData } = this.props,
              { accountPickerDef, componentData } = this.getComponentParams(operationData),
              accountId = componentData.get(accountPickerDef.elementValueId),
              accountElementId = accountPickerDef.elementId,
              btnElementId = accountPickerDef.btnElementId;
        
        const accountValidityState = (accountId > 0),
              accountElement = document.getElementById(accountElementId),
              accountBtnElement = document.getElementById(btnElementId);

        showLabelValidityState(accountElement, accountValidityState, 'fi__label_invalid');
        return [ accountValidityState, accountBtnElement ];
    }

    validateLinkedOrder() {
        const { operationData } = this.props,
              { linkedToOrderDef, componentData } = this.getComponentParams(operationData),
              orderId = componentData.get(linkedToOrderDef.elementValueId),
              orderElementId = linkedToOrderDef.elementId,
              btnElementId = linkedToOrderDef.btnElementId;
        
        const linkedOrderValidityState = (orderId > 0),
              orderElement = document.getElementById(orderElementId),
              orderBtnElement = document.getElementById(btnElementId);

        showLabelValidityState(orderElement, linkedOrderValidityState, 'fi__label_invalid');
        return [ linkedOrderValidityState, orderBtnElement ];
    }

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState({ 
            filterValues: filterValues.set(id, value)
        });
    }

    onPickerCancel(elementId) {
        // set state and clear picker data 
        const { operationData } = this.props,
              { accountPickerDef, linkedToOrderDef } = this.getComponentParams(operationData);
        
        switch(elementId) {
            case accountPickerDef.pickerElementId: {
                this.setState({ pickAccountElementCollapsed: true });
                break;        
            }
            case linkedToOrderDef.pickerElementId: {
                this.setState({ pickLinkedOrdersElementCollapsed: true });
                break;        
            }
        }
    } 

    onDataPick(value) {
        const { elementId } = value;
        this.onPickerCancel(elementId);
        this.props.onDataPick(value);
        // validate data after picker apply - if data invalid clear it 
        switch (elementId) {
            case 'id_fi-pf-supplier-account-picker':
            case 'id_fi-pf-buyer-account-picker': {
                const _this = this;
                setTimeout(() => { _this.validateAccount() }, 500);
                break;
            }
            case 'id_fi-pf-supplier-linked-order-picker':
            case 'id_fi-pf-buyer-linked-order-picker': {
                const _this = this;
                setTimeout(() => { _this.validateLinkedOrder() }, 500);
                break;
            }
        }
    }

    onfilterButtonClick(e) {
        e.preventDefault();
        const { operationData } = this.props,
              { accountPickerDef, 
                linkedToOrderDef } = this.getComponentParams(operationData),
                elementId = e.currentTarget.id;

        switch(elementId) {
            case accountPickerDef.btnElementId: {
                const pickAccountElementCollapsed = this.state.pickAccountElementCollapsed;
                this.setState({ pickAccountElementCollapsed: !pickAccountElementCollapsed });
                break;        
            }
            case linkedToOrderDef.btnElementId: {
                const pickLinkedOrdersElementCollapsed = this.state.pickLinkedOrdersElementCollapsed;
                this.setState({ pickLinkedOrdersElementCollapsed: !pickLinkedOrdersElementCollapsed });
                break;        
            }
        }
    }

    internalValidate(markValidationState = false) {
        let validationState = true,
            firstInvalidElement = undefined;

        this.componentRefs.forEach(elementRef => {
            const referredElement = elementRef.current.validate ? 
                                    elementRef.current : 
                                    null;
            if (referredElement) {
                // call internalValidate here 
                const [ currentElementState, localInvalidElement ] = referredElement.internalValidate.call(referredElement);
                if (!currentElementState) {
                    validationState = false;
                    firstInvalidElement = firstInvalidElement ? 
                                          firstInvalidElement : 
                                          localInvalidElement;
                    markValidationState && firstInvalidElement.setFocus();
                }

                markValidationState && 
                   referredElement.setValidationState(currentElementState);
                
            }
        })
        console.log('ValidationState: ', validationState, 
                    'markValidationState:', markValidationState,
                    'firstInvalidElement: ', firstInvalidElement);
        return [validationState, firstInvalidElement];
    }

    componentDidUpdate(prevProps, prevState) {
        // handle pickerChanges 
        const { pickAccountElementCollapsed, 
                pickLinkedOrdersElementCollapsed,
                filterValues } = this.state,
                prevPickAccountElementCollapsed = prevState.pickAccountElementCollapsed,
                prevPickLinkedOrdersElementCollapsed = prevState.pickLinkedOrdersElementCollapsed,
                prevFilterValues = prevState.filterValues;

        const { getPickerData, operationData, processPickerOpen, processAccountChange } = this.props,
              {  isSupplyComponent, 
                 componentData, 
                 accountPickerDef, 
                 linkedToOrderDef } = this.getComponentParams(operationData);

        const accountId = componentData.get(accountPickerDef.elementValueId),
              prevComponentData = this.getComponentParams(prevProps.operationData).componentData,
              prevAccountId = prevComponentData.get(accountPickerDef.elementValueId);

        if (pickAccountElementCollapsed != prevPickAccountElementCollapsed) {
            getPickerData(accountPickerDef.pickerElementId, 
                          pickAccountElementCollapsed,
                          { accountType: accountPickerDef.accSelectorCodeParameter });
            !pickAccountElementCollapsed && processPickerOpen(isSupplyComponent, 
                                                              accountPickerDef.pickerElementId)
        }
        if (pickLinkedOrdersElementCollapsed != prevPickLinkedOrdersElementCollapsed || 
            (!pickLinkedOrdersElementCollapsed && !filterValues.equals(prevFilterValues))) {
            const accountId = componentData.get(accountPickerDef.elementValueId),
                  filterDates = this.state.filterValues.get(isSupplyComponent ?  
                                                 'id_fi-pf-supplier-lo-picker-date' : 
                                                 'id_fi-pf-buyer-lo-picker-date' ),
                  startDate = filterDates.startDate,
                  endDate = filterDates.endDate,
                  linkedOrderId = null;
                  
            getPickerData(linkedToOrderDef.pickerElementId, 
                          pickLinkedOrdersElementCollapsed,
                          { accountId, startDate, endDate, linkedOrderId });
            !pickLinkedOrdersElementCollapsed && processPickerOpen(isSupplyComponent, 
                                                                   linkedToOrderDef.pickerElementId)
        }

        if (accountId != prevAccountId) {
            processAccountChange();
        }
    }

    getComponentParams(operationData) {
        const { defs } = this.props;
        const operationType = operationData.get('operationType'),
              isSupplyComponent = (operationType == supplyOperationType),
              componentData = isSupplyComponent ? 
                              operationData.get('supplyData') :
                              operationData.get('settlementData');
        
        const accountPickerDef = defs.accountPicker,
              accountClientDef = defs.accountClient, 
              linkedToOrderDef = defs.linkedToOrder,
              pickerData = isSupplyComponent ? 
                                operationData.get('supplyPickerData') :
                                operationData.get('settlementPickerData');
        
        return { isSupplyComponent, 
                 componentData, 
                 accountPickerDef, 
                 accountClientDef,
                 linkedToOrderDef,
                 pickerData }
    } 

    render() {
        const { operationIsEdited, 
                onClearBtnClick, 
                operationData, 
                onLinkClick } = this.props,
              { pickAccountElementCollapsed,
                pickLinkedOrdersElementCollapsed,                
                filterValues } = this.state;

        const {  isSupplyComponent, 
                 componentData, 
                 accountPickerDef, 
                 accountClientDef,
                 linkedToOrderDef,
                 pickerData } = this.getComponentParams(operationData);

        const accountNumber = componentData.get(accountPickerDef.elementValue),
              clientId = componentData.get(accountClientDef.elementValueId),
              clientName = componentData.get(accountClientDef.elementValue),
              linkedOrderId = componentData.get(linkedToOrderDef.elementValueId),
              linkedOrderInfo = linkedOrderId ? 
                                componentData.get(linkedToOrderDef.orderInfoElement)
                                             .get(linkedToOrderDef.orderNumberValue) + 
                                ' от ' + 
                                getDateStr(componentData.get(linkedToOrderDef.orderInfoElement)
                                                        .get(linkedToOrderDef.orderDateValue)) : '';

        const accountPickerName = isSupplyComponent ? 
                                  'supplyAccounts' : 
                                  'settlementAccounts',
              linkedOrdersPickerName = isSupplyComponent ? 
                                       'supplyLinkedOrdersData' : 
                                       'settlementLinkedOrdersData';
        const accountsPickerData = pickerData.get(accountPickerName),
              linkedOrdersPickerData = pickerData.get(linkedOrdersPickerName);

        const onfilterButtonClick = this.onfilterButtonClick.bind(this);
        
        return (
            <div>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountPickerDef.elementName }</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' 
                                   id={ accountPickerDef.elementId }>{ accountNumber }</label>
                            { operationIsEdited ? 
                            <button className='btn img-wrapper img-wrapper__btn' 
                                    aria-label='Выбрать счет'
                                    id={ accountPickerDef.btnElementId }
                                    data-toggle='collapse'
                                    data-target={ '#'+accountPickerDef.pickerElementId }
                                    aria-expanded= { !pickAccountElementCollapsed }
                                    onClick={ onfilterButtonClick }>
                                <img className='img-wrapper__img' 
                                     src='static/images/select-from-list.png' 
                                     alt='Select account' />
                            </button> : <div className='fi-stub'></div> }
                        </div>
                    </div>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountClientDef.elementName }</div>
                        <label className='fi__label fi__element' 
                                id={ accountClientDef.elementId }>{ clientName }</label>
                    </div>
                </div>
                <Picker
                    id={ accountPickerDef.pickerElementId }
                    isExpanded = { !pickAccountElementCollapsed }
                    listData = { accountsPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerAccsListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { operationPickerAccsFilterFieldsFunc(
                                        accountPickerDef.pickerFilterFieldId, 
                                        accountPickerDef.pickerClientTypeFieldId) }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = { isSupplyComponent
                                         ? 'Выберите счет поставщика'
                                         : 'Выберите счет покупателя'}/>

                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ linkedToOrderDef.elementName }</div>
                        <div className='form-input__element-group'>
                            { linkedOrderId && !operationIsEdited ? (
                            <a href='#' 
                               className='fi__label fi__element fi__label_href' 
                               id={ linkedToOrderDef.elementId }
                               data-order-id={ linkedOrderId.toString() }
                               onClick={ onLinkClick }>{ linkedOrderInfo }</a> )
                            : (
                               <label 
                               className='fi__label fi__element' 
                               id={ linkedToOrderDef.elementId }>{ linkedOrderInfo }</label> )
                            }
                            { operationIsEdited ? 
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        aria-label='Выбрать ордер'
                                        id={ linkedToOrderDef.btnElementId }
                                        onClick={ onfilterButtonClick }
                                        disabled={ !(clientId > 0) }>                                 
                                    <img className='img-wrapper__img' 
                                         src='static/images/select-from-list.png' 
                                         alt='Select order' />
                                </button> : null }                                  
                            { operationIsEdited ? 
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        aria-label='Очистить'
                                        id={ linkedToOrderDef.btnClearElementId }
                                        onClick={ onClearBtnClick }>
                                    <img className='img-wrapper__img' 
                                         src='static/images/filter-clear.png' 
                                         alt='Clear' />                                    
                                </button> : null } 
                        </div>
                    </div>
                </div>
                <Picker
                    id={ linkedToOrderDef.pickerElementId }
                    isExpanded = { !pickLinkedOrdersElementCollapsed }
                    listData = { linkedOrdersPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerLOListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { operationPickerLOFilterFieldsFunc(
                                        linkedToOrderDef.pickerDateFieldId,
                                        linkedToOrderDef.pickerOrderTypeFieldId,
                                        linkedToOrderDef.pickerElementId + '__btn',
                                        linkedToOrderDef.btnElementId
                                    ) }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = { isSupplyComponent
                                         ? 'Выберите ордер поставщика'
                                         : 'Выберите ордер покупателя'}/>
            </div>
            )
    }
}   

OperationPartedCommon.propTypes = {
    operationId: PropTypes.number,
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map).isRequired,    
    onDataPick: PropTypes.func.isRequired,
    getPickerData: PropTypes.func.isRequired,
    processPickerOpen: PropTypes.func.isRequired,
    onClearBtnClick: PropTypes.func.isRequired,
    defs: PropTypes.object.isRequired,
    onLinkClick: PropTypes.func.isRequired
}

export { OperationPartedCommon };
