import React, { Component, Suspense } from 'react';
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom'));

// import DatePickerCustom from '../elements/DatePickerCustom';

/* Order main information part: Main Sum Info  */ 

class OrderEditDate extends Component {
    constructor(props) {
        super(props);
        this.DatePickerCustomRef = React.createRef();
    }

    validate() {
        return this.DatePickerCustomRef.current 
            && this.DatePickerCustomRef.current.checkAndSetValidity();
    }

    setFocus() {
        if (this.DatePickerCustomRef.current) {
            this.DatePickerCustomRef.current.setFocus()
        }
    }

    setValidationState() {
        this.DatePickerCustomRef.current 
          && this.DatePickerCustomRef.current.setInputElementValidityState();
    }


    render() {

        const { orderIsEdited, orderBodyData, onDatePickerChange } = this.props;
        const orderDate = orderBodyData.get('orderDate');

        return (
           <Suspense fallback={ <RefreshIndicator/> }
                      >
                <DatePickerCustom 
                id='id_fi-order-date'
                name='order-date'
                title='Дата ордера'
                nextElementId='id_fi-order-account-picker'
                prevElementId='id_fi-order-type'
                value={ orderDate }
                onChangeHandle={ onDatePickerChange }
                disabled = { !orderIsEdited }
                blockElementClass='form-input__block'
                headerElementClass='fi-block__header'
                bodyElementGroup='form-input__element'
                showValidClass = { orderIsEdited }
                forwardedRef={ this.DatePickerCustomRef }
            />
        </Suspense>);
    }
}

OrderEditDate.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired, 
    orderBodyData: PropTypes.instanceOf(Map),
    onDatePickerChange: PropTypes.func
}

export default OrderEditDate;