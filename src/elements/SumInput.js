import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { floatPrettyPrintForInput } from '../core/core_functions';
import { validateNumberRe, showElementValidityState } from '../core/validation_functions';


class SumInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: Map({
                isValid: undefined,
                currentEditValue: ''
            })
        }
    }

    componentDidMount() {
        const { value, fractionDigits } = this.props;
        if (value) {
            this.setState( { data: this.state.data.merge(
                                 { currentEditValue: floatPrettyPrintForInput(value,
                                    fractionDigits ? fractionDigits : 2) })
            }) 
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.state.data.equals(prevState.data)) {
            // state has been changed!

            const isValid = this.state.data.get('isValid');
            // call external onValidation if hook is set 
            if ( isValid != prevState.data.get('isValid')) {
                const { onValidation, id } = this.props;
                if (onValidation) {
                    onValidation(isValid, id);
                }   
            
            }
        }
        
        const oldValue = prevProps.value,
              currentValue = this.props.value,
              changeValueOnElementChange = this.props,
              thisElement = document.getElementById(this.props.id),
              elementIsFocused = (thisElement == document.activeElement);
        
        if (oldValue != currentValue && 
            !(changeValueOnElementChange && elementIsFocused)) {
            // render new value 
            const { fractionDigits } = this.props;
        
            this.setState( { data:  this.state.data.merge(
                { currentEditValue: floatPrettyPrintForInput(currentValue,
                                    fractionDigits ? fractionDigits : 2) })
            });                        
        }
    }

    simpleChange(value) {
        this.performValidateProcess(value);
        const { changeValueOnElementChange } = this.props;
        if (changeValueOnElementChange) this.valueChange(value, false)
        else this.performValidateProcess(value);
    }

    valueChange(value, changeState = false) {
        const isValid = this.performValidateProcess(value);
        /* and set new float value of component */ 
        if (isValid) {
            const { onElementChange, id } = this.props,
                  newValue = this.convertValue(value);
                  const { fractionDigits } = this.props;
                  const newEditValue = floatPrettyPrintForInput(newValue,
                                          fractionDigits ? fractionDigits : 2),   
                        boundedNewValue = parseFloat(newEditValue);
                  
        
            
            if (onElementChange && isFinite(boundedNewValue)) {
                onElementChange(boundedNewValue, id);
                if (changeState) {
                    this.performValidateProcess(newEditValue);
                }
            }
        }
    }

    onChange(e) {
        /* we perform validation only on change */ 
        const value = e.target.value;
        this.simpleChange(value);        
    }

    onBlur(e) {
        /* validate and change input on Blur event */ 
        const value = e.target.value;
        this.valueChange(value, true);
    }

    convertValue(inputValue) {
        /* convert input value into number */ 

        const newValue = (typeof(inputValue) == 'string') ? validateNumberRe(inputValue) : undefined;
        const parsedValue = newValue ? parseFloat(newValue) : undefined;
        return parsedValue;
    }

    internalValidate(inputValue) {
        /* validate inputValue and return true/false depends on input value */ 
        const parsedValue = this.convertValue(inputValue);
        const { prohibitNegativeValues, maxValue } = this.props;
        if (prohibitNegativeValues === true && parsedValue < 0) return false;
        if (maxValue && parsedValue > maxValue) return false;
        return isFinite(parsedValue);
    }

    validate(needToSetFocus = false) {
        /* function for validating element */ 
        const { id, showComponentStateOnValidation } = this.props,
              element = document.getElementById(id);
        if (element) {
            const value = element.value,
                  validateState = this.internalValidate(value);
            
            if (needToSetFocus && !validateState) this.setFocus(); // adopted for mass validation calls
            if (showComponentStateOnValidation) 
               showElementValidityState(element, validateState);
            return validateState;
        }
        else return false;
    }
    
    setFocus() {
        const element = document.getElementById(this.props.id);
        element && element.focus();
    }

    performValidateProcess(value) {
        /* validate input value and set state of element 
           call onValidation function if it set

           param: text input 
           return: status of validation 
        */
        const isValid = this.internalValidate(value);
        this.setState( { data: this.state.data.set('isValid', isValid)
                                              .set('currentEditValue', value) });
        return isValid;
    } 

    
    render() {
        const { className, id } = this.props,
              fixedClassName = className ? className : '';

        const textValue = this.state.data.get('currentEditValue');
        return (
            <input className = { fixedClassName } 
                   id = { id }
                   onChange = { this.onChange.bind(this) }
                   onBlur = { this.onBlur.bind(this) }
                   value = { textValue }
            />
        );
    }
}

SumInput.propTypes = {
    id: PropTypes.string.isRequired,
    className: PropTypes.string,
    value: PropTypes.number,
    onElementChange: PropTypes.func.isRequired,
    onValidation: PropTypes.func,
    fractionDigits: PropTypes.number,
    prohibitNegativeValues: PropTypes.bool,
    changeValueOnElementChange: PropTypes.bool,
    showComponentStateOnValidation: PropTypes.bool,
    maxValue: PropTypes.number,
}

export default SumInput;