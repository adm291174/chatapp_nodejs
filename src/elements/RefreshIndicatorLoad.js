import React, { Suspense } from 'react';

const RefreshIndicator = React.lazy(() => import('../elements/RefreshIndicator'));

const RefreshIndicatorLoad = function(isLoading = true) {
    if (isLoading) {
        return <Suspense fallback={ <div className='refresh-indicator refresh-indicator__text'>Загрузка компонентов ...</div> }>
            <RefreshIndicator/>
        </Suspense>
    }
    else return null;
}

export default RefreshIndicatorLoad;