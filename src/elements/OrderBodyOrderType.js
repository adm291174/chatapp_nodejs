import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { orderTypes } from '../constants/order_constants';
import { Map } from 'immutable';

/* Order main information part: Order Type info */ 

class OrderBodyOrderType extends Component {

    renderSelectOptions() {
        return orderTypes.map(value => {
            return <option value={ value.orderType } 
                           key={ value.orderType }>{ value.orderTypeDescription }</option>
        });
    }

    render() {
        const { orderId, orderIsEdited, orderBodyData, onChange } = this.props;


        if (orderIsEdited && !orderId) {
            const orderType = orderBodyData.get('orderType');

            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Тип ордера</div>
                    <div className='form-input__element-group'>
                        <select className='form-control fi__element' 
                                id='id_fi-order-type'
                                value={ orderType } 
                                onChange = { onChange }>
                            { this.renderSelectOptions() }
                        </select>
                        <div className='fi-stub'></div>
                    </div>
                </div>
            );
        }
        else {
            const orderTypeDescription = orderBodyData.get('orderTypeDescription');
            
            return(
                <div className='form-input__block'>
                    <div className='fi-block__header'>Тип ордера</div>
                    <div className='form-input__element-group'>
                        <div className='fi__label fi__element' 
                            id='id_fi-order-type_ro'>
                            { orderTypeDescription }                                
                        </div>
                        <div className='fi-stub'></div>
                    </div>
                </div>
            );
        }
    }
}

OrderBodyOrderType.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired, 
    orderBodyData: PropTypes.instanceOf(Map),
}

export default OrderBodyOrderType;