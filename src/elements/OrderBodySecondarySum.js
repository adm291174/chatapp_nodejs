import React from 'react';
import { PropTypes } from 'prop-types';
import { floatPrettyPrint } from '../core/core_functions';
import { Map } from 'immutable';

/* Order main information part: Secondary Sum Info */ 

const OrderBodySecondarySum = function(props) {
    const { orderBodyData } = props;

    const secondarySum = orderBodyData.get('orderSecondarySum'), 
          secondaryCurrency = orderBodyData.get('orderAccountCurrencyCode')
    
    const strData = secondarySum 
                    ? floatPrettyPrint(secondarySum) + ' ' + secondaryCurrency 
                    : '';
    return (
        <div className='form-input__row'>
            <div className='form-input__block'>
                <div className='fi-block__header'>Сумма поставки</div>
                <div className='form-input__element-group'>
                    <div className='fi__label_noborder' 
                            id='id_fi-amount-fee-sum_ro'>
                        { strData }
                    </div>
                </div>
            </div>                            
        </div>
    );
}


OrderBodySecondarySum.propTypes = {
    orderBodyData: PropTypes.instanceOf(Map)
}

export default OrderBodySecondarySum;