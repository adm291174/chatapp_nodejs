import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { Map } from 'immutable';
import SumInput from '../elements/SumInput';
import { getDateStr, floatPrettyPrint, getCurrentDate } from '../core/core_functions';
import { showElementValidityState, showLabelValidityState } from '../core/validation_functions';
import { operationPickerAccsFilterFieldsFunc, 
        operationPickerAccsListColumns,
        operationPickerPayReqsFilterFields,
        operationPickerPayReqsListColumns,
        operationPickerLOFilterFieldsFunc, 
        operationPickerLOListColumns } from '../constants/picker_constants';
import Picker from '../elements/picker/Picker';



class OperationTransferPart extends Component {
    
    constructor(props) {
        super(props);
        this.sumElementRef = React.createRef();
        this.state={
            pickAccountElementCollapsed: true,
            pickLinkedOrdersElementCollapsed: true,
            pickPayReqsElementCollapsed: true,
            filterValues: Map({
                'id_fi-payer-account-picker-filter': '',
                'id_fi-payer-account-picker-client-ro': 'Покупатель',
                'id_fi-payee-account-picker-filter': '',
                'id_fi-payee-account-picker-client-ro': 'Поставщик',
                'id_fi-payer-payreq-picker-filter': '',
                'id_fi-payee-payreq-picker-filter': '',
                'id_fi-payer-payreq-picker-client-ro': '',
                'id_fi-payee-payreq-picker-client-ro': '',
                'id_fi-payer-lo-picker-order-type-ro': 'Покупка',
                'id_fi-payer-lo-picker-date': {
                    startDate: getCurrentDate(),
                    endDate: getCurrentDate()
                },
                'id_fi-payee-lo-picker-order-type-ro': 'Поставка',
                'id_fi-payee-lo-picker-date': {
                    startDate: getCurrentDate(),
                    endDate: getCurrentDate()
                }                
            })
        }
    }

    validate(needToSetFocus) {
        /* validate function for component. If needToSetFocus is true - focus on first invalid element 
           don't pass needToSetFocus == true, if precedent OperationTransferPart component is invalid  */  
        const [ accountElementState, accountElement ] = this.validateAccount(),
              [ sumElementState, sumElement ] = this.validateSum(),
              [ linkedOrderElementState, linkedOrderElement] = this.validateLinkedOrder(),
              [ payReqElementState, payReqElement] = this.validatePaymentRequisit(),
              validationState = (accountElementState && sumElementState && linkedOrderElementState && payReqElementState);


        if (needToSetFocus && !validationState) {
            if (!accountElementState) accountElement.focus()
            else if (!sumElementState) sumElement.setFocus() 
            else if (!linkedOrderElementState) linkedOrderElement.focus()
            else if (!payReqElementState) payReqElement.focus()
        }
        return validationState;
    }

    validateAccount() {
        /* validate account data and returns state and element reference 
           set element invalid class if validation failed  
           return button element for focus station 
        */
        const { defs, operationData } = this.props,
              accountPickerDef = defs.accountPicker,
              transferData = operationData.get('transferData'),
              accountId = transferData.get(accountPickerDef.elementValueId),
              accountElementId = accountPickerDef.elementId,
              btnElementId = accountPickerDef.btnElementId;
        
        const accountValidityState = (accountId > 0),
              accountElement = document.getElementById(accountElementId),
              accountBtnElement = document.getElementById(btnElementId);

        showLabelValidityState(accountElement, accountValidityState, 'fi__label_invalid');
        return [ accountValidityState, accountBtnElement ];
    }

    validateSum() {
        const { defs } = this.props,
               operationSumDef = defs.operationSum,
               readOnly = operationSumDef.readOnly;
        
        if (readOnly) { // don-t check read only element 
            return [ true, null ];
        }
        const sumValidityState = this.sumElementRef.current.validate(),
              sumElement = document.getElementById(this.sumElementRef.current.props.id);
        showElementValidityState(sumElement, sumValidityState);
        return [ sumValidityState, this.sumElementRef.current ]
    }

    validateLinkedOrder() {
        return [ true, null ]; // no validaton performed 
    }

    validatePaymentRequisit() {
        const { defs, operationData } = this.props,
              paymentRequisitDef = defs.paymentRequisit,
              transferData = operationData.get('transferData'),
              payReqId = transferData.get(paymentRequisitDef.elementValueId),
              payReqElementId = paymentRequisitDef.elementId,
              btnElementId = paymentRequisitDef.btnElementId;
        
        const payReqValidityState = (payReqId > 0),
              payReqElement = document.getElementById(payReqElementId),
              payReqBtnElement = document.getElementById(btnElementId);

        showLabelValidityState(payReqElement, payReqValidityState, 'fi__label_invalid');
        return [ payReqValidityState, payReqBtnElement ];
    }

    onValidation(isValid, id) {
        switch (id) {
            case 'id_fi-transfer-operation-sum': {
                const sumElement = document.getElementById(id);
                showElementValidityState(sumElement, isValid);
                break;
            } 
        }
    }

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState({ 
            filterValues: filterValues.set(id, value)
        });
    }

    onPickerCancel(elementId) {
        // set state and clear picker data 
        const { defs } = this.props,
              accountPickerDef = defs.accountPicker,
              linkedToOrderDef = defs.linkedToOrder, 
              paymentRequisitDef = defs.paymentRequisit;
        
        switch(elementId) {
            case accountPickerDef.pickerElementId: {
                this.setState({ pickAccountElementCollapsed: true });
                break;        
            }
            case linkedToOrderDef.pickerElementId: {
                this.setState({ pickLinkedOrdersElementCollapsed: true });
                break;        
            }
            case paymentRequisitDef.pickerElementId: {
                this.setState({ pickPayReqsElementCollapsed: true });
                break;        
            }
        }
    } 

    onDataPick(value) {
        const { elementId } = value;
        this.onPickerCancel(elementId);

        this.props.onDataPick(value);
        // validate data after picker apply - if data invalid clear it 
        switch (elementId) {
            case 'id_fi-payer-account-picker':
            case 'id_fi-payee-account-picker': {
                const _this = this;
                setTimeout(() => { _this.validateAccount() }, 500);
                break;
            }
            case 'id_fi-payer-pay-req-picker':
            case 'id_fi-payee-pay-req-picker': {
                const _this = this;
                setTimeout(() => { _this.validatePaymentRequisit() }, 500);
                break;
            }

        }
    }

    onfilterButtonClick(e) {
        e.preventDefault();
        const { defs } = this.props,
              accountPickerDef = defs.accountPicker,
              linkedToOrderDef = defs.linkedToOrder, 
              paymentRequisitDef = defs.paymentRequisit,
              elementId = e.currentTarget.id;

        switch(elementId) {
            case accountPickerDef.btnElementId: {
                const pickAccountElementCollapsed = this.state.pickAccountElementCollapsed;
                this.setState({ pickAccountElementCollapsed: !pickAccountElementCollapsed });
                break;        
            }
            case linkedToOrderDef.btnElementId: {
                const pickLinkedOrdersElementCollapsed = this.state.pickLinkedOrdersElementCollapsed;
                this.setState({ pickLinkedOrdersElementCollapsed: !pickLinkedOrdersElementCollapsed });
                break;        
            }
            case paymentRequisitDef.btnElementId: {
                const pickPayReqsElementCollapsed = this.state.pickPayReqsElementCollapsed;
                this.setState({ pickPayReqsElementCollapsed: !pickPayReqsElementCollapsed });
                break;        
            }
        }
    }

    internalValidate(markValidationState = false) {
        let validationState = true,
            firstInvalidElement = undefined;

        this.componentRefs.forEach(elementRef => {
            const referredElement = elementRef.current.validate ? 
                                    elementRef.current : 
                                    null;
            if (referredElement) {
                // call internalValidate here 
                const [ currentElementState, localInvalidElement ] = referredElement.internalValidate.call(referredElement);
                if (!currentElementState) {
                    validationState = false;
                    firstInvalidElement = firstInvalidElement ? 
                                          firstInvalidElement : 
                                          localInvalidElement;
                    markValidationState && firstInvalidElement.setFocus();
                }

                markValidationState && 
                   referredElement.setValidationState(currentElementState);
                
            }
        })
        console.log('ValidationState: ', validationState, 
                    'markValidationState:', markValidationState,
                    'firstInvalidElement: ', firstInvalidElement);
        return [validationState, firstInvalidElement];
    }

    componentDidUpdate(prevProps, prevState) {
        // handle pickerChanges 
        const { pickAccountElementCollapsed, 
                pickLinkedOrdersElementCollapsed,
                pickPayReqsElementCollapsed,
                filterValues } = this.state,
                prevPickAccountElementCollapsed = prevState.pickAccountElementCollapsed,
                prevPickLinkedOrdersElementCollapsed = prevState.pickLinkedOrdersElementCollapsed,
                prevPickPayReqsElementCollapsed = prevState.pickPayReqsElementCollapsed,
                prevFilterValues = prevState.filterValues;

        const { defs, getPickerData, operationData, partRole, processPickerOpen } = this.props,
              isPayerPart = (partRole == 'payerPart'),
              accountPickerDef = defs.accountPicker,
              linkedToOrderDef = defs.linkedToOrder,
              accountClientDef = defs.accountClient,
              paymentRequisitDef = defs.paymentRequisit,
              transferData = operationData.get('transferData');

        if (pickAccountElementCollapsed != prevPickAccountElementCollapsed) {
            getPickerData(accountPickerDef.pickerElementId, 
                          pickAccountElementCollapsed);
            !pickAccountElementCollapsed && processPickerOpen(isPayerPart, accountPickerDef.pickerElementId)
        }
        if (pickLinkedOrdersElementCollapsed != prevPickLinkedOrdersElementCollapsed || 
            (!pickLinkedOrdersElementCollapsed && !filterValues.equals(prevFilterValues))) {
            const accountId = transferData.get(accountPickerDef.elementValueId),
                  filterDates = filterValues.get(isPayerPart ?  
                                                 'id_fi-payer-lo-picker-date' : 
                                                 'id_fi-payee-lo-picker-date' ),
                  startDate = filterDates.startDate,
                  endDate = filterDates.endDate,
                  // ID of payer linked order (if set)
                  linkedOrderId = isPayerPart ? null : transferData.get('buyerOrderId');
            getPickerData(linkedToOrderDef.pickerElementId, 
                          pickLinkedOrdersElementCollapsed,
                          { accountId, startDate, endDate, linkedOrderId });
            !pickLinkedOrdersElementCollapsed && processPickerOpen(isPayerPart, linkedToOrderDef.pickerElementId)
        }
        if (pickPayReqsElementCollapsed != prevPickPayReqsElementCollapsed) {
            const clientId = transferData.get(accountClientDef.elementValueId),
                  clientName = transferData.get(accountClientDef.elementValue);
            getPickerData(paymentRequisitDef.pickerElementId, 
                          pickPayReqsElementCollapsed,
                          { clientId: clientId});
            !pickPayReqsElementCollapsed && processPickerOpen(isPayerPart, paymentRequisitDef.pickerElementId);
            if (!pickPayReqsElementCollapsed) {
                isPayerPart && this.onFilterChanged('id_fi-payer-payreq-picker-client-ro', clientName);
                !isPayerPart && this.onFilterChanged('id_fi-payee-payreq-picker-client-ro', clientName);
            }
        }
    }

    renderOperationSum(operationIsEdited, operationSumDef, operationSum, currencyCode, onSumChange) {
        const { readOnly, elementSign } = operationSumDef;

        const signValue = (elementSign == 'minus') ? '-' : '+',
              currencyCodeForPrint = currencyCode ? currencyCode : '';

        if (!readOnly && operationIsEdited) {
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>{ operationSumDef.elementName }</div>
                    <div className='form-input__element-group'>
                        <label className='fi__label_noborder' id={ operationSumDef.elementLabelId }>−</label>
                        <SumInput className='fi__input fi__element form-control' 
                                    id={ operationSumDef.elementId }
                                    value={ operationSum }
                                    fractionDigits={ 2 }
                                    prohibitNegativeValues={ true }
                                    ref={ this.sumElementRef }
                                    onElementChange={ onSumChange }
                                    changeValueOnElementChange={ true }
                                    onValidation={ this.onValidation }
                                    />
                        <label className='fi__label_noborder' id='id_fi-amount-currency'>{ currencyCodeForPrint }</label>
                    </div>
                </div> );
            }
        else {
            const operationSumDesc = (operationSum !== null && operationSum !== undefined ) ? signValue + ' ' + floatPrettyPrint(operationSum) + ' ' + currencyCodeForPrint : '';
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>{ operationSumDef.elementName }</div>
                        <div className='form-input__element-group'>
                            <div className='form-input__element-group'>
                                <div className='fi__label_noborder fi__label_noborder-wide' 
                                        id={ operationSumDef.elementLabelId }>
                                    { operationSumDesc }
                            </div>
                            <div className='fi-stub'></div>
                        </div>
                    </div>
                </div> );
        }        
    }

    renderPayRequisitLabel(paymentRequisitDef, transferData) {
        const requisitId = transferData.get(paymentRequisitDef.elementValueId),
              requisitInfo = transferData.get(paymentRequisitDef.elementValue),
              taxNumber = requisitInfo.get('payReqTaxNumber'),
              orgName = requisitInfo.get('payReqOrgName'),
              accountNumber = requisitInfo.get('payReqAccountNumber'),
              bankName =  requisitInfo.get('payReqBankName');
        
        return (
            <div className='fi__label fi__element fi__label_payreq' 
                   id={ paymentRequisitDef.elementId }>
                { requisitId ? 
                (<div>
                    <div>ИНН { taxNumber }</div><div>{ orgName }</div>
                    <div>Счет № { accountNumber }</div><div>{  bankName }</div>
                </div>) : null }
            </div>)
    }

    renderMainPart() {
        const { operationIsEdited, defs, operationData, partRole, 
                onSumChange, onClearBtnClick, onLinkClick } = this.props,
              { pickAccountElementCollapsed,
                pickLinkedOrdersElementCollapsed,
                pickPayReqsElementCollapsed,
                filterValues } = this.state,
              transferData = operationData.get('transferData'),
              isPayerPart = (partRole == 'payerPart'),
              transferPickerData = operationData.get('transferPickerData'),
              accountPickerDef = defs.accountPicker,
              accountClientDef = defs.accountClient, 
              linkedToOrderDef = defs.linkedToOrder, 
              operationSumDef = defs.operationSum, 
              paymentRequisitDef = defs.paymentRequisit;

        const accountNumber = transferData.get(accountPickerDef.elementValue),
              clientId = transferData.get(accountClientDef.elementValueId),
              clientName = transferData.get(accountClientDef.elementValue),
              operationSum = operationData.get(operationSumDef.elementValue),
              currencyCode = transferData.get('operationCurrencyCode'),
              linkedOrderId = transferData.get(linkedToOrderDef.elementValueId),
              linkedOrderInfo = linkedOrderId ? transferData.get(linkedToOrderDef.orderInfoElement)
                                                            .get(linkedToOrderDef.orderNumberValue) + 
                                ' от ' + 
                                getDateStr(transferData.get(linkedToOrderDef.orderInfoElement)
                                                       .get(linkedToOrderDef.orderDateValue)) : '';
        
        const accountsPickerData = isPayerPart 
                                   ? transferPickerData.get('transferPayerAccounts') 
                                   : transferPickerData.get('transferPayeeAccounts');
        const payReqsPickerData = isPayerPart 
                                   ? transferPickerData.get('transferPayerPayReqs') 
                                   : transferPickerData.get('transferPayeePayReqs');
        const linkedOrdersPickerData = isPayerPart 
                                   ? transferPickerData.get('transferPayerOrders') 
                                   : transferPickerData.get('transferPayeeOrders');
        
        
        return (
            <div>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountPickerDef.elementName }</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' id={ accountPickerDef.elementId }>{ accountNumber }</label>
                            { operationIsEdited ? 
                            <button className='btn img-wrapper img-wrapper__btn' aria-label='Выбрать счет'
                                    id={ accountPickerDef.btnElementId }
                                    data-toggle='collapse'
                                    data-target={ '#'+accountPickerDef.pickerElementId }
                                    aria-expanded= { !pickAccountElementCollapsed }
                                    onClick={ this.onfilterButtonClick.bind(this) }>
                                <img className='img-wrapper__img' src='static/images/select-from-list.png' alt='Select account' />
                            </button> : <div className='fi-stub'></div> }
                        </div>
                    </div>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountClientDef.elementName }</div>
                        <label className='fi__label fi__element' 
                                id={ accountClientDef.elementId }>{ clientName }</label>
                    </div>
                </div>
                <Picker
                    id={ accountPickerDef.pickerElementId }
                    isExpanded = { !pickAccountElementCollapsed }
                    listData = { accountsPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerAccsListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { operationPickerAccsFilterFieldsFunc(
                                        accountPickerDef.pickerFilterFieldId, 
                                        accountPickerDef.pickerClientTypeFieldId) }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = { (partRole == 'payerPart')
                                         ? 'Выберите счет плательщика'
                                         : 'Выберите счет получателя'}/>

                <div className='form-input__row'>
                    { this.renderOperationSum(operationIsEdited, operationSumDef, operationSum, currencyCode, onSumChange) }
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ linkedToOrderDef.elementName }</div>
                        <div className='form-input__element-group'>
                            { linkedOrderId && !operationIsEdited ? (
                            <a href='#' 
                               className='fi__label fi__element fi__label_href' 
                               id={ linkedToOrderDef.elementId }
                               data-order-id={ linkedOrderId.toString() } 
                               onClick={ onLinkClick }>{ linkedOrderInfo }</a>
                            )
                            : (
                                <label className='fi__label fi__element' 
                                   id={ linkedToOrderDef.elementId }>{ linkedOrderInfo }</label>
                            )}       
                            { operationIsEdited ? 
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        aria-label='Выбрать ордер'
                                        id={ linkedToOrderDef.btnElementId }
                                        onClick={ this.onfilterButtonClick.bind(this) }
                                        disabled={ !(clientId > 0) }>                                 
                                    <img className='img-wrapper__img' 
                                         src='static/images/select-from-list.png' 
                                         alt='Select order' />
                                </button> : null }                                  
                            { operationIsEdited ? 
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        aria-label='Очистить'
                                        id={ linkedToOrderDef.btnClearElementId }
                                        onClick={ onClearBtnClick }>
                                    <img className='img-wrapper__img' 
                                         src='static/images/filter-clear.png' 
                                         alt='Clear' />                                    
                                </button> : null } 
                        </div>
                    </div>
                </div>
                <Picker
                    id={ linkedToOrderDef.pickerElementId }
                    isExpanded = { !pickLinkedOrdersElementCollapsed }
                    listData = { linkedOrdersPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerLOListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { operationPickerLOFilterFieldsFunc(
                                        linkedToOrderDef.pickerDateFieldId,
                                        linkedToOrderDef.pickerOrderTypeFieldId,
                                        linkedToOrderDef.pickerElementId + '__btn',
                                        linkedToOrderDef.btnElementId
                                    ) }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = { (partRole == 'payerPart')
                                         ? 'Выберите ордер покупателя'
                                         : 'Выберите ордер поставщика'}/>

                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ paymentRequisitDef.elementName }</div>
                        <div className='form-input__element-group'>
                            { this.renderPayRequisitLabel(paymentRequisitDef, transferData) }
                            { operationIsEdited ? (
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        aria-label={ paymentRequisitDef.ariaLabel}
                                        id={ paymentRequisitDef.btnElementId }
                                        onClick={ this.onfilterButtonClick.bind(this) }
                                        disabled={ !(clientId > 0) }>
                                    <img className='img-wrapper__img' 
                                         src='static/images/select-from-list.png' 
                                         alt='Select payment requisit' />
                                </button>)
                            : <div className='fi-stub'></div> } 
                        </div>
                    </div>
                </div>
                <Picker
                    id={ paymentRequisitDef.pickerElementId }
                    isExpanded = { !pickPayReqsElementCollapsed }
                    listData = { payReqsPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerPayReqsListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { operationPickerPayReqsFilterFields(
                                        paymentRequisitDef.pickerFilterFieldId,
                                        paymentRequisitDef.pickerClientTypeFieldId
                                    ) }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = { (partRole == 'payerPart')
                                         ? 'Выберите платежный реквизит плательщика'
                                         : 'Выберите платежный реквизит получателя'}/>
            </div>
            )
    }
    
    render() {
        const { partHeader } = this.props;

        return (
        <div className='block-operation-part'>
            <div className='form-input__row'>
                <div className='form-input__block'>
                    <div className='fi__header-label'>{ partHeader }</div>
                </div>
            </div>
            { this.renderMainPart() }
        </div>);
    }
}   

OperationTransferPart.propTypes = {
    operationId: PropTypes.number,
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map).isRequired,
    partHeader: PropTypes.string.isRequired,
    partRole: PropTypes.string.isRequired,
    onSumChange: PropTypes.func.isRequired,
    onClearBtnClick: PropTypes.func.isRequired,
    onDataPick: PropTypes.func.isRequired,
    getPickerData: PropTypes.func.isRequired,
    processPickerOpen: PropTypes.func.isRequired,
    processAccountChange: PropTypes.func.isRequired,
    onLinkClick: PropTypes.func.isRequired
}

export { OperationTransferPart };
