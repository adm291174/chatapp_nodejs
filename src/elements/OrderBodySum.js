import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { floatPrettyPrint } from '../core/core_functions';
import { Map } from 'immutable';
import SumInput from './SumInput';

import { showElementValidityState } from '../core/validation_functions';


/* Order main information part: Main Sum Info  */ 

class OrderBodySum extends Component {
    constructor(props) {
        super(props);
        this.sumInputRef = React.createRef();
    }
    
    validate() {
        if (this.sumInputRef.current) {
            return this.sumInputRef.current.validate();
        } 
        else return true;
    }

    setFocus() {
        const element = document.getElementById('id_fi-order-sum');
        element && element.focus();
    }

    setValidationState(validationState) {
        const sumElement = document.getElementById('id_fi-order-sum');
        if (sumElement)
            showElementValidityState(sumElement, validationState);        
    }

    
    render() {
        const { orderIsEdited, orderBodyData, onSumChange, onValidation } = this.props;
        const orderSum = orderBodyData.get('orderSum'), 
              orderCurrency = orderBodyData.get('orderAccountCurrencyCode');

        if (orderIsEdited) {
            return (
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Сумма ордера</div>
                        <div className='form-input__element-group'>
                            <SumInput className='fi__input fi__element form-control' 
                                    id='id_fi-order-sum' 
                                    value={ orderSum }
                                    onElementChange = { onSumChange }
                                    fractionDigits = { 2 } 
                                    prohibitNegativeValues = { true }
                                    onValidation = { onValidation }
                                    changeValueOnElementChange = { true }
                                    ref = { this.sumInputRef }
                                    />
                            <div className='fi__label_noborder' 
                                    id='id_fi-amount-currency'>
                                { orderCurrency }
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return(
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Сумма ордера</div>
                        <div className='form-input__element-group'>
                            <div className='fi__label_noborder' 
                                 id='id_fi-order-amount_ro'>
                                { floatPrettyPrint (orderSum) + ' ' + orderCurrency }
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

OrderBodySum.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired,
    orderBodyData: PropTypes.instanceOf(Map),
    onSumChange: PropTypes.func.isRequired,
    onValidation: PropTypes.func.isRequired,
}

export default OrderBodySum;