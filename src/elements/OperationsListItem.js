import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { getDateStr, floatPrettyPrint } from '../core/core_functions';
import { List } from 'immutable';

class OperationsListItem extends Component { 

    renderClientName(isSmall, operationClients) {
        const operationClientsName = operationClients.map( value => {
            return <div key={value.operationClientId}>{ value.operationClientName }</div>
            });
        return !isSmall ? <td className='align-text-left'>{ operationClientsName }</td> : null;
    }

    renderNumber(isSmall, operationId, operationNumber, operationDate) {
        const { onOperationEditClick } = this.props;
        return (isSmall ? <td>
                    <a href='#' onClick={ onOperationEditClick } 
                       data-operation-id={ operationId.toString() }>
                          { operationNumber }</a><br/>
                          { getDateStr(operationDate) }
                    </td>
               : <td><a href='#' onClick= { onOperationEditClick } 
                    data-operation-id={ operationId.toString() } >{ operationNumber }</a></td>
        );
    }
    
    renderDate(isSmall, operationDate) {
        return !isSmall ? <td>{ getDateStr(operationDate) }</td> : null;
    }


    render() {
        const { operationId, operationNumber, operationDate, 
                operationStatusDescription, clients, operationSum, 
                operationCurrencyCode, operationTypeDescription,
                isSmall } = this.props;

        const sumDescription = floatPrettyPrint(operationSum) + ' ' + operationCurrencyCode;
        return (
        <tr>
            { this.renderNumber(isSmall, operationId, operationNumber, operationDate) }
            { this.renderDate(isSmall, operationDate) }
            <td>{ operationTypeDescription }</td>                        
            <td>{ operationStatusDescription }</td>
            { this.renderClientName(isSmall, clients) }
            <td className='align-text-right'>{ sumDescription }</td>
        </tr>
        );
    }
}

OperationsListItem.propTypes = {
    operationId: PropTypes.number.isRequired,
    operationDate: PropTypes.instanceOf(Date).isRequired,
    operationNumber: PropTypes.string.isRequired,
    operationStatusDescription: PropTypes.string.isRequired,
    clients: PropTypes.instanceOf(List).isRequired,
    operationSum: PropTypes.number.isRequired,
    operationCurrencyCode: PropTypes.string.isRequired,
    operationTypeDescription: PropTypes.string.isRequired,
    onOperationEditClick: PropTypes.func.isRequired,
    isSmall: PropTypes.bool.isRequired, 
}

export default OperationsListItem; 

