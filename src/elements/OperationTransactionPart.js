import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { floatPrettyPrint } from '../core/core_functions';
import { Map, List } from 'immutable';
import SumInput from './SumInput';
import { operationPickerAccsForTPListColumns } from '../constants/picker_constants';
import Picker from './picker/Picker';
import { operationPartDefs } from '../constants/operations_constants';

import { showElementValidityState, showLabelValidityState, validateMix, validateElement } from '../core/validation_functions';

class OperationTransactionPart extends Component {
    constructor(props) {
        super(props);
        this.sumInputRef = React.createRef();
        this.state = {
            pickerCollapsed: true,
            filterValues: Map().set(this.getFilterFieldName(), '')
        }
    }
    
    validate(needToSetFocus) {
        const { transactionPartData } = this.props;

        const operationPartType = transactionPartData.get('operationPartType');

        const validationList = [ 
            {
                validationType: 'function',
                element: this.sumInputRef,
                functionLink: this.validateSum.bind(this),
            },
            {
                validationType: 'function',
                element: 'id_fi-oper-purpose-' + operationPartType,
                functionLink: this.validateAccount.bind(this),
            },
            {
                validationType: 'element',
                element: 'id_fi-oper-purpose-' + operationPartType,
                rules: [{name: 'noEmpty'}]
            }
        ]
        return validateMix(validationList, needToSetFocus);

    }

    validateSum(needToSetFocus, validationState = true) {
        /* validate sum element */ 
        const sumValidityState = this.sumInputRef.current.validate(),
              sumElement = document.getElementById(this.sumInputRef.current.props.id);
        showElementValidityState(sumElement, sumValidityState);
        if (!sumValidityState && needToSetFocus && validationState ) {
            sumElement.focus();
        }
        return sumValidityState; 
    }

    validateAccount(needToSetFocus, validationState = true) {
        /* validate account data and returns state and element reference 
           set element invalid class if validation failed  
           return button element for focus station 
        */
        const { transactionPartData } = this.props,
                accountId = transactionPartData.get('operationPartAccountId');

        const accountValidityState = (accountId > 0),
              operationPartType = transactionPartData.get('operationPartType'),
              accountElementId = 'id_fi-oper-corr-account-' + operationPartType,
              btnElementId = 'id_fi-oper-corr-account-btn-' + operationPartType, 
              accountElement = document.getElementById(accountElementId),
              accountBtnElement = document.getElementById(btnElementId);

        showLabelValidityState(accountElement, accountValidityState, 'fi__label_invalid');
        if (!accountValidityState && needToSetFocus && validationState) accountBtnElement.focus();
        return accountValidityState;
    }

    getFilterFieldName() {
        const { transactionPartData } = this.props,
                operationPartType = transactionPartData.get('operationPartType');
        return 'id_fi-account-name-' + operationPartType;

    }

    filterFields() {
        return List([
            Map({
                fieldId: this.getFilterFieldName(),
                fieldHeader: 'Название счета',
                fieldElement: (id, value, onChange) => {
                                return <input className='form-control'
                                            id={ id}
                                            value = { value } 
                                            onChange = { onChange } ></input>
                        },
            fieldType: 'filter'
            })
        ])
    }

    onfilterButtonClick(e) {
        e.preventDefault();
        const pickerCollapsed = this.state.pickerCollapsed;
        this.setState({ pickerCollapsed: !pickerCollapsed});
    }

    onPickerCancel() {
        this.setState({ pickerCollapsed: true });
    } 

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState( { 
            filterValues: filterValues.set(id, value)
        });
    }

    onValidation(isValid, id) {
        const sumElement = document.getElementById(id);
        showElementValidityState(sumElement, isValid);
    }
    
    onDataPick(value) {
        const { elementId } = value;
            this.onPickerCancel(elementId);
            this.props.onDataPick(value);

        const _this = this;
        setTimeout(() => { _this.validateAccount(false) }, 500);
        
    }

    onPurposeBlur(e) {
        validateElement(e.target.id, [{name: 'noEmpty'}], true, true, false);
    }
    
    componentDidUpdate(prevProps, prevState) {
        const { pickerCollapsed } = this.state,
                prevPickCollapsed = prevState.pickerCollapsed;


        if (pickerCollapsed != prevPickCollapsed) {
            const { getPickerData, processPickerOpen, transactionPartData } = this.props,
                    operationPartType = transactionPartData.get('operationPartType'),
                    transactionPartDefinition = operationPartDefs.get(operationPartType),
                    pickerElementId = 'id_fi-oper-account-picker-' + operationPartType;

            getPickerData(pickerElementId, 
                          pickerCollapsed,
                          { accountType: transactionPartDefinition.accSelectorCodeParameter });
            !pickerCollapsed && processPickerOpen(this.props.isSupplyComponent, 
                                                  pickerElementId)
        }
    }

    render() {
        const { operationIsEdited, 
                transactionPartData,
                currencyCode,
                onSumChange,
                accountPickerData,
                onChange } = this.props;

        const operationPartType = transactionPartData.get('operationPartType'),
              transactionPartDefinition = operationPartDefs.get(operationPartType),
              componentHeader = transactionPartDefinition.partHeader,
              sumElementHeader = transactionPartDefinition.sumElementHeader,
              accountPickerElementHeader = transactionPartDefinition.accountPickerElementHeader,
              purposeElementHeader = transactionPartDefinition.purposeElementHeader,
              signValue = transactionPartDefinition.sumSign;
        
        const // operationPartAccountId = transactionPartData.get('operationPartAccountId'),
              operationPartAccountNumber = transactionPartData.get('operationPartAccountNumber'),
              operationPartAccountName = transactionPartData.get('operationPartAccountName'),
              operationPartSum = transactionPartData.get('operationPartSum'),
              operationPartPurpose = transactionPartData.get('operationPartPurpose');

        const { pickerCollapsed, filterValues } = this.state;

        return (
            <div className='fi__operation-part'>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi__header-label'>{ componentHeader }</div>
                    </div>
                </div>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ sumElementHeader }</div>
                            { operationIsEdited ? (
                            <div className='form-input__element-group'>
                                <label className='fi__label_noborder' 
                                       id={ 'id_fi-amount-sum-' + operationPartType } 
                                       >{ signValue }</label>
                                <SumInput className='fi__input fi__element form-control' 
                                          id={ 'id_fi-operation-sum-' + operationPartType }
                                          value={ operationPartSum }
                                          onElementChange = { onSumChange }
                                          fractionDigits = { 2 } 
                                          prohibitNegativeValues = { true }
                                          onValidation = { this.onValidation.bind(this) }
                                          changeValueOnElementChange = { true }
                                          ref = { this.sumInputRef }
                                    />
                                <div className='fi__label_noborder'>{ currencyCode }</div>
                            </div>) 
                            : (
                            <div className='form-input__element-group'>
                                <label className='fi__label_noborder' 
                                       id={ 'id_fi-amount-sum-ro' + operationPartType }
                                       >{ signValue }</label>
                                <div className='fi__label_noborder' 
                                     id={ 'id_fi-order-amount_ro' + operationPartType }>
                                { floatPrettyPrint (operationPartSum) + ' ' + currencyCode }
                                </div>
                            </div>) }
                    </div>
                </div>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountPickerElementHeader }</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' 
                                   id={ 'id_fi-oper-corr-account-' + operationPartType }>
                                { operationPartAccountNumber }
                            </label>
                            { operationIsEdited ? (
                            <button className='btn img-wrapper img-wrapper__btn'
                                 aria-label='Выбрать счет'
                                 onClick={ this.onfilterButtonClick.bind(this) } 
                                 data-toggle='collapse'
                                 id={ 'id_fi-oper-corr-account-btn-' + operationPartType }
                                 data-target={ '#' + 'id_fi-oper-account-picker-' + operationPartType }
                                 aria-expanded= { !pickerCollapsed } >
                                <img className='img-wrapper__img' 
                                        src='static/images/select-from-list.png' 
                                        alt='Select account'/>
                            </button> ) : null }                
                        </div>
                    </div>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Наименование счета</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' 
                                   id={ 'id_fi-oper-corr-account-name-' + operationPartType }>
                                { operationPartAccountName }
                            </label>
                        </div>
                    </div>                    
                </div>
                <Picker
                    id={ 'id_fi-oper-account-picker-' + operationPartType }
                    isExpanded = { !pickerCollapsed }
                    listData = { accountPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerAccsForTPListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { this.filterFields() }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = 'Выберите счет'/>

                <div className='form-input__row'>
                    <div className='form-input__block form-input__block_wide'>
                        <div className='fi-block__header'>{ purposeElementHeader }</div>
                        <textarea className='fi-text_wide form-control'
                                  id={ 'id_fi-oper-purpose-' + operationPartType }
                                  value={ operationPartPurpose }
                                  onChange={ onChange }
                                  onBlur={ this.onPurposeBlur.bind(this) }
                                  disabled={!operationIsEdited}></textarea>
                    </div>
                </div>
        </div>)
    }
}

OperationTransactionPart.propTypes = {
    transactionPartData: PropTypes.instanceOf(Map),        // data for visualization 
    operationIsEdited: PropTypes.bool.isRequired,
    onSumChange: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    currencyCode: PropTypes.string,
    accountPickerData: PropTypes.instanceOf(List),
    onDataPick: PropTypes.func.isRequired,
    getPickerData: PropTypes.func.isRequired,
    processPickerOpen: PropTypes.func.isRequired,
    isSupplyComponent: PropTypes.bool.isRequired
}


export { OperationTransactionPart };
