import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { List, Map } from 'immutable';

import PickerList from '../picker/PickerList';
import PickerFilter from '../picker/PickerFilter';

import { Collapse } from 'react-bootstrap';

class Picker extends Component {
    constructor(props) {
        super(props);
 
        this.state = {
            data: Map({
                filteredList: List(),
                filterTextIndex: this.findFilterFieldId()
            })
        }
    }

    onCancelBtnClick() {
        const { onCancel, id } = this.props;
        onCancel(id);
    }

    onDataPick({ keyFileldValue, selectedListValues }) {
        const { onDataPick } = this.props,
              elementId = this.props.id;
        onDataPick({ keyFileldValue, selectedListValues, elementId });
    }

    findFilterFieldId() {
        // Function for finding filterField form 
        return this.props.filterFields.findIndex(filterRecord => {
            return (filterRecord.get('fieldType') == 'filter')
        });
    }
    
    performListFilter() {
        // function for filtering data
        const filterTextIndex = this.state.data.get('filterTextIndex')

        const { listData, filterFields, filterValues, listColumns } = this.props;
            
        const filteredFieldName = listColumns.get('filteredField');

        if (filteredFieldName && filterTextIndex != -1) {
            // perform filtering here 
            const textFilterInfo = filterFields.get(filterTextIndex),
                  textFilterId = textFilterInfo.get('fieldId'),
                  textFilterValue = filterValues.get(textFilterId),
                  textFilterValueLower = textFilterValue 
                                       ? textFilterValue.toLowerCase()
                                       : '';
            
            const filteredList =  listData.filter(dataValue => {
                const filteredFieldValue = dataValue.get(filteredFieldName),
                      filteredFieldValueLower = filteredFieldValue 
                                                ? filteredFieldValue.toLowerCase()
                                                : '';
                    
                return (filteredFieldValueLower.includes(textFilterValueLower))
            })
            return filteredList;
        }
        else return listData;
    }

    componentDidMount() {
        const filteredList = this.performListFilter();
        this.setState ( { data : this.state.data.set('filteredList', filteredList)});
    }

    componentDidUpdate(prevProps) {
        const prevFilterFields = prevProps.filterFields,
              currentFilterFields = this.props.filterFields;
        
        if (!currentFilterFields.equals(prevFilterFields)) {
            // filter structure has been changed 
            const stateData = this.state.data;
            this.setState( { data: stateData.set(
                                    'filterTextIndex',
                                    this.findFilterFieldId()
                                  )
                          } )

        }

        const prevFilterValues = prevProps.filterValues,
              currentFilterValues = this.props.filterValues;
        
        if (!currentFilterValues.equals(prevFilterValues)) {
            // filter values changed ... filter data if textFilter changed
            currentFilterFields.forEach(filterInfo => {
                const fieldType = filterInfo.get('fieldType'),
                      fieldId = filterInfo.get('fieldId'),
                      oldFilterData = prevFilterValues.get(fieldId),
                      currentFilterData = currentFilterValues.get(fieldId)
                if (oldFilterData != currentFilterData) {
                    if (fieldType == 'filter') {
                       const filteredList = this.performListFilter(),
                             stateData = this.state.data;
                       this.setState( { data: stateData.set(
                                                'filteredList',
                                                filteredList)})
                    }
                }
            })
        }

        const oldListData = prevProps.listData,
              currentListData = this.props.listData;

        if (!currentListData.equals(oldListData)) {
            // listData changed - filter it 
            const filteredList = this.performListFilter(),
                    stateData = this.state.data;
            this.setState( { data: stateData.set(
                                    'filteredList',
                                    filteredList)
                         } )
        }

        if (prevProps.isExpanded == false && 
                prevProps.isExpanded != this.props.isExpanded) {
            const filterTextIndex = this.state.data.get('filterTextIndex');
            if (filterTextIndex != -1) {
                const { filterFields } = this.props,
                      elementId = filterFields.get(filterTextIndex).get('fieldId'),
                      element = document.getElementById(elementId);

                element && setTimeout(() => { element.focus() }, 200);

            }    
        }
    }

    render() {
        const { id, 
                isExpanded,
                listColumns,
                filterFields, 
                filterValues,
                filterHeaderName,
                onFilterChanged } = this.props;

        const stateData = this.state.data,
              filteredList = stateData.get('filteredList');

        return (
            <Collapse in ={ isExpanded } >
                <div id={ id }>
                    <div className='popup-container'>
                        <PickerFilter 
                            filterFields = { filterFields }
                            filterValues = { filterValues }
                            filterHeaderName = { filterHeaderName }
                            onCancel = { this.onCancelBtnClick.bind(this) }
                            onFilterChanged = { onFilterChanged } 
                            cancelBtnId = { id + '__btn' }
                        />
                        <PickerList 
                            listData = { filteredList }
                            listColumns = { listColumns } 
                            onDataPick = { this.onDataPick.bind(this) }
                        />
                    </div>
                </div>
            </Collapse>
        );
    }
}

Picker.propTypes = {
    isExpanded: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    listData: PropTypes.instanceOf(List),
    onDataPick: PropTypes.func,
    listColumns: PropTypes.instanceOf(Map).isRequired,
    onCancel: PropTypes.func,
    filterFields: PropTypes.instanceOf(List),
    filterValues: PropTypes.instanceOf(Map),
    onFilterChanged: PropTypes.func,
    filterHeaderName: PropTypes.string.isRequired
}

export default Picker;