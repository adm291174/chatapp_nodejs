import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { List, Map } from 'immutable';


class PickerList extends Component {

    onClick(e) {
        e.preventDefault();
        const pickedId = + e.currentTarget.getAttribute('data-id');
        const { listData, listColumns } = this.props,
                keyField = listColumns.get('keyField');
        const foundIndex = listData.findIndex(value => {
            return value.get(keyField) == pickedId;
        })
        if (foundIndex != -1) {
            this.props.onDataPick({ 
                keyFileldValue: pickedId, 
                selectedListValues: listData.get(foundIndex)
            });
        }
    }   

    renderRowValues(listFields, rowValues) {
        return (listFields && listFields.map( listFieldInfo => {
            const className = listFieldInfo.className ? listFieldInfo.className : '',
                  fieldName = listFieldInfo.fieldName,
                  fieldValue = rowValues.get(listFieldInfo.fieldName),
                  transform = listFieldInfo.transform;

            return <td className={ className } 
                       key={ fieldName }>
                       { transform ? transform(fieldValue) : fieldValue }
                    </td>
        }));
    }

    renderList() {
        const { listData, listColumns } = this.props,
              keyField = listColumns.get('keyField'), 
              listFields = listColumns.get('listFields');
        
        const _this = this;
        const rowsData = listData.map( rowValues => {
            const key = rowValues.get(keyField);
            return (
            <tr className='pc-main__row' 
                   key={ key }
                   data-id={ key }
                   onClick = { _this.onClick.bind(_this) }>
                   {  _this.renderRowValues(listFields, rowValues) }
            </tr>
            );
        });    

        return rowsData

    }

    render() {
        const { listData } = this.props;
        if (listData.size > 0) {
            return (
                <div className='popup-container__main'>
                    <table>
                        <tbody>
                        { this.renderList() }
                        </tbody>
                    </table>
                </div>                                
            );
        }
        else return <div className='popup-container__main popup-container__main_empty'>
                        Нет данных для отображения
                    </div>;
    }
}

PickerList.propTypes = {
    listData: PropTypes.instanceOf(List).isRequired,
    listColumns: PropTypes.instanceOf(Map).isRequired,
    onDataPick: PropTypes.func.isRequired
}

export default PickerList;