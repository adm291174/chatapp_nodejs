import React, { Component, Suspense } from 'react';
import PropTypes from 'prop-types';

import RefreshIndicator from '../../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../DatePickerCustom'));
// import DatePickerCustom from '../DatePickerCustom';


class PickerFilterDate extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.datePickerStartRef = React.createRef();
        this.datePickerEndRef = React.createRef();
    }

    onDatePickerChange(changedId, changedValue) {
        const { id, onChange, value }  = this.props,
              startDateIsChanging = (changedId == (id + '-start')),
              endDateIsChanging = (changedId == (id + '-end')),
              startDate =  startDateIsChanging ? changedValue : value.startDate,
              endDate =  endDateIsChanging ? changedValue : value.endDate;

        const newValue = {
                  startDate: startDateIsChanging ? startDate : ((startDate > endDate) ? endDate : startDate),
                  endDate: endDateIsChanging ? endDate : ((startDate > endDate) ? startDate : endDate)
              };
        onChange(id, newValue);
    }

    render() {
        const { id, value, nextElementId, prevElementId  } = this.props,
               startDate = value.startDate,
               endDate = value.endDate,
               onDatePickerChange = this.onDatePickerChange.bind(this);
        

        return (
            <div className='pc-header__elem-group_line'>
                <Suspense fallback={<RefreshIndicator/>}>
                    <DatePickerCustom 
                        key= { id + '-start' }
                        id={ id + '-start' }
                        name={ id + '-start' }
                        title='Дата с'
                        nextElementId={ id + '-end' }
                        prevElementId={ prevElementId }
                        value={ startDate }
                        onChangeHandle={ onDatePickerChange }
                        disabled = { false }
                        blockElementClass='pc-header__elem-group pc-header__elem-group_date'
                        headerElementClass='elem-group__header'
                        bodyElementGroup=''
                        forwardedRef = { this.datePickerStartRef }
                        showValidClass = { true }
                    />
                </Suspense>
                <Suspense fallback={<RefreshIndicator/>}>
                <DatePickerCustom
                        key= { id + '-end' }
                        id={ id + '-end' }
                        name={ id + '-end' }
                        title='Дата по'
                        nextElementId= { nextElementId }
                        prevElementId= { id + '-start' }
                        value={ endDate }
                        onChangeHandle={ onDatePickerChange }
                        disabled = { false }
                        blockElementClass='pc-header__elem-group pc-header__elem-group_date'
                        headerElementClass='elem-group__header'
                        bodyElementGroup=''
                        forwardedRef = { this.datePickerEndRef }
                        showValidClass = { true }
                    />
                </Suspense>
            </div>
        )
    }
}

PickerFilterDate.propTypes = {
    id: PropTypes.string.isRequired,
    value: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    nextElementId: PropTypes.string.isRequired,
    prevElementId: PropTypes.string.isRequired
}

export default PickerFilterDate;