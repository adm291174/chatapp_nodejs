import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { List, Map } from 'immutable';

class PickerFilter extends Component {

    onCancelBtnClick(e) {
        e.preventDefault();
        const { onCancel } = this.props;
        onCancel && onCancel();
    }

    renderFilterContents() {
        const { filterFields, filterValues } = this.props;
                
        return filterFields.map( filterInfo => {
            const fieldHeader = filterInfo.get('fieldHeader'),
                  fieldId = filterInfo.get('fieldId'),
                  fieldValue = filterValues.get(fieldId),
                  fieldElement = filterInfo.get('fieldElement'),
                  fieldType = filterInfo.get('fieldType'),
                  isWide = filterInfo.get('isWide'),
                  headerGroupClass = 'pc-header__elem-group' + 
                                     (isWide ? ' pc-header__elem-group_wide' : '');
                  
                  

            if (fieldType == 'group') {
                return fieldElement(fieldId, fieldValue, this.onFieldChange.bind(this))
            }
            else 
            return (
                <div className={ headerGroupClass } key={ fieldId }>
                    <div className='elem-group__header'>{ fieldHeader }</div>
                        { fieldElement(fieldId, fieldValue, this.onFieldChange.bind(this)) }
                </div>
            );
        } )
    } 

    onFieldChange(e, value) {
        // this function get field value and execute callback function 
        const { onFilterChanged } = this.props;

        if (value !== undefined) {
            onFilterChanged(e, value);
        } 
        else {
            const element = e.target,
                fieldId = element.id,
                fieldValue = (element.type == 'checkbox') ? element.checked : element.value;
            
            fieldId && onFilterChanged && onFilterChanged(fieldId, fieldValue);
        }
    }

    render() {
        const { filterHeaderName, cancelBtnId } = this.props;

        return (
            <div>
                <div className='popup-container__header'>
                    <div className='pc-header__elem pc-header__elem_free'>{ filterHeaderName }</div>
                </div>
                <div className='popup-container__header'>
                    { this.renderFilterContents() }
                    <button className='btn form-control 
                                   pc-header__elem main-block__btn' 
                        type='button'
                        id={ cancelBtnId }
                        onClick={ this.onCancelBtnClick.bind(this) }>Отмена</button>
                </div>
                
            </div>
        )
    }
}                            
                            

PickerFilter.propTypes = {
    filterHeaderName: PropTypes.string.isRequired,
    onFilterChanged: PropTypes.func,
    filterFields: PropTypes.instanceOf(List).isRequired,
    filterValues: PropTypes.instanceOf(Map).isRequired,
    onCancel: PropTypes.func,
    cancelBtnId: PropTypes.string.isRequired
}                               

export default PickerFilter;