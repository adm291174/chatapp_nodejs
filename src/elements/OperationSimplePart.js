import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Map, List } from 'immutable';
import { operationPickerAccsForSOPListColumns, accPickerFilterFields } from '../constants/picker_constants';
import Picker from './picker/Picker';
import { simpleOperationPartDefs } from '../constants/operations_constants';

import { showLabelValidityState,  } from '../core/validation_functions';

class OperationSimplePart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pickerCollapsed: true,
            filterValues: Map().set(this.getFilterFieldName(), '')
        }
    }
    
    validate(needToSetFocus) {
        const { operationPartData } = this.props,
              accountId = operationPartData.get('accountId');

        const elementValidityState = (accountId > 0);
        this.setValidationState(elementValidityState, needToSetFocus);
        return elementValidityState
    }

    getAccountElements() {
        const { operationPartData } = this.props,
              operationPartType = operationPartData.get('operationPartType');
              
        const accountElementId = 'id_fi-oper-part-account-' + operationPartType,
              accountBtnElementId = 'id_fi-oper-part-account-btn-' + operationPartType,
              accountElement = document.getElementById(accountElementId),
              accountBtnElement = document.getElementById(accountBtnElementId);
        
        return { accountElementId, accountBtnElementId, accountElement, accountBtnElement }
        
    }

    setValidationState(validationState, needToSetFocus) {
        const { accountElement, 
                accountBtnElement } = this.getAccountElements();
        showLabelValidityState(accountElement, validationState, 'fi__label_invalid');
        if (!validationState && needToSetFocus) accountBtnElement && accountBtnElement.focus();        
        
    }    

    getFilterFieldName() {
        const { operationPartData } = this.props,
                operationPartType = operationPartData.get('operationPartType');
        return 'id_fi-account-name-' + operationPartType;
    }

    /*  replaced with accPickerFilterFields
        filterFields() {
        return List([
            Map({
                fieldId: this.getFilterFieldName(),
                fieldHeader: 'Название счета',
                fieldElement: (id, value, onChange) => {
                                return <input className='form-control'
                                            id={ id}
                                            value = { value } 
                                            onChange = { onChange } ></input>
                        },
            fieldType: 'filter'
            })
        ])
    } */

    onPickerButtonClick(e) {
        e.preventDefault();
        const pickerCollapsed = this.state.pickerCollapsed;
        this.setState({ pickerCollapsed: !pickerCollapsed});
    }

    onPickerCancel() {
        this.setState({ pickerCollapsed: true });
    } 

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState( { 
            filterValues: filterValues.set(id, value)
        });
    }

    onDataPick(value) {
        const { elementId } = value;
            this.onPickerCancel(elementId);
            this.props.onDataPick(value);

        const _this = this;
        setTimeout(() => { _this.validate(false) }, 500);        
    }

    componentDidUpdate(prevProps, prevState) {
        const { pickerCollapsed } = this.state,
                prevPickCollapsed = prevState.pickerCollapsed;


        if (pickerCollapsed != prevPickCollapsed) {
            const { getPickerData, processPickerOpen, operationPartData } = this.props,
                    operationPartType = operationPartData.get('operationPartType'),
                    pickerElementId = 'id_fi-oper-part-account-picker-' + operationPartType,
                    operationPartDefinition = simpleOperationPartDefs.get(operationPartType)

            getPickerData(pickerElementId, 
                          pickerCollapsed,
                          { accountType: operationPartDefinition.accSelectorCodeParameter});
            !pickerCollapsed && processPickerOpen(pickerElementId)
        }
    }

    render() {
        const { operationPartData,
                operationIsEdited,
                accountPickerData } = this.props,
              { pickerCollapsed, filterValues } = this.state ;

        const operationPartType = operationPartData.get('operationPartType'),
              operationPartDefinition = simpleOperationPartDefs.get(operationPartType),
              componentHeader = operationPartDefinition.partHeader,
              accountElementHeader = operationPartDefinition.accountElementHeader,
              accountElementNameHeader = operationPartDefinition.accountElementNameHeader,
              clientElementHeader = operationPartDefinition.clientElementHeader;

        const operationPartAccountNumber = operationPartData.get('accountNumber'), 
              operationPartAccountName = operationPartData.get('accountName'),
              operationPartClientName = operationPartData.get('clientName');

        return (
            <div className='fi__simple-operation-part'>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi__header-label'>{ componentHeader }</div>
                    </div>
                </div>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountElementHeader }</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' 
                                   id={ 'id_fi-oper-part-account-' + operationPartType }>
                                { operationPartAccountNumber }
                            </label>
                            { operationIsEdited ? (
                            <button className='btn img-wrapper img-wrapper__btn'
                                 aria-label='Выбрать счет'
                                 onClick={ this.onPickerButtonClick.bind(this) } 
                                 data-toggle='collapse'
                                 id={ 'id_fi-oper-part-account-btn-' + operationPartType }
                                 data-target={ '#' + 'id_fi-oper-account-picker-' + operationPartType }
                                 aria-expanded= { !pickerCollapsed } >
                                <img className='img-wrapper__img' 
                                        src='static/images/select-from-list.png' 
                                        alt='Select account'/>
                            </button> ) : null }                
                        </div>
                    </div>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ accountElementNameHeader }</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' 
                                   id={ 'id_fi-oper-part-account-name-' + operationPartType }>
                                { operationPartAccountName }
                            </label>
                        </div>
                    </div>                    
                </div>
                <Picker
                    id={ 'id_fi-oper-part-account-picker-' + operationPartType }
                    isExpanded = { !pickerCollapsed }
                    listData = { accountPickerData }
                    onDataPick = { this.onDataPick.bind(this) }
                    listColumns = { operationPickerAccsForSOPListColumns }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterFields = { accPickerFilterFields(this.getFilterFieldName()) }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    filterHeaderName = 'Выберите счет'/>

                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>{ clientElementHeader }</div>
                        <div className='form-input__element-group'>
                            <label className='fi__label fi__element' 
                                   id={ 'id_fi-oper-part-client-' + operationPartType }>
                                { operationPartClientName }
                            </label>
                        </div>
                    </div>
                </div>
        </div>)
    }
}

OperationSimplePart.propTypes = {
    operationPartData: PropTypes.instanceOf(Map).isRequired, 
    operationIsEdited: PropTypes.bool.isRequired,
    accountPickerData: PropTypes.instanceOf(List),
    onDataPick: PropTypes.func.isRequired,
    getPickerData: PropTypes.func.isRequired,
    processPickerOpen: PropTypes.func.isRequired,
}


export { OperationSimplePart };
