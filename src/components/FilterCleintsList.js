import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';


class FilterClientsList extends Component {

    onfilterButtonClick(e) {
        // Handler for other filter buttons click 
        e.preventDefault();
        const { dispatch } = this.props;
        const elementId = e.currentTarget.id;
        switch (elementId) {
            case 'id_client-filter-new-client': {
                // New client edit window 
                dispatch( {type: 'APP_CONTEXT_SET', 
                    payload: {
                        window: 'clientEdit',
                        context: Map({
                            windowContent: Map({
                                 clientId: null
                            })
                        })    
                    }
                });
                break;
            }
        }
    }

    onGetDataClick(e) {
        // Handler for refresh filter data click 
        e.preventDefault();
        console.log('Form submitted');
        const { dispatch, getElementData } = this.props;
        const filterText = getElementData(document.getElementById('id_clients-filter__name'));
        dispatch({
            type: 'GET_CLIENTS_LIST_DATA',
            payload: {
                filterText: filterText
            }
        });
    }

    render() {
        const { filterContext, onElementChange, 
                onFilterClearClick, isManager } = this.props;

        const filterValues = filterContext.get('filterValues');
        
        const name = filterValues.get('clients-filter__name');
        const status = filterValues.get('clients-filter__acc-status');
        

        return (
            <form id='id_form-clients-filter' onSubmit={ this.onGetDataClick.bind(this) }>
                <div className='main-block__filter'>
                    <div className='mbf__block' key={1}>
                        <div className='mbf-block__header'>Фильтр по имени</div>
                        <div className='mbf-block__element-group'>
                            <input type='text' className='form-control' placeholder='' 
                            id='id_clients-filter__name' name='clients-filter__name'
                            value={ name } onChange={onElementChange}
                            />
                            <div className='img-wrapper' aria-label='Очистить' 
                                htmlFor='id_clients-filter__name'
                                onClick={onFilterClearClick} >
                                <img src='static/images/filter-clear.png' alt='Filter clear'/>
                            </div>
                        </div>
                    </div>
                    <div className='mbf__block' key={2}>
                        <div className='mbf-block__header'>Статус</div>
                        <div className='mbf-block__element'>
                            <select className='form-control' id='id_clients-filter__acc-status' 
                                    name='clients-filter__acc-status'
                                    onChange={ onElementChange }  value={ status } 
                                    disabled >
                                <option value='acc_status_open'>Открытые</option>
                                <option value='acc_status_all'>Все счета</option>
                            </select>
                        </div>
        
                    </div>
                    <div className='mbf__block mbf__block_align-end' key={3}>
                        <div className='mbf-block__element'>
                            <button type='submit' 
                                    className='btn' 
                                    key={1} 
                                    onClick={ this.onGetDataClick.bind(this) }
                                    id='id_filter-refresh'>
                                        Обновить
                            </button>
                            { isManager ? (
                            <button type='button' 
                                    className='btn' 
                                    key={2}
                                    onClick={ this.onfilterButtonClick.bind(this) }
                                    id='id_client-filter-new-client'>Создать клиента</button>
                            ) : null }
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

FilterClientsList.propTypes = {
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired,
    getElementData: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    isManager: PropTypes.bool.isRequired,
    isClient: PropTypes.bool.isRequired
}

export { FilterClientsList };