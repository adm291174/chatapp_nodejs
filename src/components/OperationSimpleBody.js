import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import { Map, List } from 'immutable';
import { OperationSimplePart } from '../elements/OperationSimplePart';
import { debetOperationPart, creditOperationPart } from '../constants/operations_constants';

import { validateElement, validateMix, showElementValidityState } from '../core/validation_functions';
import SumInput from '../elements/SumInput';

import { floatPrettyPrint } from '../core/core_functions';


class OperationSimpleBody extends Component {

    constructor(props) {
        super(props);
        this.componentRefs = [ React.createRef(),   // First transaction part 
                               React.createRef()];  // Second transaction part 
        
        this.sumElementRef = React.createRef();
        
        this.validationList = [ 
            {
                validationType: 'component',
                element: this.componentRefs[0]
            },
            {
                validationType: 'component',
                element: this.componentRefs[1]
            },
            {
                validationType: 'component',
                element: this.sumElementRef
            },            
            {
                validationType: 'element',
                element: 'id_fi-simple-oper-purpose',
                rules: [{name: 'noEmpty'}]
            }
        ]
    } 

    validate(needToSetFocus) {
        const markValidationState = true;
        const validationState = this.internalValidate(markValidationState, needToSetFocus);
        // check if accounts are equal
        const { operationData } = this.props, 
                simpleOperationData = operationData.get('simpleOperationData'),
                debetOperationPart = simpleOperationData.get('debetOperationPart'),
                creditOperationPart = simpleOperationData.get('creditOperationPart'),
                debetAccountId = debetOperationPart.get('accountId'),
                creditAccountId = creditOperationPart.get('accountId');

        if (debetAccountId && creditAccountId && debetAccountId == creditAccountId) {
            this.componentRefs[1].current && 
            this.componentRefs[1].current.setValidationState(false, true);
            return false;
        }

        return validationState;
    }    

    internalValidate(markValidationState = false, needToSetFocus = false) {
        return validateMix(this.validationList, needToSetFocus, markValidationState);                
    }

    
    // values change routines 
    onDataPick({ elementId, keyFileldValue, selectedListValues }) {
        // fill new picked data 
        const { dispatch, operationData } = this.props, 
              simpleOperationData = operationData.get('simpleOperationData'),
              debetPartAffected = (elementId == 'id_fi-oper-part-account-picker-' + debetOperationPart),
              transactionPartName = debetPartAffected ? 'debetOperationPart' : 'creditOperationPart',
              currentCurrencyId = simpleOperationData.get('operationCurrencyId');

            
        const accountNumber = selectedListValues.get('accountNumber'),
              accountName = selectedListValues.get('accountName'),
              clientId = selectedListValues.get('clientId'),
              clientName = selectedListValues.get('clientName'),
              currencyId = selectedListValues.get('accountCurrencyId'),
              currencyCode = selectedListValues.get('accountCurrencyCode'),
              data = simpleOperationData.get(transactionPartName);

        let values = [
                        {fieldName: transactionPartName, 
                            fieldValue: data.set('accountId', keyFileldValue)
                                            .set('accountNumber', accountNumber)
                                            .set('accountName', accountName)
                                            .set('clientId', clientId)
                                            .set('clientName', clientName)},
                        
                     ]
        if (debetPartAffected || !currentCurrencyId) {
            values.push({ fieldName: 'operationCurrencyId', fieldValue: currencyId});
            values.push({ fieldName: 'operationCurrencyCode', fieldValue: currencyCode});
        }
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'simpleOperationData',
                    values: values
                }
        });
    }

    getPickerData(elementId, clearData = false, elementExtraData = {}) {
        // account picker

        const { dispatch, operationData } = this.props,
                debetPartAffected = (elementId == 'id_fi-oper-part-account-picker-' + debetOperationPart),
                pickerName = debetPartAffected ? 'accountsDebet' : 'accountsCredit';
        
        let currencyId = null;
        
        if (!debetPartAffected) {
            const simpleOperationData = operationData.get('simpleOperationData');
            
            currencyId = simpleOperationData.get('operationCurrencyId');
        }

        if (clearData) {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'simpleOperationPickerData', 
                    values: [{
                            fieldName: pickerName,
                            fieldValue: List()}]                        
                }
            });
        }
        else {
            dispatch( {
                type: 'GET_OPERATION_PICKER_DATA_ACCS',
                payload: {
                    currencyId: currencyId,
                    accountType: elementExtraData.accountType,
                    pickerName: pickerName
                }
            });
        }
    }

    processPickerOpen(pickerElementId) {
        /* process picker open - close any leased picker elements */ 

        const debetPartComponentRef = this.componentRefs[0].current,
              creditPartComponentRef = this.componentRefs[1].current;


        const elements = [
            {
                componentRef: debetPartComponentRef,
                visibilityState: debetPartComponentRef.state.pickerCollapsed,
                elementId: 'id_fi-oper-part-account-picker-' + debetOperationPart
            },
            {
                componentRef: creditPartComponentRef,
                visibilityState: creditPartComponentRef.state.pickerCollapsed,
                elementId: 'id_fi-oper-part-account-picker-' + creditOperationPart
            }
        ];

        elements.forEach( element => {
            if (element.componentRef && 
                       element.elementId != pickerElementId && 
                       !element.visibilityState) {
                element.componentRef.onPickerCancel(element.elementId);
            }
        });
    
    } 

    onPurposeBlur(e) {
        validateElement(e.target.id, [{name: 'noEmpty'}], true, true, false);
    }

    onChange(e) {
        /* on "purpose" component change handler */ 
        const { dispatch } = this.props;

        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'simpleOperationData', 
                    values: [
                        {fieldName: 'operationPurpose', 
                            fieldValue: e.target.value }
                    ]
                }
        });
    }

    onValidation(isValid, id) {
        switch (id) {
            case 'id_fi-simple-oper-sum': {
                const sumElement = document.getElementById(id);
                showElementValidityState(sumElement, isValid);
                break;
            } 
        }
    }

    onSumChange(value, elementId) {
        const { dispatch } = this.props;

        switch (elementId) {
            case 'id_fi-simple-oper-sum': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: null, 
                        values: [ { fieldName: 'operationSum', fieldValue: value } ]                        
                    }
                });
                break;                 
            }
        }
    }

    componentDidUpdate(prevProps) {
        const { operationData } = this.props,
              prevOperationData = prevProps.operationData,
              simpleOperationData = operationData.get('simpleOperationData'),
              prevSimpleOperationData = prevOperationData.get('simpleOperationData'),
              currencyId = simpleOperationData.get('operationCurrencyId'),
              prevCurrencyId = prevSimpleOperationData.get('operationCurrencyId'); 

        if ( !(currencyId && !prevCurrencyId) && (currencyId != prevCurrencyId)) {
            // clear credit information 
            const data = simpleOperationData.get('creditOperationPart');

            let values = [ {fieldName: 'creditOperationPart', 
                            fieldValue: data.set('accountId', null)
                                            .set('accountNumber', '')
                                            .set('accountName', '')
                                            .set('clientId', null)
                                            .set('clientName', '')},
                            
                        ]
            this.props.dispatch({
                type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'simpleOperationData',
                        values: values
                    }
            });
        }
    }

    renderOperationSum(operationIsEdited, 
                       operationSum, 
                       currencyCode,
                       onValidation, 
                       onSumChange) {
        
        const currencyCodeForPrint = currencyCode ? currencyCode : '';

        if (operationIsEdited) {
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Сумма операции</div>
                    <div className='form-input__element-group'>
                        <SumInput className='fi__input fi__element form-control' 
                                    id='id_fi-simple-oper-sum'
                                    value={ operationSum }
                                    fractionDigits={ 2 }
                                    prohibitNegativeValues={ true }
                                    ref={ this.sumElementRef }
                                    onElementChange={ onSumChange }
                                    changeValueOnElementChange={ true }
                                    onValidation={ onValidation }
                                    showComponentStateOnValidation = { true }
                                    />
                        <label className='fi__label_noborder' 
                               id='id_fi-simple-oper-amount-currency'>{ currencyCodeForPrint }</label>
                    </div>
                </div> );
            }
        else {
            const operationSumDesc = (operationSum !== null && operationSum !== undefined ) 
                                      ? floatPrettyPrint(operationSum) + ' ' + currencyCodeForPrint : '';
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Сумма операции</div>
                        <div className='form-input__element-group'>
                            <div className='form-input__element-group'>
                                <div className='fi__label_noborder fi__label_noborder-wide' 
                                        id='id_fi-simple-oper-sum-ro'>
                                    { operationSumDesc }
                            </div>
                            <div className='fi-stub'></div>
                        </div>
                    </div>
                </div> );
        }        
    }


    render() {
        const { operationIsEdited, operationData } = this.props,
              onDataPick = this.onDataPick.bind(this),
              getPickerData = this.getPickerData.bind(this),
              processPickerOpen = this.processPickerOpen.bind(this),
              onChange = this.onChange.bind(this),
              onPurposeBlur = this.onPurposeBlur.bind(this),
              onSumChange = this.onSumChange.bind(this),              
              onValidation = this.onValidation.bind(this),              
              simpleOperationData = operationData.get('simpleOperationData'),
              operationPartPurpose = simpleOperationData.get('operationPurpose');

        const operationSum = operationData.get('operationSum'),
              simpleOperationPickerData = operationData.get('simpleOperationPickerData'),
              currencyCode = simpleOperationData.get('operationCurrencyCode'),
              debetPartPickerData = simpleOperationPickerData.get('accountsDebet'),
              creditPartPickerData = simpleOperationPickerData.get('accountsCredit'),
              debetPartData = simpleOperationData.get('debetOperationPart'),
              creditPartData = simpleOperationData.get('creditOperationPart');
              
        
        return (
            <div>
                <OperationSimplePart operationIsEdited={ operationIsEdited }
                                     onDataPick={ onDataPick } 
                                     getPickerData={ getPickerData } 
                                     processPickerOpen={ processPickerOpen }
                                     ref={ this.componentRefs[0] }
                                     accountPickerData={ debetPartPickerData }
                                     operationPartData={ debetPartData}
                />
                <OperationSimplePart operationIsEdited={ operationIsEdited }
                                     onDataPick={ onDataPick } 
                                     getPickerData={ getPickerData } 
                                     processPickerOpen={ processPickerOpen }
                                     ref={ this.componentRefs[1] }
                                     accountPickerData={ creditPartPickerData }
                                     operationPartData={ creditPartData}
                />
                <div className='form-input__row'>
                    { this.renderOperationSum(operationIsEdited, 
                                              operationSum, 
                                              currencyCode, 
                                              onValidation,
                                              onSumChange) }

                </div>
                <div className='form-input__row'>
                    <div className='form-input__block form-input__block_wide'>
                        <div className='fi-block__header'>Назначение платежа</div>
                        <textarea className='fi-text_wide form-control'
                                  id={ 'id_fi-simple-oper-purpose' }
                                  value={ operationPartPurpose }
                                  onChange={ onChange }
                                  onBlur={ onPurposeBlur }
                                  disabled={ !operationIsEdited }></textarea>
                    </div>
                </div>
            </div>);
    }

}


OperationSimpleBody.propTypes = {
    operationId: PropTypes.number, 
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map).isRequired
}

function mapStateToProps(state) {
    const windowName = 'operationEdit',
          rootState = state.get('root').data,
          operationsState = state.get('operations').data,
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
          
    
    const operationId = windowContents.get('operationId'),
          operationIsEdited =  windowContents.get('operationIsEdited'),
          operationData = operationsState.get('operationData')

    return {
        operationId: operationId,
        operationIsEdited: operationIsEdited,
        operationData: operationData
    }
}

export default connect(mapStateToProps, null, null, {forwardRef : true})(OperationSimpleBody);
