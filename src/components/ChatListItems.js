import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Map, List } from 'immutable';
import { filterGroupNames } from '../constants/main_constants';
import ChatListGroup  from './ChatListGroup';


class ChatListItems extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    onGroupCategoryChange(key, value) {
        this.props.handleGroupCollapsedState(key, value);
    }

    renderGroups() {
        
        /* 
        Render valid groups depend on selected group and filter state 
        params: 
            showClientsGroup: boolean, if true, render Clients groups, 
                                       if false render Time Category groups 
        returns: 
            list of React components 
        */

        const showClientsGroup = this.props.filterGroup === filterGroupNames[1];

        const processedList = showClientsGroup ? this.props.groupsList : this.props.timeCategoriesList;
        const { filterEnabled, 
                filterText, 
                collapsedState, 
                handleChatClick,
                chatIsActive, 
                activeChatId } = this.props;
        return processedList.map((value) => {
            return <ChatListGroup renderedGroup={value} 
                                  filterEnabled={filterEnabled} 
                                  filterText={filterText} 
                                  collapsedState={collapsedState}
                                  onGroupCategoryChange={this.onGroupCategoryChange.bind(this)}
                                  key={ value.get('key')}
                                  handleChatClick={handleChatClick}
                                  chatIsActive={ chatIsActive }
                                  activeChatId={ activeChatId } />
        })
    }


    render() {
        
        return <div className='chat-list-w'>
                { this.renderGroups() }
               </div>
    }
}

ChatListItems.propTypes = {
    filterGroup: PropTypes.string.isRequired, 
    filterEnabled: PropTypes.bool.isRequired,
    filterText: PropTypes.string.isRequired,
    groupsList: PropTypes.instanceOf(List).isRequired,
    timeCategoriesList: PropTypes.instanceOf(List).isRequired,
    collapsedState: PropTypes.instanceOf(Map).isRequired,
    handleGroupCollapsedState: PropTypes.func.isRequired,
    handleChatClick: PropTypes.func.isRequired,
    chatIsActive: PropTypes.bool.isRequired,
    activeChatId: PropTypes.number
}


export default ChatListItems;