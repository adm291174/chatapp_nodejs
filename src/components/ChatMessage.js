import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { replaceCr, getDateTimeStrWithStrMonth, getTimeStr } from '../core/core_functions';


class ChatMessage extends Component {
    constructor (props) {
        super(props);
    }

    renderFileInfo(messageFileId, messageFileName) {
        if (messageFileId) {
            return (
                <div className='message-file'>
                    <img height='16' className='attach-icon' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAQOSURBVGhD7ZlbqFVVFEBvWmYRZZAfYg8rJaMPS1CxggiKwiipD8MHgkaE9ADxoxQVhKKPwCLCCHpb0YdYfkX00lIqgqKPjAiCiLD3g95Z2hgXN0wm67zuPXvfE94BA73nrLX2nnufvdZccw+NM87Rw0Q8FY8f/ut/hid9E+7Fg3j4yL/v40Y8DRvnGLwETx/+qzMr8AB68q38BddgY0zBV9CD/44G1Arb7sR80u18DI/FWpmGH2I88MNY4lz8GGPbyi/xRXwZ/z7yWdTgJ2EtzMTPMB90CWYuwm8wtzWwG3ACVpyJuzG3NcgTsa94Yl9hPpgPaWYe/oC57YN4Apbw6j+Luc8O9HnsC5fjzxgP8C/eipkF+BPGtn/iSuyEU/IzGPvqHThqrsE/MA78Fy7FTCmIX/EK7BaDeQ7jGI55Mo6YG9GTjoP+hoswUwrCu3gp9spk/ADjWKtwRNjxH4yDeaKlqbZVEBfjSLkW43iPYM/cjocwDvQtzsVMHUGIC20c0+m4J9ZjHEBdkS/AzGiCOA43oFf6PD9IXIlx3Mexa+7F2Fk/x1mYGU0Qrg2uEVW/jzDzJMax/ZV0xHn6IYwd9VM8CzP9DEJNHGMW7Jrl9F597//PxrYYhLlNHFj3o+lIpt9B6CascGF8D+P3LoodKf2cTKtLKXUdQdyPFV7UvCA63ZuztcWUOXbSt9GMNVN3EHIf5ja3YVuuxrxOvIMnYaaJIEq/DB/4trjFNI2OnXywp2JmrIJ4CTum8dsxdjLdNkXPjFUQe9D+bbkKc0cTw8xAB+GskKc2p97MfBzYIOQyjB1/xFMwMgPzzm6ggpCnMHa+GzOvYmwzcEG4R/4aq85u+PPKnRM1p+duNkWNBSGzMQ7wLmaex9hmM3ai0SBkMcZBHsBMXFvcZ7vetKPxIGQ1xoHWYSZmnG/6QRvGJAi5GeNgd2HGB7v6/hM/aMGYBSHXYxzwHsy8hrHNcsxMx7cwttNGghC3k3HQfZix9hTbOLN5587A89F9Q6l62FgQ4qoep193ZXnPUdrUdHIrRmoNomIbxgPkKynWYb/A2K6kE0N+zhoJQuZgLPNYfCvtvkznd2EuCVV61xZipLEgKl7AeDC3ta0OZgnIko1pv+nNFszpihlDqXBRaxBiNcJ9cDyoV9+aa69Y+ShV0GsPouIWzAf3qvfyYtJnyTQnj/M6NhJExaOYT8L1odMLSWe/Zfgd5v7eWQvQjeJPKT8vao13LeYT8m5dh1ZZch99Amt/79cK669PY+nEfMPqHTIjfgO/x1I7F82uypl140/lTqzee/eilZduNl2NciF65UsnnHWLbPZc21vXfuAbJkv4ue7l6zeTSt8b5n3+wGPp9Bz0pUt8nTzOOMMMDf0H0VbLVkp/OLIAAAAASUVORK5CYII=' />
                    <a href={'/chat/files/' + messageFileId.toString() + '/'}>{messageFileName}</a>
                </div>
            )

        }
        else return null;

    }

    render() {
        const { elementId, created, modified, messageText, messageFileId, messageFileName,
                messageAuthorText, authorIsMe } = this.props;

        const messageDateTime = modified ?  getDateTimeStrWithStrMonth(modified, true ) + ', ' + getTimeStr(modified) 
                                          : getDateTimeStrWithStrMonth(created, true)  + ', ' + getTimeStr(created);

        return (<article className={'chat-message card' + (authorIsMe ? ' chat-message_own' : '')}  id={elementId}>
                    <div className={'card-header chat-message__header' + ( authorIsMe ? ' chat-message__header-own' : '' ) }>
                        <div className='row justify-content-between'>
                        <div className='col author-name'>{ authorIsMe ? 'Мое сообщение' : messageAuthorText }</div>
                        <div className='col text-right message-time'>{ messageDateTime }</div>
                        </div>
                    </div>
                    <div className='card-body card-message-body'>
                        <p className='card-text card-message-text' dangerouslySetInnerHTML={ {__html: replaceCr(messageText)} }>
                            
                        </p>
                        { this.renderFileInfo(messageFileId, messageFileName) }
                    </div>
                </article>
        )    
    } 
}

ChatMessage.propTypes = {    
    messageId:  PropTypes.number.isRequired,
    elementId: PropTypes.string.isRequired,
    created: PropTypes.instanceOf(Date).isRequired,
    modified: PropTypes.instanceOf(Date),
    messageText: PropTypes.string.isRequired,
    messageFileId: PropTypes.number,
    messageFileName: PropTypes.string,
    messageAuthor: PropTypes.number.isRequired,
    chatHistoryId: PropTypes.number.isRequired,
    messageAuthorText: PropTypes.string.isRequired,
    authorIsMe: PropTypes.bool.isRequired
}
 
export default connect()(ChatMessage);