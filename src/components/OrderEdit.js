import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import OrderEditControlHeader from './OrderEditControlHeader';
import OrderEditBody from './OrderEditBody';
import { showElementValidityState } from '../core/validation_functions';
import { getDateStrForQuery } from '../core/core_functions';

import { orderTypeCommissions } from '../constants/order_constants';



class OrderEdit extends Component {
    constructor(props) {
        super(props);
        this.orderEditBodyRef = React.createRef();
    } 

    componentDidUpdate(prevProps) {
        if (prevProps.orderIsEdited !== this.props.orderIsEdited || 
           prevProps.orderId != this.props.orderId) {
                window.scrollTo(0, 0);
        }
    }

    componentWillUnmount() {
        this.props.dispatch(
            {
                type: 'WINDOW_TITLE_SET',
                payload: {
                    windowName: 'orderEdit',
                    windowTitle: 'Ордер'
                }
            }
        );    
    }

    processOrderActions(action) {
        /* process actions on order 
           param: GUID of action 
        */ 
        const { dispatch, orderId } = this.props;
        dispatch ({
                type: 'ORDER_PROCESS_ACTION', 
                payload: {
                    orderId: orderId,
                    actionId: action
                }
        });
    }

    processOrderBtnClick(e) {
        /* process clicks of lower buttons */ 
        const element = e.target,
              elementId = element.id,
              { dispatch, orderId } = this.props;
            
        
        switch (elementId) {
            case 'id_order-click-btn-cancel': {
                /* Cancel button click in Edit mode */
                if (orderId) {
                    dispatch ({
                        type: 'APP_CONTEXT_SET', 
                        payload: {
                            window: 'orderEdit',
                            context: Map({
                                windowContent: Map({
                                    orderId: orderId,
                                    orderIsEdited: false
                                })
                            })
                        }
                    });
                }
                else {
                    dispatch (
                        {
                            type: 'APP_CONTEXT_SET',
                            payload: {
                                window: 'ordersList',
                                context: Map()
                            }
                        }); 
                    dispatch(
                        {
                            type: 'WINDOW_TITLE_SET',
                            payload: {
                                windowName: 'orderEdit',
                                windowTitle: ''
                            }
                        }
                    );
                }
                break;
            }
            case 'id_order-click-btn-to-orders-list': {
                /* 'Go to orders list' button click in View mode */
                dispatch (
                    {type: 'APP_CONTEXT_SET',
                    payload: {window: 'ordersList',
                                context: Map()
                                }
                    }); 
                break;
            }
            case 'id_order-click-btn-save':
            case 'id_order-click-btn-save-and-exit': {
                /* Save button click in Edit mode */
                const validationState = this.validate();
                if (validationState) {
                    
                    const { dispatch, orderData, orderId, 
                            linkedOrdersToRemove, linkedOrdersToAdd } = this.props,
                            orderBodyData = orderData.get('orderBodyData');

                    
                    dispatch({
                        type: 'ORDER_DATA_SAVE',
                        payload: {
                            data: {
                                orderId: orderId,
                                orderDate: getDateStrForQuery(orderBodyData.get('orderDate')),
                                orderType: orderBodyData.get('orderType'),
                                accountId: orderBodyData.get('orderAccountId'),
                                accountCurrencyId: orderBodyData.get('orderAccountCurrencyId'),
                                clientId: orderBodyData.get('orderClientId'),
                                orderSum: orderBodyData.get('orderSum'),
                                commissionType: orderBodyData.get('orderCommissionType'),
                                commissionPercent: orderBodyData.get('orderCommissionPercent'),
                                commissionSum: orderBodyData.get('orderCommissionSum'),
                                secondarySum: orderBodyData.get('orderSecondarySum'),
                                addOrders: linkedOrdersToAdd.map(value => (value.get('orderId'))).toJS(),
                                removeOrders: linkedOrdersToRemove.map(value => (value.get('orderId'))).toJS()
                            },
                            postAction: (elementId == 'id_order-click-btn-save-and-exit') ? 'exitOrder' : 'viewOrder'
                        }
                    }) 
                }
                else {
                    this.setFocus();
                }
                break;
            }
            case 'id_order-click-btn-edit': {
                /* Edit button click in View mode */
                dispatch (
                    {type: 'APP_CONTEXT_SET', 
                        payload: {
                        window: 'orderEdit',
                        context: Map({
                            windowContent: Map({
                                orderId: orderId,
                                orderIsEdited: true
                            })
                        })
                    }
                });
                break;
            }
        }
    }    

    onActionToggle() {
        // get actions for actions list (refresh current)
        const { dispatch, orderId } = this.props;
        dispatch( {
            type: 'GET_ORDER_DATA',
            payload: {
                orderId: orderId,
                getOrderBodyData: false,
                getOrderAdditionalData: false,
                getOrderActions: true
            }
        });
    }

    onDatePickerChange(id, dateValue) {
        /* process date change */ 
        const { dispatch } = this.props; 
        dispatch( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderDate',
                fieldValue: dateValue
            }
        });
    }

    onChange(e) {
        const { dispatch } = this.props, 
              element = e.currentTarget, 
              elementId = element.id;

        switch (elementId) {
            case 'id_fi-order-type': {
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderType',
                        fieldValue: element.value
                    }
                });
                break;                 
            }
            case 'id_fi-order-comm-type': {
                const commissionType = element.value;
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionType',
                        fieldValue: commissionType
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionDescription',
                        fieldValue: orderTypeCommissions.get(commissionType).description
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionPercent',
                        fieldValue: orderTypeCommissions.get(commissionType).commissionPercent
                    }
                });
                break;                 
            }
        }
    }

    onSumChange(value, elementId) {
    /* Handle sum input changes here */
        const { dispatch } = this.props;

        switch (elementId) {
            case 'id_fi-order-sum': {
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderSum',
                        fieldValue: value
                    }
                });
                break;                 
            }
            case 'id_fi-order-comm-percent': {
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionPercent',
                        fieldValue: value
                    }
                });
                break;
            }
            case 'id_fi-order-comm-sum': {
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionSum',
                        fieldValue: value
                    }
                });
                break;
            }
            
        }
    }

    onValidation(isValid, elementId) {
        /* function for show validation state of SumInput elements */
        const element = document.getElementById(elementId);
        element && showElementValidityState(element, isValid);
    }

    validate() {
        // Validate form before submit. Also mark valid and invalid elements 
        if (this.orderEditBodyRef.current) {
            return this.orderEditBodyRef.current.validate()
        }
        else return false;
    }

    setFocus() {
        this.orderEditBodyRef.current && 
            this.orderEditBodyRef.current.setFocus();
    }

    renderFormControlHeader(orderIsEdited) {
        if (!orderIsEdited) {
            const { orderId, 
                    orderData } = this.props,
                    orderBodyData = orderData.get('orderBodyData'),
                    orderActions = orderData.get('orderActions');

            const orderStatusDescription = orderBodyData.get('orderStatusDescription'),
                  actionsList = orderActions;

            return (
                <OrderEditControlHeader 
                    orderId = { orderId }
                    orderStatusDescription = { orderStatusDescription }
                    onActionProcess = { this.processOrderActions.bind(this) }
                    actionsList = { actionsList } 
                    onActionToggle = { this.onActionToggle.bind(this) }
                />
            );    
        }
        else return null;
    }

    renderBodyPart(orderIsEdit) {
        const { orderId, 
                orderData } = this.props;

        const orderDataId = orderData.get('orderDataId');

        if (orderDataId === orderId) {
            const orderBodyData = orderData.get('orderBodyData'), 
                  orderLinkedOrders = orderData.get('orderLinkedOrders'), 
                  orderLinkedOperations = orderData.get('orderLinkedOperations')

            return (
                <OrderEditBody 
                    orderId = { orderId }
                    orderIsEdited = { orderIsEdit }
                    orderBodyData = { orderBodyData }
                    orderLinkedOrders = { orderLinkedOrders }
                    orderLinkedOperations = { orderLinkedOperations } 
                    onChange = { this.onChange.bind(this) } 
                    onOrderDateChange = { this.onDatePickerChange.bind(this) }
                    onSumChange = { this.onSumChange.bind(this) }
                    onValidation = { this.onValidation.bind(this) }
                    ref = { this.orderEditBodyRef }
                />
            );
        }
        else return null;

    }

    renderButtons(orderIsEdit, canBeEdited) {
        if (orderIsEdit) {
            return (
                <div className='main-block__buttons'>
                    <button type='button' 
                            className='btn main-block__btn' 
                            id='id_order-click-btn-save-and-exit'
                            onClick={ this.processOrderBtnClick.bind(this) }>
                            Сохранить и выйти
                    </button>
                    <button type='button' 
                            className='btn main-block__btn main-block__buttons_btn_mr2' 
                            id='id_order-click-btn-save'
                            onClick={ this.processOrderBtnClick.bind(this) }>
                            Сохранить
                    </button>
                    <button type='button' 
                            className='btn main-block__btn main-block__btn-cancel' 
                            id='id_order-click-btn-cancel'
                            onClick={ this.processOrderBtnClick.bind(this) }>
                            Отмена
                    </button>
                </div> 
            )        
        }
        else {
            return (
                <div className='main-block__buttons'>
                    { canBeEdited ? (
                    <button type='button' 
                            className='btn main-block__btn' 
                            id='id_order-click-btn-edit'
                            onClick={ this.processOrderBtnClick.bind(this) }>
                            Редактировать
                    </button>) : null }
                    <button type='button' 
                            className='btn main-block__btn main-block__buttons_btn_mr2' 
                            id='id_order-click-btn-to-orders-list'
                            onClick={ this.processOrderBtnClick.bind(this) }>
                            К списку ордеров
                    </button>
                </div>
            );        
        }
    }
    
    render() {
        const { orderIsEdited, orderId, orderData } = this.props,
                orderDataId = orderData.get('orderDataId'),
                canBeEdited = orderData.get('orderBodyData').get('canBeEdited');

                
        if (orderId && !orderDataId) {
            return <div className='main-block__body'>Получение данных ордера... </div>
        }
        else {
            return (
                <div className='main-block__body'>
                    <form id='id_form-order'>
                        { this.renderFormControlHeader(orderIsEdited) }
                        { this.renderBodyPart(orderIsEdited) }
                        { this.renderButtons(orderIsEdited, canBeEdited) }
                    </form>
                </div>
            )
        }
    }

}

OrderEdit.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired,
    orderData: PropTypes.instanceOf(Map)
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName,
          rootState = state.get('root').data,
          ordersState = state.get('orders').data,
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
          
    
    const orderId = windowContents.get('orderId'),
          orderIsEdited =  windowContents.get('orderIsEdited'),
          orderData = ordersState.get('orderData'),
          linkedOrdersToRemove = windowContents.get('linkedOrdersToRemove'),
          linkedOrdersToAdd = windowContents.get('linkedOrdersToAdd');
    

    return {
        orderId: orderId,
        orderIsEdited: orderIsEdited,
        linkedOrdersToRemove: linkedOrdersToRemove,
        linkedOrdersToAdd: linkedOrdersToAdd, 
        orderData: orderData,
        userRoles: rootState.get('userRoles'),
        isManager: rootState.get('isManager'),
        isClient: rootState.get('isClient')
    }
}

export default connect(mapStateToProps)(OrderEdit);