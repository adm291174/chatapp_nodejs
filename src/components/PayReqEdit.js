import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import { showElementValidityState } from '../core/validation_functions';
import { validateByRules } from '../core/validation_functions';


const fieldsMappings = {
    'id_fi-payreq-orgname': {
        fieldName: 'orgName',
        validationRules: [{ name: 'noEmpty'}]
    },        
    'id_fi-payreq-tax-number': {
        fieldName: 'orgTaxNumber',
        validationRules: [{ name: 'noEmpty' }, 
                          { name: 'strictLength', length: [10, 12] },
                          { name: 'onlyDigits' } ]
    },    
    'id_fi-payreq-tax-reason-code': {
        fieldName: 'orgTaxReasonCode',
        validationRules: [{ name: 'strictLength', length: [0, 9] },
                          { name: 'onlyDigitsOrEmpty' } ]
    },
    'id_fi-payreq-acc-number': {
        fieldName: 'accountNumber',
        validationRules: [{ name: 'noEmpty' }, 
                          { name: 'strictLength', length: 20 },
                          { name: 'onlyDigits' } ]
    },
    'id_fi-payreq-bankname': {
        fieldName: 'bankName',
        validationRules: [{ name: 'noEmpty'}]
    },
    'id_fi-payreq-bic-code': {
        fieldName: 'bankCode',
        validationRules: [{ name: 'noEmpty' }, 
                          { name: 'strictLength', length: 9 },
                          { name: 'onlyDigits' } ]
    },
    'id_fi-payreq-corr-account': {
        fieldName: 'bankCorrAccNumber',
        validationRules: [{ name: 'noEmpty' }, 
                          { name: 'strictLength', length: 20 },
                          { name: 'onlyDigits' } ]
    },
    'id_fi-payreq-enabled': {
        fieldName: 'enabled',
        validationRules: []
    }
}

class PayReqEdit extends Component {

    onFormSubmit(e) {
        e.preventDefault();
        if (this.validateForm()) {
            const { dispatch, requisitId, clientId, orgName, orgTaxNumber, orgTaxReasonCode, 
                    accountNumber, bankName, bankCode, bankCorrAccNumber, enabled } = this.props;
            dispatch({
                type: 'PAYREQ_DATA_SAVE',
                payload: {
                    requisitId: requisitId ? requisitId: null,
                    clientId: clientId,
                    orgName: orgName,
                    orgTaxNumber: orgTaxNumber,
                    orgTaxReasonCode: orgTaxReasonCode ? orgTaxReasonCode : '',
                    accountNumber: accountNumber,
                    bankName: bankName,
                    bankCode: bankCode,
                    bankCorrAccNumber: bankCorrAccNumber,
                    enabled: enabled
                }
            }) 
        }
    }

    validateForm() {
        const validatedElements = [
            'id_fi-payreq-orgname',
            'id_fi-payreq-tax-number',
            'id_fi-payreq-tax-reason-code',
            'id_fi-payreq-acc-number',
            'id_fi-payreq-bankname',
            'id_fi-payreq-bic-code',
            'id_fi-payreq-corr-account'
        ]

        const _this = this;
        let firstInvalidElement = undefined,
            formIsValid = true;
        validatedElements.forEach(elementId => {
            const validateState = _this.validateElement(null, elementId);
            if (!validateState) {
                if (!firstInvalidElement) 
                    firstInvalidElement = document.getElementById(elementId);
                formIsValid = false;
            }
        })
        
        if (!formIsValid && firstInvalidElement) {
            firstInvalidElement.focus();
        }
        return formIsValid;
    }    

    validateElement(e, elementId) {
        const validatedLement = e ? e.target : document.getElementById(elementId),
              validatedLementId = elementId ? elementId : e.target.id;

        const elementDescription = fieldsMappings[validatedLementId];
        const elementValue = this.props[elementDescription.fieldName];

        const elementState = validateByRules(elementValue, elementDescription.validationRules);
        showElementValidityState(validatedLement, elementState);
        return elementState;
    }


    onCancelClick(e) {
        e.preventDefault();
        if (e.currentTarget.id == 'id-payreq-edit-btn-cancel') {
            const { clientId, dispatch } = this.props; 
            // switch to client edit form
            dispatch ({
                type: 'APP_CONTEXT_SET',
                payload: {  
                    window: 'clientEdit',
                    context: Map({
                        windowContent: Map({
                            clientId: clientId
                        })
                    })
                }
            })
        }
    }

    onChange(e) {
        const { dispatch } = this.props, 
              element = e.currentTarget, 
              elementId = element.id,
              windowName = 'paymentRequsitEdit';

        switch (elementId) {
            case 'id_fi-payreq-orgname':
            case 'id_fi-payreq-tax-number':
            case 'id_fi-payreq-tax-reason-code':
            case 'id_fi-payreq-acc-number': 
            case 'id_fi-payreq-bankname':
            case 'id_fi-payreq-bic-code':
            case 'id_fi-payreq-corr-account':
            {
                dispatch( {
                    type: 'WINDOW_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        fieldName: fieldsMappings[elementId].fieldName,
                        fieldValue: element.value
                    }
                });
                break;                 
            }
            case 'id_fi-payreq-enabled': {
                dispatch( {
                    type: 'WINDOW_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        fieldName: 'enabled',
                        fieldValue:  element.checked
                    }
                });
                break;                 
            }
        }
    }

    render() {
        const { clientName, orgName, orgTaxNumber, orgTaxReasonCode, 
                accountNumber, bankName, bankCode, bankCorrAccNumber, enabled } = this.props;

        return (
        <div className='main-block__body'>
            <form id='id_form-payreq' onSubmit={ this.onFormSubmit.bind(this) }>
                <div className='form-input form-input-payreq'>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Наименование клиента</div>
                                <div className='form-input__element-group'>
                                    <div className='fi__label fi__element' 
                                        id='id_fi-payreq-client-name'>
                                        { clientName }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi__header-label fi__header-label_alone'>
                                     Платежные реквизиты
                                </div>
                            </div>
                        </div>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi-block__header'>Наименование организации</div>
                                <input className='fi__input fi__element form-control fi__element_wide' 
                                       id='id_fi-payreq-orgname' value={ orgName }
                                       onChange={ this.onChange.bind(this) }
                                       onBlur={ this.validateElement.bind(this) }                                            
                                       />
                            </div>
                        </div>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi-block__header'>ИНН организации</div>
                                <div className='form-input__element-group'>
                                    <input className='fi__input fi__element form-control' 
                                           id='id_fi-payreq-tax-number' 
                                           onChange={ this.onChange.bind(this) }
                                           onBlur={ this.validateElement.bind(this) }                                            
                                           value={ orgTaxNumber } />  
                                </div>
                            </div>    
                            <div className='form-input__block'>
                                <div className='fi-block__header'>КПП организации</div>
                                <div className='form-input__element-group'>
                                    <input className='fi__input fi__element form-control' 
                                           id='id_fi-payreq-tax-reason-code'
                                           onChange={ this.onChange.bind(this) }
                                           onBlur={ this.validateElement.bind(this) }                                            
                                           value={ orgTaxReasonCode } />
                                </div>
                            </div>    
                        </div>

                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi-block__header'>Номер банковского счета</div>
                                <input className='fi__input fi__element form-control fi__element_account' 
                                       id='id_fi-payreq-acc-number' 
                                       onChange={ this.onChange.bind(this) }
                                       onBlur={ this.validateElement.bind(this) }                                            
                                       value={ accountNumber } />
                            </div>
                        </div>

                        <div className='form-input__row'>
                            <div className='form-input__block form-input__block_wide'>
                                <div className='fi-block__header'>Наименование банка</div>
                                <textarea className='fi-text_wide form-control' 
                                          id='id_fi-payreq-bankname'
                                          onChange={ this.onChange.bind(this) }
                                          onBlur={ this.validateElement.bind(this) }                                            
                                          value={ bankName } />
                            </div>
                        </div>

                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi-block__header'>БИК банка</div>
                                <div className='form-input__element-group'>
                                    <input className='fi__input fi__element form-control' 
                                           id='id_fi-payreq-bic-code' 
                                           onChange={ this.onChange.bind(this) }
                                           onBlur={ this.validateElement.bind(this) }                                            
                                           value={ bankCode } />
                                </div>
                            </div>    
                            <div className='form-input__block'>
                                <div className='fi-block__header'>Корреспондентский счет</div>
                                <div className='form-input__element-group'>
                                    <input className='fi__input fi__element form-control fi__element_account' 
                                           id='id_fi-payreq-corr-account'
                                           onChange={ this.onChange.bind(this) }
                                           onBlur={ this.validateElement.bind(this) }                                            
                                           value= { bankCorrAccNumber } />
                                </div>
                            </div>    
                        </div>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='form-input__element-group'>
                                    <input type='checkbox' 
                                           className='form-control fi__element_override-form-control' 
                                           id='id_fi-payreq-enabled' 
                                           onChange={ this.onChange.bind(this) }
                                           onBlur={ this.validateElement.bind(this) }                                            
                                           checked={ enabled } />
                                    <label className='fi__label_smallfont' 
                                           htmlFor='id_fi-payreq-enabled'>Действует</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='main-block__buttons'>
                        <button type='submit' 
                                className='btn main-block__btn' 
                                id='id-payreq-edit-btn-submit'
                                onClick={ null }>Сохранить</button>
                        <button type='button'
                                className='btn main-block__btn main-block__btn-cancel' 
                                id='id-payreq-edit-btn-cancel'
                                onClick={ this.onCancelClick.bind(this) }>Отмена</button>
                    </div>
            </form>
        </div>)
    }
}

PayReqEdit.propTypes = {
    windowName: PropTypes.string.isRequired,
    clientId: PropTypes.number.isRequired,
    clientName: PropTypes.string.isRequired,
    orgName: PropTypes.string.isRequired,
    orgTaxNumber: PropTypes.string.isRequired,
    orgTaxReasonCode: PropTypes.string.isRequired,
    accountNumber: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    bankCode: PropTypes.string.isRequired,
    bankCorrAccNumber: PropTypes.string.isRequired,
    enabled: PropTypes.bool.isRequired,
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext').get('windowContext').get(windowName);
    const windowContent = windowContext.get('windowContent');
    // const clientState = state.get('clients').data;
    return {
        requisitId: + windowContent.get('requisitId'),
        clientId: + windowContent.get('clientId'),
        clientName: windowContent.get('clientName'),
        orgName: windowContent.get('orgName'),
        orgTaxNumber: windowContent.get('orgTaxNumber'),
        orgTaxReasonCode: windowContent.get('orgTaxReasonCode'),
        accountNumber: windowContent.get('accountNumber'),
        bankName: windowContent.get('bankName'),
        bankCode: windowContent.get('bankCode'),
        bankCorrAccNumber: windowContent.get('bankCorrAccNumber'),
        enabled: windowContent.get('enabled')
    }
}

export default connect(mapStateToProps)(PayReqEdit);