import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map } from 'immutable';

const descriptions = {
    accounts: 'Счета и клиенты',
    orders: 'Ордеры клиентов',
    operations: 'Операции клиентов'
};


class HeaderButtons extends Component {
    constructor(props) {
        super(props);
        this.buttons = [
            {
                name: 'accounts',
                clientView: true
            },
            {
                name: 'orders',
                clientView: false
            },
            {
                name: 'operations',
                clientView: false
            }
        ];
    }

    onBtnClick(event) {
        let element = event.target;
        let data = element.getAttribute('data-decription');
        event.preventDefault();
        // if (data) this.props.btnClickHandler(data);
        // else {
            switch (data) {
                case 'Счета и клиенты': {
                    this.props.dispatch({type: 'APP_CONTEXT_SET',
                                         payload: {window: 'clients',
                                                   context: Map()
                                                  }
                                        })
                    break;
                }
                case 'Ордеры клиентов': {
                    this.props.dispatch({type: 'APP_CONTEXT_SET',
                                         payload: {window: 'ordersList',
                                                   context: Map()
                                                  }
                                        }) 
                    break;
                }
                case 'Операции клиентов': {
                    this.props.dispatch({type: 'APP_CONTEXT_SET',
                                         payload: {window: 'operationsList',
                                                   context: Map()
                                                  }
                                        }) 
                    break;
                }
            }    
        // }
        this.props.btnClickHandler();
    }

    render() {
        const { userInfo } = this.props,
                isManager = userInfo.get('isManager'),
                isClient = userInfo.get('isClient'),
                onBtnClick = this.onBtnClick.bind(this);

        return <div className='header-button-panel'>
                   { this.buttons.map( value => {
                        
                        const { name, clientView } = value,
                                description = descriptions[name];
                        if (isManager || (isClient && clientView)) {
                            return React.createElement(
                                'span',
                                { className: 'header-button-panel__button',
                                id: 'id_hb-' + name,
                                key: 'header-button_' + name,
                                'data-decription': description,
                                onClick: onBtnClick},
                                description
                            )
                        }
                        else return null;
                    }, this)
                   }
               </div>
    }
}

HeaderButtons.propTypes = {
    btnClickHandler: PropTypes.func.isRequired,
    userInfo: PropTypes.instanceOf(Map).isRequired
} 

export default connect()(HeaderButtons); 