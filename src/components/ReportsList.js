import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import { Map, List } from 'immutable';


class ReportsList extends Component {

    onClick(e) {
        e.preventDefault();
        const { dispatch } = this.props,
              reportId = e.target.getAttribute('data-report-id');

        // clear reports data 
        dispatch({
            type: 'REPORTS_DATA',
            payload: {
                reportId: reportId,
                reportData: null
            }
        })
        dispatch({
            type: 'APP_CONTEXT_SET',
            payload: {
                window: 'report',
                context: Map({
                    windowContent: Map({
                        reportId: reportId
                    })
                })    
            }

        })
    }

    filterReports(windowFilter, reports) {
        const filterValue = windowFilter.get('filterValues')
                                        .get('reports-filter__name')
                                        .toLowerCase();
        let result = Map();
        reports.forEach(value => {
            const reportGroupId = value.reportGroupId,
                  groupRecord = result.has(reportGroupId) 
                                ? result.get(reportGroupId)
                                : List(),
                  reportName = value.reportName;
            
            if (filterValue.length == 0 || reportName.toLowerCase().includes(filterValue)) {
                result = result.set(reportGroupId, groupRecord.push(Map(
                    {
                        reportId: value.reportId,
                        reportName: reportName,
                        reportOrder: value.reportOrder
                    })));
            }
            else result = result.set(reportGroupId, groupRecord);

        })
        return result;
        
    }

    renderReports(reports) {
        if (reports.size > 0) {
            return reports.map(value => {
                const reportName = value.get('reportName'),
                    reportId = value.get('reportId');

                return (
                    <div className='report-item' 
                        key={ reportId }>
                        <a href='#'  
                        data-report-id={ reportId }
                        onClick={ this.onClick.bind(this) }>{ reportName }</a>
                    </div>
                );
            })
        }
        else {
            return <div className='report-item'>Нет подходящих значений</div> 
        }
    }

    renderGroups(reportsGroups, filteredReports) {
        return reportsGroups.map(group => {
            const { reportGroupId, reportGroupName } = group,
                  groupReports = filteredReports.get(reportGroupId);
            if (groupReports.size > 0)
            return <div className='report-group' key={ reportGroupName }>
                <div className='report-group__name'>{ reportGroupName }</div>
                { this.renderReports(filteredReports.get(reportGroupId))}
            </div>
            else return null;
        });
    }

    render() {
        const { reportsGroups, windowFilter, reports } = this.props;
        if (reportsGroups.length) {
            const filteredReports = this.filterReports(windowFilter, reports);

            return (            
                <div className='main-block__body'>
                    <div className='reports-list'>
                        { this.renderGroups(reportsGroups, filteredReports) }
                    </div>
                </div>
            )
        }   
        else return (            
                <div className='main-block__body'>
                    <div className='reports-list'>
                        Нет данных для отображения 
                    </div>
                </div>
        )
        
    }
}

ReportsList.propTypes = {
    windowName: PropTypes.string.isRequired
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext').get('windowContext').get(windowName);
    return {
        windowFilter: windowContext.get('windowFilter'),
        reportsGroups: rootState.get('reportsGroups'),
        reports: rootState.get('reports')
    }
}


export default connect(mapStateToProps)(ReportsList);