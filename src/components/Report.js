// Report window body (visualization part)

import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { filterReportData } from '../report/report_defs';
import { renderReportBody } from '../report/report_render';
import { Map } from 'immutable';


class Report extends Component {
    constructor(props) {
        super(props);
        const { reportDataWithId } = props;
        this.state = {
            filteredDataWithId: this.filterData(reportDataWithId)
        }
    }

    filterData(reportDataWithId) {
        // extra filter processing for data visualization 
        const { reportId, filterValues } = this.props;
        return filterReportData(reportId, filterValues, reportDataWithId);
    }

    renderReportData(filteredDataWithId) {
        // custom data render function 
        const { reportId } = this.props,
                dataReportId = filteredDataWithId.get('reportId'),
                reportData = filteredDataWithId.get('reportData');
        if (reportId && dataReportId && reportData && reportId == dataReportId) {
            return renderReportBody(filteredDataWithId);
        }
        else return <div>Нет данных для отображения</div>
    }
    
    componentDidUpdate(prevProps) {
        const { reportId, filterValues, reportDataWithId } = this.props,
              prevReportId = prevProps.reportId,
              prevFilterValues = prevProps.filterValues,
              prevReportDataWithId = prevProps.reportDataWithId;
        
        if (reportId != prevReportId || 
            !filterValues.equals(prevFilterValues) ||
            !reportDataWithId.equals(prevReportDataWithId)) {
                this.setState({ filteredDataWithId: this.filterData(reportDataWithId)});
        }
    }

    render() {
        const { dataProcessing } = this.props,
              { filteredDataWithId } = this.state;

        if (dataProcessing) {
            return <div className='main-block__body'>Загрузка данных отчета ...</div>
        }
        else {
            return <div className='main-block__body'>{ this.renderReportData(filteredDataWithId) }</div>
        }        
    }
}

Report.propTypes = {
    windowName: PropTypes.string.isRequired
}


const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data,
          reportsData = state.get('reports').data;
    const windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowFilter = windowContext.get('windowFilter'),
          windowContent = windowContext.get('windowContent'),
          reportId = windowContent.get('reportId'),
          currentReportData = reportsData.get(reportId),
          reportFilterValues = windowFilter.get('identifiedFilterValues').get(reportId);

    return {
        reportId: reportId, 
        filterValues: reportFilterValues ? reportFilterValues.get('filterValues') : Map(), // dictinct filter values
        reportDataWithId: Map({
            reportId: reportId,  // extra identity for data accuracy 
            reportData: currentReportData ? currentReportData.get('reportData') : null
        }),
        dataProcessing: currentReportData ? currentReportData.get('dataProcessing') : undefined
    }
}


export default connect(mapStateToProps)(Report);
