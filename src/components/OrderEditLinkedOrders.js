import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { List, Map } from 'immutable';
import { connect } from 'react-redux';

import { floatPrettyPrint, getDateStr } from '../core/core_functions';

import { Dropdown, Collapse, ButtonGroup } from 'react-bootstrap';

import Picker from '../elements/picker/Picker';
import { linkedOrdersPickerFilterFields, linkedOrdersPickerListColumns } from '../constants/picker_constants';
import { buyOrderType, supplyOrderType, orderTypesById } from '../constants/order_constants';
import { getCurrentDate } from '../core/core_functions';


/* Order linked orders  */ 

class OrderEditLinkedOrders extends Component {
    constructor(props) {
        super(props);
        this.state ={
            ordersListIsCollapsed: true,
            ordersPickerIsCollapsed: true,
            filterValues: Map({
                'id-order-linked-orders-picker-filter-name': '',
                'id-order-linked-orders-picker-client-type': buyOrderType,
                'id-order-linked-orders-picker-date': {
                                                    startDate: getCurrentDate(),
                                                    endDate: getCurrentDate()
                                                },
                'id-order-linked-orders-picker-linked-status': true
            })
        }
    }

    onShowButtonClick() {
        this.setState({ordersListIsCollapsed: !this.state.ordersListIsCollapsed});
    }

    onPickOrdersButtonClick() {
        this.setState({ordersPickerIsCollapsed: !this.state.ordersPickerIsCollapsed});
    }

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState( { 
            filterValues: filterValues.set(id, value)
        });
    }

    onDataPick({ keyFileldValue, selectedListValues }) {
        // fill new picked data 
        const { dispatch, linkedOrdersToAdd, orderLinkedOrders } = this.props;

        const linkedOrderId = keyFileldValue,
              orderTypeDescription = selectedListValues.get('orderTypeDescription'),
              orderDate = selectedListValues.get('orderDate'),
              orderNumber = selectedListValues.get('orderNumber'),
              orderStatusDescription = selectedListValues.get('orderStatusDescription'),
              orderSum = selectedListValues.get('orderSum'),
              orderAccountCurrencyCode = selectedListValues.get('orderAccountCurrencyCode'),
              orderClientName = selectedListValues.get('orderClientName')

        const elementIndex = linkedOrdersToAdd.findIndex((value) => (value.get('orderId') == linkedOrderId)),
              existedElementIndex = orderLinkedOrders.findIndex((value) => (value.get('orderId') == linkedOrderId));
        
        if (elementIndex == -1 && existedElementIndex == -1) {
            dispatch({
                type: 'ORDER_EDIT_PICKER_DATA',
                payload: {responseData: {
                    linkedOrdersToAdd: linkedOrdersToAdd.push(Map({
                            orderId: linkedOrderId,
                            orderTypeDescription: orderTypeDescription,
                            orderDate: orderDate,
                            orderNumber: orderNumber,
                            orderStatusDescription: orderStatusDescription,
                            orderSum: orderSum,
                            orderAccountCurrencyCode: orderAccountCurrencyCode,
                            orderClientName: orderClientName
                        }))
                    }
                }
            });
        }
        this.setState({ ordersPickerIsCollapsed: true });
    }
    
    onPickerCancel() {
        this.setState({ ordersPickerIsCollapsed: true });
    }

    getLinkedOrdersPickerData() {
        const { dispatch } = this.props,
              filterValues = this.state.filterValues,
              orderType = filterValues.get('id-order-linked-orders-picker-client-type'),
              filterDates =  filterValues.get('id-order-linked-orders-picker-date'),
              onlyNotLinked = filterValues.get('id-order-linked-orders-picker-linked-status');

        dispatch({
            type: 'GET_ORDER_EDIT_PICKER_DATA_LO',
            payload: {
                orderType: orderType,
                startDate: filterDates.startDate,
                endDate: filterDates.endDate,
                onlyNotLinked: onlyNotLinked
            }
        })        
    }

    onLinkedOrderDetachBtnClick(e) {
        
        const element = e.currentTarget,
              action = element.getAttribute('data-action'),
              actionOrderId = + element.getAttribute('data-order-id'),
              { dispatch, linkedOrdersToAdd, linkedOrdersToRemove } = this.props;

        
        switch (action) {
            case 'cancelAdd': {
                // remove linked order data from linkedOrdersToAdd
                const elementIndex = linkedOrdersToAdd.findIndex((value) => (value.get('orderId') == actionOrderId));
                if (elementIndex != -1) {
                    dispatch({
                        type: 'ORDER_EDIT_PICKER_DATA',
                        payload: {
                            responseData: {
                                linkedOrdersToAdd: linkedOrdersToAdd.delete(elementIndex)
                            }
                        }
                    });
                }
                break;
            }
            case 'revertAttach': {
                // remove linked order data from linkedOrdersToRemove 
                const elementIndex = linkedOrdersToRemove.findIndex((value) => (value.get('orderId') == actionOrderId));
                if (elementIndex != -1) {
                    dispatch({
                        type: 'ORDER_EDIT_PICKER_DATA',
                        payload: {
                            responseData: {
                                linkedOrdersToRemove: linkedOrdersToRemove.delete(elementIndex)
                            }
                        }
                    });
                }

                break;
            }
            case 'detach': {
                // add linked order data to linkedOrdersToRemove 
                const elementIndex = linkedOrdersToRemove.findIndex((value) => (value.get('orderId') == actionOrderId));
                if (elementIndex == -1) {
                    dispatch({
                        type: 'ORDER_EDIT_PICKER_DATA',
                        payload: {
                            responseData: {
                                linkedOrdersToRemove: linkedOrdersToRemove.push(Map({orderId: actionOrderId}))
                            }
                        }
                    });
                }
                break;
            }
        }
    }

    onOrderLinkClick(e) {
        e.preventDefault();
        const { dispatch } = this.props,
            element = e.currentTarget,
            clickedOrderId = + element.getAttribute('data-order-id');

        if (clickedOrderId) {
            dispatch ({
                type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'orderEdit',
                    context: Map({
                        windowContent: Map({
                            orderId: clickedOrderId,
                            orderIsEdited: false
                        })
                    })
                }
            });
        }
    } 

    onCreateLinkedOrderClick() {

        const { dispatch, orderLinkedOrders, orderType, orderData, orderId } = this.props,
            newOrderType = (orderType == buyOrderType) ? supplyOrderType : buyOrderType,
            orderBodyData = orderData.get('orderBodyData');
        
        const orderNumber = orderBodyData.get('orderNumber'),
              orderTypeDescription = orderBodyData.get('orderTypeDescription'),
              orderStatusDescription = orderBodyData.get('orderStatusDescription'),
              orderDate = orderBodyData.get('orderDate'),
              orderAccountCurrencyCode = orderBodyData.get('orderAccountCurrencyCode'),
              orderClientName = orderBodyData.get('orderClientName'),
              orderSum = orderBodyData.get('orderSum');

        const linkedOrdersToAdd = orderLinkedOrders.map(value => {
            return Map({
                orderId: value.get('orderId'),
                orderTypeDescription: value.get('orderTypeDescription'),
                orderDate: value.get('orderDate'),
                orderNumber: value.get('orderNumber'),
                orderStatusDescription: value.get('orderStatusDescription'),
                orderSum: value.get('orderSum'),
                orderAccountCurrencyCode: value.get('orderAccountCurrencyCode'),
                orderClientName: value.get('orderClientName')
            })
        }).push(Map({
            orderId: orderId,
            orderTypeDescription: orderTypeDescription,
            orderDate: orderDate,
            orderNumber: orderNumber,
            orderStatusDescription: orderStatusDescription,
            orderSum: orderSum,
            orderAccountCurrencyCode: orderAccountCurrencyCode,
            orderClientName: orderClientName
        }));

        dispatch({
            type: 'APP_CONTEXT_SET', 
            payload: {
                window: 'orderEdit',
                context: Map({
                    windowContent: Map({
                        orderId: null,
                        orderIsEdited: true
                    })
                }),
                orderBodyData: Map({
                    orderType: newOrderType,
                    orderTypeDescription: orderTypesById.get(newOrderType),
                    orderDate: getCurrentDate(),
                    orderAccountId: null,
                    orderAccountCurrencyId: undefined,
                    orderAccountCurrencyCode: '',
                    orderAccountNumber: '',
                    orderClientId: null,
                    orderClientName: ''
                })
            }            
        }); 
        dispatch({
            type: 'ORDER_EDIT_PICKER_DATA',
            payload: {
                responseData: {
                    linkedOrdersToAdd: linkedOrdersToAdd
                }
            }
        });

    }

    componentDidUpdate(prevProps, prevState) {
        // get new picker data if filter state or data changed 
        const { filterValues, ordersPickerIsCollapsed } = this.state,
               filterHasBeenChanged = !(filterValues.delete('id-order-linked-orders-picker-filter-name')
                                                 .equals(
                                                    prevState.filterValues.delete('id-order-linked-orders-picker-filter-name')
                                                 )),
               needNewData = 
                  ((prevState.ordersPickerIsCollapsed != ordersPickerIsCollapsed) // collapse state has been changed 
                  || filterHasBeenChanged);

        if  (needNewData) {
            // data picker visible state has been changed 
            if (filterHasBeenChanged || !this.state.pickAccountElementCollapsed) {
                // get orders PickerData 
                this.getLinkedOrdersPickerData();
            }
            else {
                const { dispatch } = this.props;
                // clear data 
                dispatch( {
                    type: 'ORDER_EDIT_PICKER_DATA',
                    payload: {
                        responseData: {                        
                            linkedOrdersPickerData: List()
                        }
                    }
                });
            }
        }
    }

    renderHeader(orderIsEdited) {
        /* render linked orders header */ 
        const { ordersListIsCollapsed } = this.state,
              { orderLinkedOrders } = this.props;

        return (
            <div className='form-input__row'>
                <div className='form-input__block'>
                    <div className='fi__header-label'>Связанные ордеры</div>
                </div>
                { orderIsEdited ? 
                    <div className='form-input__block'>
                        <button type='button' 
                            className='btn main-block__btn'
                            id='id_order-attach-linked-order'
                            onClick={ this.onPickOrdersButtonClick.bind(this) }>
                            Привязать ордер
                        </button>
                    </div> : null
                }
                { (!orderIsEdited && orderLinkedOrders.size > 0) ? 

                    <div className='form-input__block'>
                    
                    <Dropdown as={ButtonGroup}>
                        <button type='button' 
                            className='btn main-block__btn'
                            id='id_order-show-linked-orders'
                            onClick={ this.onShowButtonClick.bind(this) }>
                            { ordersListIsCollapsed ? 'Показать ордеры' : 'Скрыть ордеры' }
                        </button>
                        
                        <Dropdown.Toggle split id='dropdown-split-basic' className='btn main-block__btn'
                                         variant={ null }/>

                        <Dropdown.Menu>
                            <Dropdown.Item href='#/action-1' 
                                           id='id_order-create-linked-order'
                                           onClick={ this.onCreateLinkedOrderClick.bind(this) }>Создать связанный ордер</Dropdown.Item>
                        
                        </Dropdown.Menu>
                    </Dropdown>
                    </div> : null 
                } 
                { (!orderIsEdited && orderLinkedOrders.size == 0) ? 
                    <div className='form-input__block'>
                        <button type='button' 
                            className='btn main-block__btn'
                                   id='id_order-create-linked-order'
                                   onClick={ this.onCreateLinkedOrderClick.bind(this) }>
                            Создать связанный ордер
                        </button>
                    </div> : null 
                }
            </div>
        );
    }

    renderLinkedOrderName(orderIsEdited, orderDate, orderNumber, orderIdStr) {
        if (orderIsEdited) {
            return (
                <div className='linked-order-elem'>
                    { orderNumber + ' от ' + getDateStr(orderDate) }
                </div>
            );
        }
        else {
            return (
                <div className='linked-order-elem'>
                <a href='#' 
                   onClick = { this.onOrderLinkClick.bind(this) }
                   data-order-id={ orderIdStr } >
                        { orderNumber + ' от ' + getDateStr(orderDate) }
                   </a>
                </div>
            )
        }
    }

    renderLinkedOrderInfo(orderInfo, orderIsEdited, linkedOrdersToAdd, linkedOrdersToRemove) {
        const orderId = orderInfo.get('orderId'),
              isAdded = (linkedOrdersToAdd.findIndex((value) => (value.get('orderId') == orderId)) != -1),
              isRemoved = (linkedOrdersToRemove.findIndex((value) => (value.get('orderId') == orderId)) != -1),
              src = isAdded 
                    ? 'static/images/filter-clear.png'
                    : (isRemoved ? 'static/images/filter-revert.png'
                                 : 'static/images/filter-clear.png'),
              alt = isAdded 
                    ? 'Cancel order attach'
                    : (isRemoved ? 'Revert order detach'
                                 : 'Detach order'),
              ariaLabel = isAdded 
                    ? 'Отменить привязку ордера'
                    : (isRemoved ? 'Вернуть привязку ордера'
                                 : 'Отвязать ордер'),
              orderClass = isAdded 
                    ? 'fi__label-order-info fi__label-order-info_added-order'
                    : (isRemoved ? 'fi__label-order-info fi__label-order-info_removed-order'
                                 : 'fi__label-order-info');

        if (orderId) {
            const orderTypeDescription = orderInfo.get('orderTypeDescription'),
                orderDate = orderInfo.get('orderDate'),
                orderNumber = orderInfo.get('orderNumber'),
                orderStatusDescription = orderInfo.get('orderStatusDescription'),
                orderSum = orderInfo.get('orderSum'),
                orderCurrency = orderInfo.get('orderAccountCurrencyCode'),
                orderClientName = orderInfo.get('orderClientName')

            return (
                <div className='form-input__row' key={ orderId }>
                    <div className='form-input__block'>
                        <div className={ orderClass }>
                            <div className='fi__label-order-info-row'>
                                <div className='linked-order-elem linked-order-elem_type'>
                                    { orderTypeDescription }
                                </div>
                                { this.renderLinkedOrderName(orderIsEdited, orderDate, orderNumber, orderId.toString()) }
                                <div className='linked-order-elem linked-order-elem_status'>
                                    { orderStatusDescription }
                                </div>
                            </div>
                            <div className='fi__label-order-info-row'>
                                <div className='linked-order-elem linked-order-elem_fininfo'>
                                    { floatPrettyPrint(orderSum) + ' ' + 
                                    orderCurrency + ', ' +  orderClientName }
                                </div>
                            </div>
                        </div>
                    </div>
                    { orderIsEdited ? 
                        <div className='form-input__block form-input__block_small'>
                            <div className='img-wrapper' 
                                 aria-label={ ariaLabel }
                                 data-action= { isAdded ? 'cancelAdd' : (isRemoved ? 'revertAttach' : 'detach') }
                                 data-order-id={ orderId.toString() }
                                 onClick = { this.onLinkedOrderDetachBtnClick.bind(this) } >
                                <img className='img-wrapper__img' 
                                        src={ src }
                                        alt={ alt }/> 
                            </div>                                 
                        </div> : null } 
                </div>
            );
        }
        else return null;
    }

    filterPickerOrdersData(pickerOrdersData) {
        const { orderLinkedOrders, linkedOrdersToAdd, 
                linkedOrdersToRemove, orderId } = this.props;
        let excludedOrders = Map();
        orderLinkedOrders.forEach(orderInfo => {
            excludedOrders = excludedOrders.set(orderInfo.get('orderId'), true);
        });
        linkedOrdersToAdd.forEach(orderInfo => {
            excludedOrders = excludedOrders.set(orderInfo.get('orderId'), true);
        });
        linkedOrdersToRemove.forEach(orderInfo => {
            excludedOrders = excludedOrders.set(orderInfo.get('orderId'), true);
        });

        excludedOrders = excludedOrders.set(orderId, true)
        return pickerOrdersData.filter(orderInfo => (!excludedOrders.has(orderInfo.get('orderId'))));

    }

    renderOrdersPicker(orderIsEdited) {
        if (orderIsEdited) {
            // render contents if area is not collapsed 
            const { ordersPickerIsCollapsed, filterValues } = this.state,
                  { linkedOrdersPickerData } = this.props,
                  filteredOrdersList = this.filterPickerOrdersData(linkedOrdersPickerData);

            
            return (
                <Picker 
                    id = 'id_order-pick-linked-order'
                    isExpanded = { !ordersPickerIsCollapsed }
                    listData = { filteredOrdersList }
                    listColumns = { linkedOrdersPickerListColumns }
                    filterFields = { linkedOrdersPickerFilterFields }
                    filterValues = { filterValues }
                    onFilterChanged = { this.onFilterChanged.bind(this) }
                    onDataPick = { this.onDataPick.bind(this) }
                    onCancel = { this.onPickerCancel.bind(this) }
                    filterHeaderName = 'Выберите ордер для привязки'/>
         
            );
        }
        else return null;
    }

    renderContents(orderIsEdited) {
        const { orderLinkedOrders, linkedOrdersToAdd, linkedOrdersToRemove } = this.props,
              { ordersListIsCollapsed } = this.state;

        
        const ordersListComponent = orderLinkedOrders.map(orderInfo => 
                                                    this.renderLinkedOrderInfo(orderInfo, 
                                                        orderIsEdited,
                                                        linkedOrdersToAdd,
                                                        linkedOrdersToRemove));

        if (orderIsEdited) {
            const addedListComponent = linkedOrdersToAdd.map(orderInfo => this.renderLinkedOrderInfo(orderInfo, 
                                                                                                     orderIsEdited,
                                                                                                     linkedOrdersToAdd,
                                                                                                     linkedOrdersToRemove));
            if (ordersListComponent.size + addedListComponent.size > 0) {
                return <div>
                    { ordersListComponent }
                    { addedListComponent }
                </div>
            }
            else return <div className='form-input__row'>Нет связанных ордеров</div>;
        }
        else {
            if (orderLinkedOrders.size > 0) {
                return <Collapse in ={ !ordersListIsCollapsed } >
                    <div>
                        { ordersListComponent }
                    </div>
                    </Collapse>
            }
            else return <div className='form-input__row'>Нет связанных ордеров</div>;
        }
    }

    render() {
        const { orderIsEdited } = this.props;
        return (
            <div className='form-input-order__linked-orders'>
                { this.renderHeader(orderIsEdited) }
                { this.renderOrdersPicker(orderIsEdited) }
                { this.renderContents(orderIsEdited) }
            </div>
        )    
    }
}

OrderEditLinkedOrders.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired,
    orderLinkedOrders: PropTypes.instanceOf(List).isRequired,
    linkedOrdersPickerData: PropTypes.instanceOf(List).isRequired,
    linkedOrdersToRemove: PropTypes.instanceOf(List).isRequired,
    linkedOrdersToAdd:  PropTypes.instanceOf(List).isRequired
}

const mapStateToProps = function(state) {
    const windowName = 'orderEdit',
          rootState = state.get('root').data,
          ordersState = state.get('orders').data,
          orderData = ordersState.get('orderData'),
          orderBodyData = orderData.get('orderBodyData'),
          orderLinkedOrders = orderData.get('orderLinkedOrders'),
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
              
    return {
        orderLinkedOrders: orderLinkedOrders, 
        linkedOrdersPickerData: windowContents.get('linkedOrdersPickerData'),
        linkedOrdersToRemove: windowContents.get('linkedOrdersToRemove'),
        linkedOrdersToAdd: windowContents.get('linkedOrdersToAdd'),
        orderData: orderData,
        orderType: orderBodyData.get('orderType'),
        orderTypeDescription: orderBodyData.get('orderTypeDescription')
    };
}

export default connect(mapStateToProps)(OrderEditLinkedOrders);
