import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Map, List } from 'immutable';
import { Dropdown, Collapse, ButtonGroup } from 'react-bootstrap';
import { floatPrettyPrint, getDateStr, getCurrentDate } from '../core/core_functions';
import { operationTypesById, operationStatuses, transferOperationType } from '../constants/operations_constants';
import { fillEmptyTransferData, fillEmptySettlementData, fillEmptySupplyData } from '../core/operations_functions';
import { buyOrderType, supplyOrderType } from '../constants/order_constants';


import { connect } from 'react-redux';



/* Order linked operations  */ 

class OrderEditLinkedOperations extends Component {
    constructor(props) {
        super(props);
        this.state ={
            operationsListIsCollapsed: true
        }
    }

    onShowButtonClick(e) {
        e.preventDefault();
        const { orderLinkedOperations } = this.props;
        if (orderLinkedOperations.size > 0)
            this.setState({operationsListIsCollapsed: !this.state.operationsListIsCollapsed});
    }

    onOperationLinkClick(e) {
        e.preventDefault();
        const { dispatch } = this.props,
                element = e.currentTarget,
                clickedOperationId = + element.getAttribute('data-operation-id');

        if (clickedOperationId) {
            dispatch ({
                type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'operationEdit',
                    context: Map({
                        windowContent: Map({
                            operationId: clickedOperationId,
                            operationIsEdited: false
                        })
                    })    
                }
            });
        }
    } 

    fillTransferData(orderData, transferData, isBuyerOrder) {
        /* function for filling settlementData with valid data 
           for Buyer order find supplier order (must be one
               if not one, don't fill supplier part) 
               
           for Supplier order find buyer order (must be one
               if not one, don't fill ), 
        
        */
        const orderBodyData = orderData.get('orderBodyData'),
              orderLinkedOrders = orderData.get('orderLinkedOrders');

        const buyOrdersList = orderLinkedOrders.filter(value => {
                  return value.get('orderType') == buyOrderType
              }),
              supplyOrdersList = orderLinkedOrders.filter(value => {
                  return value.get('orderType') == supplyOrderType
              });
        let buyerPart = Map({}),
            supplierPart = Map({});

        if (isBuyerOrder) {
            buyerPart = Map({
                buyerOrderId: orderData.get('orderDataId'),
                buyerOrderInfo: Map({
                    orderNumber: orderBodyData.get('orderNumber'),
                    orderDate: orderBodyData.get('orderDate'),
                }),
                buyerClientId: orderBodyData.get('orderClientId'),
                buyerClientName: orderBodyData.get('orderClientName'),
                buyerAccountId: orderBodyData.get('orderAccountId'),
                buyerAccountNumber: orderBodyData.get('orderAccountNumber'),
            });

            if (supplyOrdersList.size == 1) {
                const supplyOrderInfo = supplyOrdersList.get(0);
                supplierPart = Map({
                    supplierOrderId: supplyOrderInfo.get('orderId'),
                    supplierOrderInfo: Map({
                        orderNumber: supplyOrderInfo.get('orderNumber'),
                        orderDate: supplyOrderInfo.get('orderDate'),
                    }),
                    supplierClientId: supplyOrderInfo.get('orderClientId'),
                    supplierClientName: supplyOrderInfo.get('orderClientName'),
                    supplierAccountId: supplyOrderInfo.get('orderAccountId'),
                    supplierAccountNumber: supplyOrderInfo.get('orderAccountNumber')
                });

            }
        }
        else {
            if (buyOrdersList.size == 1) {
                const buyOrderInfo = buyOrdersList.get(0);
                buyerPart = Map({
                    buyerOrderId: buyOrderInfo.get('orderId'),
                    buyerOrderInfo: Map({
                        orderNumber: buyOrderInfo.get('orderNumber'),
                        orderDate: buyOrderInfo.get('orderDate'),
                    }),
                    buyerClientId: buyOrderInfo.get('orderClientId'),
                    buyerClientName: buyOrderInfo.get('orderClientName'),
                    buyerAccountId: buyOrderInfo.get('orderAccountId'),
                    buyerAccountNumber: buyOrderInfo.get('orderAccountNumber')
                });
                supplierPart = Map({
                    supplierOrderId: orderData.get('orderDataId'),
                    supplierOrderInfo: Map({
                        orderNumber: orderBodyData.get('orderNumber'),
                        orderDate: orderBodyData.get('orderDate'),
                    }),
                    supplierClientId: orderBodyData.get('orderClientId'),
                    supplierClientName: orderBodyData.get('orderClientName'),
                    supplierAccountId: orderBodyData.get('orderAccountId'),
                    supplierAccountNumber: orderBodyData.get('orderAccountNumber'),
                });

            }
        }
        
        return transferData.merge({
                              operationCurrencyId: orderBodyData.get('orderAccountCurrencyId'),
                              operationCurrencyCode: orderBodyData.get('orderAccountCurrencyCode')
                         }).merge(buyerPart)
                           .merge(supplierPart);

    }

    fillSupplyData(orderData, supplyData, isBuyerOrder) {
        /* fill data if isBuyerOrder == false */
        const orderBodyData = orderData.get('orderBodyData');
        
        if (isBuyerOrder) return supplyData
        else {
            return supplyData.merge(
                Map({
                    supplierOrderId: orderData.get('orderDataId'),
                    supplierOrderInfo: Map({
                        orderClientId: orderBodyData.get('orderClientId'),
                        orderNumber: orderBodyData.get('orderNumber'),
                        orderDate: orderBodyData.get('orderDate'),
                    }),
                    supplierClientId: orderBodyData.get('orderClientId'),
                    supplierClientName: orderBodyData.get('orderClientName'),
                    supplierAccountId: orderBodyData.get('orderAccountId'),
                    supplierAccountNumber: orderBodyData.get('orderAccountNumber'),
                    operationCurrencyId: orderBodyData.get('orderAccountCurrencyId'),
                    operationCurrencyCode: orderBodyData.get('orderAccountCurrencyCode')
                })
            );
        }
    }

    fillSettlementData(orderData, settlementData, isBuyerOrder) {
        /* fill data if isBuyerOrder == true */
        const orderBodyData = orderData.get('orderBodyData');
        
        if (isBuyerOrder) {
            return settlementData.merge(
                Map({
                    buyerOrderId: orderData.get('orderDataId'),
                    buyerOrderInfo: Map({
                        orderClientId: orderBodyData.get('orderClientId'),
                        orderNumber: orderBodyData.get('orderNumber'),
                        orderDate: orderBodyData.get('orderDate'),
                    }),
                    buyerClientId: orderBodyData.get('orderClientId'),
                    buyerClientName: orderBodyData.get('orderClientName'),
                    buyerAccountId: orderBodyData.get('orderAccountId'),
                    buyerAccountNumber: orderBodyData.get('orderAccountNumber'),
                    buyerCurrencyId: orderBodyData.get('orderAccountCurrencyId'),
                    buyerCurrencyCode: orderBodyData.get('orderAccountCurrencyCode')
                })
            );
        }
        
        else {
            return settlementData
        }
    }

    onCreateLinkedOperBtnClick(e) {
        e.preventDefault();

        // get order type 
        const { orderData, dispatch } = this.props,
              orderBodyData = orderData.get('orderBodyData'),
              orderType = orderBodyData.get('orderType'),
              isBuyerOrder = (orderType == buyOrderType);
              
        let transferData = this.fillTransferData(orderData, 
                                            fillEmptyTransferData(), 
                                            isBuyerOrder),
            supplyData = this.fillSupplyData(orderData, 
                                             fillEmptySupplyData(),
                                             isBuyerOrder),
            settlementData = this.fillSettlementData(orderData,
                                                     fillEmptySettlementData(),
                                                     isBuyerOrder);

        const operationType = transferOperationType;
        
        const operationData = Map({
            operationDataId: null,
            operationType: operationType,
            operationTypeDescription: operationTypesById.get(operationType),
            operationDate: getCurrentDate(),
            operationSum: orderBodyData.get('orderSum'),
            transferData: transferData,
            supplyData: supplyData,
            settlementData: settlementData
        }),
        payload = {
                window: 'operationEdit',
                context: Map({
                    windowContent: Map({
                        operationId: null,
                        operationIsEdited: true
                    })
                }),    
                operationData: operationData
            } 

        dispatch( {
            type: 'APP_CONTEXT_SET', 
            payload: payload
        });        
    }

    
    renderOperation(operationInfo) {
        const operationId = operationInfo.get('operationId'),
              operationTypeDescription = operationInfo.get('operationTypeDescription'),
              operationNumber = operationInfo.get('operationNumber'),
              operationDate = operationInfo.get('operationDate'),
              operationStatus = operationInfo.get('operationStatusDescription'),
              operationStatusId = operationInfo.get('operationStatusId'),
              operationSum = operationInfo.get('operationSum'),
              operationCurrencyCode = operationInfo.get('operationCurrencyCode'),
              clientName = operationInfo.get('clientName'),
              operationInfoView = operationNumber + ' от ' + getDateStr(operationDate),
              operationFinInfo = floatPrettyPrint(operationSum) + ' ' + 
                                 operationCurrencyCode + 
                                 (clientName ? ', ' + clientName : '');

        const statusClass = 'linked-oper-elem linked-oper-elem_status' + 
                            ((operationStatusId == operationStatuses.inactiveOperation.statusId) 
                              ? ' linked-oper-elem_status__formed' : '');
        
        return (
            <div className='form-input__block' key={ operationId }>
                <div className='fi__label-oper-info'>
                    <div className='fi__label-oper-info-row'>
                        <div className='linked-oper-elem linked-oper-elem_type'>{ operationTypeDescription }</div>
                        <div className='linked-oper-elem'>
                            <a href='#'
                               onClick = { this.onOperationLinkClick.bind(this) }
                               data-operation-id={ operationId.toString() } >{ operationInfoView }</a>
                        </div>
                        <div className={ statusClass }>{ operationStatus }</div>
                    </div>
                    <div className='fi__label-oper-info-row'>
                        <div className='linked-oper-elem linked-oper-elem_fininfo'>{ operationFinInfo }</div>
                    </div>
                </div>
            </div>
        );
    }

    renderLinkedOperations(orderLinkedOperations, operationsListIsCollapsed) {
        if (orderLinkedOperations.size > 0) {
            return (
                <Collapse in ={ !operationsListIsCollapsed }>
                    <div>
                        { orderLinkedOperations.map(value => this.renderOperation(value))}
                    </div>
                </Collapse>
            )
        }
        else return <div className='form-input__row'>Нет связанных операций</div>
        
    }

    render() {
        const { orderIsEdited, orderLinkedOperations } = this.props,
              { operationsListIsCollapsed } = this.state;
        
        if (!orderIsEdited) {
            return (
                <div className='form-input__linked-operations'>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi__header-label'>Связанные операции</div>
                        </div>
                        <div className='form-input__block'>
                            { orderLinkedOperations.size > 0 ? (
                            <Dropdown as={ButtonGroup}>
                                <button type='button' 
                                        className='btn main-block__btn'
                                        id='id_order-show-linked-operations'
                                        onClick={ this.onShowButtonClick.bind(this) }>                                        
                                        { operationsListIsCollapsed ? 'Показать операции' : 'Скрыть операции' }
                                </button>
                        
                                <Dropdown.Toggle split id='dropdown-split-basic_operations' 
                                                    className='btn main-block__btn'
                                                    variant={ null }    
                                                    />

                                <Dropdown.Menu>
                                    <Dropdown.Item href='#/action-1' 
                                                   id='id_order-create-linked-operation'
                                                   onClick={ this.onCreateLinkedOperBtnClick.bind(this) }>Создать связанную операцию</Dropdown.Item>
                            
                                </Dropdown.Menu>
                            </Dropdown> ) : (
                                <button type='button' 
                                        className='btn main-block__btn'
                                        id='id_order-create-linked-operation'
                                        onClick={ this.onCreateLinkedOperBtnClick.bind(this) }>
                                        Создать связанную операцию
                                </button> ) }                        
                        </div>
                    </div>
                    { this.renderLinkedOperations(orderLinkedOperations, 
                                                  operationsListIsCollapsed) }
                </div>
            );
        }
        else return null;
    }
}

OrderEditLinkedOperations.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired,
    orderLinkedOperations: PropTypes.instanceOf(List),
    dispatch: PropTypes.func.isRequired
}

const mapStateToProps = function(state) {
    const ordersState = state.get('orders').data,
          orderData = ordersState.get('orderData'),
          orderLinkedOperations = orderData.get('orderLinkedOperations');
              
    return {
        orderData: orderData, 
        orderLinkedOperations: orderLinkedOperations        
    };
}

export default connect(mapStateToProps)(OrderEditLinkedOperations);