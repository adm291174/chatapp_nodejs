import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { List, Map } from 'immutable';

class ClientEditUserRecord extends Component {

    onDetachUserClick() {
        const { clientId, userId, handleDetachUser } = this.props;
        handleDetachUser(clientId, userId);
    }

    render() {
        const { userLogin, userName } = this.props;

        return (
            <div className='form-input__row client-user-info'>
                <div className='client-user-info__username'>{ userLogin }</div>
                <div className='client-user-info__userdesc'>{ userName }</div>
                <div className='client-user-info__btns'>
                    <button type='button' className='btn main-block__btn client-user-info__btn'
                            onClick={ this.onDetachUserClick.bind(this) }>Отключить от клиента</button>
                </div>
            </div>
        )
    }
}

ClientEditUserRecord.propTypes = {
    clientId: PropTypes.number.isRequired,
    userId: PropTypes.number.isRequired,
    userLogin: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    handleDetachUser: PropTypes.func.isRequired,
}

class ClientEditUsers extends Component {

    handleDetachUser(clientId, userId) {
        const { dispatch } = this.props;
        dispatch({
            type: 'CLIENT_ADDUSER_SAVE',
            payload: {
                clientId: + clientId,
                userId: + userId,
                enable: false,
                callContext: 'removeUser'
            }
        }) 
        
    }

    onAddUserBtnClick(e) {
        const { dispatch, clientId, clientName } = this.props;

        e.preventDefault();
        dispatch( {
            type: 'APP_CONTEXT_SET',
            payload: { 
                window: 'clientEditAddUser',
                context: Map({ 
                    windowContent: Map({
                        clientId: clientId,
                        clientName: clientName 
                    })
                }) 
            }
        });

    }

    renderUsersInfo() {
        const { clientUsers, clientId } = this.props;
        if (clientUsers.size > 0) {
            return clientUsers.map(userInfo => {
                const userId = userInfo.get('userId');
                const userLogin = userInfo.get('userLogin');
                const userName = userInfo.get('userName');
                
                return (
                    <ClientEditUserRecord clientId = { clientId }
                                          userId = { userId } 
                                          userLogin = { userLogin }
                                          userName = { userName }
                                          handleDetachUser = { this.handleDetachUser.bind(this) }
                                          key = { userId }
                />);
            });
        }
        else return (
            <div className='form-input__row client-user-info'>
                <p>Привязанные к клиенту пользователи отсутствуют</p>
            </div>
        )

    }


    render() {
        const { clientId } = this.props;

        if (clientId) {
            return (
                <form id='id_form-client-users'>
                    <div className='form-input form-input__users-info'>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi__header-label fi__header-label_alone'>Пользователи клиента</div>
                                <div className='form-input__element-group'>
                                    <button type='button' className='btn main-block__btn'
                                            onClick={ this.onAddUserBtnClick.bind(this) }>Добавить пользователя</button>
                                </div>
                            </div>
                        </div>
                        { this.renderUsersInfo() }
                    </div>
                </form>
            );
        }
        else return null;
    }
}

ClientEditUsers.propTypes = {
    clientId: PropTypes.number.isRequired,
    clientName: PropTypes.string.isRequired,
    clientUsers: PropTypes.instanceOf(List).isRequired,
    dispatch: PropTypes.func.isRequired
}

export { ClientEditUsers };