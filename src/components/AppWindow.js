import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { AppWindowHeader } from './AppWindowHeader';
import AppWindowFilter from './AppWindowFilter';
import { AppWindowContents } from './AppWindowContents';
import { connect } from 'react-redux';

class AppWindow extends Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate(prevProps) {
        const { windowName } = this.props;
        if (prevProps.windowName != windowName) {
            // Scroll to top on window context change 
            const titleElement = document.getElementById('id_app-window-title');
            if (titleElement) {
                window.scrollTo(0, 0);
            }
        }
    }
    
    render() {
        const { windowName, windowTitle, windowFilter, reportId, 
                dispatch, isManager, isClient } = this.props 
        return (
        <div className='main-block__appwindow'>
            <AppWindowHeader title={ windowTitle }/>
            <AppWindowFilter windowName={ windowName } 
                             filterContext={ windowFilter } 
                             dispatch={ dispatch }
                             isManager={ isManager }
                             isClient={ isClient }
                             reportId={ reportId } 
                            />
            <AppWindowContents windowName={windowName}/>
        </div>
        );
    }
}

AppWindow.propTypes = {
    windowName: PropTypes.string.isRequired  // pass windowName for proper component behavior 
}

function mapStateToProps(state, ownProps) {
    const windowName = ownProps.windowName,
          rootState =  state.get('root').data;

    const windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowTitle = windowContext.get('windowTitle'),
          isManager = rootState.get('isManager'),
          isClient = rootState.get('isClient');

    const reportId = (windowName == 'report') ? windowContext.get('windowContent').get('reportId') : null;

    return {
        windowTitle: windowTitle,
        windowFilter: windowContext.get('windowFilter'),
        reportId: reportId, 
        isManager: isManager,
        isClient: isClient
    }

}

export default connect(mapStateToProps)(AppWindow);