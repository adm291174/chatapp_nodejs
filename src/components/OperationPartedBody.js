import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import { Map, List } from 'immutable';
import { OperationPartedCommon } from '../elements/OperationPartedCommon';
import { OperationTransactionPart } from '../elements/OperationTransactionPart';

import { supplyOperationType, 
         supplyMoneyPart,
         supplyCommissionPart,
         settlementMoneyPart,
         settlementCommissionPart,
         commonPartedComponentsDefs,
         operationPartPickerIds,
         operationPartSumInputIds,
         operationPartDefs,
         transactionPartNames,
         PartPickerNames,
         operationPartPurposeIds } from '../constants/operations_constants';

import { validateMix } from '../core/validation_functions';
import { buyOrderType, supplyOrderType } from '../constants/order_constants';



class OperationPartedBody extends Component {

    constructor(props) {
        super(props);
        this.componentRefs = [ React.createRef(),   // Common part 
                               React.createRef(),   // First transaction part 
                               React.createRef()];  // Second transaction part 
        this.validationList = [ 
            {
                validationType: 'component',
                element: this.componentRefs[0]
            },
            {
                validationType: 'component',
                element: this.componentRefs[1]
            },
            {
                validationType: 'component',
                element: this.componentRefs[2]
            }
        ]
    } 

    validate(needToSetFocus) {
        const markValidationState = true;
        const validationState= this.internalValidate(markValidationState, needToSetFocus);
        return validationState;
    }    

    internalValidate(markValidationState = false, needToSetFocus = false) {
        return validateMix(this.validationList, needToSetFocus, markValidationState);
    }


    // values change routines 
    onDataPick({ elementId, keyFileldValue, selectedListValues }) {
        // fill new picked data 
        switch (elementId) {
            case 'id_fi-pf-supplier-account-picker': 
            case 'id_fi-pf-buyer-account-picker': {
                this.processMainAccountPick(elementId, keyFileldValue, selectedListValues);
                break;
            } 
            case operationPartPickerIds[0]:
            case operationPartPickerIds[1]:
            case operationPartPickerIds[2]:
            case operationPartPickerIds[3]: {
                this.processPartAccountPick(elementId, keyFileldValue, selectedListValues);
                break;
            } 
            case 'id_fi-pf-supplier-linked-order-picker': 
            case 'id_fi-pf-buyer-linked-order-picker': {
                this.processOrderPick(elementId, keyFileldValue, selectedListValues);
                break;
            }
        }
    }

    
    processMainAccountPick(elementId, keyFileldValue, selectedListValues) {
    
        const { dispatch, operationData } = this.props, 
              { isSupplyComponent } = this.getComponentParams(operationData),
                operationPart = isSupplyComponent ? 'supplyData' : 'settlementData';

        
        const accountNumber = selectedListValues.get('accountNumber'),
              clientId = selectedListValues.get('clientId'),
              clientName = selectedListValues.get('clientName'),
              // accountId = selectedListValues.get('accountNumber'),
              currencyId = selectedListValues.get('accountCurrencyId'),
              currencyCode = selectedListValues.get('accountCurrencyCode');

        // common part picker clicked, this changes could affect currency of transaction
        let accountIdDef = null, 
            accountNumberDef = null,
            clientIdDef = null,
            clientNameDef = null; 

        if (elementId == 'id_fi-pf-supplier-account-picker') {
            accountIdDef = 'supplierAccountId', 
            clientIdDef = 'supplierClientId', 
            accountNumberDef = 'supplierAccountNumber',
            clientNameDef = 'supplierClientName';
        }
        else {
            accountIdDef = 'buyerAccountId', 
            clientIdDef = 'buyerClientId', 
            accountNumberDef = 'buyerAccountNumber',
            clientNameDef = 'buyerClientName';
        }
            
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: operationPart, 
                values: [
                    {fieldName: accountIdDef, fieldValue: keyFileldValue },
                    {fieldName: accountNumberDef, fieldValue: accountNumber },
                    {fieldName: clientIdDef, fieldValue: clientId },
                    {fieldName: clientNameDef, fieldValue: clientName },
                    {fieldName: 'operationCurrencyId', fieldValue: currencyId },
                    {fieldName: 'operationCurrencyCode', fieldValue: currencyCode },
                ]
            }

        })
    }

    processPartAccountPick(elementId, keyFileldValue, selectedListValues) {
    
        const { dispatch, operationData } = this.props, 
              { isSupplyComponent, componentData } = this.getComponentParams(operationData),
                operationPart = isSupplyComponent ? 'supplyData' : 'settlementData';

    
        // transaction info part picker clicked
        const elementIndex = operationPartPickerIds.indexOf(elementId);
        
        const transactionPartName = transactionPartNames[elementIndex],
              data = componentData.get(transactionPartName),
              accountNumber = selectedListValues.get('accountNumber'),
              accountName = selectedListValues.get('accountName');

        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: operationPart, 
                    values: [
                        {fieldName: transactionPartName, 
                            fieldValue: data.set('operationPartAccountId', keyFileldValue)
                                            .set('operationPartAccountNumber', accountNumber)
                                             .set('operationPartAccountName', accountName)}
                    ]
                }
        });
    }

    processOrderPick(elementId, keyFileldValue, selectedListValues) {
        /* if keyFileldValue == null -> clear requisit 
           we use this option for clearing dependend parts of operation  
        */
        const { dispatch, operationData } = this.props, 
              { isSupplyComponent, commonDefs } = this.getComponentParams(operationData),
                operationPart = isSupplyComponent ? 'supplyData' : 'settlementData';

        // get fields definitions 
        const orderIdDef = commonDefs.linkedToOrder.elementValueId,
              orderInfoDef = commonDefs.linkedToOrder.orderInfoElement,
              orderInfoMap = Map({
                  orderNumber: selectedListValues.get('orderNumber'),
                  orderDate: selectedListValues.get('orderDate')
              });
        
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: operationPart,
                values: [
                    {fieldName: orderIdDef, fieldValue: keyFileldValue},
                    {fieldName: orderInfoDef, fieldValue: orderInfoMap}
                ]
            }

        })
    }

    getPickerData(elementId, clearData = false, elementExtraData = {}) {
        // account picker
        if (['id_fi-pf-supplier-account-picker', 
             'id_fi-pf-buyer-account-picker'].includes(elementId)) {
            this.processMainAccountGetPickerData(elementId, clearData, elementExtraData);
        }
        if (operationPartPickerIds.includes(elementId)) {
            this.processPartAccountGetPickerData(elementId, clearData, elementExtraData);
        }
        if (['id_fi-pf-supplier-linked-order-picker', 
             'id_fi-pf-buyer-linked-order-picker'].includes(elementId)) {
            this.processOrdersGetPickerData(elementId, clearData, elementExtraData);
        }
    }

    processMainAccountGetPickerData(elementId, clearData, elementExtraData) {
        const { dispatch, operationData } = this.props,
              { isSupplyComponent } = this.getComponentParams(operationData),
                pickerSection = isSupplyComponent ? 'supplyPickerData' : 'settlementPickerData',
                pickerName = isSupplyComponent ? 'supplyAccounts' : 'settlementAccounts';
        
        if (clearData) {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: pickerSection, 
                    values: [{
                            fieldName: pickerName,
                            fieldValue: List()}]                        
                }
            });
        }
        else {
            dispatch( {
                type: 'GET_OPERATION_PICKER_DATA_ACCS',
                payload: {
                    currencyId: null,
                    pickerName: pickerName,
                    accountType: elementExtraData.accountType
                }
            });
        }
    }

    processPartAccountGetPickerData(elementId, clearData, elementExtraData = {}) {
        const { dispatch, operationData } = this.props,
              { isSupplyComponent, componentData } = this.getComponentParams(operationData),
                pickerSection = isSupplyComponent ? 'supplyPickerData' : 'settlementPickerData';

        const elementIndex = operationPartPickerIds.indexOf(elementId),
              pickerName = PartPickerNames[elementIndex];
        
        const operationCurrencyId = componentData.get('operationCurrencyId');
        if (clearData) {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: pickerSection, 
                    values: [{
                            fieldName: pickerName,
                            fieldValue: List()}]                        
                }
            });
        }
        else {
            dispatch( {
                type: 'GET_OPERATION_PICKER_DATA_ACCS',
                payload: {
                    currencyId: operationCurrencyId,
                    pickerName: pickerName,
                    accountType: elementExtraData.accountType
                }
            });
        }
    }

    processOrdersGetPickerData(elementId, clearData, elementExtraData) {

        const { dispatch, operationData } = this.props,
              { isSupplyComponent } = this.getComponentParams(operationData),
                pickerSection = isSupplyComponent ? 'supplyPickerData' : 'settlementPickerData',
                pickerName = isSupplyComponent ? 'supplyLinkedOrdersData' : 'settlementLinkedOrdersData',
                orderType = isSupplyComponent ? supplyOrderType : buyOrderType;

        const { accountId, startDate, endDate, linkedOrderId } = elementExtraData;
        if (clearData) {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: pickerSection,
                    values: [{ fieldName: pickerName, fieldValue: List()}]                        
                }
            });
        }
        else {
            dispatch( {
                type: 'GET_OPERATION_PICKER_DATA_ORDERS',
                payload: {
                    accountId: accountId,
                    orderType: orderType,
                    startDate: startDate,
                    endDate: endDate,
                    linkedOrderId: linkedOrderId,
                    pickerName: pickerName
                }
            });
        }
    }

    processPickerOpen(isSupplyComponent, pickerElementId) {
        /* process picker open - close any leased picker elements */ 

        const commonDefs = isSupplyComponent ?  
                commonPartedComponentsDefs.supplyComponentPart : 
                commonPartedComponentsDefs.settlementComponentPart,
              transactionIndices = isSupplyComponent ? [0, 1] : [2, 3],
              commonPartComponent = this.componentRefs[0].current,
              firstTranComponentRef = this.componentRefs[1].current,
              secondTranComponentRef = this.componentRefs[2].current;


        const elements = [
            {
                componentRef: commonPartComponent,
                visibilityState: commonPartComponent.state.pickAccountElementCollapsed,
                elementId: commonDefs.accountPicker.pickerElementId
            },
            {
                componentRef: commonPartComponent,
                visibilityState: commonPartComponent.state.pickLinkedOrdersElementCollapsed,
                elementId: commonDefs.linkedToOrder.pickerElementId
            },
            {
                componentRef: firstTranComponentRef,
                visibilityState: firstTranComponentRef && 
                                 firstTranComponentRef.state.pickerCollapsed,
                elementId: operationPartPickerIds[transactionIndices[0]]
            },
            {
                componentRef: secondTranComponentRef,
                visibilityState: secondTranComponentRef &&
                                 secondTranComponentRef.state.pickerCollapsed,
                elementId: operationPartPickerIds[transactionIndices[1]]
            }
        ];

        elements.forEach( element => {
            if (element.componentRef && 
                       element.elementId != pickerElementId && 
                       !element.visibilityState) {
                element.componentRef.onPickerCancel(element.elementId);
            }
        });
    
    } 

    onClearBtnClick(e) {
        e.preventDefault();        
        const { dispatch } = this.props;
        const elementId = e.currentTarget.id;
        switch (elementId) {
            case 'id_fi-pf-supplier-linked-order-clear-btn': {
                // clear linked order info (for payer)
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'supplyData', 
                        values: [
                            { fieldName: 'supplierOrderId', fieldValue: null },
                            { fieldName: 'supplierOrderInfo', 
                              fieldValue: Map({
                                 orderNumber: '',
                                 orderDate: null
                              })}
                        ]                        
                    }
                });
                break;
            }
            case 'id_fi-pf-buyer-linked-order-clear-btn': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'settlementData', 
                        values: [
                            { fieldName: 'buyerOrderId', fieldValue: null },
                            { fieldName: 'buyerOrderInfo', 
                              fieldValue: Map({
                                 orderNumber: '',
                                 orderDate: null
                              })}
                        ]                        
                    }
                });
                break;
            }
        }
    }

    onSumChange(value, elementId) {
        const { dispatch, operationData } = this.props,
              { isSupplyComponent, componentData } = this.getComponentParams(operationData),
              operationPart = isSupplyComponent ? 'supplyData' : 'settlementData';

        const elementIdIndex = operationPartSumInputIds.indexOf(elementId);
        if (elementIdIndex > -1) {
            const transactionPartName = transactionPartNames[elementIdIndex];
        
            const data = componentData.get(transactionPartName);

                dispatch({
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                        payload: {
                            operationPart: operationPart, 
                            values: [
                                {fieldName: transactionPartName, 
                                 fieldValue: data.set('operationPartSum', value) }
                            ]
                        }
                });
            
            const changeMainPart = (elementIdIndex == 0 || elementIdIndex == 2);
                        
            if (changeMainPart)
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: null, 
                        values: [ { fieldName: 'operationSum', fieldValue: value} ]
                    }
                });
        }
    }

    onChange(e) {
        /* on "purpose" component change handler */ 
        const { dispatch, operationData } = this.props,
              { isSupplyComponent, componentData } = this.getComponentParams(operationData),
              operationPart = isSupplyComponent ? 'supplyData' : 'settlementData';

        const elementIdIndex = operationPartPurposeIds.indexOf(e.target.id);

        if (elementIdIndex > -1) {
            const transactionPartName = transactionPartNames[elementIdIndex];
        
            const data = componentData.get(transactionPartName);

                dispatch({
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                        payload: {
                            operationPart: operationPart, 
                            values: [
                                {fieldName: transactionPartName, 
                                 fieldValue: data.set('operationPartPurpose', e.target.value) }
                            ]
                        }
                });
        }

    }
    
    onLinkClick(e) {
        e.preventDefault();
        const { dispatch } = this.props,
            element = e.currentTarget,
            clickedOrderId = + element.getAttribute('data-order-id');

        if (clickedOrderId) {
            dispatch ({
                type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'orderEdit',
                    context: Map({
                        windowContent: Map({
                            orderId: clickedOrderId,
                            orderIsEdited: false
                        })
                    })
                }
            });
        }
    } 

    
    processAccountChange() {
        const { dispatch, operationData } = this.props;
        const { transactionPartData, 
                isSupplyComponent,
                transactionPartNamesList } = this.getComponentParams(operationData);
        
        if (isSupplyComponent) {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'supplyData',
                    values: [{ fieldName: 'supplierOrderId', fieldValue: null },
                             { fieldName: 'supplierOrderInfo', fieldValue: Map({
                                    orderNumber: null,
                                    orderDate: null
                                })
                             },
                             { fieldName: transactionPartNamesList[0], fieldValue: 
                                transactionPartData[0].set('operationPartAccountId', null)
                                                      .set('operationPartAccountNumber', '')
                                                      .set('operationPartAccountName', '')
                               
                             },
                             { fieldName: transactionPartNamesList[1], fieldValue: 
                                transactionPartData[1].set('operationPartAccountId', null)
                                                      .set('operationPartAccountNumber', '')
                                                      .set('operationPartAccountName', '')
                               
                             },
                    ]
                }
            });
        }
        else {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'settlementData',
                    values: [{ fieldName: 'buyerOrderId', fieldValue: null },
                             { fieldName: 'buyerOrderInfo', fieldValue: Map({
                                    orderNumber: null,
                                    orderDate: null
                                })
                             },
                             { fieldName: transactionPartNamesList[0], fieldValue: 
                                transactionPartData[0].set('operationPartAccountId', null)
                                                      .set('operationPartAccountNumber', '')
                                                      .set('operationPartAccountName', '')
                               
                             },
                             { fieldName: transactionPartNamesList[1], fieldValue: 
                                transactionPartData[1].set('operationPartAccountId', null)
                                                      .set('operationPartAccountNumber', '')
                                                      .set('operationPartAccountName', '')
                               
                             },
                    ]
                }
            });

        }
    }

    getComponentParams(operationData) {
        const operationType = operationData.get('operationType'),
              isSupplyComponent = (operationType == supplyOperationType),
              componentData = isSupplyComponent ? 
                              operationData.get('supplyData') :
                              operationData.get('settlementData'),
              commonDefs = isSupplyComponent ?  
                           commonPartedComponentsDefs.supplyComponentPart : 
                           commonPartedComponentsDefs.settlementComponentPart,
              transactionDefs = isSupplyComponent ? 
                     [operationPartDefs.get(supplyMoneyPart), 
                      operationPartDefs.get(supplyCommissionPart)] :
                     [operationPartDefs.get(settlementMoneyPart), 
                      operationPartDefs.get(settlementCommissionPart)],
              transactionPartData = isSupplyComponent ? 
                     [componentData.get('supplyMoneyPart'), 
                      componentData.get('supplyCommissionPart')] :
                     [componentData.get('settlementMoneyPart'), 
                      componentData.get('settlementCommissionPart')],
              transactionPartNamesList =  isSupplyComponent ? 
                     ['supplyMoneyPart', 'supplyCommissionPart'] :
                     ['settlementMoneyPart', 'settlementCommissionPart'] ;


        return { isSupplyComponent, 
                 componentData,
                 commonDefs, 
                 transactionDefs,
                 transactionPartData,
                 transactionPartNamesList }
    } 

    render() {
        const { operationId, 
                operationIsEdited, 
                operationData } = this.props;

        const { commonDefs, 
                componentData, 
                transactionPartData, 
                isSupplyComponent } = this.getComponentParams(operationData), 
                currencyCode = componentData.get('operationCurrencyCode'),
                onDataPick = this.onDataPick.bind(this),
                getPickerData = this.getPickerData.bind(this),
                onClearBtnClick = this.onClearBtnClick.bind(this),
                processPickerOpen = this.processPickerOpen.bind(this),
                onChange = this.onChange.bind(this),
                onSumChange = this.onSumChange.bind(this),
                processAccountChange = this.processAccountChange.bind(this),
                onLinkClick = this.onLinkClick.bind(this);

        const pickerSection = isSupplyComponent ? 'supplyPickerData' : 'settlementPickerData',
              pickerListsIndeces = isSupplyComponent ? [0, 1] : [2, 3],
              moneyPartPickerData = operationData
                                    .get(pickerSection)
                                    .get(PartPickerNames[pickerListsIndeces[0]]),
              commissionPartPickerData = operationData
                                    .get(pickerSection)
                                    .get(PartPickerNames[pickerListsIndeces[1]]),
              accountIdParamName = isSupplyComponent ? 'supplierAccountId' : 'buyerAccountId',
              accountId = componentData.get(accountIdParamName);

        return (
            <div>
                <OperationPartedCommon operationId={ operationId }
                                       operationIsEdited={ operationIsEdited }
                                       operationData={ operationData }
                                       onDataPick={ onDataPick } 
                                       getPickerData={ getPickerData } 
                                       onClearBtnClick={ onClearBtnClick } 
                                       processPickerOpen={ processPickerOpen } 
                                       defs={ commonDefs }
                                       ref={ this.componentRefs[0] }
                                       processAccountChange={ processAccountChange }
                                       onLinkClick={ onLinkClick }

                />
                { accountId ? 
                <OperationTransactionPart operationIsEdited={ operationIsEdited }
                                          transactionPartData={ transactionPartData[0] }
                                          onDataPick={ onDataPick } 
                                          getPickerData={ getPickerData } 
                                          processPickerOpen={ processPickerOpen }
                                          onSumChange={ onSumChange }
                                          onChange={ onChange }
                                          currencyCode={ currencyCode }
                                          ref={ this.componentRefs[1] }
                                          isSupplyComponent={ isSupplyComponent }
                                          accountPickerData={ moneyPartPickerData }
                /> : null }
                { accountId ? 
                <OperationTransactionPart operationIsEdited={ operationIsEdited }
                                          transactionPartData={ transactionPartData[1] }
                                          onDataPick={ onDataPick } 
                                          getPickerData={ getPickerData } 
                                          processPickerOpen={ processPickerOpen }
                                          onSumChange={ onSumChange }
                                          onChange={ onChange }
                                          currencyCode={ currencyCode }
                                          ref={ this.componentRefs[2] }
                                          isSupplyComponent={ isSupplyComponent }
                                          accountPickerData={ commissionPartPickerData }
                /> : null }
            </div>);
    }
}

OperationPartedBody.propTypes = {
    operationId: PropTypes.number,
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map).isRequired
}

function mapStateToProps(state) {
    const windowName = 'operationEdit',
          rootState = state.get('root').data,
          operationsState = state.get('operations').data,
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
          
    
    const operationId = windowContents.get('operationId'),
          operationIsEdited =  windowContents.get('operationIsEdited'),
          operationData = operationsState.get('operationData')

    return {
        operationId: operationId,
        operationIsEdited: operationIsEdited,
        operationData: operationData
    }
}

export default connect(mapStateToProps, null, null, {forwardRef : true})(OperationPartedBody);
