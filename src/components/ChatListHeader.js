import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { filterGroupNames } from '../constants/main_constants'



class ChatListHeader extends Component {
    constructor(props) {
        super(props);
    }

    btnGroupClickHandle(e) {
        const selectorById = { 
            'id_chat-group-span-1': 0,
            'id_chat-group-span-2': 1
        };    

        this.props.handleGroupChange(filterGroupNames[selectorById[e.target.id]]);
    }

    btnFilterClickHandle() {
        this.props.handleFilterStateChange(!this.props.filterEnabled);
    }

    filterInputChangeHandle(e) {
        this.props.handleFilterValueChange(e.target.value);
    }

    onChangeVoid() {
        return false;
    }

    render() {
        const { filterGroup, filterEnabled, filterText } = this.props;
        const groupByTimeChecked = (filterGroup === filterGroupNames[0]);
        

        return <div className='chat-list-header'>
                <input type='checkbox' className='chat-list__checkbox' 
                       id='id_filter-chat-toggle' 
                       checked={filterEnabled}
                       onChange={this.onChangeVoid}
                       />
                <input type='radio' className='chat-list__radio' 
                       name='chat-group-style' id='id_arrange-chat-active'
                       checked={groupByTimeChecked} 
                       onChange={this.onChangeVoid}
                       />
                <input type='radio' className='chat-list__radio'
                       name='chat-group-style' id='id_arrange-chat-group' 
                       checked={!groupByTimeChecked} 
                       onChange={this.onChangeVoid}
                       /> 
                <div className='chat-list-buttons'>
                    <label className='chat-list-buttons__label' htmlFor='id_arrange-chat-active'>
                        <span className='chat-list-button chat-list-button_left' 
                            id='id_chat-group-span-1' 
                            onClick={this.btnGroupClickHandle.bind(this)}>Активные
                            
                        </span>
                    </label>
                    <label className='chat-list-buttons__label' htmlFor='id_arrange-chat-group'>
                        <span className='chat-list-button chat-list-button_right' 
                            id='id_chat-group-span-2'
                            onClick={this.btnGroupClickHandle.bind(this)}>Группы
                            
                        </span>
                    </label>
                    <label className='chat-list-buttons__label' htmlFor='id_filter-chat-toggle'>
                        <span className='chat-list-button chat-list-button_filter' 
                            id='id_chat-filter-span'
                            onClick={this.btnFilterClickHandle.bind(this)}>Фильтр
                        </span>
                    </label>
                </div>
                
                <div className='chat-list-buttons chat-list-buttons__hidden'>
                    <input type='text' className='chat-list-buttons__input' 
                        id='id_filter-input' value={filterText} 
                        onChange={this.filterInputChangeHandle.bind(this)}
                        />
                </div>
            </div>
    }
}

ChatListHeader.propTypes = {
    filterGroup: PropTypes.string, 
    filterEnabled: PropTypes.bool,
    filterText: PropTypes.string,
    handleGroupChange: PropTypes.func.isRequired,
    handleFilterStateChange: PropTypes.func.isRequired,
    handleFilterValueChange: PropTypes.func.isRequired
} 

export { filterGroupNames, ChatListHeader }