import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { ClientListItem } from './ClientListItem';
import { List } from 'immutable';

class ClientList extends Component {
    constructor(props) {
        super(props);
    }

    renderClients() {
        const userIsManager = this.props.isManager;
        const clientsList = this.props.clients;
        const accounts = this.props.accounts;
        let result = []
        const { dispatch } = this.props;
        clientsList.forEach(client => {
            const clientId = client.get('clientId');
            const clientName = client.get('clientName');            
            const clientAccounts = accounts.has(clientId) ?  accounts.get(clientId) : List();

            result.push(
                <ClientListItem 
                    clientId={ clientId }
                    clientName={ clientName } 
                    accounts={ clientAccounts }
                    userIsManager={ userIsManager }
                    key={ clientId }
                    dispatch = { dispatch }
                />
            )
        })
        return result;
    } 

    render() {
        const clientsList = this.props.clients;
        if (clientsList.size>0)
        return (
            <div className='main-block__body'>
                { this.renderClients()}
            </div>
        )
        else return null;
    }
}


ClientList.propTypes = {
    windowName: PropTypes.string.isRequired  // pass windowName for proper component behavior 
}


function mapStateToProps(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext').get('windowContext').get(windowName);
    const clientState = state.get('clients').data;
    return {
        windowFilter: windowContext.get('windowFilter'),
        clients: clientState.get('clientsList'),
        accounts: clientState.get('accountsList'),
        userRoles: rootState.get('userRoles'),
        isManager: rootState.get('isManager'),
        isClient: rootState.get('isClient'),
    }
}

export default connect(mapStateToProps)(ClientList);