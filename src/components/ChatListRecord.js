import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { capitalizeFirstLetter,          
         messageTimeCategory,
         getTimeStr } from '../core/core_functions'

import { monthName } from '../constants/main_constants'


class ChatListRecord extends Component {
    constructor(props) {
        super(props);
        
    }

    renderUnreadMsgCount(msgCount) {
        if (msgCount) {
            return ( 
                <div className='chat-item__msg-count'>{msgCount.toString()}</div>
            )
        }
        else return null;
    }

    renderLastMessageTime(messageTime, timeCategoryId) {
        if (messageTime) {
            let timeDescription = messageTimeCategory.getTimeCategoryName(timeCategoryId);
            let firstRow = ''

            if (timeCategoryId < messageTimeCategory.thisWeek) {
                firstRow = capitalizeFirstLetter(timeDescription);
            }
            else {
                firstRow = messageTime.getDate().toString() + ' ' +
                           monthName[messageTime.getMonth()] + ' ' + 
                           messageTime.getFullYear().toString();

            }    
            /* let hours = this.props.lastMessageTimeDate.getHours();
            let minutes = this.props.lastMessageTimeDate.getMinutes();
            let secondRow = ((hours < 10) ? '0' + hours : hours) + ':' +
                            ((minutes < 10) ? '0' + minutes : minutes); */

            let secondRow = getTimeStr(this.props.lastMessageTimeDate);

            return (
                <div className='chat-item__chat-time'>
                    { firstRow }<br/>{ secondRow }
                </div>
            )
        }        
        else return null;
    }

    onChatRecordClick() {
        let { chatHistoryId, handleChatClick } = this.props;
        handleChatClick(chatHistoryId);
        return false;
    }

    render() {
        const { oCssGroup, oGroupName, oIcon, unreadMessagesCnt, lastMessageTimeDate,
                timeCategoryId, oUserName, elementId, 
                chatHistoryId, activeChatId, chatIsActive} = this.props;

        return (
            <li className={ 'chat-item' + 
                            (oCssGroup ? (' ' + oCssGroup) : '') + 
                            (chatIsActive && activeChatId == chatHistoryId ? ' chat-item_active': '') }  
                id={ elementId }
                onClick={this.onChatRecordClick.bind(this)}>
                <div className='chat-item__chat-icon' aria-label={oGroupName}>
                    <img className='chat-item__chat-image' 
                         src={ oIcon ? 
                               oIcon : '/static/images/icons8-contacts-50.png' }
                         width='25' 
                         alt='avatar' />
                </div>
                { this.renderUnreadMsgCount(unreadMessagesCnt) }
                { this.renderLastMessageTime(lastMessageTimeDate, timeCategoryId) }
                { oUserName}
            </li>
        )
    }
}

ChatListRecord.propTypes = {
    chatId: PropTypes.number.isRequired,
    chatHistoryId: PropTypes.number.isRequired, 
    lastMessageTime: PropTypes.string,
    multiChat: PropTypes.bool.isRequired,
    lastView: PropTypes.string,
    visibleToUser: PropTypes.bool.isRequired,
    visibleName: PropTypes.string.isRequired,
    oUserId: PropTypes.number.isRequired,
    oUserName: PropTypes.string.isRequired,
    oGroupId: PropTypes.number.isRequired,
    oGroupName: PropTypes.string.isRequired,
    oCssGroup: PropTypes.string,
    oIcon: PropTypes.string,
    unreadMessagesCnt: PropTypes.number.isRequired,
    lastMessageTimeDate: PropTypes.instanceOf(Date),
    lastViewDate: PropTypes.instanceOf(Date),
    timeCategoryId: PropTypes.number.isRequired,
    elementId: PropTypes.string.isRequired,
    handleChatClick: PropTypes.func.isRequired,
    activeChatId: PropTypes.number,
    chatIsActive: PropTypes.bool.isRequired
}

export default ChatListRecord;