import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map, List } from 'immutable';
import OperationsListItem from '../elements/OperationsListItem';
import OperationsListSmall from '../elements/OperationsListSmall';


class OperationsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filteredOperationsList: List(),
            filteredByClientsOperationsList: List() 
        }
    }

    componentDidMount() {
        const filterContents = this.props.windowFilter,
              filterValues = filterContents.get('filterValues'),
              filterNameValue = filterValues.get('operations-filter__name'),
              operationsList = this.props.operationsList;
        const filteredOperationsList = this.filterOperationsList(operationsList, 
                                                                 filterNameValue),
              filterOperationsByClientsList = this.filterOperationsByClients(filteredOperationsList);

        this.setState ({ 
            filteredOperationsList: filteredOperationsList, 
            filteredByClientsOperationsList:  filterOperationsByClientsList
        });     
    }

    componentDidUpdate(prevProps, prevState) {
        const prevOperationsList = prevProps.operationsList,
              prevFilterContents = prevProps.windowFilter,
              prevFilterValues = prevFilterContents.get('filterValues'),
              prevFilterNameValue = prevFilterValues.get('operations-filter__name');
        
        const operationsList = this.props.operationsList,
              filterContents = this.props.windowFilter,
              filterValues = filterContents.get('filterValues'),
              filterNameValue = filterValues.get('operations-filter__name');
        
        if (!operationsList.equals(prevOperationsList) || 
             prevFilterNameValue != filterNameValue) {
            const newFilteredOperationsList = this.filterOperationsList(operationsList, 
                                                                        filterNameValue);
            if (!newFilteredOperationsList.equals(prevState.filteredOperartionsList)) {
                const newClientsFilteredList = this.filterOperationsByClients(newFilteredOperationsList);
                this.setState( { filteredOperationsList: newFilteredOperationsList,
                                 filteredByClientsOperationsList: newClientsFilteredList} );
            }
        }    
    }

    filterOperationsList(operationsList, filterText) {
        /* 
           filter operationsList with text filter. 
           We process all client records - if any of them matched - this record is matched 
        */
        if (filterText.trim().length == 0) {
            return operationsList; // if no filter set ... return original operationsList
        }
        else {
            const lowerCaseFilterValue = filterText.toLowerCase().trim();
            return operationsList.filter(operationsItem => {
                const clientsList = operationsItem.get('clients'),
                      filteredList = clientsList.filter(clientRecord => {
                          const clientName = clientRecord.operationClientName.toLowerCase();
                          return clientName.includes(lowerCaseFilterValue);
                      })
                return (filteredList.size > 0);
            })
        }
    }

    filterOperationsByClients(operationsList) {
        if (operationsList.size == 0) {
            return List(); // if empty list - return empty result 
        }
        else {
            // group records by Client 
            let clients = Map();
            operationsList.forEach( operationItem => {
                const clientsList = operationItem.get('clients');
                
                clientsList.forEach(clientRecord => {
                    const clientId = clientRecord.operationClientId,
                          clientName = clientRecord.operationClientName;
                    if (!clients.has(clientId)) {
                        clients = clients.set(clientId, Map({
                                    clientId: clientId,
                                    clientName: clientName,
                                    clientOperations: List([operationItem])
                        }));
                    }
                    else {
                        const clientRecord = clients.get(clientId),
                              clientOperations = clientRecord.get('clientOperations');
                        
                        clients = clients.set(clientId, 
                                              clientRecord.set(
                                                  'clientOperations',
                                                  clientOperations.push(operationItem)
                                              ))
                    }
                    
                    return true;
                });

                return true;
            });

            // perform sorting here 
            let clientsList = clients.toList()
                                     .sortBy( value => { 
                                         return value.get('clientName').toLowerCase()
                                     });
            return clientsList;
        }
    }

    onOperationEditClick(e) {
        e.preventDefault();
        const element = e.target,
              operationId = + element.getAttribute('data-operation-id'),
              { dispatch } = this.props;
        
        if (operationId) {
            dispatch( {type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'operationEdit',
                    context: Map({
                        windowContent: Map({
                            operationId: operationId,
                            operationIsEdited: false
                        })
                    })    
                }
            });
        }
    }

    renderOperationsListWide(filteredOperationsList) {
        if (filteredOperationsList.size > 0) {
            return filteredOperationsList.map(operationItem => {
                const operationId = operationItem.get('operationId');
                return <OperationsListItem 
                    key={ operationId }
                    operationId={ operationId }
                    operationNumber={ operationItem.get('operationNumber') }
                    operationDate={ operationItem.get('operationDate') }
                    operationStatusDescription={ operationItem.get('operationStatusDescription') }
                    clients={ operationItem.get('clients') }
                    operationSum={ operationItem.get('operationSum') }
                    operationCurrencyCode={ operationItem.get('operationCurrencyCode') }
                    operationTypeDescription={ operationItem.get('operationTypeDescription') }
                    onOperationEditClick={ this.onOperationEditClick.bind(this) }
                    isSmall = { false }
                />
            });
        }
        else return null;
    }

    render() {
        const { filteredOperationsList, 
                filteredByClientsOperationsList } = this.state;

        if (filteredOperationsList.size > 0) {
            return (
            <div className='main-block__body'>
                <div className='operations-list operations-list_wide'>
                    <table>
                        <thead>
                            <tr>
                                <td>Номер</td>
                                <td>Дата</td>
                                <td>Тип операции</td>
                                <td>Статус</td>
                                <td>Наименование клиента</td>
                                <td>Сумма</td>
                            </tr>
                        </thead>
                        <tbody>
                            { this.renderOperationsListWide(filteredOperationsList) }
                        </tbody>
                    </table>
                </div>
                <div className='operations-list operations-list_small'>
                    <OperationsListSmall  
                        clientsList={ filteredByClientsOperationsList }
                        onOperationEditClick={ this.onOperationEditClick.bind(this) }
                    />
                </div>
            </div>);
        }
        else {
            return (
                <div className='main-block__body'>
                    <div className='operations-list'>
                        Нет данных для отображения 
                    </div>
                </div>
            )
        }        
    }
}

OperationsList.propTypes = {
    windowName: PropTypes.string.isRequired,
    windowFilter: PropTypes.instanceOf(Map).isRequired,
    operationsList: PropTypes.instanceOf(List).isRequired,
    userRoles: PropTypes.instanceOf(List).isRequired,
    isManager: PropTypes.bool.isRequired,
    isClient: PropTypes.bool.isRequired
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext')
                                   .get('windowContext')
                                   .get(windowName);
    const operationsState = state.get('operations').data;
    return {
        windowFilter: windowContext.get('windowFilter'),
        operationsList: operationsState.get('operationsList'),
        userRoles: rootState.get('userRoles'),
        isManager: rootState.get('isManager'),
        isClient: rootState.get('isClient'),
    }
}

export default connect(mapStateToProps)(OperationsList);

