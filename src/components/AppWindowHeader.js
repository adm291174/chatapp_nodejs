import React, { Component } from 'react'

class AppWindowHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { title } = this.props;
        return (
            <div className='main-block__header' id='id_app-window-title'>{ title }</div>
        );    
    }
}

export { AppWindowHeader };