import React, { Component, Suspense } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { Dropdown } from 'react-bootstrap';
import { getDateStrForQuery }  from '../core/core_functions';
import { ordersListStatusQueryParams } from '../constants/logic_constants';

import { buyOrderType, supplyOrderType, orderTypesById } from '../constants/order_constants';

import { getCurrentDate }  from '../core/core_functions';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom'));


class FilterOrdersList extends Component {

    onfilterButtonClick(e) {
        // Handler for other filter buttons click 
        e.preventDefault();
        const { dispatch } = this.props;
        const elementId = e.target.id;
        let orderType = null;
        switch (elementId) {
            case 'id_orders-filter-add-buy-order': {
                orderType = buyOrderType;
                break;
            }
            case 'id_orders-filter-add-supply-order': {
                orderType = supplyOrderType;
                break;
            }
        }
        // New order edit window 
        dispatch( {type: 'APP_CONTEXT_SET', 
            payload: {
                window: 'orderEdit',
                context: Map({
                    windowContent: Map({
                        orderId: null,
                        orderIsEdited: true
                    })
                }),    
                orderBodyData: Map({
                    orderType: orderType,
                    orderTypeDescription: orderTypesById.get(orderType),
                    orderDate: getCurrentDate()

                })
            }
        });
    }


    componentDidUpdate(prevProps) {
        const prevFilterValues = prevProps.filterContext.get('filterValues'), 
              currentFilterValues = this.props.filterContext.get('filterValues');

        if (!currentFilterValues.equals(prevFilterValues)) {
            // detect changes 
            const prevStartDate = prevFilterValues.get('orders-filter__start-date');
            const prevEndDate = prevFilterValues.get('orders-filter__end-date');
            const prevStatus = prevFilterValues.get('orders-filter__status');

            const currentStartDate = currentFilterValues.get('orders-filter__start-date');
            const currentEndDate = currentFilterValues.get('orders-filter__end-date');
            const currentStatus = currentFilterValues.get('orders-filter__status');

            if (prevStartDate != currentStartDate || prevEndDate !=currentEndDate ||
                currentStatus != prevStatus) {
                    const ordersStatusForQuery = ordersListStatusQueryParams[currentStatus];

                    // perform database query 
                    this.props.dispatch ( {
                        type: 'GET_ORDERS_LIST_DATA',
                        payload: {
                            'startDate': getDateStrForQuery(currentStartDate),
                            'endDate': getDateStrForQuery(currentEndDate),
                            'status': ordersStatusForQuery ? ordersStatusForQuery : []
                        }
                    })
                }
        }
    }


    render() {
        const { filterContext, onElementChange, 
                onFilterClearClick, onDatePickerChange } = this.props;

        const filterValues = filterContext.get('filterValues');

        const period = filterValues.get('orders-filter__period');
        const startDate = filterValues.get('orders-filter__start-date');
        const endDate = filterValues.get('orders-filter__end-date');
        const status = filterValues.get('orders-filter__status');
        const name = filterValues.get('orders-filter__name');

        const datePickerState = (period != 'custom_period');

        return (
            <form id='id_orders-filter'>
                <div className='main-block__filter'>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Выберите период</div>
                        <div className='mbf-block__element'>
                            <select className='form-control' 
                                    id='id_form-orders-filter-period'
                                    onChange={ onElementChange }
                                    name='orders-filter__period'
                                    value={ period }>

                                <option value='today'>Сегодня</option>
                                <option value='this_week'>Текущая неделя</option>
                                <option value='this_month'>Этот месяц</option>
                                <option value='custom_period'>Произвольный</option>
                            </select>
                        </div>
                    </div>
                    
                    <div className='mbf__double-block mbf__double-block__datepicker'>
                        <Suspense fallback={<RefreshIndicator/>}> <DatePickerCustom 
                                id='id_form-orders-filter-start-date'
                                name='orders-filter__start-date'
                                blockElementClass='mbf__block mbf__block__datepicker'
                                headerElementClass='mbf-block__header'
                                bodyElementGroup='mbf-block__element-group'
                                title='Дата начала'
                                nextElementId='id_form-orders-filter-end-date'
                                prevElementId='id_form-orders-filter-period'
                                value={ startDate  }
                                onChangeHandle={ onDatePickerChange.bind(this) }
                                disabled = { datePickerState }
                            />
                        </Suspense>
                        <Suspense fallback={<RefreshIndicator/>}> <DatePickerCustom 
                                id='id_form-orders-filter-end-date'
                                name='orders-filter__end-date'
                                title='Дата окончания'
                                blockElementClass='mbf__block mbf__block__datepicker'
                                headerElementClass='mbf-block__header'
                                bodyElementGroup='mbf-block__element-group'
                                nextElementId='id_form-orders-filter-status'
                                prevElementId='id_form-orders-filter-start-date'
                                value={ endDate  }
                                onChangeHandle={ onDatePickerChange.bind(this) }
                                disabled = { datePickerState }
                            />
                        </Suspense>
                    </div>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Статус</div>
                        <div className='mbf-block__element'>
                            <select className='form-control' 
                                    id='id_form-orders-filter-status'
                                    onChange={ onElementChange }
                                    name='orders-filter__status'
                                    value={ status }>
                                <option value='edit_status'>Формируется</option>
                                <option value='all_statuses'>Все статусы</option>
                            </select>
                        </div>
                    </div>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Фильтр по имени</div>
                        <div className='mbf-block__element-group'>
                            <input type='text' className='form-control' placeholder='' 
                                   id='id_form-orders-filter-name' 
                                   onChange={ onElementChange }
                                   name='orders-filter__name' 
                                   value={ name }
                                   />
                            <div className='img-wrapper' 
                                 aria-label='Очистить'
                                 htmlFor='id_form-orders-filter-name'
                                 onClick={ onFilterClearClick }>
                                <img src='static/images/filter-clear.png' alt='Filter clear' />
                            </div>
                        </div>
                    </div>
                    <div className='mbf__block mbf__block_align-end'>
                        <div className='mbf-block__element'>
                            <Dropdown>
                                <Dropdown.Toggle id='id_orders-filter-add-order'
                                          variant={ null }>
                                    Создать ордер
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item 
                                        id='id_orders-filter-add-buy-order'
                                        onClick={ this.onfilterButtonClick.bind(this) }>Покупка</Dropdown.Item>
                                    <Dropdown.Item 
                                        id='id_orders-filter-add-supply-order'
                                        onClick={ this.onfilterButtonClick.bind(this) }>Поставка</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div> 

                </div>                        
            </form>                
        );
    }
}

FilterOrdersList.propTypes = {
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired,
    onDatePickerChange: PropTypes.func.isRequired,
    getElementData: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
}

export { FilterOrdersList };