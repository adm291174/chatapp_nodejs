import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { getDateStr } from '../core/core_functions';
import { List } from 'immutable';


class ClientEditAccountRecord extends Component {

    renderDatesInfo(isOpenAccountInfo, dateOfAccount) {
        
        const divClass = isOpenAccountInfo ? 'client-account-info__opened' : 'client-account-info__closed';

        return (
            <div className={ divClass }>
                <p>{ dateOfAccount ? (isOpenAccountInfo ? 'Дата открытия' : 'Дата закрытия') : '' }</p>
                <p>{ dateOfAccount ? getDateStr(dateOfAccount) : '' }</p>                
            </div>
        );
    }

    render() {
        const { accountId, accountNumber, accountStatus, 
                dateOfOpen, dateOfClose, isBuyerAccount, isSupplierAccount } = this.props;
        
        const accountIsActive = (accountStatus == 'opened') ? true : false,
              accountState = accountIsActive ? 'Активен' : 'Не активен'

        const noAccountRoleSet = !(isBuyerAccount || isSupplierAccount);

        return (
            <div className='form-input__row client-account-info' 
                 id={ 'id_account-info-' + accountId }>
                <div className='client-account-info__account'>{ accountNumber }</div>
                <div className={ 'client-account-info__status ' + 
                                 ( accountIsActive 
                                 ? 'client-account-info__status_active' 
                                 : 'client-account-info__status_inactive') }>{ accountState }</div>
                <div className={ 'client-account-info__type' + (noAccountRoleSet ? ' client-account-info__type-hidden' : '')} >
                     { isBuyerAccount ? 'Покупатель' : (isSupplierAccount ? 'Поставщик' : '') }</div>
                <div className='client-account-info__dates'>
                    { this.renderDatesInfo(true, dateOfOpen) }
                    { this.renderDatesInfo(false, dateOfClose) }
                </div>
                <div className='client-account-info__btns'>
                </div>
            </div>
        );
    }
}

ClientEditAccountRecord.propTypes = {
    accountId: PropTypes.number.isRequired,
    clientId: PropTypes.number.isRequired,
    accountStatus: PropTypes.string.isRequired,
    dateOfOpen: PropTypes.instanceOf(Date),
    dateOfClose: PropTypes.instanceOf(Date),
    isBuyerAccount: PropTypes.bool.isRequired,
    isSupplierAccount: PropTypes.bool.isRequired
}


class ClientEditAccounts extends Component {

    
    renderAccounts(accountsList) {
        if (accountsList.size > 0) {
            return accountsList.map(accountItem => {
                const accountId = accountItem.get('accountId'),
                      clientId = accountItem.get('clientId'),
                      accountNumber = accountItem.get('accountNumber'),
                      accountStatus = accountItem.get('accountStatus'),
                      dateOfOpen = accountItem.get('dateOfOpen'),
                      dateOfClose = accountItem.get('dateOfClose'),
                      isBuyerAccount = accountItem.get('isBuyerAccount'),
                      isSupplierAccount = accountItem.get('isSupplierAccount')
                
                return <ClientEditAccountRecord 
                    accountId = { accountId }
                    clientId = { clientId }
                    accountNumber = { accountNumber }
                    accountStatus = { accountStatus }
                    dateOfOpen = { dateOfOpen }
                    dateOfClose = { dateOfClose }
                    isBuyerAccount = { isBuyerAccount }
                    isSupplierAccount = { isSupplierAccount }
                    key= { accountId }
                />
            })
        }
        else return (
            <div className='form-input__row client-account-info'>
                <p>Счета отсутствуют</p>
            </div>
        );

    }

    render() {
        const { clientId, clientAccounts } = this.props;
        if (clientId) {
            return (
            <form id='id_form-client-accounts'>
                <div className='form-input form-input__account-info'>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi__header-label fi__header-label_alone'>Счета клиента</div>
                        </div>
                    </div>
                { this.renderAccounts(clientAccounts) }
                </div>
            </form>
            );
        }
        else return null;
    }    
}

ClientEditAccounts.propTypes = {
    clientId: PropTypes.number, 
    clientAccounts: PropTypes.instanceOf(List)
}

export { ClientEditAccounts };