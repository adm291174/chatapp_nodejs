import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { floatPrettyPrint } from '../core/core_functions';
import { Map } from 'immutable';
import { getCurrentDate }  from '../core/core_functions';
import { buyOrderType, supplyOrderType, orderTypesById } from '../constants/order_constants';
import { transferOperationType, operationTypesById, supplyOperationType } from '../constants/operations_constants';
import { fillEmptyTransferData, 
         fillEmptySupplyData, 
         fillEmptySettlementData } from '../core/operations_functions';


class ClassListItemAccount extends Component {
    
    onBtnClick(e) {
        e.preventDefault();
        if (e.target.id.includes ('id_btn_statement-account')) {
            const { dispatch, clientId, clientName, accountId, accountNumber, accountName } = this.props;
            dispatch({
                type: 'APP_CONTEXT_SET',
                payload: {
                    window: 'statement',
                    context: Map({
                        windowContent: Map({
                            accountId: accountId,
                            clientId: clientId,
                            clientName: clientName,
                            accountNumber: accountNumber,
                            accountName: accountName,
                            statementData: Map(),
                            dataIsRetrieving: true
                        })
                    })
                }
            })
        }
        if (e.target.id.includes ('id_btn_orders-list-account')) {
            const { dispatch, clientName } = this.props;
            dispatch({
                type: 'FILTER_DATA_CHANGED',
                payload: {
                    windowName: 'ordersList',
                    filterElementName: 'orders-filter__name',
                    filterData: clientName
                }
            })
            dispatch({
                type: 'APP_CONTEXT_SET',
                payload: {
                    window: 'ordersList',
                    context: Map()
                }
            })
        }
        if (e.target.id.includes ('id_btn_operations-list-account')) {
            const { dispatch, clientName } = this.props;
            dispatch({
                type: 'FILTER_DATA_CHANGED',
                payload: {
                    windowName: 'operationsList',
                    filterElementName: 'operations-filter__name',
                    filterData: clientName
                }
            })
            dispatch({
                type: 'APP_CONTEXT_SET',
                payload: {
                    window: 'operationsList',
                    context: Map()
                }
            })
        }
        if (e.target.id.includes ('id_btn_new-order-account')) {
            const { dispatch, isBuyerAccount, 
                    clientId, clientName, accountId,
                    accountNumber, accountCurrency, accountCurrencyId } = this.props,
                  orderType = isBuyerAccount ? buyOrderType : supplyOrderType;

            dispatch( {type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'orderEdit',
                    context: Map({
                        windowContent: Map({
                            orderId: null,
                            orderIsEdited: true
                        })
                    }),    
                    orderBodyData: Map({
                        orderType: orderType,
                        orderTypeDescription: orderTypesById.get(orderType),
                        orderDate: getCurrentDate(),
                        orderAccountId: accountId,
                        orderAccountCurrencyId: accountCurrencyId,
                        orderAccountCurrencyCode: accountCurrency,
                        orderAccountNumber: accountNumber,
                        orderClientId: clientId,
                        orderClientName: clientName
                    })
                }
            });
        }
        if (e.target.id.includes ('id_btn_new-operation-account')) {
            const { dispatch, isBuyerAccount,
                    clientId, clientName, accountId,
                    accountNumber, accountCurrency, accountCurrencyId } = this.props,
                    operationType = isBuyerAccount ? transferOperationType : supplyOperationType,
                    
                   payload = {
                        window: 'operationEdit',
                        context: Map({
                            windowContent: Map({
                                operationId: null,
                                operationIsEdited: true
                            })
                        }),    
                        operationData: Map({
                            operationDataId: null,
                            operationType: operationType,
                            operationTypeDescription: operationTypesById.get(operationType),
                            operationDate: getCurrentDate(),
                            transferData: fillEmptyTransferData().merge(
                                {
                                    buyerClientId: isBuyerAccount ? clientId : null,
                                    buyerClientName: isBuyerAccount ? clientName : '',
                                    buyerAccountId: isBuyerAccount ? accountId : null,
                                    buyerAccountNumber: isBuyerAccount ? accountNumber : '',
                                    operationCurrencyId: accountCurrencyId,
                                    operationCurrencyCode: accountCurrency
                                }
                            ),
                            supplyData: fillEmptySupplyData().merge(
                                {
                                    supplierClientId: !isBuyerAccount ? clientId : null,
                                    supplierClientName: !isBuyerAccount ? clientName : '',
                                    supplierAccountId: !isBuyerAccount ? accountId : null,
                                    supplierAccountNumber: !isBuyerAccount ? accountNumber : '',
                                    operationCurrencyId: accountCurrencyId,
                                    operationCurrencyCode: accountCurrency
                                },
                                
                            ),
                            settlementData: fillEmptySettlementData().merge(
                                {
                                    buyerClientId: isBuyerAccount ? clientId : null,
                                    buyerClientName: isBuyerAccount ? clientName : '',
                                    buyerAccountId: isBuyerAccount ? accountId : null,
                                    buyerAccountNumber: isBuyerAccount ? accountNumber : '',
                                    operationCurrencyId: accountCurrencyId,
                                    operationCurrencyCode: accountCurrency

                                }
                            )

                        })
                };

            dispatch( {type: 'APP_CONTEXT_SET', 
                       payload: payload
            });            
        }
    }

    renderFinButtons(userIsManager) {
        const accountId = this.props.accountId;
        const buttonOrders = userIsManager ? 
           <button type='button' className='amount-secondary-button btn' 
                   id={ 'id_btn_orders-list-account-' + accountId.toString() } 
                   onClick={ this.onBtnClick.bind(this) }>Ордеры</button> : null;
        const buttonOperations = userIsManager ? 
           <button type='button' className='amount-secondary-button btn' 
                   id={ 'id_btn_operations-list-account-' + accountId.toString() } 
                   onClick={this.onBtnClick.bind(this) }>Операции</button> : null;
        return (
        <div className='fin__buttons'>
            { buttonOrders }
            { buttonOperations }
            <button type='button' className='amount-button btn'
                    id={ 'id_btn_statement-account-' + accountId.toString() } 
                    onClick={this.onBtnClick.bind(this) }>Выписка</button>
        </div>
        )
    }

    renderAccountBtns(userIsManager) {
        const { accountStatus, isBuyerAccount, isSupplierAccount } = this.props;
        if (userIsManager && accountStatus == 'opened' && (isBuyerAccount || isSupplierAccount)) {
            const accountId = this.props.accountId;        
            return (
                <div className='account-block__btns'>
                    <button type='button' className='btns__btn btn' 
                            id={ 'id_btn_new-order-account-' + accountId.toString() } 
                            onClick={ this.onBtnClick.bind(this) }>Новый ордер</button>
                    <button type='button' className='btns__btn btn' 
                            id={ 'id_btn_new-operation-account-' + accountId.toString() }
                            onClick={ this.onBtnClick.bind(this) }>Новая операция</button>
                </div>
            );
        }
        else return null;
    }

    render() {
        const { userIsManager, accountId, accountNumber, accountAmount, accountCurrency } = this.props;
        
        return (
            <div className='account-block' id={ 'id_account-' + accountId.toString() } >
                <div className='account-block__fin'>
                    <div className='fin__account'>{ accountNumber }</div>
                    <div className='fin__amount'>{ floatPrettyPrint(accountAmount) } { accountCurrency }</div>
                    { this.renderFinButtons(userIsManager) }                
                </div>
                { this.renderAccountBtns(userIsManager) }
            </div>
        );
    }
}

ClassListItemAccount.propTypes = {
    clientId: PropTypes.number,
    clientName: PropTypes.string,
    userIsManager: PropTypes.bool.isRequired,
    accountId: PropTypes.number.isRequired,
    accountNumber: PropTypes.string.isRequired,
    accountName: PropTypes.string.isRequired,
    accountAmount: PropTypes.number.isRequired,
    accountCurrencyId: PropTypes.number.isRequired,
    accountCurrency: PropTypes.string.isRequired,
    accountStatus: PropTypes.string.isRequired,
    isBuyerAccount: PropTypes.bool.isRequired,
    isSupplierAccount: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired   
}


export { ClassListItemAccount };