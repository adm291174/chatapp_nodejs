import React, { Component, Suspense } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import { Map, List } from 'immutable';
import { OperationTransferPart } from '../elements/OperationTransferPart';

import { transferPartComponentDefs } from '../constants/operations_constants';
import { buyOrderType, supplyOrderType } from '../constants/order_constants';

import { validateMix } from '../core/validation_functions';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom')); 

// import DatePickerCustom from '../elements/DatePickerCustom';


class OperationTransferBody extends Component {

    constructor(props) {
        super(props);
        this.componentRefs = [ React.createRef(), 
                               React.createRef()];
        this.operationPaymentDateRef = React.createRef();
        this.validationList = [ 
            {
                validationType: 'component',
                element: this.componentRefs[0]
            },
            {
                validationType: 'component',
                element: this.componentRefs[1]
            },
            {
                validationType: 'element',
                element: 'id_fi-operation-payment-number',
                rules: [{ name: 'noEmpty'}]
            },
            {
                validationType: 'datepicker',
                element: this.operationPaymentDateRef
            },
            {
                validationType: 'element',
                element: 'id_fi-operation-purpose',
                rules: [{ name: 'noEmpty'}]
            }
        ]
    } 

    validate(needToSetFocus) {
        const markValidationState = true;
        const validationState= this.internalValidate(markValidationState, needToSetFocus);
        return validationState;
    }    


    internalValidate(markValidationState = false, needToSetFocus = false) {
        return validateMix(this.validationList, needToSetFocus, markValidationState);
    }
   
    // values change routines 
    onDataPick({ elementId, keyFileldValue, selectedListValues }) {
        // fill new picked data 
        switch (elementId) {
            case 'id_fi-payer-account-picker':
            case 'id_fi-payee-account-picker': {
                this.processAccountPick(elementId, keyFileldValue, selectedListValues);
                break;
            } 
            case 'id_fi-payer-pay-req-picker':
            case 'id_fi-payee-pay-req-picker': {
                this.processPayReqPick(elementId, keyFileldValue, selectedListValues);
                break;
            } 
            case 'id_fi-payer-linked-order-picker':
            case 'id_fi-payee-linked-order-picker': {
                this.processOrderPick(elementId, keyFileldValue, selectedListValues);
                break;
            }
        }
    }

    processAccountPick(elementId, keyFileldValue, selectedListValues) {
        /* if keyFileldValue == null -> clear requisit 
           we use this option for clearing dependend parts of operation  
        */
        const { dispatch } = this.props;
        // get fields definitions 
        const transferPartDef = (elementId == 'id_fi-payer-account-picker') ?
                                transferPartComponentDefs.payerComponentPart
                                : transferPartComponentDefs.payeeComponentPart,
              accountIdDef = transferPartDef.accountPicker.elementValueId,
              accountNumberDef = transferPartDef.accountPicker.elementValue,
              clientIdDef = transferPartDef.accountClient.elementValueId,
              clientNameDef = transferPartDef.accountClient.elementValue;
        
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: 'transferData', 
                values: [
                    {fieldName: accountIdDef, fieldValue: keyFileldValue },
                    { fieldName: accountNumberDef, 
                      fieldValue: selectedListValues.get('accountNumber') },
                    {fieldName: clientIdDef, 
                      fieldValue: selectedListValues.get('clientId') },
                    {fieldName: clientNameDef, 
                      fieldValue: selectedListValues.get('clientName') },
                    {fieldName: 'operationCurrencyId', 
                     fieldValue: selectedListValues.get('accountCurrencyId') },
                    { fieldName: 'operationCurrencyCode', 
                      fieldValue: selectedListValues.get('accountCurrencyCode') }
                ]
            }
        })
    } 

    processPayReqPick(elementId, keyFileldValue, selectedListValues) {
        /* if keyFileldValue == null -> clear requisit 
           we use this option for clearing dependend parts of operation  
        */
        const { dispatch, operationData } = this.props;
        // get fields definitions 
        const transferPartDef = (elementId == 'id_fi-payer-pay-req-picker') ?
                                transferPartComponentDefs.payerComponentPart
                                : transferPartComponentDefs.payeeComponentPart,
              requisitIdDef = transferPartDef.paymentRequisit.elementValueId,
              requisitDef = transferPartDef.paymentRequisit.elementValue,
              clientIdDef = transferPartDef.accountClient.elementValueId;

        
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: 'transferData', 
                values: [
                    {fieldName: requisitIdDef, fieldValue: keyFileldValue},
                    {fieldName: requisitDef, 
                     fieldValue: Map({
                           payRequisitClientId: operationData.get('transferData').get(clientIdDef),
                           payReqTaxNumber: selectedListValues.get('orgTaxNumber'),
                           payReqOrgName: selectedListValues.get('orgName'),
                           payReqAccountNumber: selectedListValues.get('accountNumber'),
                           payReqBankName: selectedListValues.get('bankName')})
                    }
                ]
            }
        })
    }

    processOrderPick(elementId, keyFileldValue, selectedListValues) {
        /* if keyFileldValue == null -> clear requisit 
           we use this option for clearing dependend parts of operation  
        */
        const { dispatch } = this.props;
        // get fields definitions 
        const transferPartDef = (elementId == 'id_fi-payer-linked-order-picker') ?
                                transferPartComponentDefs.payerComponentPart
                                : transferPartComponentDefs.payeeComponentPart,
              orderIdDef = transferPartDef.linkedToOrder.elementValueId,
              orderInfoDef = transferPartDef.linkedToOrder.orderInfoElement,
              // orderNumberDef = transferPartDef.linkedToOrder.orderNumberValue,
              // orderDateDef = transferPartDef.linkedToOrder.orderDateValue,
              orderInfoMap = Map({
                  orderNumber: selectedListValues.get('orderNumber'),
                  orderDate: selectedListValues.get('orderDate')
              })
              ;

        
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: 'transferData', 
                values: [
                    {fieldName: orderIdDef, fieldValue: keyFileldValue},
                    {fieldName: orderInfoDef, fieldValue: orderInfoMap}
                ]
            }

        })
    }

    getPickerData(elementId, clearData = false, elementExtraData = {}) {
        const { dispatch, operationData } = this.props;

        switch(elementId) {
            // account picker
            case 'id_fi-payer-account-picker':
            case 'id_fi-payee-account-picker': {
                const isPayerPart = (elementId == 'id_fi-payer-account-picker');
                if (clearData) {
                    dispatch( {
                        type: 'OPERATION_EDIT_DATA_CHANGED',
                        payload: {
                            operationPart: 'transferPickerData', 
                            values: [{
                                    fieldName: isPayerPart
                                               ? 'transferPayerAccounts' : 'transferPayeeAccounts',
                                    fieldValue: List()}]                        
                        }
                    });
                }
                else {
                    const operationCurrencyId = operationData.get('transferData')
                                                             .get('operationCurrencyId');
                    dispatch( {
                        type: 'GET_OPERATION_PICKER_DATA_ACCS',
                        payload: {
                            currencyId: isPayerPart ? null : operationCurrencyId,
                            pickerName: isPayerPart
                                        ? 'transferPayerAccounts'
                                        : 'transferPayeeAccounts',
                            accountType: isPayerPart ? 'buyerAccount' : 'supplierAccount'
                        }
                    });
                }
                break;
            }
            // payment requisits picker 
            case 'id_fi-payer-pay-req-picker':
            case 'id_fi-payee-pay-req-picker': {
                const { clientId } = elementExtraData,
                       isPayerPart = (elementId == 'id_fi-payer-pay-req-picker');
                if (clearData) {
                    dispatch( {
                        type: 'OPERATION_EDIT_DATA_CHANGED',
                        payload: {
                            operationPart: 'transferPickerData',
                            values: [{
                                    fieldName: isPayerPart
                                               ? 'transferPayerPayReqs' : 'transferPayeePayReqs',
                                    fieldValue: List()}]                        
                        }
                    });
                }
                else {
                    dispatch( {
                        type: 'GET_OPERATION_PICKER_DATA_PAYREQS',
                        payload: {
                            clientId: clientId,
                            pickerName: isPayerPart
                                        ? 'transferPayerPayReqs'
                                        : 'transferPayeePayReqs'
                        }
                    });
                }
                break;
            }
            // linked orders picker 
            case 'id_fi-payer-linked-order-picker':
            case 'id_fi-payee-linked-order-picker': {
                const { accountId, startDate, endDate, linkedOrderId } = elementExtraData,
                       isPayerPart = (elementId == 'id_fi-payer-linked-order-picker');
                if (clearData) {
                    dispatch( {
                        type: 'OPERATION_EDIT_DATA_CHANGED',
                        payload: {
                            operationPart: 'transferPickerData',
                            values: [{
                                    fieldName: isPayerPart
                                               ? 'transferPayerOrders' : 'transferPayeeOrders',
                                    fieldValue: List()}]                        
                        }
                    });
                }
                else {
                    dispatch( {
                        type: 'GET_OPERATION_PICKER_DATA_ORDERS',
                        payload: {
                            accountId: accountId,
                            orderType: isPayerPart
                                        ? buyOrderType
                                        : supplyOrderType,
                            startDate: startDate,
                            endDate: endDate,
                            linkedOrderId: linkedOrderId,
                            pickerName: isPayerPart
                                        ? 'transferPayerOrders'
                                        : 'transferPayeeOrders'
                        }
                    });
                }
                break;
            }

        }
    }
    
    processPickerOpen(isPayerPart, pickerElementId) {

        const payerComponentDefs = transferPartComponentDefs.payerComponentPart,
              payeeComponentDefs = transferPartComponentDefs.payeeComponentPart,
              payerComponentRef = this.componentRefs[0].current,
              payeeComponentRef = this.componentRefs[1].current;

        const elements = [  
            {
                componentRef: payerComponentRef,
                visibilityState: payerComponentRef.state.pickLinkedOrdersElementCollapsed,
                elementId: payerComponentDefs.linkedToOrder.pickerElementId,
            },
            {
                componentRef: payeeComponentRef,
                visibilityState: payeeComponentRef && 
                                 payeeComponentRef.state.pickLinkedOrdersElementCollapsed,
                elementId: payeeComponentDefs.linkedToOrder.pickerElementId,
            },
            {
                componentRef: payerComponentRef,
                visibilityState: payerComponentRef.state.pickAccountElementCollapsed,
                elementId: payerComponentDefs.accountPicker.pickerElementId,
            },
            {
                componentRef: payeeComponentRef,
                visibilityState: payeeComponentRef && 
                                 payeeComponentRef.state.pickAccountElementCollapsed,
                elementId: payeeComponentDefs.accountPicker.pickerElementId,
            },
            {
                componentRef: payerComponentRef,
                visibilityState: payerComponentRef.state.pickPayReqsElementCollapsed,
                elementId: payerComponentDefs.paymentRequisit.pickerElementId,
            },
            {
                componentRef: payeeComponentRef,                
                visibilityState: payeeComponentRef && 
                                 payeeComponentRef.state.pickPayReqsElementCollapsed,
                elementId: payeeComponentDefs.paymentRequisit.pickerElementId,
            }];

        elements.forEach( element => {
            if (element.componentRef && 
                       element.elementId != pickerElementId && 
                       !element.visibilityState) {
                element.componentRef.onPickerCancel(element.elementId);
            }
        });
    
    } 

    processAccountChange(isPayerPart) {
        const { dispatch } = this.props;
        
        if (isPayerPart) {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'transferData',
                    values: [{ fieldName: 'buyerOrderId', fieldValue: null },
                             { fieldName: 'buyerOrderInfo', fieldValue: Map({
                                    orderClientId: null,
                                    orderNumber: null,
                                    orderDate: null
                                })
                             },
                             { fieldName: 'buyerPaymentRequisitId', fieldValue: null },
                             { fieldName: 'buyerPaymentRequisitInfo', fieldValue: Map({
                                    payRequisitClientId: null,
                                    payReqTaxNumber: '',
                                    payReqOrgName: '',
                                    payReqAccountNumber: '',
                                    payReqBankName: ''
                                }) 
                             },
                             { fieldName: 'supplierOrderId', fieldValue: null },
                             { fieldName: 'supplierClientId', fieldValue: null },
                             { fieldName: 'supplierClientName', fieldValue: '' },
                             { fieldName: 'supplierAccountId', fieldValue: null },
                             { fieldName: 'supplierAccountNumber', fieldValue: '' },
                             { fieldName: 'supplierOrderId', fieldValue: null },
                             { fieldName: 'supplierOrderInfo', fieldValue: Map({
                                    orderClientId: null,
                                    orderNumber: null,
                                    orderDate: null
                                })
                             },
                             { fieldName: 'supplierPaymentRequisitId', fieldValue: null },
                             { fieldName: 'supplierPaymentRequisitInfo', fieldValue: Map({
                                    payRequisitClientId: null,
                                    payReqTaxNumber: '',
                                    payReqOrgName: '',
                                    payReqAccountNumber: '',
                                    payReqBankName: ''
                                }) 
                             }
                    ]
                }
            });
        }
        else {
            dispatch( {
                type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: 'transferData',
                    values: [{ fieldName: 'supplierOrderId', fieldValue: null },
                             { fieldName: 'supplierOrderInfo', fieldValue: Map({
                                    orderClientId: null,
                                    orderNumber: null,
                                    orderDate: null
                                })
                             },
                             { fieldName: 'supplierPaymentRequisitId', fieldValue: null },
                             { fieldName: 'supplierPaymentRequisitInfo', fieldValue: Map({
                                    payRequisitClientId: null,
                                    payReqTaxNumber: '',
                                    payReqOrgName: '',
                                    payReqAccountNumber: '',
                                    payReqBankName: ''
                                }) 
                             }
                    ]
                }
            });
        }
    }

    onDatePickerChange(id, dateValue) {
        /* process date change */ 
        const { dispatch } = this.props; 
        dispatch( {
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: 'transferData',
                values: [{
                    fieldName: 'paymentDocumentDate',
                    fieldValue: dateValue
                }]
            }
        });
    }

    onChange(e) {
        const { dispatch } = this.props, 
              element = e.currentTarget, 
              elementId = element.id
              
        switch (elementId) {
            case 'id_fi-operation-payment-number': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'transferData',
                        values: [{
                            fieldName: 'paymentDocumentNumber',
                            fieldValue: element.value
                        }]
                    }
                });
                validateMix(this.validationList.slice(2, 3), false, true);
                break;                 
            }    
            case 'id_fi-operation-purpose': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'transferData',
                        values: [{
                            fieldName: 'paymentDocumentPurpose',
                            fieldValue: element.value
                        }]
                    }
                });
                validateMix(this.validationList.slice(4, 5), false, true);
                break;                 
            }    
        }
    }

    onSumChange(value, elementId) {
        const { dispatch } = this.props;

        switch (elementId) {
            case 'id_fi-transfer-operation-sum': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: null, 
                        values: [
                            {
                                fieldName: 'operationSum',
                                fieldValue: value
                            }
                        ]                        
                    }
                });
                break;                 
            }
        }
    }

    onClearBtnClick(e) {
        e.preventDefault();        
        const { dispatch } = this.props;
        const elementId = e.currentTarget.id;        
        switch (elementId) {
            case 'id_fi-payer-linked-order-clear-btn': {
                // clear linked order info (for payer)
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'transferData', 
                        values: [
                            { fieldName: 'buyerOrderId', fieldValue: null },
                            { fieldName: 'buyerOrderInfo', 
                              fieldValue: Map({
                                 orderNumber: '',
                                 orderDate: null })
                            }
                        ]                        
                    }
                });
                break;
            }
            case 'id_fi-payee-linked-order-clear-btn': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: 'transferData', 
                        values: [
                            { fieldName: 'supplierOrderId', fieldValue: null },
                            { fieldName: 'supplierOrderInfo', 
                              fieldValue: Map({
                                 orderNumber: '',
                                 orderDate: null })
                            }
                        ]                        
                    }
                });
                break;
            }
        }
    }

    onLinkClick(e) {
        e.preventDefault();
        const { dispatch } = this.props,
            element = e.currentTarget,
            clickedOrderId = + element.getAttribute('data-order-id');

        if (clickedOrderId) {
            dispatch ({
                type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'orderEdit',
                    context: Map({
                        windowContent: Map({
                            orderId: clickedOrderId,
                            orderIsEdited: false
                        })
                    })
                }
            });
        }
    } 

    componentWillUnmount() {
        const { dispatch } = this.props;
        dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: 'transferData', 
                values: [
                    {fieldName: 'buyerAccountId', fieldValue: null},
                    {fieldName: 'supplierAccountId', fieldValue: null}
                ]
            }
        })
    }

    render() {
        const { operationId, 
                operationIsEdited, 
                operationData } = this.props;

        const transferData = operationData.get('transferData'),
              buyerClientId = transferData.get('buyerClientId'),
              operationPaymentNumber = transferData.get('paymentDocumentNumber'),  
              operationPaymentDate = transferData.get('paymentDocumentDate'),
              operationPaymentPurpose = transferData.get('paymentDocumentPurpose'),
              onSumChange = this.onSumChange.bind(this),
              onClearBtnClick = this.onClearBtnClick.bind(this);

        return (
            <div>
                <OperationTransferPart operationId={ operationId }
                                       operationIsEdited={ operationIsEdited }
                                       operationData={ operationData }
                                       partHeader='Плательщик'
                                       partRole='payerPart'
                                       defs={ transferPartComponentDefs.payerComponentPart }
                                       ref={ this.componentRefs[0] }
                                       onSumChange={ onSumChange }
                                       onClearBtnClick={ onClearBtnClick }
                                       onDataPick={ this.onDataPick.bind(this) } 
                                       getPickerData={ this.getPickerData.bind(this) } 
                                       processPickerOpen={ this.processPickerOpen.bind(this) } 
                                       processAccountChange={ this.processAccountChange.bind(this) }
                                       onLinkClick={ this.onLinkClick.bind(this) }
                />
                { buyerClientId ? 
                <OperationTransferPart operationId={ operationId }
                                       operationIsEdited={ operationIsEdited }
                                       operationData={ operationData }
                                       partHeader='Получатель'
                                       partRole='supplierPart'
                                       defs={ transferPartComponentDefs.payeeComponentPart }
                                       ref={ this.componentRefs[1] }
                                       onSumChange={ onSumChange }
                                       onClearBtnClick={ onClearBtnClick }
                                       onDataPick={ this.onDataPick.bind(this) } 
                                       getPickerData={ this.getPickerData.bind(this) } 
                                       processPickerOpen={ this.processPickerOpen.bind(this) }
                                       processAccountChange={ this.processAccountChange.bind(this) }
                                       onLinkClick={ this.onLinkClick.bind(this) }
                /> : null } 
                { buyerClientId ? 
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Номер документа платежа</div>
                        <div className='form-input__element-group'>
                            <input className='form-control fi__element fi__input' 
                                id='id_fi-operation-payment-number'
                                value={ operationPaymentNumber }
                                onChange={ this.onChange.bind(this) }  
                                disabled={ !operationIsEdited }
                                />
                            <div className='fi-stub'></div>
                        </div>
                    </div>
                    <Suspense fallback={<RefreshIndicator/>}>
                    <DatePickerCustom 
                            nextElementId='id_fi-operation-purpose'
                            prevElementId='id_fi-operation-payment-number'
                            title='Дата платежа'
                            id='id_fi-operation-payment-date'
                            name='operation-payment-date'
                            value={ operationPaymentDate }
                            onChangeHandle={ this.onDatePickerChange.bind(this) }
                            blockElementClass='form-input__block'
                            headerElementClass='fi-block__header'
                            bodyElementGroup='form-input__element-group'
                            showValidClass={ operationIsEdited }
                            useStub={ false }
                            disabled={ !operationIsEdited }
                            forwardedRef={ this.operationPaymentDateRef }
                        />
                    </Suspense>
                </div> : null }       
                { buyerClientId ? 
                <div className='form-input__row'>
                    <div className='form-input__block form-input__block_wide'>
                        <div className='fi-block__header'>Назначение платежа</div>
                        <textarea className='fi-text_wide form-control'
                                  id='id_fi-operation-purpose'
                                  value={ operationPaymentPurpose }
                                  onChange={ this.onChange.bind(this) }
                                  disabled={ !operationIsEdited }></textarea>
                    </div>
                </div> : null }
            </div>)
    }
}   

OperationTransferBody.propTypes = {
    operationId: PropTypes.number,
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map).isRequired
}

function mapStateToProps(state) {
    const windowName = 'operationEdit',
          rootState = state.get('root').data,
          operationsState = state.get('operations').data,
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
          
    
    const operationId = windowContents.get('operationId'),
          operationIsEdited =  windowContents.get('operationIsEdited'),
          operationData = operationsState.get('operationData')

    return {
        operationId: operationId,
        operationIsEdited: operationIsEdited,
        operationData: operationData
    }
}

export default connect(mapStateToProps, null, null, {forwardRef : true})(OperationTransferBody);


