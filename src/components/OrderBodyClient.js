import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Map, List } from 'immutable';
import { connect } from 'react-redux';


import Picker from '../elements/picker/Picker';
import { orderPickerFilterFields, 
         orderPickerListColumns } from '../constants/picker_constants';

/* Order main information part: Client Info  */ 

class OrderBodyClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pickAccountElementCollapsed: true, 
            filterValues: Map({
                'id-order-picker-filter-name': '',
                'id-order-picker-client-type-ro': ''
            })
        }
    }

    validate() {
        /* dummy validation method validate
           could be called via ref link 
        */ 
        /* const accountElement = document.getElementById('id_fi-order-account-number');
        

        const clientAccountNumber = accountElement.innerHTML; 
        
        return (clientAccountNumber && 
                clientAccountNumber.length > 0 ); */
        const { accountId }  = this.props;
        return (accountId && accountId > 0);
    }

    setValidationState(validationState) {
        const accountElement = document.getElementById('id_fi-order-account-number');
        if (accountElement) {
            if (validationState) accountElement.classList.remove('fi__label_invalid');
            else accountElement.classList.add('fi__label_invalid');
        }
    }
    
    setFocus() {
        const accountPickerElement = document.getElementById('id_fi-order-account-picker');
        accountPickerElement && accountPickerElement.focus();        
    }

    onfilterButtonClick(e) {
        e.preventDefault();
        const pickAccountElementCollapsed = this.state.pickAccountElementCollapsed;
        this.setState({ pickAccountElementCollapsed: !pickAccountElementCollapsed});
    }

    getPickerData(orderType) {
        /* function for retrieving picker data */
        const { dispatch } = this.props;
        
        dispatch( {
            type: 'GET_ORDER_EDIT_PICKER_DATA_ACCS',
            payload: {
                orderType: orderType
            }
        });
    }

    onPickerCancel() {
        this.setState({ pickAccountElementCollapsed: true });
    } 

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState( { 
            filterValues: filterValues.set(id, value)
        });
    }

    onDataPick({ keyFileldValue, selectedListValues }) {
        // fill new picked data 
        const { dispatch } = this.props;
        const orderAccountId = keyFileldValue,
              orderAccountCurrencyId = selectedListValues.get('accountCurrencyId'),
              orderAccountCurrencyCode = selectedListValues.get('accountCurrencyCode'),
              orderAccountNumber = selectedListValues.get('accountNumber'),
              orderClientId = selectedListValues.get('clientId'),
              orderClientName = selectedListValues.get('clientName')

        dispatch  ( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderAccountId',
                fieldValue: orderAccountId
            }
        });

        dispatch  ( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderAccountCurrencyId',
                fieldValue: orderAccountCurrencyId
            }
        });

        dispatch  ( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderAccountCurrencyCode',
                fieldValue: orderAccountCurrencyCode
            }
        });

        dispatch  ( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderAccountNumber',
                fieldValue: orderAccountNumber
            }
        });

        dispatch  ( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderClientId',
                fieldValue: orderClientId
            }
        });
    
        dispatch  ( {
            type: 'ORDER_EDIT_DATA_CHANGED',
            payload: {
                fieldName: 'orderClientName',
                fieldValue: orderClientName
            }
        });
        this.setValidationState(true);
        this.onPickerCancel();
    }


    componentDidUpdate(prevProps, prevState) {
        const { orderTypeDescription } = this.props,
               needNewData = 
                  (prevState.pickAccountElementCollapsed != this.state.pickAccountElementCollapsed)
                  || (prevProps.orderTypeDescription != orderTypeDescription);

        
        if  (needNewData) {
            const filterValues = this.state.filterValues;
            this.setState( { 
                filterValues: filterValues.set('id-order-picker-client-type-ro', 
                                                orderTypeDescription) 
            });

            // data picker visible state has been changed 
            if (!this.state.pickAccountElementCollapsed) {
                // getPickerData 
                const { orderType } = this.props;                      
                this.getPickerData(orderType);
            }
            else {
                const { dispatch } = this.props;
                // clear data 
                dispatch( {
                    type: 'ORDER_EDIT_PICKER_DATA',
                    payload: {
                        responseData: {                        
                            accountPickerData: List()
                        }
                    }
                });
            }
        }
    }

    render() {
        const { orderIsEdited, accountsPickerData, clientAccount, clientName } = this.props;

        const { pickAccountElementCollapsed, filterValues } = this.state;

        if (orderIsEdited) {
            return(
                <div>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Наименование клиента</div>
                            <div className='fi__label fi__element'>
                                { clientName } 
                            </div>
                        </div>
                    </div>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Номер счета</div>
                            <div className='form-input__element-group'>
                                <div className='fi__label fi__element' 
                                        id='id_fi-order-account-number'>
                                        { clientAccount }
                                </div>
                                <div className='fi-stub fi__element_hidden'></div>
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        id='id_fi-order-account-picker'
                                        aria-label='Выбрать счет'
                                        onClick={ this.onfilterButtonClick.bind(this) } 
                                        data-toggle='collapse'
                                        data-target='#id_order-pick-account'
                                        aria-expanded= { !pickAccountElementCollapsed } >
                                    <img className='img-wrapper__img' 
                                            src='static/images/select-from-list.png' 
                                            alt='Select account'/>
                                </button>                                 
                            </div>
                        </div>
                    </div>
                    <Picker 
                        id = 'id_order-pick-account'
                        isExpanded = { !pickAccountElementCollapsed }
                        listData = { accountsPickerData }
                        onDataPick = { this.onDataPick.bind(this) }
                        listColumns = { orderPickerListColumns }
                        onCancel = { this.onPickerCancel.bind(this) }
                        filterFields = { orderPickerFilterFields }
                        filterValues = { filterValues }
                        onFilterChanged = { this.onFilterChanged.bind(this) }
                        filterHeaderName = 'Выберите счет клиента'/>
                </div>
            );
        }
        else {
            return(
                <div>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Наименование клиента</div>
                            <div className='fi__label fi__element'>
                                { clientName }
                            </div>
                        </div>
                    </div>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Номер счета</div>
                            <div className='form-input__element-group'>
                                <div className='fi__label fi__element' 
                                    id='id_fi-account-number_ro'>
                                    { clientAccount }
                                </div>
                                <div className='fi-stub'></div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
                    
}

OrderBodyClient.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired,
    filterData: PropTypes.instanceOf(List),
    accountsPickerData: PropTypes.instanceOf(List)
}

const mapStateToProps = function(state) {
    const windowName = 'orderEdit',
          rootState = state.get('root').data,
          ordersState = state.get('orders').data,
          orderData = ordersState.get('orderData'),
          orderBodyData = orderData.get('orderBodyData'),
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
              
    return {
        accountsPickerData: windowContents.get('accountsPickerData'),
        clientAccount: orderBodyData.get('orderAccountNumber'),
        clientName: orderBodyData.get('orderClientName'),
        accountId: orderBodyData.get('orderAccountId'),
        orderType: orderBodyData.get('orderType'),
        orderTypeDescription: orderBodyData.get('orderTypeDescription')
    };
}

export default connect(mapStateToProps, null, null, {forwardRef : true} )(OrderBodyClient);
