import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import OrderBodyOrderType from '../elements/OrderBodyOrderType';
import OrderBodyClient from './OrderBodyClient';
import OrderBodyCommission from './OrderBodyCommission';
import OrderBodySecondarySum from '../elements/OrderBodySecondarySum';
import OrderBodySum from '../elements/OrderBodySum';
import OrderEditDate from '../elements/OrderEditDate';
import OrderEditLinkedOrders from './OrderEditLinkedOrders';
import OrderEditLinkedOpeartions from './OrderEditLinkedOperations';

import { Map } from 'immutable';

/* Order main information part */ 

class OrderEditBody extends Component {
    constructor(props) {
        super(props);
        this.componentRefs = [];
        this.OrderEditDateRef = React.createRef();
        this.componentRefs.push(this.OrderEditDateRef);
        this.OrderBodyClientRef = React.createRef();
        this.componentRefs.push(this.OrderBodyClientRef);
        this.OrderBodySumRef = React.createRef();
        this.componentRefs.push(this.OrderBodySumRef);
        this.OrderBodyCommissionRef = React.createRef();
        this.componentRefs.push(this.OrderBodyCommissionRef);
    }  

    validate() {
        // validate component - call children validate method to determine form validity 
        const markValidationState = true;
        const validationState= this.internalValidate(markValidationState)[0];
        return validationState;
    }

    internalValidate(markValidationState = false) {
        let validationState = true,
            firstInvalidElement = undefined;

        this.componentRefs.forEach(elementRef => {
            const referredElement = elementRef.current.validate ? 
                                    elementRef.current : 
                                    null;
            if (referredElement) {
                const currentElementState = referredElement.validate.call(referredElement);
                if (!currentElementState) {
                    validationState = false;
                    firstInvalidElement = firstInvalidElement ? 
                                          firstInvalidElement : 
                                          referredElement;
                    markValidationState && firstInvalidElement.setFocus();
                }

                markValidationState && 
                   referredElement.setValidationState(currentElementState);
                
            }
        })
        /* console.log('ValidationState: ', validationState, 
                    'markValidationState:', markValidationState,
                    'firstInvalidElement: ', firstInvalidElement); */
        return [validationState, firstInvalidElement];
    }

    setFocus() {
        // set focus on invalid element
        const markValidationState = false,
              [validationState, firstInvalidElement] = 
                     this.internalValidate(markValidationState);
        if (!validationState) 
            firstInvalidElement && firstInvalidElement.setFocus();

    }

    render() {
        const { orderId, 
                orderIsEdited, 
                orderBodyData,
                onOrderDateChange,
                onChange,
                onSumChange,
                onValidation } = this.props;

        return (
            <div className='form-input form-input-order'>
                <div className='form-input__row'>
                    <OrderBodyOrderType 
                        orderId = { orderId } 
                        orderIsEdited = { orderIsEdited }   
                        orderBodyData = { orderBodyData }
                        onChange = { onChange }                        
                    />
                    <OrderEditDate 
                        orderId = { orderId } 
                        orderIsEdited = { orderIsEdited }
                        orderBodyData = { orderBodyData }
                        onDatePickerChange = { onOrderDateChange }
                        ref = { this.OrderEditDateRef }

                    />
                </div>
                <OrderBodyClient 
                    orderId = { orderId } 
                    orderIsEdited = { orderIsEdited }
                    ref = { this.OrderBodyClientRef }
                />
                <OrderBodySum 
                    orderId = { orderId } 
                    orderIsEdited = { orderIsEdited }
                    orderBodyData = { orderBodyData }
                    onSumChange = { onSumChange }
                    onValidation = { onValidation }
                    ref = { this.OrderBodySumRef }
                />
                <OrderBodyCommission 
                    orderId = { orderId } 
                    orderIsEdited = { orderIsEdited }
                    orderBodyData = { orderBodyData }
                    onChange = { onChange }
                    onSumChange = { onSumChange }
                    onValidation = { onValidation }
                    ref = { this.OrderBodyCommissionRef }
                />
                <OrderBodySecondarySum 
                    orderBodyData = { orderBodyData }
                />
                <OrderEditLinkedOrders 
                    orderId = { orderId } 
                    orderIsEdited = { orderIsEdited }
                />
                <OrderEditLinkedOpeartions 
                    orderId = { orderId } 
                    orderIsEdited = { orderIsEdited }
                />                
            </div>
        );        
    }
}

OrderEditBody.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired, 
    orderBodyData: PropTypes.instanceOf(Map),
    onOrderDateChange: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onSumChange: PropTypes.func.isRequired,
    onValidation: PropTypes.func.isRequired,
}

export default OrderEditBody;