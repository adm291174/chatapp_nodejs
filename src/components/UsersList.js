import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map, List } from 'immutable';


class UsersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filteredUsersList: List()
        }
    }

    getFilterValue(props) {
        const filterContents = props.windowFilter,
              filterValues = filterContents.get('filterValues');
        return filterValues.get('users-filter__name');        
    }

    componentDidMount() {
        const usersList = this.props.usersList,
              filterNameValue = this.getFilterValue(this.props);
        this.setState( { filteredUsersList: this.filterUsersList(usersList, filterNameValue) });
    }

    componentDidUpdate(prevProps, prevState) {
        const prevUsersList = prevProps.usersList,
              prevFilterNameValue = this.getFilterValue(prevProps);
        
        const usersList = this.props.usersList,
              filterNameValue = this.getFilterValue(this.props);
        
        if (!usersList.equals(prevUsersList) || 
             prevFilterNameValue != filterNameValue) {
             const newFilteredUsersList = this.filterUsersList(usersList, 
                                                               filterNameValue);
            if (!newFilteredUsersList.equals(prevState.filteredUsersList)) {
                this.setState( { filteredUsersList: newFilteredUsersList } );
            }
        }    
    }

    filterUsersList(usersList,
                    filterNameValue) {
        // we filter by login and userName separately, then merge results 
        if (filterNameValue.length > 0) {
            const filterNameValue_ = filterNameValue.toLowerCase();
            return usersList.filter(value => {
                const login = value.get('login'),
                      userName = value.get('userName');

                return (login.toLowerCase().includes(filterNameValue_) ||
                        userName.toLowerCase().includes(filterNameValue_));
            })
        }
        else return usersList;
    }

    onUserEditClick(e) {
        e.preventDefault();
        const element = e.target,
              userId = + element.getAttribute('data-operation-id'),
              { dispatch } = this.props;
        
        if (userId) {
            dispatch( {type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'userEdit',
                    context: Map({
                        windowContent: Map({
                            userId: userId
                        })
                    })
                }
            });
        }
    }

    renderUsersRecords(usersList) {
        if (usersList.size > 0) {
            return usersList.map(userRecord => {
                const userId = userRecord.get('userId'),
                      login = userRecord.get('login'),
                      userName = userRecord.get('userName'),
                      isActive = userRecord.get('isActive');

                const activeClassName = isActive ? '' : 'td__blocked',
                      activeValue = isActive ? 'Активный' : 'Доступ отключен';

                return (
                    <tr key={ userId }>
                        <td className='align-text-left'>
                            <a href='#' 
                               onClick={ this.onUserEditClick.bind(this) }
                               data-operation-id={ userId }>
                                  { login }
                            </a>
                        </td>
                        <td className='align-text-left'>{ userName }</td>
                        <td className={ activeClassName }>{ activeValue }</td>
                    </tr>
                );
            })
        }
        else return null;
    }

    render() {
        const usersList = this.state.filteredUsersList;
        if (usersList.size > 0) {
            return (
                <div className='main-block__body'>
                    <div className='users-list'>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Логин</td>
                                        <td>Наименование</td>
                                        <td>Статус</td>                                
                                    </tr>
                                </thead>
                                <tbody>
                                    { this.renderUsersRecords(usersList) }
                                </tbody>
                            </table>
                    </div>
                </div>
            )
        }
        else return (<div className='main-block__body'>Нет данных для отображения</div>);
    }
}

UsersList.propTypes = {
    windowName: PropTypes.string.isRequired,
    windowFilter: PropTypes.instanceOf(Map).isRequired,
    usersList: PropTypes.instanceOf(List).isRequired    
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext')
                                   .get('windowContext')
                                   .get(windowName);
    const windowContent = windowContext.get('windowContent');
    return {
        windowFilter: windowContext.get('windowFilter'),
        usersList: windowContent.get('usersListData')
    }
}

export default connect(mapStateToProps)(UsersList);




