import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map, List } from 'immutable';


class ClientEditAddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedUserId: null,
            rowClicks: List(),
            filteredList: List()
        }
    }

    onSubmit(e) {
        e.preventDefault();
        if (this.validateForm()) {
            const { clientId, dispatch } = this.props;
            const { selectedUserId } = this.state;

            dispatch({
                type: 'CLIENT_ADDUSER_SAVE',
                payload: {
                    clientId: + clientId,
                    userId: + selectedUserId,
                    enable: true,
                    callContext: 'addUser'
                }
            }) 
        }
    }

    onTableRowClick(e) {
        // process selection click here; 
        const element = e.currentTarget;
        const clickedUserId = element.getAttribute('data-user-id');
        if (clickedUserId) {
            this.setState( { rowClicks: this.state.rowClicks.push(Map({ userId: clickedUserId,
                                                                        clickTime: new Date() })) });
        }
    }
    
    getUsersListData() {
        /* refresh users list data */ 
        const { clientId, queryUserState, dispatch }  = this.props,
                onlyAvailable = (queryUserState == 'not-linked') ? true : false;
        dispatch( {
            type: 'GET_CLIENT_USERS_FORADD',
            payload: {
                clientId: clientId,
                onlyAvailable: onlyAvailable
            }
        }) 
    }

    onCancelBtnClick(e) {
        e.preventDefault();
        const { clientId, dispatch } = this.props;
        dispatch({
            type: 'APP_CONTEXT_SET', 
            payload: {
                window: 'clientEdit',
                context: Map({
                    windowContent: Map({
                        clientId: clientId
                    })
                })
            }
        });
    }

    validateForm() {
        return (this.state.selectedUserId > 0)
    }    

    componentDidMount() {
        this.getUsersListData();
    }

    componentDidUpdate(prevProps, prevState) {

        const usersList = this.props.usersList,
              filteredList = this.state.filteredList;

        // process users clicks         
        if (this.state.rowClicks.size > 0) {
            const processedItem = this.state.rowClicks.get(0);
            this.setState({ rowClicks: this.state.rowClicks.pop() });
            // now process selections 
            const selectedUserId = this.state.selectedUserId,
                  clickedId = processedItem.get('userId') ;
            
            if (filteredList.findIndex(userItem => ( userItem.get('userId') == clickedId)) != -1) {
                this.setState({ selectedUserId: (clickedId == selectedUserId) 
                                                 ? null
                                                 : clickedId });
            }
            else {
                this.setState({ selectedUserId: null });
            }
            
        }

        if (!filteredList.equals(prevState.filteredList)) {
            // clear selection and rowsHistory if selected item is not in filtered list 
            const selectedUserId = this.state.selectedUserId;
            if (filteredList.findIndex(userItem => {
                    return ( userItem.get('userId') == selectedUserId );
                }) == -1) {
                this.setState({ selectedUserId: null,
                                rowClicks: List() });
            }
        }
        const filterText = this.props.filterText;

        if ((!usersList.equals(prevProps.usersList)) || 
            (filterText != prevProps.filterText)) {
            // usersList changed - change filter list 
            const newFilteredList = this.performListFiltration(filterText, usersList);
            if (!newFilteredList.equals(filteredList)) {
                this.setState( { filteredList: newFilteredList} );
            }
        }

        const { queryUserState } = this.props;
        if (queryUserState != prevProps.queryUserState) {
            this.getUsersListData();
        }
    }

    performListFiltration(filterText, usersList) {
        const filterTextLower = filterText.toLowerCase();
        if (filterTextLower.length > 0) {
            return usersList.filter(value => {
                const userLogin = value.get('userLogin').toLowerCase(),
                      userName = value.get('userName').toLowerCase();
                    
                return (userLogin.indexOf(filterTextLower) != -1 ||
                        userName.indexOf(filterTextLower) != -1); 
            }) 
        }
        else return usersList;
    }

    renderUsersList() {
        const usersList = this.state.filteredList;
        const { selectedUserId } = this.state;

        const onTableRowClick = this.onTableRowClick.bind(this);

        return usersList.map(userListItem => {
            const userId = userListItem.get('userId'),
                  isSelected = selectedUserId == userId ? 
                               true : false,
                  userLogin = userListItem.get('userLogin'),
                  userName = userListItem.get('userName');

            return (
                <tr className={ isSelected ? 'tr-selected' : ''} key={ userId } 
                    data-user-id={ userId } 
                    onClick = { onTableRowClick } >
                    <td>{ userLogin }</td>
                    <td>{ userName }</td>
                </tr>
            )
        }) 
    } 


    render() {
        const { clientName } = this.props;
        const { selectedUserId } = this.state; 

        return(
            <div className='main-block__body'>
            <form id='id_form-client-adduser' onSubmit={ this.onSubmit.bind(this) }>
                <div className='form-input form-input-adduser'>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Наименование клиента</div>
                            <div className='form-input__element-group'>
                                <div className='fi__label fi__element' 
                                     id='id_fi-adduser-clientname'>
                                        { clientName }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='form-input__row'>
                        <table>
                            <thead>
                                <tr>
                                    <td>Логин</td>
                                    <td>Описание пользователя</td>
                                </tr>
                            </thead>
                            <tbody>
                            { this.renderUsersList() }
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className='main-block__buttons'>
                    <button type='submit' 
                            className='btn main-block__btn'
                            enabled = { (selectedUserId) ? 'true' : 'false' } >Сохранить</button>
                    <button type='button' 
                            className='btn main-block__btn main-block__btn-cancel'
                            onClick={ this.onCancelBtnClick.bind(this)}>Отмена</button>
                </div>
            </form>
        </div>
        );    
    }
}

ClientEditAddUser.propTypes = {
    clientId: PropTypes.number.isRequired,
    clientName: PropTypes.string.isRequired,
    usersList: PropTypes.instanceOf(List).isRequired,
    filterText: PropTypes.string.isRequired,
    queryUserState: PropTypes.string.isRequired
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const windowContext = state.get('root').data
                               .get('appContext')
                               .get('windowContext')
                               .get(windowName),
          windowFilter = windowContext.get('windowFilter'),
          filterValues = windowFilter.get('filterValues'),
          windowContent = windowContext.get('windowContent');
                    
    return {
        clientId: windowContent.get('clientId'),
        clientName: windowContent.get('clientName'),
        usersList: windowContent.get('usersList'),
        filterText: filterValues.get('clients-adduser-filter__name'),
        queryUserState: filterValues.get('clients-adduser-filter__query-user-state')
    }
}

export default connect(mapStateToProps)(ClientEditAddUser);