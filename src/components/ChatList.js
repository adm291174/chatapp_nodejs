import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import memoizeOne from 'memoize-one';
import { Map, List } from 'immutable';
import { connect } from 'react-redux';

import { filterGroupNames, ChatListHeader } from './ChatListHeader';
import ChatListItems from './ChatListItems';
import { getGroupedChatList, extractGroups } from '../core/functions_chatlist';
import { messageTimeCategory } from '../core/core_functions';
import { processUIData } from '../core/handler_functions';
import { chatListRefreshTimeout } from '../constants/main_constants';

import { bindActionCreators } from 'redux';
import { actionClickChatListBtn } from '../actions/HeaderActions';


class ChatList extends Component {
    
    constructor(props) {
        super(props);
        // set state of filter and chatList here 
        let state = Map({
            filterGroup: filterGroupNames[0], 
            filterEnabled: false,
            filterText: '',
            groupsMap: Map(),
            timeCategoriesMap: Map(),
            groupsList: List(),
            timeCategoriesList: List(),
            collapsedState: Map(),
            isScrolled: false,  // whether this list is fixed 
            timerId: undefined
        });
        
        const {dispatch} = props;
        this.boundActionCreators = bindActionCreators(actionClickChatListBtn, dispatch);

        if (this.props.chatListData && this.props.chatListData.size) {
            let result = this.calculateChatItems(this.props.chatListData);
            let collapsedState = this.fillCollapsedState(result.get('groupsList'),
                                                         result.get('timeCategoriesList'),
                                                         state.get('collapsedState'));
            state = state.merge(result).set('collapsedState', collapsedState);
        }

        this.state = { data: state };
    }

    refreshChatListData() {
        // regular update event for chat 
        const { hasChatAccess } = this.props;
        if (hasChatAccess) {
            this.props.dispatch({type: 'GET_CHATLIST_INFO_DATA',
                        payload: null})
        }
    }

    // handle routines 
    handleGroupChange(filterGroup) {
        this.setState((state) => ({ data: state.data.set('filterGroup', filterGroup) }));
    }

    handleFilterStateChange(filterState) {
        this.setState((state) => ({ data: state.data.set('filterEnabled', filterState) }));
    }

    handleFilterValueChange(value) {
        this.setState((state) => ({ data: state.data.set('filterText', value) }));
    }

    // Store collapsed state of changed Group item 
    handleCollapsedValueChange(key, value) {
        this.setState((state) => { 
            let collapsedState = state.data.get('collapsedState');
            return {data: state.data.set('collapsedState', collapsedState.set(key, value))}
        });
    }

    handleChatClick(chatHistoryId) {
        this.props.dispatch({type: 'APP_CONTEXT_SET',
                             payload: {window: 'chat',
                                       context: Map({chatHistoryId: chatHistoryId})
                           }
                    });
        if (this.props.isOverlap && this.props.isVisible) {
            this.boundActionCreators(Map({isVisible: !this.props.isVisible}));
        }
    }

    handleChatListRender() {
        processUIData(this.props.dispatch);
    }

    componentDidMount() {
        this.refreshChatListData();
        var timerId = setInterval(this.refreshChatListData.bind(this), chatListRefreshTimeout);
        this.setState ({ data: this.state.data.set('timerId', timerId) });
    }

    componentDidUpdate(prevProps) {

        if (!(this.props.chatListData.equals(prevProps.chatListData))) {
            // update calculated lists here 
            let result = this.calculateChatItems(this.props.chatListData);
            let collapsedState = this.fillCollapsedState(result.get('groupsList'),
                                                         result.get('timeCategoriesList'),
                                                         this.state.data.get('collapsedState'));
            result = result.set('collapsedState', collapsedState);
            // update state with new data
            this.setState( state => ({data: state.data.merge(result)}));
        }
        if (this.props.isOverlap != prevProps.isOverlap) {
            console.log( this.props.isOverlap ? 'Overalapped' : 'Not overlapped');
        }

        if (this.props.chatIsActive != prevProps.chatIsActive || 
           this.props.isVisible != prevProps.isVisible) {
            this.handleChatListRender();
        }    
    }

    componentWillUnmount() {
        const timerId = this.state.data.get('timerId');
        if (timerId) {
            clearInterval(timerId);
        }
    }

    calculateChatItems = memoizeOne ((chatListData) => {
        /* Calculate chat items for children components use */
        const groupsMap = getGroupedChatList(chatListData, 
                                            'oGroupId',
                                            (value) => (value.get('oGroupName')));
        const timeCategoriesMap = getGroupedChatList(
                                    chatListData, 
                                    'timeCategoryId',
                                    value => {
                                        let timeCategoryId = value.get('timeCategoryId');
                                        return messageTimeCategory.getTimeCategoryName(timeCategoryId);
                                    });

        const groupsList = extractGroups(groupsMap, 
                                                'oUserName', 
                                                true, // sort by Name
                                                'group', // key prefix value 
                                                false); // sorting of elements in group 
        const timeCategoriesList = extractGroups(timeCategoriesMap, 
                                                    'lastMessageTimeDate', 
                                                    false,  // don't sort by Name
                                                    'time-category', // key prefix value
                                                    true); // sorting of elements in group 
        
        return Map({groupsMap, timeCategoriesMap, groupsList, timeCategoriesList});
    });


    fillCollapsedState = memoizeOne ((groupsList, timeCategoriesList, collapsedState) => {
        var fillInitial = (processedList, value) => {
            let result = Map({});
            
            for (let item of processedList) {
                let key = item.get('key');
                let _value = value;                
                if (collapsedState.has(key)) {
                    _value = collapsedState.get(key);                    
                }
                result = result.set(key, _value);
            }
            return result;
        }
        
        let result = fillInitial(groupsList, true);
        result = result.merge(fillInitial(timeCategoriesList, false));
        return result;
    });
    

    render() {
        const { filterGroup, 
                filterEnabled, 
                filterText,
                groupsList,
                timeCategoriesList,
                collapsedState } = this.state.data.toObject();
        
        const { isVisible, hasChatAccess, chatIsActive, activeChatId } = this.props;
        
        if (hasChatAccess && groupsList.size || timeCategoriesList.size) {
            return (<div className={ 'chat-list-block' + 
                                     ( !isVisible ? ' chat-list-block-hidden' : '')
                                   }
                         id='id_chat-list-block'>
                        <div className='chat-list'>
                        <ChatListHeader filterGroup={filterGroup} filterEnabled={filterEnabled}
                                        filterText={filterText} 
                                        handleGroupChange={this.handleGroupChange.bind(this)} 
                                        handleFilterStateChange={this.handleFilterStateChange.bind(this)}
                                        handleFilterValueChange={this.handleFilterValueChange.bind(this)} />
                        <ChatListItems  filterGroup={filterGroup} 
                                        filterEnabled={filterEnabled}
                                        filterText={filterText} 
                                        groupsList={groupsList}
                                        timeCategoriesList={timeCategoriesList}
                                        collapsedState={collapsedState}
                                        handleGroupCollapsedState={this.handleCollapsedValueChange.bind(this)}
                                        handleChatClick={this.handleChatClick.bind(this)}
                                        chatIsActive={chatIsActive}
                                        activeChatId={activeChatId}
                                        /> 
                        </div>
                </div>)
        }                
        else {
            // supress empty chatList
            return (<div className='chat-list-block chat-list-block-hidden'></div>);
        }
    }
}

ChatList.propTypes = {
    chatListData: PropTypes.instanceOf(List),
    isVisible: PropTypes.bool,
    isOverlap: PropTypes.bool,
    hasChatAccess: PropTypes.bool
}

function mapStateToProps(state) {
    const chatList = state.get('chatList');
    const root = state.get('root');
    const hasChatAccess = root.data.get('hasChatAccess');
    const chatContext = root.data.get('appContext').get('chatContext'),
          chatIsActive = chatContext.get('isActive'),
          activeChatId = chatContext.get('chatHistoryId');

    return {
        chatListData: chatList.data.get('chatListData'),
        isVisible: chatList.data.get('isVisible'),
        isOverlap: chatList.data.get('isOverlap'),
        hasChatAccess: hasChatAccess,
        chatIsActive: chatIsActive,
        activeChatId: activeChatId
    }
}


export default (connect(mapStateToProps)(ChatList));