import React, { Component, Suspense } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { reportDefs } from '../report/report_defs';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom'));


class FilterReport extends Component {
    onBtnClick(e) {
        e.preventDefault();
        const { dispatch, reportId } = this.props,
              { paramsExtractFunc } = reportDefs(reportId),
              element = e.target;
        switch (element.id) {
            case 'id-report_get-report': 
            case 'id-report_export-excel': {
                const reportOutput = (element.id == 'id-report_get-report') ? 'json' : 'file',
                      filterValues = this.getFilterData().get('filterValues'),
                      reportParams = paramsExtractFunc && paramsExtractFunc(filterValues);

                dispatch({
                    type: 'GET_REPORTS_DATA',
                    payload: {
                        reportId: reportId,
                        reportParams: reportParams,
                        reportOutput: reportOutput
                    }
                })
                break;
            }
        }
    }

    renderField(fieldDef, filterValue) {
        const { onDatePickerChange } = this.props,
              { caption, id, type, disabled } = fieldDef;
        switch (type) {
            case 'DATEPICKER': {
                const { nextElementId, prevElementId, name } = fieldDef;
                return (
                    <Suspense key={ id } fallback={<RefreshIndicator/>}> <DatePickerCustom 
                            id={ id }                            
                            name={ name }
                            title={ caption }
                            blockElementClass='mbf__block mbf__block__datepicker'
                            headerElementClass='mbf-block__header'
                            bodyElementGroup='mbf-block__element-group'
                            nextElementId={ nextElementId }
                            prevElementId={ prevElementId }
                            value={ filterValue }
                            onChangeHandle={ onDatePickerChange }
                            disabled = { disabled }
                        />
                    </Suspense>
                ) 
            }
            default: {
                return null;
            }
        }
    }

    renderBlock(blockDef, filterValues) {
        const { id } = blockDef;
        return (<div className='mbf__double-block' id={ id } key={ id }> 
            { blockDef.fields.map(fieldDef => {
                const filterValue = filterValues.get(fieldDef.name)
                return this.renderField(fieldDef, filterValue)                
            }) }
        </div>);
    }

    renderFields(filterFields, filterData) {
        const filterValues = filterData.get('filterValues');
        return (
            filterFields.map(fieldDef => {
                if (fieldDef.type == 'BLOCK') {
                    return this.renderBlock(fieldDef, filterValues)
                }
                else {
                    const filterValue = filterData.get(fieldDef.name)
                    return this.renderField(fieldDef, filterValue)
                }
            }) 
        )        
    }

    renderButtons(filterButtons) {
        return (<div className='mbf__block mbf__block_align-end'>
                    <div className='mbf-block__element'>
            { filterButtons.map(buttonDef => {
                  const { caption, id } = buttonDef;
                  return <button type='button' 
                                 className='btn'
                                 id={ id }
                                 key={ id }
                                 onClick={ this.onBtnClick.bind(this) }>{ caption }</button>
                             
              }) }
            </div>
        </div>)
    }

    renderDef(reportId, filterData) {
        // render filter depends on definition function 
        const { filterFields, filterButtons } = reportDefs(reportId);
        if (filterData) {
            return (<div className='main-block__filter'>
                { this.renderFields(filterFields, filterData) }
                { this.renderButtons(filterButtons) }
            </div>);
        }
        else {
            const errorStr = 'Для данного отчета отсутствует описание фильтра (ID: ' + reportId + ')';
            return <div className='main-block__filter'>{ errorStr }</div>
        }
    }

    getFilterData() {
        const { reportId, filterContext } = this.props,
              filterData = filterContext.get('identifiedFilterValues').get(reportId);
        return filterData;
    }

    render() {
        const { reportId } = this.props,
              filterData = this.getFilterData();

        return this.renderDef(reportId, filterData);
    }
}

FilterReport.propTypes = {
    reportId: PropTypes.string.isRequired,
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired,
    onDatePickerChange: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
}

export { FilterReport }