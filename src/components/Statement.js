import React, { Component, Suspense } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map, List } from 'immutable';

import Picker from '../elements/picker/Picker';

import { statementPickerAccsColumns, accPickerFilterFields } from '../constants/picker_constants';
import { showLabelValidityState, validateMix } from '../core/validation_functions';
import { floatPrettyPrint, getDateStr } from '../core/core_functions';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom'));
// import DatePickerCustom from '../elements/DatePickerCustom';

class Statement extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pickerCollapsed: true,
            filterValues: Map({
                'id_fi-statement-account-picker-filter': ''
            })
        }
        this.datePickerStartDate = React.createRef()
        this.datePickerEndDate = React.createRef()

        this.validationList = [ 
            {
                validationType: 'function',
                functionLink: this.validateAccountPart.bind(this),
                element: 'account-picker'
            },
            {
                validationType: 'datepicker',
                element: this.datePickerStartDate
            },
            {
                validationType: 'datepicker',
                element: this.datePickerEndDate
            },
        ]
    } 

    validate(needToSetFocus) {
        /* validate account button and datepicker elements state */         

        const markValidationState = true;
        const validationState = this.internalValidate(markValidationState, needToSetFocus);
        return validationState;
    }
    
    internalValidate(markValidationState = false, needToSetFocus = false) {
        return validateMix(this.validationList, needToSetFocus, markValidationState);                
    }

    validateAccountPart(needToSetFocus, validationState) {
        /* validation function for validateMix processing */ 
        const { accountId } = this.props,
                buttonElement = document.getElementById('id_statement-acc-pick-btn'),
                labelElement = document.getElementById('id_fi-statement-account-number');
        
        let localValidationState = true;

        if (!accountId) {
            localValidationState = false;
            if (needToSetFocus && validationState) {
                buttonElement.focus();
            }
        }
        showLabelValidityState(labelElement, localValidationState, 'fi__label_invalid');
        return localValidationState
    }

    onDatePickerChange(id, dateValue) {
        const { statementStartDate, statementEndDate, windowName, dispatch } = this.props,
              startDateChanged = id == ('id_fi-statement-start-date');

        dispatch({
            type: 'WINDOW_DATA_CHANGED',
            payload: {
                windowName: windowName,
                fieldName: startDateChanged ? 'statementStartDate' : 'statementEndDate',
                fieldValue: dateValue
            }
        });

        if (startDateChanged && dateValue > statementEndDate) {
            dispatch({
                type: 'WINDOW_DATA_CHANGED',
                payload: {
                    windowName: windowName,
                    fieldName: 'statementEndDate',
                    fieldValue: dateValue
                }
            });
        }

        if (!startDateChanged && dateValue < statementStartDate) {
            dispatch({
                type: 'WINDOW_DATA_CHANGED',
                payload: {
                    windowName: windowName,
                    fieldName: 'statementStartDate',
                    fieldValue: dateValue
                }
            });
        }
    }

    onFilterChanged(id, value) {
        const filterValues = this.state.filterValues;
        this.setState( { 
            filterValues: filterValues.set(id, value)
        });
    }

    onPickerCancel() {
        this.setState({ pickerCollapsed: true });
    } 

    onDataPick({ keyFileldValue, selectedListValues }) {
        const { dispatch, windowName } = this.props;
        this.onPickerCancel();
        dispatch({
                type: 'WINDOW_DATA_MASS_CHANGED',
                payload: {
                    windowName: windowName,
                    windowContent: Map({
                        accountId: keyFileldValue,
                        accountNumber: selectedListValues.get('accountNumber'),
                        accountName: selectedListValues.get('accountName'),
                        clientId: selectedListValues.get('clientId'),
                        clientName: selectedListValues.get('clientName')
                    })
                }
            }); 
        const _this = this;
        setTimeout(() => { _this.validate(false) }, 500);
    }

    getData() {
        const { dispatch } = this.props;
        if (this.validate(true)) {
            const { accountId, statementStartDate, statementEndDate} = this.props;
            dispatch({
                type: 'GET_STATEMENT_DATA',
                payload: {
                    accountId: accountId,
                    startDate: statementStartDate,
                    endDate: statementEndDate
                }
            });
        }
    }

    onBtnClick(e) {
        e.preventDefault();
        const elementId = e.currentTarget.id,
              { dispatch } = this.props;
        if (elementId == 'id_fi-statement-cancel-btn') {
            dispatch({
                type: 'APP_CONTEXT_SET',
                payload: {
                    window: 'clients'
                }
            });
        }
        if (elementId == 'id_statement-acc-pick-btn') {
            this.setState({ pickerCollapsed: !this.state.pickerCollapsed });
        }
        if (elementId == 'id_fi-statement-submit-btn') {
            this.getData()
        }
    }

    getPickerData(clearData) {
        const { dispatch, windowName } = this.props;
        if (clearData) {
            dispatch({
                type: 'WINDOW_DATA_CHANGED',
                payload: {
                    windowName: windowName,
                    fieldName: 'accountPickerData',
                    fieldValue: List()
                }
            });
        }
        else {
            dispatch({
                type: 'GET_STATEMENT_PICKER_DATA_ACCS',
                payload: null
            });
        }
    }

    componentDidMount() {
        if (this.props.accountId) {
            this.getData()
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { pickerCollapsed } = this.state,
                prevPickCollapsed = prevState.pickerCollapsed;

        if (pickerCollapsed != prevPickCollapsed) {
            this.getPickerData(pickerCollapsed);
        }
        if (prevProps.accountId != this.props.accountId) {
            const { dispatch, windowName } = this.props;
            dispatch({
                type: 'WINDOW_DATA_CHANGED',
                payload: {
                    windowName: windowName,
                    fieldName: 'dataIsRetrieving',
                    fieldValue: true
                }
            });

        }

    }    

    renderHeader() {
        const {accountNumber,
               accountName, clientId, clientName,
               statementStartDate, statementEndDate,
               accountPickerData } = this.props, 
               visibleName = clientId ? clientName : accountName,
               onDatePickerChange = this.onDatePickerChange.bind(this),
               onFilterChanged = this.onFilterChanged.bind(this),
               onDataPick = this.onDataPick.bind(this), 
               onPickerCancel = this.onPickerCancel.bind(this),
               onBtnClick = this.onBtnClick.bind(this),
               { pickerCollapsed, filterValues } = this.state;

        return (
            <div className='form-input form-input-statement'>
                <form name='statement'>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi-block__header'>Номер счета</div>
                            <div className='form-input__element-group'>
                                <div className='fi__label fi__element' 
                                        id='id_fi-statement-account-number'>{ accountNumber }
                                </div>
                                <button className='btn img-wrapper img-wrapper__btn' 
                                        aria-label='Выбрать счет'
                                        id='id_statement-acc-pick-btn'
                                        onClick={ onBtnClick }>
                                    <img className='img-wrapper__img' 
                                            src='static/images/select-from-list.png' 
                                            alt='Select account'/>
                                </button>                            
                            </div>
                        </div>
                        <div className='form-input__block-group 
                                        form-input__block-bottom-aligned'>

                            <Suspense fallback={<RefreshIndicator/>}>
                            <DatePickerCustom 
                                    id='id_fi-statement-start-date'
                                    name='statement__start-date'
                                    blockElementClass='form-input__block form-input__block__small'
                                    headerElementClass='fi-block__header'
                                    bodyElementGroup='form-input__element-group'
                                    title='Дата начала'
                                    nextElementId='id_fi-statement-end-date'
                                    prevElementId='id_statement-acc-pick-btn'
                                    value={ statementStartDate  }
                                    onChangeHandle={ onDatePickerChange }
                                    forwardedRef={ this.datePickerStartDate }
                                />
                            </Suspense> 
                            <Suspense fallback={<RefreshIndicator/>}>
                            <DatePickerCustom 
                                    id='id_fi-statement-end-date'
                                    name='statement__end-date'
                                    blockElementClass='form-input__block form-input__block__small'
                                    headerElementClass='fi-block__header'
                                    bodyElementGroup='form-input__element-group'
                                    title='Дата окончания'
                                    nextElementId='id_fi-statement-submit-btn'
                                    prevElementId='id_fi-statement-start-date'
                                    value={ statementEndDate  }
                                    onChangeHandle={ onDatePickerChange }
                                    forwardedRef={ this.datePickerEndDate }
                                />
                            </Suspense>
                        </div>
                        <div className='form-input__block-group 
                                        form-input__block-bottom-aligned
                                        form-input__block-bottom-aligned__mt'>
                            <button type='submit'
                                    className='btn' autoFocus
                                    id='id_fi-statement-submit-btn' 
                                    onClick={ onBtnClick }>Получить</button>
                            <button type='button'
                                    className='btn btn-cancel'
                                    id='id_fi-statement-cancel-btn'
                                    onClick={ onBtnClick }>В список счетов</button>
                        </div>
                    </div>
                </form>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Наименование счета/клиента</div>
                        <div className='fi__label fi__element'>
                            { visibleName }
                        </div>                                
                    </div>
                </div>
                <Picker
                    id={ 'id_fi-statement-account-picker' }
                    isExpanded = { !pickerCollapsed }
                    listData = { accountPickerData }
                    onDataPick = { onDataPick }
                    listColumns = { statementPickerAccsColumns }
                    onCancel = { onPickerCancel }
                    filterFields = { accPickerFilterFields('id_fi-statement-account-picker-filter') }
                    filterValues = { filterValues }
                    onFilterChanged = { onFilterChanged }
                    filterHeaderName = 'Выберите счет'/>
            </div>

        )
    }

    renderDateOperations(operationsData) {
        if (operationsData.size) {
            let key = 1;
            const baseClassName = 'row-amount amount-item'
            return operationsData.map(value => {
                const operationSum = value.get('operationSum'),
                      operationPrintSum = value.get('formattedSum'),
                      operationPurpose = value.get('operationPurpose')
                
                const amountClassName = baseClassName + ' ' + 
                                        ((operationSum > 0) ? 'amount-item-increase' : 'amount-item-decrease');
                return (<div className='statement-item row' key={ key++ }>
                    <div className='col-6 col-md-4 amount-column'>
                        <div className={ amountClassName }>{ operationPrintSum }</div>
                        </div>
                    <div className='col-12 col-md-8'>{ operationPurpose }</div>
                </div>)   
            });
        }
        else return null;
    }

    renderStatementDateRows(statementRows) {
        if (statementRows.size) {
            return statementRows.map(value => {
                const recordDate = value.get('recordDate'),
                      endAmount = value.get('endAmount'),
                      formatedDate = getDateStr(recordDate),
                      operationData = value.get('operations');

                return (
                <div className='statement-row container'
                     key={ formatedDate }>
                    <div className='row'>
                        <div className='col-12 col-md-4 statement-date'>{ formatedDate }</div>
                    </div>
                    { this.renderDateOperations(operationData) }
                    <div className='row'>
                        <div className='col-12 col-md-4'>{ 'Остаток за ' + formatedDate }</div>
                        <div className='col-sm-auto amount-column amount-item'>{ floatPrettyPrint(endAmount) }</div>
                    </div>   
                    
                </div>)
            })
        }
        else {
            return (
            <div className='statement-row container'>
                 <div className='row'>Нет операций в отчетном периоде</div>
            </div>)
        }
    }

    renderBody(statementData, dataIsRetrieving) {
        if (!dataIsRetrieving) {
            const currencyIsoCode = statementData.get('currencyIsoCode'),
                  startAmount = statementData.get('startAmount'), 
                  endAmount = statementData.get('endAmount'), 
                  statementRows = statementData.get('statementRecords') 

            return (
                <div className='statement-rows'>
                    <div className='row'>
                        <div className='col-12 acc-currency-item' 
                             id='acc-statement-currency'>{ 'Валюта счета ' + currencyIsoCode }
                        </div>
                    </div>
                    <div className='row statement-total'>
                        <div className='col-12 col-md-4'>Остаток на начало периода</div>
                        <div className='col-sm-auto amount-item' 
                             id='acc-statement-amount-start'>{ floatPrettyPrint(startAmount) }</div>
                    </div>
                    { this.renderStatementDateRows(statementRows) }
                    <div className='row statement-total'>
                    <div className='col-12 col-md-4'>Остаток на конец периода</div>
                    <div className='col-sm-2 amount-column amount-item' 
                                id='acc-statement-amount-end'>{ floatPrettyPrint(endAmount) }</div>
                </div>

                </div>
    
            
            );
        }
        else return null;
    }

    render() {
        const { statementData, dataIsRetrieving } = this.props;

        return (
            <div className='main-block__body'>
                { this.renderHeader() }
                { this.renderBody(statementData, dataIsRetrieving) }
            </div>
        );
    }
}

Statement.propTypes = {
    windowName: PropTypes.string.isRequired,
    accountId: PropTypes.number,
    accountNumber: PropTypes.string.isRequired,
    accountName: PropTypes.string.isRequired,
    clientId: PropTypes.number,
    clientName: PropTypes.string,
    statementStartDate: PropTypes.instanceOf(Date),
    statementEndDate: PropTypes.instanceOf(Date),
    dataIsRetrieving: PropTypes.bool.isRequired,
    statementData: PropTypes.instanceOf(List).isRequired,
    userRoles: PropTypes.instanceOf(List).isRequired,
    accountPickerData: PropTypes.instanceOf(List).isRequired,
    isManager: PropTypes.bool.isRequired,
    isClient: PropTypes.bool.isRequired
}


const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext')
                                   .get('windowContext')
                                   .get(windowName),
          windowContent = windowContext.get('windowContent')
                                
    return {
        accountId: windowContent.get('accountId'),
        statementData: windowContent.get('statementData'),
        accountNumber: windowContent.get('accountNumber'),
        accountName: windowContent.get('accountName'),
        clientId: windowContent.get('clientId'),
        clientName: windowContent.get('clientName'),
        statementStartDate: windowContent.get('statementStartDate'),
        statementEndDate: windowContent.get('statementEndDate'),
        dataIsRetrieving: windowContent.get('dataIsRetrieving'),
        accountPickerData: windowContent.get('accountPickerData'),
        userRoles: rootState.get('userRoles'),
        isManager: rootState.get('isManager'),
        isClient: rootState.get('isClient'),
    }
}

export default connect(mapStateToProps)(Statement);

