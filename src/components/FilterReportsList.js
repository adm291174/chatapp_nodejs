import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';


class FilterReportsList extends Component {
    render() {
        const { filterContext, onElementChange, 
                onFilterClearClick } = this.props;

        const filterValues = filterContext.get('filterValues');

        const name = filterValues.get('reports-filter__name');


        return (
            <form id='id_orders-filter'>
                <div className='main-block__filter'>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Фильтр по имени</div>
                        <div className='mbf-block__element-group'>
                            <input type='text' className='form-control' placeholder='' 
                                   id='id_form-reports-filter-name'
                                   onChange={ onElementChange }
                                   name='reports-filter__name'
                                   value={ name }
                                   />
                            <div className='img-wrapper' 
                                 aria-label='Очистить'
                                 htmlFor='id_form-reports-filter-name'
                                 onClick={ onFilterClearClick }>
                                <img src='static/images/filter-clear.png' alt='Filter clear' />
                            </div>
                        </div>
                    </div>

                </div>                        
            </form>                
        );
    }
}

FilterReportsList.propTypes = {
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired
}

export { FilterReportsList };