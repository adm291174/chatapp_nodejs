import React, { Component, Suspense } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import { operationTypes,
         operationImages,
         allTypes,
         operationTypesById,
         operaitionDateNextElement } from '../constants/operations_constants';
import { OperationEditBody } from './OperationEditBody';
import { prepareForSave } from '../core/operations_functions';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom')); 

// import DatePickerCustom from '../elements/DatePickerCustom';


class OperationEdit extends Component {
    constructor(props) {
        super(props);
        this.operationDateRef = React.createRef();
        this.operationEditBodyRef = React.createRef();
    }

    onChange(e) {
        const { dispatch } = this.props,
              element = e.currentTarget,
              elementId = element.id
        
        switch (elementId) {
            case 'id_fi-operation-type': {
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: null, 
                        values: [
                            {
                                fieldName: 'operationType',
                                fieldValue: element.value
                            },
                            {
                                fieldName: 'operationTypeDescription',
                                fieldValue: operationTypesById.get(element.value)
                            }
                        ]                        
                    }
                });
                break;                 
            }
            case 'id_fi-oper-enabled':
                dispatch( {
                    type: 'OPERATION_EDIT_DATA_CHANGED',
                    payload: {
                        operationPart: null, 
                        values: [
                            {
                                fieldName: 'operationStatus',
                                fieldValue: element.checked
                            }
                        ]                        
                    }
                });
                break;                 
            /* 
            case 'id_fi-order-comm-type': {
                const commissionType = element.value;
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionType',
                        fieldValue: commissionType
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionDescription',
                        fieldValue: orderTypeCommissions.get(commissionType).description
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderCommissionPercent',
                        fieldValue: orderTypeCommissions.get(commissionType).commissionPercent
                    }
                });

                break;                 
            } */
        }
    }

    onBtnClick(e) {
        e.preventDefault();
        const elementId = e.currentTarget.id;

        switch (elementId) {
            case 'id_fi-operation-save-btn': {
                if (this.validate()) {
                    const { operationData, dispatch } = this.props;
                    dispatch({
                        type: 'SAVE_OPERATION_DATA',
                        payload: prepareForSave(operationData)
                    });

                }
                break;
            }
            case 'id_fi-operation-cancel-btn': {
                const { operationId, operationIsEdited, dispatch } = this.props;
                if (operationId && operationIsEdited) {
                    dispatch({
                        type: 'APP_CONTEXT_SET',
                        payload: {
                            window: 'operationEdit',
                            context: Map({
                                windowContent: Map({
                                    operationId: operationId,
                                    operationIsEdited: false
                                })
                            })    
                        }
                    });                    
                }
                else {
                    dispatch({
                        type: 'APP_CONTEXT_SET',
                        payload: {
                            window: 'operationsList',
                            context: Map()
                        }
                    });
                }
                break;
            }
            case 'id_fi-operation-edit-btn': {
                const { operationId, dispatch } = this.props;
                dispatch({
                    type: 'APP_CONTEXT_SET',
                    payload: {
                        window: 'operationEdit',
                        context: Map({
                            windowContent: Map({
                                operationId: operationId,
                                operationIsEdited: true
                            })
                        })    
                    }
                }); 
                break;
            }
        }
    }

    onDatePickerChange(id, dateValue) {
        /* process date change */ 
        const { dispatch } = this.props; 
        dispatch( {
            type: 'OPERATION_EDIT_DATA_CHANGED',
            payload: {
                operationPart: null,
                values: [{
                    fieldName: 'operationDate',
                    fieldValue: dateValue
                }]
            }
        });
    }


    validate() {
        const operationDateValidationState = this.operationDateRef.current.checkAndSetValidity();
        if (!operationDateValidationState) {
            this.operationDateRef.current.setFocus();
        }
        const operationEditBodyState = this.operationEditBodyRef.current.validate(operationDateValidationState);
        return (operationDateValidationState && operationEditBodyState);
    }

    componentDidUpdate(prevProps) {        
        if (prevProps.operationIsEdited !== this.props.operationIsEdited || 
           prevProps.operationId != this.props.operationId) {
                window.scrollTo(0, 0);
        }
    }

    componentWillUnmount() {
        this.props.dispatch(
            {
                type: 'WINDOW_TITLE_SET',
                payload: {
                    windowName: 'operationEdit',
                    windowTitle: 'Операция'
                }
            }
        );
        /* this.props.dispatch({
            type: 'OPERATION_EDIT_DATA_CHANGED',
                payload: {
                    operationPart: null, 
                    values: [
                        { fieldName: 'operationType', fieldValue: undefined },
                        { fieldName: 'operationTypeDescription', fieldValue: 'Не определено' }
                    ]
                }
        }); */

    }

    renderOperationTypeSelector() {
        return operationTypes.filter(value => (value.orderType != allTypes))
                             .map(value => {
            return <option value={ value.orderType } key={ value.orderType }>{value.orderTypeDescription }</option>
        })
    }

    renderHeader(operationId, 
                 operationIsEdited, 
                 operationType, 
                 operationTypeDescription,
                 operationDate) {
        // render header of operation (operation type selector)
        const imagePictureName = operationImages.get(operationType);


        return (
            <div className='form-input__row'>
                <div className='form-input__block'>
                    <div className='fi-block__header'>Тип операции</div>
                    <div className='form-input__element-group'>
                        { !operationId && operationIsEdited 
                          ? (<select className='form-control fi__element' 
                                id='id_fi-operation-type' value={ operationType }
                                onChange={ this.onChange.bind(this) }>
                            { this.renderOperationTypeSelector() }
                             </select>) 
                          : <div className='fi__label fi__element' 
                                 id='id_fi-operation-type_ro'>{ operationTypeDescription }</div> }
                        <div className='img-wrapper' aria-label={ operationTypeDescription }>
                            { imagePictureName ? <img className='img-wrapper__img_nohover' src={ imagePictureName } alt='Тип операции' /> : null }
                        </div>
                    </div>
                </div>
                <Suspense fallback={<RefreshIndicator/>}>
                <DatePickerCustom 
                    nextElementId={operaitionDateNextElement.get(operationType) }
                    prevElementId='id_fi-operation-type'
                    title='Дата операции'
                    id='id_fi-operation-date'
                    name='operation-date'
                    value={ operationDate }
                    onChangeHandle={ this.onDatePickerChange.bind(this) }
                    blockElementClass='form-input__block'
                    headerElementClass='fi-block__header'
                    bodyElementGroup='form-input__element-group'
                    showValidClass={ operationIsEdited }
                    useStub={ false }
                    disabled={ !operationIsEdited }
                    forwardedRef={ this.operationDateRef }
                />
                </Suspense>
            </div>
        );
    }

    renderButtons(operationIsEdited, canBeEdited) {
        if (operationIsEdited) {
            return (<div className='main-block__buttons'>
                <button type='button' 
                        className='btn main-block__btn'
                        id='id_fi-operation-save-btn'
                        onClick={ this.onBtnClick.bind(this) }>Сохранить</button>
                <button type='button' 
                        className='btn main-block__btn main-block__btn-cancel'
                        id='id_fi-operation-cancel-btn'
                        onClick={ this.onBtnClick.bind(this) }>Отмена</button>
            </div>)
        }
        else {
            return (<div className='main-block__buttons'>
                { canBeEdited ? (<button type='button' 
                                    className='btn main-block__btn'
                                           id='id_fi-operation-edit-btn'
                                           onClick={ this.onBtnClick.bind(this) }>Редактировать</button>) : null }
                <button type='button' 
                        className='btn main-block__btn main-block__btn-cancel'
                        id='id_fi-operation-cancel-btn'
                        onClick={ this.onBtnClick.bind(this) }>В список операций</button>
            </div>)
        }
        
    }

    render() {
        const { operationId, operationIsEdited, operationData } = this.props,
                operationDataId = operationData.get('operationDataId'),
                operationStatus = operationData.get('operationStatus');
                
                
        if (operationId && !operationDataId) {
            return <div className='main-block__body'>Получение данных операции... </div>
        }
        else {
            const operationType = operationData.get('operationType'),
                  operationTypeDescription = operationData.get('operationTypeDescription'),
                  operationDate =  operationData.get('operationDate'),
                  canBeEdited = this.props.isManager;

            return (
                <div className='main-block__body'>
                    <form id='id_form-operation'>
                    <div className='form-input form-input-operation'>
                        { this.renderHeader(operationId, operationIsEdited, operationType, operationTypeDescription, operationDate) }
                        <OperationEditBody operationId={ operationId } 
                                        operationIsEdited={ operationIsEdited } 
                                        operationData={ operationData }
                                        ref={ this.operationEditBodyRef }
                        />

                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='form-input__element-group'>
                                    <input  type='checkbox' 
                                            className='form-control fi__element_override-form-control' 
                                            id='id_fi-oper-enabled' 
                                            checked={ operationStatus }
                                            onChange={ this.onChange.bind(this) }
                                            disabled={ !operationIsEdited }
                                           />
                                    <label className='fi__label_smallfont' htmlFor='id_fi-oper-enabled'>Учитывается</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    { this.renderButtons(operationIsEdited, canBeEdited) }
                    </form>
                </div>
            )
        }
    }
}

OperationEdit.propTypes = {
    operationId: PropTypes.number,
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map)
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName,
          rootState = state.get('root').data,
          operationsState = state.get('operations').data,
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');
          
    
    const operationId = windowContents.get('operationId'),
          operationIsEdited =  windowContents.get('operationIsEdited'),
          operationData = operationsState.get('operationData')
    

    return {
        operationId: operationId,
        operationIsEdited: operationIsEdited,
        operationData: operationData,
        userRoles: rootState.get('userRoles'),
        isManager: rootState.get('isManager'),
        isClient: rootState.get('isClient')
    }
}

export default connect(mapStateToProps)(OperationEdit);