import React, { Component } from 'react'
import { Nav, Navbar } from 'react-bootstrap';
import { PropTypes } from 'prop-types';


class AppMenu extends Component {
    constructor(props) {
        super(props);
        this.navBarRef = React.createRef();
        this.state = {
            collapsed: true
        }
    }

    processLogout() {
        this.props.dispatch({type: 'PROCESS_LOGOUT',
                    payload: null
                });        
    }

    processPasswordChange() {
        this.props.dispatch({type: 'PROCESS_REDIRECT',
                    payload: '/accounts/password/change/'
                });
    }

    processAdminInterface() {
        const { userInfo, dispatch } = this.props,
              adminAppUrl = userInfo.get('adminAppUrl');
        dispatch({
            type: 'PROCESS_REDIRECT',
            payload: adminAppUrl
        });
    }

    processReportsExecute() {
        const { dispatch, onItemPick } = this.props;
        
        dispatch({
            type: 'APP_CONTEXT_SET',
            payload: {
                window: 'reportsList'
            }
        });
        onItemPick();
    }

    processUserAdminExecute() {
        const { dispatch, onItemPick } = this.props;
        
        dispatch({
            type: 'APP_CONTEXT_SET',
            payload: {
                window: 'usersList'
            }
        });
        onItemPick();
    }

    renderAdminLink(isAdmin) {
        if (isAdmin) {
            return <Nav.Item className='nav-item active'>
                        <Nav.Link className='nav-link' 
                                      onClick={this.processAdminInterface.bind(this)}>Административный интерфейс</Nav.Link>
                        </Nav.Item>
        }
        else return null;
    }

    renderUserAdminLink(isUserAdmin) {
        if (isUserAdmin) {
            return <Nav.Item className='nav-item active'>
                        <Nav.Link className='nav-link' 
                                      onClick={this.processUserAdminExecute.bind(this)}>Управление пользователями</Nav.Link>
                        </Nav.Item>
        }
        else return null;
    }

    renderReportsLink(reportsGroups) {
        if (reportsGroups.length > 0) {
            return <Nav.Item className='nav-item active'>
                <Nav.Link className='nav-link' 
                      onClick={this.processReportsExecute.bind(this)}>Отчеты</Nav.Link>
                </Nav.Item>

        }
        else return null;
    }

    renderAppVersion(appVersion, appDate) {
        if (appVersion) {
            return (<Nav.Item className='nav-item disabled'>
                <Nav.Link className='nav-link nav-link-version-info'>{ 'Версия ' + appVersion + ' от ' + appDate }</Nav.Link>
            </Nav.Item>);
        }
        return null;
    }
     
    renderMenuItems() { 
        const { userInfo } = this.props,
              isAdmin = userInfo.get('isAdmin'),
              isUserAdmin = userInfo.get('isUserAdmin'),
              reportsGroups = userInfo.get('reportsGroups'),
              appVersion = userInfo.get('appVersion'),
              appDate = userInfo.get('appDate');

        return (<Nav className='mr-auto'>
                    { this.renderReportsLink(reportsGroups) }
                    { this.renderAdminLink(isAdmin) }
                    { this.renderUserAdminLink(isUserAdmin) }
                    <Nav.Item className='nav-item active'>
                        <Nav.Link className='nav-link' 
                                  onClick={ this.processPasswordChange.bind(this) }>Сменить пароль<span className='sr-only'>(current)</span></Nav.Link>
                    </Nav.Item> 
                    <Nav.Item className='nav-item active'>
                        <Nav.Link className='nav-link' 
                                  onClick={ this.processLogout.bind(this) }>Выйти из системы</Nav.Link>
                    </Nav.Item>
                    { this.renderAppVersion(appVersion, appDate) }
                </Nav>);
                
        /* 
            <Nav.Item className='nav-item'>
                <Nav.Link className='nav-link' href='/enablejs.html'>Home 
                    <span className='sr-only'>(current)</span>
                </Nav.Link>
            </Nav.Item>

        return <ul className='navbar-nav mr-auto'>
                  <li className='nav-item active'>
                     <a className='nav-link' href='/enablejs.html'>Home 
                         <span className='sr-only'>(current)</span>
                     </a>
                  </li>
                  <li className='nav-item'>
                     <a className='nav-link' href='/enablejs.html'>Link</a>
                  </li>
                  <li className='nav-item dropdown'>
                    <a className='nav-link dropdown-toggle' href='/enablejs.html' id='navbarDropdown' 
                       role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    Dropdown
                    </a>
                    <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
                        <a className='dropdown-item' href='/enablejs.html'>Action</a>
                        <a className='dropdown-item' href='/enablejs.html'>Another action</a>
                        <div className='dropdown-divider'></div>
                        <a className='dropdown-item' href='/enablejs.html'>Something else here</a>
                    </div>
                </li>
                <li className='nav-item'>
                    <a className='nav-link disabled' href='/enablejs.html'>Disabled</a>
                </li>
            </ul>
        */ 
    }


    render() {
        const { menuCollapsed } = this.props;
        return (
            <Navbar.Collapse in={ !menuCollapsed }
                id='responsive-navbar-nav' 
                ref={ this.navBarRef }>
                    { this.renderMenuItems() }
            </Navbar.Collapse>        
            
        );    
               
    }
}

AppMenu.propTypes = {
    dispatch: PropTypes.func.isRequired,
    menuCollapsed: PropTypes.bool.isRequired,
    onItemPick: PropTypes.func.isRequired
}

export default AppMenu;