import React, { Component, Suspense } from 'react'
import ClientList from './ClientList';
import ClientEdit from './ClientEdit';
import PayReqEdit from './PayReqEdit';
import ClientEditAddUser from './ClientEditAddUser';
import OrdersList from './OrdersList';
import OrderEdit from './OrderEdit.js';
import OperationsList from './OperationsList';
import OperationEdit from './OperationEdit';
import Statement from './Statement';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';

const ReportsList = React.lazy(() => import('./ReportsList'));
const Report = React.lazy(() => import('./Report'));
const UsersList = React.lazy(() => import('./UsersList'));
const UserEdit = React.lazy(() => import('./UserEdit'));

/* const ReportsListTest = React.lazy(() => { 
    return new Promise( resolve => { setTimeout(resolve, 5000) })
               .then( () => 
                   // Math.floor(Math.random() * 10) >= 4 ?
                   import('./ReportsList') // : Promise.reject(new Error())
               )
    }); */

const componentMapping = {
    'clients': {
        component: ClientList,
        dynamic: false
    },
    'clientEdit': {
        component: ClientEdit,
        dynamic: false
    },
    'paymentRequsitEdit': {
        component: PayReqEdit,
        dynamic: false
    },
    'clientEditAddUser': {
        component: ClientEditAddUser,
        dynamic: false
    },
    'ordersList': {
        component: OrdersList,
        dynamic: false
    },
    'orderEdit': {
        component: OrderEdit,
        dynamic: false
    },
    'operationsList': {
        component: OperationsList,
        dynamic: false
    },
    'operationEdit': {
        component: OperationEdit,
        dynamic: false
    },
    'statement': {
        component: Statement,
        dynamic: false
    },
    'reportsList': {
        component: ReportsList, // ReportsListTest,
        dynamic: true 
    },    
    'report': {
        component: Report,
        dynamic: true
    },
    'usersList': {
        component: UsersList,
        dynamic: true
    },
    'userEdit': {
        component: UserEdit,
        dynamic: true
    }    
}


class AppWindowContents extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { windowName } = this.props;
        if (componentMapping.hasOwnProperty(windowName)) {
            const mappedComponentInfo = componentMapping[windowName],
                  MappedComponent = mappedComponentInfo.component;
            
            if (mappedComponentInfo.dynamic) {

                return  (
                <div className='main-block__component-wrapper'>
                    <Suspense fallback={<RefreshIndicator/>}>
                        <MappedComponent windowName = { windowName }/>
                    </Suspense>
                </div>);
            }
            else {
                return  (
                <div className='main-block__component-wrapper'>
                    <MappedComponent windowName = { windowName }/>
                </div>);
            }
        }
        else {
            /* const DynamicComponent = getDynamicComponent(windowName);
            if (DynamicComponent) {
                return DynamicComponent;
            } */
            console.log('Неизвестное описание компонента', windowName);
            return (
            <div className='main-block__component-wrapper'>
                <div>{ 'Неизвестный тип компонента ' + windowName }</div>
            </div>);
        }    
    }
}

export { AppWindowContents };