import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import OrdersListItem  from '../elements/OrdersListItem';
import OrdersListSmall from '../elements/OrdersListSmall';
import { List, Map } from 'immutable';


class OrdersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filteredOrdersList: List(),
            filteredByClientsOrdersList: List() 
        }
    }

    componentDidMount() {
        const filterContents = this.props.windowFilter,
              filterValues = filterContents.get('filterValues'),
              filterNameValue = filterValues.get('orders-filter__name'),
              ordersList = this.props.ordersList;
        const filteredOrdersList = this.filterOrdersList(ordersList, filterNameValue),
              filterOrdersByClientsList = this.filterOrdersByClients(filteredOrdersList);

        this.setState ({ 
            filteredOrdersList: filteredOrdersList, 
            filteredByClientsOrdersList:  filterOrdersByClientsList
        });     
    }  

    componentDidUpdate(prevProps, prevState) {
        /* filter this.props.ordersList -> this.state.filteredOrdersList
           if text filter changed or ordersList has been changed 
        */
        const prevOrdersList = prevProps.ordersList,
              prevFilterContents = prevProps.windowFilter,
              prevFilterValues = prevFilterContents.get('filterValues'),
              prevFilterNameValue = prevFilterValues.get('orders-filter__name');
        
        const ordersList = this.props.ordersList,
              filterContents = this.props.windowFilter,
              filterValues = filterContents.get('filterValues'),
              filterNameValue = filterValues.get('orders-filter__name');
        
        if (!ordersList.equals(prevOrdersList) || prevFilterNameValue != filterNameValue) {
            const newFilteredOrdersList = this.filterOrdersList(ordersList, filterNameValue);
            if (!newFilteredOrdersList.equals(prevState.filteredOrdersList)) {
                const newClientsFilteredList = this.filterOrdersByClients(newFilteredOrdersList);
                this.setState( { filteredOrdersList: newFilteredOrdersList,
                                 filteredByClientsOrdersList: newClientsFilteredList} );
            }
        }    
    }

    filterOrdersList(ordersList, filterText) {
        /* filter orders list and return filteredOrdersList (List) */
        if (filterText.trim().length == 0) {
            return ordersList; // if no filter set ... return original ordersList
        }
        else {
            const lowerCaseFilterValue = filterText.toLowerCase().trim();
            return ordersList.filter(orderItem => {
                const orderItemSearchText = orderItem.get('orderClientName').toLowerCase();
                return orderItemSearchText.includes(lowerCaseFilterValue);
            })
        }
    }

    filterOrdersByClients(ordersList) {
        /* filter orders list by clients and return filteredByClientsOrdersList (List) 
           List is a collection of Lists 
           We can process filtered list - for faster processing - no morew filtering is needed 
        */
        if (ordersList.size == 0) {
            return List(); // if empty list - return empty result 
        }
        else {
            // group records by Client 
            let clients = Map();
            ordersList.forEach( orderItem => {
                const clientId = orderItem.get('orderClientId'),
                      clientName = orderItem.get('orderClientName');

                if (!clients.has(clientId)) {
                    clients = clients.set(clientId, Map({
                                  clientId: clientId,
                                  clientName: clientName,
                                  clientOrders: List()
                    }));
                }
                return true;
            });

            ordersList.forEach( orderItem => {
                const clientId = orderItem.get('orderClientId'),
                      clientsRecord = clients.get(clientId),
                      clientOrders = clientsRecord.get('clientOrders');
                                      
                clients = clients.set(clientId,
                                      clientsRecord.set(
                                          'clientOrders', 
                                          clientOrders.push(orderItem)
                                      ));   
                return true;
            });
            // perform sorting here 
            let clientsList = clients.toList()
                                     .sortBy( value => { return value.get('clientName').toLowerCase()});

            return clientsList;
        }
    }

    onOrderEditClick(e) {
        e.preventDefault();
        const element = e.target,
              orderId = + element.getAttribute('data-order-id'),
              dispatch = this.props.dispatch;
        
        if (orderId) {
            dispatch( {type: 'APP_CONTEXT_SET', 
                payload: {
                    window: 'orderEdit',
                    context: Map({
                        windowContent: Map({
                            orderId: orderId,
                            orderIsEdited: false
                        })
                    })    
                }
            });
        }
    }

    renderOrdersListWide(filteredOrdersList) {
        if (filteredOrdersList.size > 0) {
            return filteredOrdersList.map(orderItem => {
                const orderId = orderItem.get('orderId');
                return <OrdersListItem 
                    key={ orderId }
                    orderId={ orderId }
                    orderNumber={ orderItem.get('orderNumber') }
                    orderDate={ orderItem.get('orderDate') }
                    orderStatusDescription={ orderItem.get('orderStatusDescription') }
                    orderClientName={ orderItem.get('orderClientName') }
                    orderSum={ orderItem.get('orderSum') }
                    orderCurrencyCode={ orderItem.get('orderCurrencyCode') }
                    orderTypeDescription={ orderItem.get('orderTypeDescription') }
                    onOrderEditClick={ this.onOrderEditClick.bind(this) }
                    isSmall = { false }
                />
            });
        }
        else return null;
    }
    

    render() {
        const { filteredOrdersList, filteredByClientsOrdersList } = this.state;

        if (filteredOrdersList.size > 0) {
            return (
            <div className='main-block__body'>
                <div className='orders-list orders-list_wide'>
                    <table>
                        <thead>
                            <tr>
                                <td>Номер</td>
                                <td>Дата</td>
                                <td>Статус</td>
                                <td>Наименование клиента</td>
                                <td>Сумма</td>
                                <td>Тип ордера</td>
                            </tr>
                        </thead>
                        <tbody>
                            { this.renderOrdersListWide(filteredOrdersList) }
                        </tbody>
                    </table>
                </div>
                <div className='orders-list orders-list_small'>
                    <OrdersListSmall  
                        clientsList={ filteredByClientsOrdersList }
                        onOrderEditClick={ this.onOrderEditClick.bind(this) }
                    />
                </div>
            </div>);
        }
        else {
            return (
                <div className='main-block__body'>
                    <div className='orders-list'>
                        Нет данных для отображения 
                    </div>
                </div>
            )
        }        

    }
}

OrdersList.propTypes = {
    windowName: PropTypes.string.isRequired
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext').get('windowContext').get(windowName);
    // const ordersListContent = windowContext.get('windowContent'); // activate if any content will be here 
    const ordersState = state.get('orders').data;
    return {
        windowFilter: windowContext.get('windowFilter'),
        ordersList: ordersState.get('ordersList'),
        userRoles: rootState.get('userRoles'),
        isManager: rootState.get('isManager'),
        isClient: rootState.get('isClient'),
    }
}

export default connect(mapStateToProps)(OrdersList);

