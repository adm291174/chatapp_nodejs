import React, { Component } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import memoizeOne from 'memoize-one';
import { Map } from 'immutable';
import * as headerActions from '../actions/HeaderActions';
import { getChatListUIState, getUIData  } from '../core/handler_functions';


// style for <header__btn-chat-list__msg-count> class - override Bootstrap nav element style

const chatBtnStyle = { display: 'block !important' }

class ChatListBtn extends Component {
    constructor(props) {
        super(props);
    }

    onBtnClick(event) {
        const { chatListVisible, headerActions } = this.props;
        headerActions.actionClickChatListBtn(Map({isVisible: !chatListVisible}));
        event.preventDefault();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.chatListVisible == undefined) {
            const chatListState = getChatListUIState(getUIData());
            const { headerActions } = this.props;
            headerActions.actionClickChatListBtn(Map({isVisible: chatListState}));            
        }
    }

    render() {
        const { enabled, msgCount, visible } = this.props;
        if (enabled) { 
            let msgCountElement = null;
            if (msgCount) 
                msgCountElement = <span className='header__btn-chat-list__msg-count' 
                                  style={chatBtnStyle}>{ msgCount }</span>;
            if (visible) {
                return (<nav className='header__btn-chat-list'>
                            <label className='label__hidden-label' 
                                onClick={ this.onBtnClick.bind(this) }> 
                            { /* htmlFor='id_main__checkbox' */ }
                            <span className='header__btn-chat-list__span'>
                                <img className='header__btn-chat-list__image' 
                                    src='/static/images/icons8-secured-letter-50.png' width='22' 
                                    alt='pick-message' />
                                { msgCountElement }
                            </span>
                        </label>
                    </nav>)
            }  
            else return null;
        }                
        else return (<nav className='header__btn-chat-list' style={{display: 'none'}}></nav>);
        
    }
}

const getBtnState = memoizeOne(data => (data.size > 0));
const getMsgCount = memoizeOne(data => data.reduce((a, value) => a + value.get('unreadMessagesCnt'), 0));
                    

function mapStateToProps(state) {
    let chatListData = state.get('chatList').data.get('chatListData');
    
    return {
        enabled: getBtnState(chatListData), 
        msgCount: getMsgCount(chatListData),
        chatListVisible: state.get('chatList').data.get('isVisible')        
    }
}

function mapDispatchToProps(dispatch) {
    return {
        headerActions: bindActionCreators(headerActions, dispatch)
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ChatListBtn);