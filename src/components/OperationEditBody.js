import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { Map } from 'immutable';
import { transferOperationType,
         supplyOperationType,
         settlementOperationType,
         simpleOperationType } from '../constants/operations_constants';

import OperationTransferBody from './OperationTransferBody';
import OperationPartedBody from './OperationPartedBody';
import OperationSimpleBody from './OperationSimpleBody';


class OperationEditBody extends Component {
    constructor(props) {
        super(props);
        this.operationTransferBodyRef = React.createRef();
        this.operationSupplyBodyRef = React.createRef();
        this.operationSettlementBodyRef = React.createRef();
        this.operationSimpleBodyRef = React.createRef();
    } 

    validate(needToSetFocus) {
        // validate component - call children validate method to determine form validity 
        const { operationData } = this.props,
                operationType = operationData.get('operationType');

        let validatedComponentRef = null;
        if (operationType == transferOperationType) 
            validatedComponentRef = this.operationTransferBodyRef;
        else if (operationType == supplyOperationType) {
            validatedComponentRef = this.operationSupplyBodyRef;
        }
        else if (operationType == settlementOperationType) {
            validatedComponentRef = this.operationSettlementBodyRef;
        }
        else if (operationType == simpleOperationType) {
            validatedComponentRef = this.operationSimpleBodyRef;
        }

        return (validatedComponentRef && 
                validatedComponentRef.current && 
                validatedComponentRef.current.validate(needToSetFocus));
    }


    render() {
        const { operationData } = this.props,
                operationType = operationData.get('operationType');

        return <div className='operation-body__wrapper'>
            { operationType == transferOperationType ? (
                <OperationTransferBody ref={ this.operationTransferBodyRef }
                />
            ) : null } 
            { operationType == supplyOperationType ? (
                <OperationPartedBody ref={ this.operationSupplyBodyRef }
                />
            ) : null } 
            { operationType == settlementOperationType ? (
                <OperationPartedBody ref={ this.operationSettlementBodyRef }
                />
            ) : null } 
            { operationType == simpleOperationType ? (
                <OperationSimpleBody ref={ this.operationSimpleBodyRef }
                />
            ) : null } 

        </div>
    }
    
}

OperationEditBody.propTypes = {
    operationId: PropTypes.number,
    operationIsEdited: PropTypes.bool.isRequired,
    operationData: PropTypes.instanceOf(Map).isRequired
}

export { OperationEditBody };