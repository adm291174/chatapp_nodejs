import React, { Component, Suspense } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { Dropdown } from 'react-bootstrap';
import { getCurrentDate }  from '../core/core_functions';
import { transferOperationType,
         supplyOperationType,
         settlementOperationType,
         simpleOperationType, 
         allTypes,
         operationTypesById } from '../constants/operations_constants';
import { getDateStrForQuery } from '../core/core_functions';


import RefreshIndicator from '../elements/RefreshIndicatorLoad';
const DatePickerCustom = React.lazy(() => import('../elements/DatePickerCustom'));


class FilterOperationsList extends Component {

    onfilterButtonClick(e) {
        // Handler for other filter buttons click 
        e.preventDefault();
        const { dispatch } = this.props;
        const elementId = e.target.id;
        let operationType = null;
        switch (elementId) {
            case 'id_operations-filter-add-transfer': {
                operationType = transferOperationType;
                break;
            }
            case 'id_operations-filter-add-supply': {
                operationType = supplyOperationType;
                break;
            }
            case 'id_operations-filter-add-settlement': {
                operationType = settlementOperationType;
                break;
            }
            case 'id_operations-filter-add-simple-oper': {
                operationType = simpleOperationType;
                break;
            }
        }
        // New order edit window 
        dispatch( {type: 'APP_CONTEXT_SET', 
            payload: {
                window: 'operationEdit',
                context: Map({
                    windowContent: Map({
                        operationId: null,
                        operationIsEdited: true
                    })
                }),    
                operationData: Map({
                    operationType: operationType,
                    operationTypeDescription: operationTypesById.get(operationType),
                    operationDate: getCurrentDate()
                })
            }
        });
    }

    componentDidUpdate(prevProps) {
        const prevFilterValues = prevProps.filterContext.get('filterValues'), 
              currentFilterValues = this.props.filterContext.get('filterValues');

        if (!currentFilterValues.equals(prevFilterValues)) {
            // detect changes 
            const prevStartDate = prevFilterValues.get('operations-filter__start-date');
            const prevEndDate = prevFilterValues.get('operations-filter__end-date');
            const prevOperationType = prevFilterValues.get('operations-filter__type');

            const currentStartDate = currentFilterValues.get('operations-filter__start-date');
            const currentEndDate = currentFilterValues.get('operations-filter__end-date');
            const currentOperationType = currentFilterValues.get('operations-filter__type');

            if (prevStartDate != currentStartDate || prevEndDate !=currentEndDate ||
                prevOperationType != currentOperationType) {
                    

                    // perform database query 
                    this.props.dispatch ( {
                        type: 'GET_OPERATIONS_LIST_DATA',
                        payload: {
                            'startDate': getDateStrForQuery(currentStartDate),
                            'endDate': getDateStrForQuery(currentEndDate),
                            'operationType': ( currentOperationType == allTypes
                                             ? null : currentOperationType )

                        }
                    })
                }
        }
    }


    render() {
        const { filterContext, onElementChange, 
                onFilterClearClick, onDatePickerChange } = this.props;

        const filterValues = filterContext.get('filterValues');

        const period = filterValues.get('operations-filter__period');
        const startDate = filterValues.get('operations-filter__start-date');
        const endDate = filterValues.get('operations-filter__end-date');
        const operation_type = filterValues.get('operations-filter__type');
        const name = filterValues.get('operations-filter__name');

        const datePickerState = (period != 'custom_period'),
              onfilterButtonClick = this.onfilterButtonClick.bind(this);

        return (
            <form id='id_operations-filter'>
                <div className='main-block__filter'>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Выберите период</div>
                        <div className='mbf-block__element'>
                            <select className='form-control' 
                                    id='id_form-operations-filter-period'
                                    onChange={ onElementChange }
                                    name='operations-filter__period'
                                    value={ period }>

                                <option value='today'>Сегодня</option>
                                <option value='this_week'>Текущая неделя</option>
                                <option value='this_month'>Этот месяц</option>
                                <option value='custom_period'>Произвольный</option>
                            </select>
                        </div>
                    </div>
                    
                    <div className='mbf__double-block mbf__double-block__datepicker'>
                        <Suspense fallback={<RefreshIndicator/>}> 
                            <DatePickerCustom 
                                id='id_form-operations-filter-start-date'
                                name='operations-filter__start-date'
                                blockElementClass='mbf__block mbf__block__datepicker'
                                headerElementClass='mbf-block__header'
                                bodyElementGroup='mbf-block__element-group'
                                title='Дата начала'
                                nextElementId='id_form-operations-filter-end-date'
                                prevElementId='id_form-operations-filter-period'
                                value={ startDate  }
                                onChangeHandle={ onDatePickerChange.bind(this) }
                                disabled = { datePickerState }
                            />
                        </Suspense>
                        <Suspense fallback={<RefreshIndicator/>}> 
                            <DatePickerCustom 
                                id='id_form-operations-filter-end-date'
                                name='operations-filter__end-date'
                                title='Дата окончания'
                                blockElementClass='mbf__block mbf__block__datepicker'
                                headerElementClass='mbf-block__header'
                                bodyElementGroup='mbf-block__element-group'
                                nextElementId='id_form-operations-filter-status'
                                prevElementId='id_form-operations-filter-start-date'
                                value={ endDate  }
                                onChangeHandle={ onDatePickerChange.bind(this) }
                                disabled = { datePickerState }
                            />
                        </Suspense>
                    </div>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Тип операции</div>
                        <div className='mbf-block__element'>
                            <select className='form-control' 
                                    id='id_form-operations-filter-type'
                                    onChange={ onElementChange }
                                    name='operations-filter__type'
                                    value={ operation_type }>
                                <option value={ transferOperationType }>Перевод</option>
                                <option value={ supplyOperationType }>Поставка товара</option>
                                <option value={ settlementOperationType }>Расчеты с клиентом</option>
                                <option value={ simpleOperationType }>Простая операция</option>
                                <option value={ allTypes }>Все типы</option>
                            </select>
                        </div>
                    </div>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Фильтр по имени</div>
                        <div className='mbf-block__element-group'>
                            <input type='text' className='form-control' placeholder='' 
                                   id='id_form-operations-filter-name' 
                                   onChange={ onElementChange }
                                   name='operations-filter__name' 
                                   value={ name }
                                   />
                            <div className='img-wrapper' 
                                 aria-label='Очистить'
                                 htmlFor='id_form-operations-filter-name'
                                 onClick={ onFilterClearClick }>
                                <img src='static/images/filter-clear.png' alt='Filter clear' />
                            </div>
                        </div>
                    </div>
                    <div className='mbf__block mbf__block_align-end'>
                        <div className='mbf-block__element'>
                            <Dropdown>
                                <Dropdown.Toggle id='id_operations-filter-add-operation' variant={ null }>Создать операцию</Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item 
                                        id='id_operations-filter-add-transfer'
                                        onClick={ onfilterButtonClick }>Перевод</Dropdown.Item>
                                    <Dropdown.Item 
                                        id='id_operations-filter-add-supply'
                                        onClick={ onfilterButtonClick }>Поставка товара</Dropdown.Item>
                                    <Dropdown.Item 
                                        id='id_operations-filter-add-settlement'
                                        onClick={ onfilterButtonClick }>Расчеты с клиентом</Dropdown.Item>
                                    <Dropdown.Item 
                                        id='id_operations-filter-add-simple-oper'
                                        onClick={ onfilterButtonClick }>Простая операция</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div> 

                </div>                        
            </form>                
        );

    }
}


FilterOperationsList.propTypes = {
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired,
    onDatePickerChange: PropTypes.func.isRequired,
    getElementData: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
}

export { FilterOperationsList };