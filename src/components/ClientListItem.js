import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { ClassListItemAccount } from './ClientListItemAccount';
import { List, Map } from 'immutable';

class ClientListItem extends Component {

    onClientClick(e) {
        e.preventDefault();
        const { clientId, dispatch } = this.props;
        dispatch(  {type: 'APP_CONTEXT_SET', 
                    payload: {
                        window: 'clientEdit',
                        context: Map({
                            windowContent: Map({
                                clientId: clientId
                            })
                       })
                   }});
    }

    renderAccounts(clientId, clientName, accounts, userIsManager, dispatch) {
        
        const accountsBlock = (!(accounts && accounts.size > 0)) ?
            <div className='account-block'>
                <div className='account-block__fin'>
                    <div className='fin__account fin__account_empty'>Счета отсутствуют</div>
                </div>
            </div> : 
            accounts.map(accountItem => {
                const accountId = accountItem.get('accountId'), 
                      accountNumber = accountItem.get('accountNumber'),
                      accountName = accountItem.get('accountName'),
                      accountAmount = accountItem.get('accountAmount'),
                      accountCurrency = accountItem.get('accountCurrency'),
                      accountStatus = accountItem.get('accountStatus'),
                      isBuyerAccount = accountItem.get('isBuyerAccount'),
                      isSupplierAccount = accountItem.get('isSupplierAccount'),
                      accountCurrencyId = accountItem.get('accountCurrencyId');

                return <ClassListItemAccount 
                   clientId={ clientId }
                   clientName = { clientName }
                   userIsManager={ userIsManager }
                   accountId={ accountId }
                   accountNumber={ accountNumber }
                   accountAmount={ accountAmount }
                   accountCurrency={ accountCurrency }
                   accountStatus={ accountStatus }
                   accountName = { accountName }
                   isBuyerAccount = { isBuyerAccount }
                   isSupplierAccount = { isSupplierAccount }
                   accountCurrencyId = { accountCurrencyId }
                   key={ accountId }
                   dispatch={ dispatch }
                /> 
            }).toArray();

        return (
            <div className='client__account-group'>
                { accountsBlock }
            </div>
        );

    }

    render() {
        const { clientId, clientName, accounts, userIsManager, dispatch } = this.props;
        const elementId = 'id_client-record-' + clientId.toString();
        return (
            <div className='client'>
                { userIsManager ? ( 
                    <div className='client__info'>
                        <a href='#' onClick={ this.onClientClick.bind(this) } 
                        id={ elementId } >{ clientName }</a>
                    </div>
                 ) : (<div className='client__info'>{ clientName }</div>) }
                { this.renderAccounts(clientId, clientName, accounts, userIsManager, dispatch) }
            </div>
        );
    } 
}

ClientListItem.propTypes = {
    clientId: PropTypes.number.isRequired, 
    clientName: PropTypes.string.isRequired, 
    accounts: PropTypes.instanceOf(List).isRequired,
    userIsManager: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
}

export {ClientListItem};