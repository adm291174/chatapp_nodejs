import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Dropdown } from 'react-bootstrap';
import { List } from 'immutable';


class OrderEditControlHeader extends Component {

    onBtnActionClick(e) {
        const element = e.target,
              actionGuidValue = element.getAttribute('data-action-guid');
        if (actionGuidValue) { this.props.onActionProcess(actionGuidValue) }
    }

    renderActionsList() {
        const { actionsList } = this.props;

        return (
            actionsList.map(value => {
                const actionId = value.get('actionId'), 
                      actionDescription = value.get('actionDescription');
                return (
                <Dropdown.Item 
                    data-action-guid= { actionId }
                    onClick={ this.onBtnActionClick.bind(this) }
                    key = { actionId } >
                    { actionDescription } 
                </Dropdown.Item>
                );
            })
        )
        
    }

    render() {
        /* render this element only if order in view state */ 
        const { orderStatusDescription, onActionToggle, actionsList } = this.props;

        return (
            <div className='form-input__row'>
                <div className='form-input__block'>
                    <div className='fi__label fi__element fi__label_order-status' 
                        id='id_fi-order-status_ro'>
                        { orderStatusDescription } 
                    </div>
                <div className='fi-stub'></div>
            </div>
            { actionsList.size > 0 ? ( 
            <div className='form-input__block'>
                <Dropdown onToggle = { onActionToggle }>
                    <Dropdown.Toggle id='id_btn-process-order'>
                        Обработка ордера
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                       { this.renderActionsList() }
                    </Dropdown.Menu>
                </Dropdown>
            </div>) : null } 
        </div>
        ) 
    }
}

OrderEditControlHeader.propTypes = {
    orderId: PropTypes.number.isRequired,
    orderStatusDescription: PropTypes.string,
    onActionProcess: PropTypes.func.isRequired,
    onActionToggle: PropTypes.func.isRequired,
    actionsList: PropTypes.instanceOf(List)
}

export default OrderEditControlHeader;
