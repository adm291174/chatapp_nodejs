import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { orderTypeCommissionTypes, orderTypeCommissions } from '../constants/order_constants';
import { floatPrettyPrintForInput, floatPrettyPrint } from '../core/core_functions';

import SumInput from '../elements/SumInput';

import { showElementValidityState } from '../core/validation_functions';


/* Order main information part: Commission Info  */ 

class OrderBodyCommission extends Component {
    constructor(props) {
        super(props);
        this.commissionPercentRef = React.createRef();
        this.commissionSumRef = React.createRef();
    }

    validate() {
        return (this.validateCommissionPercent() && 
                this.validateCommissionSum());
    }

    validateCommissionPercent() {
        if (this.commissionEnabledState()) {
            return (this.commissionPercentRef.current.validate && 
                    this.commissionPercentRef.current.validate()); // undefined if component is not set 
        }
        return true;
    } 
    
    validateCommissionSum() {
        if (this.commissionEnabledState()) {
            return (this.commissionSumRef.current.validate && 
                    this.commissionSumRef.current.validate()); // undefined if component is not set 
        }
        return true;
    }
    
    setValidationState() {
        // set elements validation state 
        if (this.commissionPercentRef.current) {
            const comPercentElement = document.getElementById('id_fi-order-comm-percent');
            showElementValidityState(comPercentElement, 
                                     this.validateCommissionPercent())
        }
        if (this.commissionSumRef.current) {
            const comSumElement = document.getElementById('id_fi-order-comm-sum');
            showElementValidityState(comSumElement, 
                                     this.validateCommissionSum())
        }
    }
    
    setFocus() {
        // focus on invalid element 
        if (this.commissionPercentRef.current) {
            if (!this.validateCommissionPercent()) {
                this.commissionPercentRef.current.setFocus();
                return;
            }
        }
        if (this.commissionSumRef.current) {
            if (!this.validateCommissionSum()) {
                this.commissionSumRef.current.setFocus();
            }
        }
    }

    renderCommissionOptions() {
        return orderTypeCommissionTypes.map(value => {
            return <option key = { value.commisionTypeGuid}
                           value = { value.commisionTypeGuid}>
                       { value.commissionTypeDescription }
                   </option>
        });
    }
    
    commissionEnabledState() {
        const { orderBodyData } = this.props;
        const orderCommissionType = orderBodyData.get('orderCommissionType'), 
              mapValue = orderTypeCommissions.get(orderCommissionType);
        
        return mapValue ? !mapValue.commissionDisabled : true;
    }

    renderCommissionType(orderIsEdited) {
        const { orderBodyData, onChange } = this.props;
        const orderCommissionType = orderBodyData.get('orderCommissionType'), 
              orderCommissionDescription = orderBodyData.get('orderCommissionDescription');

        if (orderIsEdited) {       
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Тип комиссии</div>
                    <div className='form-input__element-group'>
                        <select className='form-control fi__element' 
                                id='id_fi-order-comm-type'
                                value={ orderCommissionType }
                                onChange = { onChange } >
                            { this.renderCommissionOptions() }                            
                        </select>
                        <div className='fi-stub'></div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Тип комиссии</div>
                    <div className='form-input__element-group'>
                        <div className='fi__label fi__element' 
                             id='id_fi-order-comm-type_ro'>
                            { orderCommissionDescription }
                        </div>
                        <div className='fi-stub'></div>
                    </div>
                </div>

            )
        }
    }

    renderCommissionPercent(orderIsEdited) {
        const { orderBodyData, onSumChange, onValidation } = this.props;
        const orderCommissionPercent = orderBodyData.get('orderCommissionPercent');

        if (orderIsEdited && this.commissionEnabledState() ) {
            return (                
                <div className='form-input__block'>
                    <div className='fi-block__header'>Процент комиссии</div>
                    <div className='form-input__element-group'>
                        <SumInput className='fi__input fi__element form-control' 
                                  id='id_fi-order-comm-percent' 
                                  value= { orderCommissionPercent } 
                                  onElementChange = { onSumChange }
                                  fractionDigits = { 4 } 
                                  prohibitNegativeValues = { true }
                                  onValidation = { onValidation }
                                  changeValueOnElementChange = { true }
                                  ref = { this.commissionPercentRef } 
                                  maxValue = { 100 - 0.0001 }
                                  />
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Процент комиссии</div>
                    <div className='form-input__element-group'>
                        <div className='fi__label fi__element' 
                             id='id_fi-order-comm-percent_ro'>
                            { floatPrettyPrintForInput(orderCommissionPercent, 4) }
                        </div>
                    </div>
                </div>
            );
        }    
    }

    renderCommissionSum(orderIsEdited) {
        const { orderBodyData, onSumChange, onValidation  } = this.props;
        const orderCommissionSum = orderBodyData.get('orderCommissionSum'),
              orderCommissionCurrency = orderBodyData.get('orderAccountCurrencyCode');

        const strData = ( orderCommissionSum ? 
                          floatPrettyPrint(orderCommissionSum) :
                          '0') + 
                          ' ' + 
                          orderCommissionCurrency 

        if (orderIsEdited && this.commissionEnabledState()) {       
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Сумма комиссии</div>
                    <div className='form-input__element-group'>
                        <SumInput   className='fi__input fi__element form-control'
                                    id='id_fi-order-comm-sum' 
                                    value= { orderCommissionSum }
                                    onElementChange = { onSumChange }
                                    fractionDigits = { 2 } 
                                    prohibitNegativeValues = { true }
                                    onValidation = { onValidation }
                                    ref = { this.commissionSumRef }

                        />
                        <div className='fi__label_noborder-pt0'>
                            { orderCommissionCurrency }
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className='form-input__block'>
                    <div className='fi-block__header'>Сумма комиссии</div>
                    <div className='form-input__element-group'>
                        <div className='fi__element fi__label_noborder' 
                             id='id_fi-order-comm-sum_ro'>
                            {  strData } 
                        </div>
                    </div>
                </div>
            );
        }    
        
    }

    render() {
        const { orderIsEdited } = this.props;
        return (
            <div className='form-input__row'>
                { this.renderCommissionType(orderIsEdited) } 
                { this.renderCommissionPercent(orderIsEdited) }
                { this.renderCommissionSum(orderIsEdited) }
            </div>
        )
    }
}

OrderBodyCommission.propTypes = {
    orderId: PropTypes.number,
    orderIsEdited: PropTypes.bool.isRequired,
    orderBodyData: PropTypes.instanceOf(Map),
    onChange: PropTypes.func.isRequired,
    onSumChange: PropTypes.func.isRequired,
    onValidation: PropTypes.func
}

export default OrderBodyCommission;