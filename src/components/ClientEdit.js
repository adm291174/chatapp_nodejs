import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { showElementValidityState } from '../core/validation_functions';
import { ClientEditPayReqs } from './ClientEditPayReqs';
import { ClientEditAccounts } from './ClientEditAccounts';
import { ClientEditUsers } from './ClientEditUsers';


class ClientEdit extends Component {

    handleClick(e) {
        e.preventDefault();
        if (e.currentTarget.id == 'id_client-edit-btn-cancel') {
            // switch to clients list 
            this.props.dispatch( {
                type: 'APP_CONTEXT_SET',
                payload: { window: 'clients' }
            });
        }
        if (e.currentTarget.id == 'id_client-edit-btn-save') {
            // switch to clients list 
            this.onEditClientSubmit(e);
        }

    }

    onEditClientSubmit(e) {
        e.preventDefault();
        console.log('Submit');
        if (this.validateForm()) {
            const { clientId, clientName, isBuyer, isSupplier } = this.props;
            const closeForm = !(e.target.id == 'id_client-edit-btn-save');
            this.props.dispatch( {
                type: 'CLIENT_DATA_SAVE',
                payload: { clientId: clientId ? clientId : null,
                           clientName: clientName,
                           isBuyer: isBuyer,
                           isSupplier: isSupplier,
                           closeForm: closeForm }
            });
        }
    }
    

    validateElement(e, elementId) {
        const validatedLement = e ? e.target : document.getElementById(elementId),
              validatedLementId = elementId ? elementId : e.target.id;

        switch(validatedLementId) {
            case 'id_fi-client-name': {
                const clientNameState = (this.props.clientName.length > 0);
                showElementValidityState(validatedLement, clientNameState);
                return clientNameState;
            }
        }
        
    }

    validateForm() {
        const clientNameValid = this.validateElement(null, 'id_fi-client-name');
        if (!clientNameValid) {
            const clientNameElement = document.getElementById('id_fi-client-name');
            clientNameElement.focus();
            return false;
        }
        return true;
    }    

    onChange(e) {
        const { dispatch } = this.props, 
              element = e.currentTarget, 
              elementId = element.id,
              windowName = 'clientEdit';

        switch (elementId) {
            case 'id_fi-client-name': {
                dispatch( {
                    type: 'WINDOW_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        fieldName: 'clientName',
                        fieldValue: element.value
                    }
                });
                break;                 
            }
            case 'id_fi-client-type-1': {
                dispatch( {
                    type: 'WINDOW_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        fieldName: 'isBuyer',
                        fieldValue: element.checked
                    }
                });
                break;                 
            }
            case 'id_fi-client-type-2': {
                dispatch( {
                    type: 'WINDOW_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        fieldName: 'isSupplier',
                        fieldValue: element.checked
                    }
                });
                break;                 
            }
        }
    }

    componentDidMount() {

    }

    render() {
        const { clientId, clientName, isBuyer, isSupplier, 
                clientPaymentRequisits, clientAccounts, clientUsers, dispatch } = this.props;

        
        return (
            <div className='main-block__body'>
                <form id='id_form-client' onSubmit={this.onEditClientSubmit.bind(this)}>
                    <div className='form-input form-input-client'>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi-block__header'>Наименование клиента</div>
                                <div className='form-input__element-group'>
                                <input className='fi__input fi__element fi__element_wide form-control' 
                                           id='id_fi-client-name'
                                           onChange={ this.onChange.bind(this) } 
                                           onBlur={ this.validateElement.bind(this) } 
                                           value={ clientName } />
                                </div>
                            </div>
                        </div>
                        <div className='form-input__row'>
                            <div className='form-input__block'>
                                <div className='fi-block__header'>Тип клиента</div>
                                <div className='form-input__element-group'>
                                    <input type='checkbox' 
                                           className='form-control fi__element_override-form-control fi__element_supress-height'
                                           id='id_fi-client-type-1' checked={ isBuyer } 
                                           onChange={ this.onChange.bind(this) }     
                                           />
                                    <label className='fi__label_smallfont_spt' 
                                           htmlFor='id_fi-client-type-1'>Покупатель</label>
                                </div>
                                <div className='form-input__element-group'>
                                    <input type='checkbox' 
                                           className='form-control fi__element_override-form-control fi__element_supress-height' 
                                           id='id_fi-client-type-2' 
                                           checked={ isSupplier } 
                                           onChange={ this.onChange.bind(this) } 
                                           />
                                    <label className='fi__label_smallfont_spt' htmlFor='id_fi-client-type-2'>Поставщик</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='main-block__buttons'>
                        <button type='submit' className='btn main-block__btn'
                                id='id_client-edit-btn-save-and-exit'>Сохранить и выйти</button>
                        <button type='submit' className='btn main-block__btn'
                                id='id_client-edit-btn-save'
                                onClick={ this.handleClick.bind(this) }>Сохранить</button>
                        <button type='button' className='btn main-block__btn main-block__btn-cancel'
                                onClick={ this.handleClick.bind(this) }
                                id='id_client-edit-btn-cancel'>Отмена</button>
                    </div>
                </form>
                    <ClientEditPayReqs clientId = { clientId } 
                                       clientName = { clientName }  
                                       clientPaymentRequisits = { clientPaymentRequisits }
                                       dispatch = { dispatch }

                    />
                    <ClientEditAccounts clientId = { clientId } 
                                       clientAccounts = { clientAccounts }
                    />
                    <ClientEditUsers clientId = { clientId }
                                     clientName = { clientName }  
                                     clientUsers = { clientUsers }
                                     dispatch = { dispatch }
                    />
            </div>
            );
        
    }
}

ClientEdit.propTypes = {
    clientId: PropTypes.number,
    clientName: PropTypes.string,
    isBuyer: PropTypes.bool,
    isSupplier: PropTypes.bool
}


function mapStateToProps(state, ownProps) {
    const windowName = ownProps.windowName;
    const rootState = state.get('root').data;
    const windowContext = rootState.get('appContext').get('windowContext').get(windowName);
    const windowContent = windowContext.get('windowContent');
    // const clientState = state.get('clients').data;
    return {
        clientId: + windowContent.get('clientId'),
        clientName: windowContent.get('clientName'),
        isBuyer: windowContent.get('isBuyer'), 
        isSupplier: windowContent.get('isSupplier'),
        clientPaymentRequisits: windowContent.get('clientPaymentRequisits'),
        clientAccounts: windowContent.get('clientAccounts'),
        clientUsers: windowContent.get('clientUsers')
        // userRoles: rootState.get('userRoles')        
    }

}

export default connect(mapStateToProps)(ClientEdit);
