import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { FilterClientsList } from './FilterCleintsList';
import { FilterClientAddUser } from './FilterClientAddUser';
import { FilterOrdersList } from './FilterOrdersList';
import { FilterOperationsList } from './FilterOperationsList';
import { FilterReportsList } from './FilterReportsList';
import { FilterReport } from './FilterReport';
import { FilterUsersList } from './FilterUsersList';


class AppWindowFilterAlt extends Component {
    constructor(props) {
        super(props);
    }

    getElementData(element) {
        const elementType = element.nodeName;
        switch (elementType) {
            case 'INPUT': 
                return element.value;
            case 'SELECT': {
                return element.options[element.selectedIndex].value;
            }
            default:
                return undefined;
        }
    }

    changeData(elementId, clearValue = false, explicitData = undefined) {
        /* set changed data 
           pass clearValue == true to clear value, 
           pass explicitData to bypass getElementData call (For DatePicker component)
        */
        const { windowName, dispatch, reportId } = this.props;
        const element = document.getElementById(elementId);
        let value = explicitData;
        if (explicitData === undefined) 
            value = clearValue ? '' : this.getElementData(element);
        const elementName = element.getAttribute('name').toLowerCase();
        if (clearValue || value != undefined) {
            if (windowName == 'report') {
                dispatch({
                    type: 'FILTER_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        subFilterId: reportId,
                        filterElementName: elementName,
                        filterData: value
                    }
                })
            }
            else {
                dispatch({
                    type: 'FILTER_DATA_CHANGED',
                    payload: {
                        windowName: windowName,
                        filterElementName: elementName,
                        filterData: value
                    }
                })
            }
        }
    }

    onElementChange(e) {
        // determine which component changed 
        e.preventDefault();
        const elementId = e.target.id;
        this.changeData(elementId);
    }

    onFilterClearClick(e) {
        e.preventDefault();
        const element = e.currentTarget;
        const clearedElementId = element.getAttribute('for');
        if (clearedElementId) {
            this.changeData(clearedElementId, true);
            document.getElementById(clearedElementId).focus();
            
        }
    }

    onDatePickerChange(elementId, dateValue) {
        // get Date object as dateValue argument! 
        this.changeData(elementId, false, dateValue);
    }

    render() {
        const { windowName, filterContext, dispatch, isManager, isClient, reportId } = this.props;
        switch (windowName) {
            case 'clients': {
                return ( <FilterClientsList 
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }
                   getElementData={ this.getElementData.bind(this) }
                   dispatch={ dispatch }
                   isManager={ isManager } 
                   isClient={ isClient } 
                /> );
            }
            case 'clientEditAddUser': {
                return ( <FilterClientAddUser 
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }
                   getElementData={ this.getElementData.bind(this) }
                /> );
                
            }
            case 'ordersList': {
                return ( <FilterOrdersList
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }
                   getElementData={ this.getElementData.bind(this) }
                   onDatePickerChange={ this.onDatePickerChange.bind(this) }
                   dispatch={ dispatch }
                /> );
                
            }
            case 'operationsList': {
                return ( <FilterOperationsList
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }
                   getElementData={ this.getElementData.bind(this) }
                   onDatePickerChange={ this.onDatePickerChange.bind(this) }
                   dispatch={ dispatch }
                /> );
                
            }
            case 'reportsList': {
                return ( <FilterReportsList
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }                   
                />
                );
            }
            case 'report': {
                return ( <FilterReport
                   reportId={ reportId }
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }
                   onDatePickerChange={ this.onDatePickerChange.bind(this) } 
                   dispatch={ dispatch }
                />
                );
            }
            case 'usersList': {
                return ( <FilterUsersList
                   filterContext={ filterContext } 
                   onElementChange={ this.onElementChange.bind(this) }
                   onFilterClearClick={ this.onFilterClearClick.bind(this) }
                   dispatch={ dispatch }
                />
                );
            }
            default: return null;
        }
    }
}

AppWindowFilterAlt.propTypes = {
    windowName: PropTypes.string.isRequired,
    filterContext: PropTypes.instanceOf(Map),
    reportId: PropTypes.string,
    dispatch: PropTypes.func.isRequired
}

export default AppWindowFilterAlt;