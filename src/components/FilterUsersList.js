/* Filter Component for Users List */

import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';


class FilterUsersList extends Component {

    onfilterButtonClick(e) {
        // Handler for other filter buttons click 
        e.preventDefault();
        const { dispatch } = this.props;        
        // New order edit window 
        dispatch( {type: 'APP_CONTEXT_SET', 
            payload: {
                window: 'userEdit',
                context: Map({
                    windowContent: Map({
                        userId: null
                    })
                })
            }
        });
    }

    componentDidUpdate(prevProps) {
        const prevFilterValues = prevProps.filterContext.get('filterValues'), 
              currentFilterValues = this.props.filterContext.get('filterValues');

        if (!currentFilterValues.equals(prevFilterValues)) {
            // perform database query 
            this.props.dispatch ( {
                type: 'GET_USERS_LIST_DATA',
                payload: {}
            })
        }
    }

    render() {
        const { filterContext, onElementChange, 
                onFilterClearClick } = this.props;

        const filterValues = filterContext.get('filterValues');

        const name = filterValues.get('users-filter__name');

        return (
            <form id='id_users-filter'>
                <div className='main-block__filter'>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Фильтр по имени</div>
                        <div className='mbf-block__element-group'>
                            <input type='text' className='form-control' placeholder='' 
                                   id='id_form-users-filter-name'
                                   onChange={ onElementChange }
                                   name='users-filter__name'
                                   value={ name }
                                   />
                            <div className='img-wrapper' 
                                 aria-label='Очистить'
                                 htmlFor='id_form-users-filter-name'
                                 onClick={ onFilterClearClick }>
                                <img src='static/images/filter-clear.png' alt='Filter clear' />
                            </div>
                        </div>
                    </div>
                    <div className='mbf__block mbf__block_align-end'>
                        <div className='mbf-block__element'>
                            <button type='button' className='btn'
                                    onClick={ this.onfilterButtonClick.bind(this) }>
                                Создать пользователя
                            </button>
                        </div>
                    </div>
                </div>                        
            </form>                
        );
    }
}

FilterUsersList.propTypes = {
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired
}

export { FilterUsersList };