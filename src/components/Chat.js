import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { processUIData, setChatFileName } from '../core/handler_functions';
import ChatMessage from './ChatMessage';
import { Map, List } from 'immutable';
import { getCSRFtoken } from '../core/core_functions';



const timeShift = 2.5;
const chatRefreshTimeout = 10000;

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messageIsSaving: false,
            timerId: undefined,
            scrollInfo: undefined
        }
    }    

    getChatInfo(action) {
        
        const { chatHistoryId, lastQueryTime } = this.props;

        if (action == 'refresh') {
            console.log('refreshing chat data (refresh)', chatHistoryId);
            let queryData =  {chat_history_id: chatHistoryId};
            if (lastQueryTime) {
                queryData.last_time = new Date(lastQueryTime - timeShift * 1000)
            }
            this.props.dispatch({
                type: 'CHAT_INFO_DATA_GET',
                payload: {
                    chatHistoryId: chatHistoryId,
                    actionKind: 'refresh',
                    queryData: queryData
                }
        
            })
        }
        if (action == 'get_before') {
            const messageList = this.props.messageList;
            console.log('refreshing chat data (get_before)', chatHistoryId);
            let queryData =  {chat_history_id: chatHistoryId,
                              minIdentifier: messageList.size ? messageList.get(0).get('messageId') : 1000000000 };
            this.props.dispatch({
                type: 'CHAT_INFO_DATA_GET',
                payload: {
                    chatHistoryId: chatHistoryId,
                    actionKind: 'get_before',
                    queryData: queryData
                }
        
            })
        }
    }

    getMoreMessages(event) {
        event.preventDefault();
        this.getChatInfo('get_before');
    }

    refreshChatData() {
        // regular update event for chat 
        const { chatHistoryId, chatIsActive } = this.props;
        if (chatHistoryId && chatIsActive) {
            this.getChatInfo('refresh');
        }
    }

    getChatScrollState() {
        /* 
           function for fixing chat scroll state 
           store it in structure (Map):
              offsetTop: document.getElementById('id_chat').offsetTop
              scrollTop: document.getElementById('id_chat').scrollTop
              elements: 
                List of elements values (Maps)
                    elementId: element DOM id 
                    bottom: -383
                    height: 161
                    left: 32
                    right: 409
                    top: -544
                    width: 377
                    x: 32
                    y: -544 
                    -     getBoundingClientRect values

        */
        const chatMessagesElement = document.getElementById('id_chat')
        const offsetTop = chatMessagesElement.offsetTop;
        const scrollTop = chatMessagesElement.scrollTop;

        const messagesList = this.props.messageList;
        let mesaagesElementsInfo = []
        messagesList.forEach(value => {
            const messageElement = document.getElementById(value.get('elementId'));
            if (messageElement) {
                const elementPos = messageElement.getBoundingClientRect();
                const { x, y, width, top, bottom, height, left, right } = elementPos;
                mesaagesElementsInfo.push(Map( {
                    elementId: messageElement,
                    x: x,
                    y: y, 
                    width: width, 
                    top: top,
                    bottom: bottom,
                    height: height, 
                    left: left, 
                    right: right
                }));
            }    
        });

        return Map({
            offsetTop: offsetTop,
            scrollTop: scrollTop,
            elements: List(mesaagesElementsInfo)
        })
    }

    saveNewMessage(event) {
        /* generate action for submitting new message */ 
        console.log('Start saving');
        event.preventDefault();
        
        const { messageIsSaving } = this.state;
        if (messageIsSaving) return false;

        this.setState( {messageIsSaving: true} );
        console.log('Saving now!');
        const { formIsSubmitting, chatHistoryId } = this.props;
        
        if (!formIsSubmitting) {
            // we submit only when we can do it 
            var formValue = new FormData();
            formValue.append('csrfmiddlewaretoken', getCSRFtoken());
            formValue.append('message_text', document.getElementById('id_chat-form-message-text').value);
            formValue.append('chathistory_id', chatHistoryId);

            // add file data
            formValue.append('file', document.getElementById('id_chat_form_file').files[0]);

            this.props.dispatch({
                type: 'CHAT_MESSAGE_SAVE',
                payload: {
                    chatHistoryId: chatHistoryId,
                    queryData: formValue
                }
            });
        }
    }

    getMessageText(chatHistoryId) {
        return chatHistoryId ? this.props.messageText : '';
    }    

    setBtnSubmitState(btnState) {
        let btnSubmit = document.getElementById('id_btn-message-submit');
        const btnDisabled = btnSubmit.hasAttribute('disabled');
        if (btnState && btnDisabled) {
            btnSubmit.removeAttribute('disabled')
        }
        else {
            if (!btnState && !btnDisabled) 
              btnSubmit.setAttribute('disabled', '');
        }
    }

    closeChat(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        dispatch({ 
            type: 'CHAT_WINDOW_CLOSE',
            payload: {
                source: 'closeBtnClick'
            }
        });
    }

    componentDidMount() {
        processUIData(this.props.dispatch); // set elements' dimensions on component mount
        this.setBtnSubmitState(false); // disable message button on component mount
        this.getChatInfo('refresh'); // get first iterations of data
        var timerId = setInterval(this.refreshChatData.bind(this), chatRefreshTimeout);
        this.setState ( {timerId: timerId} );
    }

    componentDidUpdate(prevProps) {
        const { chatHistoryId, formIsSubmitting, lastSaveWasFine, messageList } = this.props;
        const prevChatHistoryId = prevProps.chatHistoryId;
        
        // Context of chat has been changed!  
        if (chatHistoryId && chatHistoryId != prevChatHistoryId) {
            // check chat context in state, set if not set 
            this.setState(() => ( { messageIsSaving: false } ));
            if (this._input) { this._input.focus(); }            
            this.getChatInfo('refresh');
        }
        
        if (prevProps.formIsSubmitting && !formIsSubmitting) {
            this.setState(() => ( { messageIsSaving: false } ));
            // update chat after message saving 
            if (lastSaveWasFine) this.getChatInfo('refresh'); 
        }
        // Checking when chatHistoryId is valid 
        const btnSubmitState = (chatHistoryId != undefined && 
                                !this.state.messageIsSaving && 
                                (this.getMessageText(chatHistoryId).trim().length > 0))
        this.setBtnSubmitState( btnSubmitState );

        const prevMessageList = prevProps.messageList;
        if (!messageList.equals(prevMessageList)) {
            if (prevMessageList.size == 0 && messageList.size > 0) {
                const lastMessage = messageList.get(messageList.size-1);
                document.getElementById(lastMessage.get('elementId')).scrollIntoView();
            }
            else {
                if (prevMessageList.size > 0 && messageList.size > 0) {
                    const prevMinMessageId = prevMessageList.get(0).get('messageId');
                    const minMessageId = messageList.get(0).get('messageId');
                    if (minMessageId < prevMinMessageId) {
                        const firstMessage = messageList.get(0);
                        document.getElementById(firstMessage.get('elementId')).scrollIntoView();
                    }
                    else {
                        const prevMaxMessageId = prevMessageList.get(prevMessageList.size-1).get('messageId');
                        const maxMessageId = messageList.get(messageList.size-1).get('messageId');
                        if (maxMessageId > prevMaxMessageId) {
                            const lastMessage = messageList.get(messageList.size-1);
                            document.getElementById(lastMessage.get('elementId')).scrollIntoView();
                        }    
                    }
                }
            }
        }
    }


    componentWillUnmount() {
        if (this.state.timerId) {
            clearInterval(this.state.timerId);
        }
    }


    onMessageTextUpdate(e) {
        const text = e.target.value;

        const { dispatch, chatHistoryId, messageText } = this.props;
        if (chatHistoryId && messageText != text) {
            dispatch({ type: 'CHAT_MESSAGE_TEXT_UPDATE',
                       chatHistoryId: chatHistoryId, 
                       payload: text});
        }
        e.preventDefault();
    }

    onFileContentChange(e) {
        // Handle file input change 
        const fileName = e ? e.target.value :  document.getElementById('id_chat_form_file').value;
        setChatFileName(fileName);        
    }

    onBtnCLearClick() {
        /* Handle clear btn click 
           Pure event handler method: expects event variable at call 
        */
        document.getElementById('id_chat_form_file').value = '';
        this.onFileContentChange();        
    }
    
    
    onChatScroll() {
        const messagesScrollInfo = this.getChatScrollState();
        this.setState( { scrollInfo: messagesScrollInfo });
    }

    renderMoreMessages(hasData, totalMessages, hasPreviousMessages) {
        if (!hasData) {
            return <div className='chat__message'>Осуществляется обновление данных</div>
        }
        if (!totalMessages) {
            return (<div className='chat__message'>Нет сообщений в этой переписке</div>)    
        }
        if (hasPreviousMessages) {
        return (
            <div className='chat__message'>
                <a href='void()' onClick={ this.getMoreMessages.bind(this) }>
                    Получить более ранние сообщения</a>
            </div>
        )
        }
        return null;
    } 

    renderMessages() {
        const messagesList = this.props.messageList;
        
        return messagesList.map((item) => { 
                const { messageId, elementId, created, modified, messageText, messageFileId, 
                        messageFileName, messageAuthor, chatHistoryId, messageAuthorText, authorIsMe } = item.toJS();

                return ( 
                   <ChatMessage key={elementId} 
                                messageId = {messageId} 
                                elementId = {elementId}
                                created = {created}
                                modified = {modified}
                                messageText = {messageText}
                                messageFileId = {messageFileId}
                                messageFileName = {messageFileName}
                                messageAuthor = {messageAuthor}
                                chatHistoryId = {chatHistoryId}
                                messageAuthorText = {messageAuthorText}
                                authorIsMe = {authorIsMe}/>
                );
            });
    }

    renderFooter() {
        
        const { chatHistoryId } = this.props;
        const messageText = this.getMessageText(chatHistoryId);

        return (
            <div className='chat-footer' id='id_chat-footer'>
                <form name='chat-add-message' method='post'
                    encType='multipart/form-data' id='id_form-new-message'> 
                    {/* <!-- {% csrf_token %} --> */ }
                    <div className='chat-form-block'>
                        <div className='input-group'>
                            <textarea autoFocus={true} className='form-control' 
                                      aria-label='With textarea'
                                      value = { messageText }
                                      onChange = { this.onMessageTextUpdate.bind(this) }
                                      ref={(c) => this._input = c} 
                                      id='id_chat-form-message-text'/>
                                <div className='input-group-append rounded-right'>
                                    <button className='btn btn-primary chat-form-block__button' 
                                    type='button' id='id_btn-message-submit'
                                    onClick={ this.saveNewMessage.bind(this) }>Отправить</button>
                                </div>
                        </div>
                    </div>    
                    <div className='chat-form-block'>
                        <div className='input-group'>
                            <div className='input-group-prepend'>
                                <button className='btn btn-outline-secondary element-hidden' 
                                        type='button' 
                                        id='id_chat-form-btn-clear'
                                        onClick={ this.onBtnCLearClick.bind(this) }>Очистить</button>
                            </div>
                            <div className='custom-file'>
                                <input type='file' className='custom-file-input' 
                                       id='id_chat_form_file'                                       
                                       onChange={this.onFileContentChange.bind(this)}
                                aria-describedby='id_file-name-label' />
                                <label className='custom-file-label' id='id_file-name-label'
                                    htmlFor='id_chat_form_file'>Выберите файл</label>
                            </div>
                        </div>
                    </div>            
                </form>
            </div>
        );    
    }

    render() {
        const { messageList, chatInfo, activeWindow } = this.props;
        const hasData = chatInfo.size;
        const totalMessages = messageList.size;
        const hasPreviousMessages = chatInfo.size && messageList.size 
                                    ? messageList.get(0).get('messageId') > chatInfo.get('minIdentifier')
                                    : false,
              windowActivated = (activeWindow && activeWindow.length > 0);
        console.log('active window prop', windowActivated);
        return (
            <div className='chat' id='id_chat' onScroll={ this.onChatScroll.bind(this)}>
                <div className='chat-wrapper' id='id_chat-wrapper'>
                    { this.renderMoreMessages(hasData, totalMessages, hasPreviousMessages) }
                    { this.renderMessages() }
                </div>
                { this.renderFooter() }
                { windowActivated ? (
                <div className='chat-close' id='id_chat-close-btn'
                     onClick={ this.closeChat.bind(this) }>  
                    <img src='/static/images/close-icon.png' />
                </div>) : null }
            </div>
        )
    }
}


Chat.propTypes = {
    chatHistoryId: PropTypes.number.isRequired,
    chatIsActive: PropTypes.bool,
    chatInfo: PropTypes.instanceOf(Map),
    messageList: PropTypes.instanceOf(List), 
    formIsSubmitting: PropTypes.bool,
    lastSaveWasFine: PropTypes.bool,
    lastSaveResult: PropTypes.object,
    lastQueryTime: PropTypes.instanceOf(Date)
}

function mapStateToProps(state, ownProps) {
    let chatData = state.get('chat').data;
    const rootAppContext = state.get('root').data.get('appContext'),
          activeWindow = rootAppContext.get('activeWindow');
    const chatHistoryId = ownProps.chatHistoryId;
    const chatHistoryMap = chatData.get(ownProps.chatHistoryId);
    if (chatHistoryMap) {
        return {
            chatInfo: chatHistoryMap.get('chatInfo'),
            messageList: chatHistoryMap.get('messageList'),
            formIsSubmitting: chatHistoryMap.get('formIsSubmitting'),
            lastSaveWasFine: chatHistoryMap.get('lastSaveWasFine'),
            lastSaveResult: chatHistoryMap.get('lastSaveResult'),
            lastQueryTime: chatHistoryMap.get('lastQueryTime'),
            messageText: chatHistoryMap.get('messageText'),
            chatIsActive: rootAppContext.get('chatContext')
                                        .get('isActive') 
                          && (rootAppContext.get('chatContext')
                                            .get('chatHistoryId') == chatHistoryId),
            activeWindow: activeWindow
        }
    }
    else {
        return {
            chatInfo: Map({}),
            messageList: List(),
            formIsSubmitting: false, 
            lastSaveWasFine: undefined,
            lastSaveResult: undefined,
            lastQueryTime: undefined, 
            messageText: '',
            chatIsActive: rootAppContext.get('chatContext').get('isActive') && (rootAppContext.get('chatContext').get('chatHistoryId') == chatHistoryId),
            activeWindow: activeWindow
        }

    }
}



export default connect(mapStateToProps)(Chat);