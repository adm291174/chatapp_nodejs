import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { List, Map } from 'immutable';


class PaymentRequisistRecord extends Component {

    onRequisitEditClick(e) {
        e.preventDefault();
        const { requisitId, onRequisitHandle } = this.props;
        // Handle click to edit requisits 
        onRequisitHandle(requisitId);
    }

    render() {
        const { orgTaxNumber, orgName, bankName, isEnabled } = this.props;
        return (
            <div className='form-input__row client-payreq-info' > 
                <div className='client-payreq-info__tax-number'>
                    <a href='#' onClick= { this.onRequisitEditClick.bind(this) } >{orgTaxNumber}</a>
                </div>
                <div className='client-payreq-info__name'>{ orgName }</div>
                <div className='client-payreq-info__bank'>{ bankName }</div>
                <div className={ 'client-payreq-info__status ' + 
                                 ( isEnabled 
                                 ? 'client-payreq-info__status_active' 
                                 : 'client-payreq-info__status_inactive' ) } >
                                 { isEnabled ? 'Действует' : 'Не действует'}
                </div>
            </div>
        )
    }
}

PaymentRequisistRecord.propTypes = {
    requisitId: PropTypes.number.isRequired,
    orgTaxNumber: PropTypes.string.isRequired,
    orgName: PropTypes.string.isRequired,
    bankName: PropTypes.string.isRequired,
    isEnabled: PropTypes.bool.isRequired,
    onRequisitHandle: PropTypes.func.isRequired
}


class ClientEditPayReqs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: ''
        }
    }    

    onNewRequisitBtnClick(e) {
        // create new payment requisit
        e.preventDefault();
        this.onEditRequisitHandle(null); 
    }

    onEditRequisitHandle(requisitId) {
        // Dispatch edit requisit here 
        const { dispatch, clientId, clientName } = this.props;
        dispatch({
            type: 'APP_CONTEXT_SET',
            payload: {
                window: 'paymentRequsitEdit',
                context: Map({
                    windowContent: Map({
                        requisitId: requisitId,
                        clientId: clientId,
                        clientName: clientName
                    })
                })
            }
        });
    }

    onFilterChange(e) {
        e.preventDefault();
        this.setState( { filterText: e.currentTarget.value } );
    }

    onFilterClearBtnClick(e) {
        e.preventDefault();
        this.setState( { filterText: '' } );
    }

    renderPaymentRequisits(requisitsList) {
        if (requisitsList.size > 0) {
            return requisitsList.map(value => {

                const requisitId = value.get('requisitId'),
                    orgTaxNumber = value.get('orgTaxNumber'), 
                    orgName = value.get('orgName'), 
                    bankName = value.get('bankName'), 
                    isEnabled = value.get('enabled');

                return (
                    <PaymentRequisistRecord 
                    requisitId = { requisitId }
                    orgTaxNumber = { orgTaxNumber }
                    orgName = { orgName }
                    bankName = { bankName }
                    isEnabled = { isEnabled }
                    key = { requisitId}
                    onRequisitHandle = { this.onEditRequisitHandle.bind(this) }
                />);
            })
        }
        else return (
            <div className='form-input__row client-payreq-info' > 
                <p>Платежные реквизиты отсутствуют</p>
            </div>
        );
    }

    render() {
        const { clientId, clientPaymentRequisits } = this.props;
        const filterText = this.state.filterText.toLowerCase(); 

        if (clientId) {
            // render only when ClientId is defined 

            // filter payRequisits list 
            const requisitsList = (filterText.length > 0) 
                                ? 
                                   clientPaymentRequisits.filter(payReqValue => { 
                                       const orgName = payReqValue.get('orgName').toLowerCase(); 
                                       return (orgName.indexOf(filterText) != -1); 
                                   }) 
                                   : clientPaymentRequisits;
            
            return (
            <form id='id_form-client-payreq'>
                <div className='form-input form-input__payreq-info'>
                    <div className='form-input__row'>
                        <div className='form-input__block'>
                            <div className='fi__header-label fi__header-label_alone'>Платежные реквизиты</div>
                            <div className='form-input__element-group'>
                                <button type='button' className='btn main-block__btn form-control fi__element' 
                                        onClick={ this.onNewRequisitBtnClick.bind(this) }>Создать реквизит</button>
                                <input type='text' className='fi__element form-control' placeholder=''
                                       onChange={ this.onFilterChange.bind(this) } 
                                       value = { filterText } />
                                <div className='img-wrapper' aria-label='Очистить' onClick={ this.onFilterClearBtnClick.bind(this) }>
                                    <img className='img-wrapper__img' src='static/images/filter-clear.png' alt='Filter clear' />
                                </div>
                            </div>
                        </div>
                    </div>
                    { this.renderPaymentRequisits(requisitsList) }
                </div>
            </form>
            )
        }
        else {
            return null;
        }
    }
}

ClientEditPayReqs.propTypes = {
    clientId: PropTypes.number,
    clientName: PropTypes.string,
    clientPaymentRequisits: PropTypes.instanceOf(List),
    dispatch: PropTypes.func.isRequired
}

export { ClientEditPayReqs };