import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { validateMix, 
         validateMixElement, 
         showElementValidityState } from '../core/validation_functions';

import { prepareForSave, validatePasswordForm, minPasswordLength } from '../core/user_functions';

const minLoginLength = 4;


class UserEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            passwordNotMatched: false,
            passwordLength: null
        }

        this.validationRuleList = [
            {
                validationType: 'element',
                element: 'id_fi-user-login',
                rules: [{ name: 'noEmpty'}, 
                        { name: 'reCheck', 
                           reValue: '^[a-z0-9_]{' + minLoginLength.toString() + ',}$'
                        }]
            },
            {
                validationType: 'function',
                element: 'id_fi-user-password_part-1',
                functionLink: validatePasswordForm,
                callBackFunction: this.passwordValidationCallBack.bind(this)
            },
            {
                validationType: 'element',
                element: 'id_fi-user-last-name',
                rules: [{ name: 'noEmpty'}]
            },
            {
                validationType: 'element',
                element: 'id_fi-user-first-name',
                rules: [{}]
            },
            {
                validationType: 'element',
                element: 'id_fi-user-chat-name',
                rules: [{ name: 'noEmpty'}]
            }

        ];

        this.blurValidationDict = {
           'id_fi-user-login': {
                validationListId: 0
            },
            'id_fi-user-password_part-2': {
                validationListId: 1
            },
            'id_fi-user-last-name': {
                validationListId: 2
            },
            'id_fi-user-first-name': {
                validationListId: 3
            },
            'id_fi-user-chat-name': {
                validationListId: 4
            }

        };
        
    }

    dictionariesEmpty() {
        const { dictionaries } = this.props;
        return (!dictionaries.has('chatGroups') || !dictionaries.has('userGroups'));
    }

    validate(needToSetFocus) {
        const markValidationState = true;
        const validationState= this.internalValidate(markValidationState, needToSetFocus);
        return validationState;
    }    

    passwordValidationCallBack(showNotMatch, value, showConfirmPassword=null) {
        showConfirmPassword = showConfirmPassword ? 
                              showConfirmPassword : 
                              this.getPasswordBlockParams();
        
        this.setPasswordMatching(showNotMatch, showConfirmPassword);
        this.setPasswordLengthInfo(value ? value.length : null, showConfirmPassword);
    }

    setPasswordMatching(showNotMatch, showConfirmPassword) {
        this.setState( { passwordNotMatched: showConfirmPassword && showNotMatch });
    }

    setPasswordLengthInfo(passwordlength, showConfirmPassword) {
        this.setState( { passwordLength: showConfirmPassword ? 
                                         passwordlength : null });
    }

    internalValidate(markValidationState = false, needToSetFocus = false) {
        return validateMix(this.validationRuleList, needToSetFocus, markValidationState);
    }

    componentDidMount() {
        const { dispatch } = this.props;
        if (this.dictionariesEmpty()) {
            dispatch({
                type: 'GET_USERS_DICTIONARY_DATA',
                payload: null
            });
        }
    }

    onBlur(e) {
        const element = e.currentTarget, 
              elementId = element.id,
              validationRule = this.blurValidationDict[elementId];

        if (validationRule) {

            validateMixElement(this.validationRuleList[validationRule.validationListId],
                              true, 
                              false, 
                              true); 
        }

        if (elementId == 'id_fi-user-password_part-1') {            
            this.setPasswordLengthInfo(element.value.length);
        }
    }

    onChange(e) {
        const { dispatch } = this.props, 
              element = e.currentTarget, 
              elementId = element.id;

        let fieldName = null,
            fieldValue = null;

        switch (elementId) {
            case 'id_fi-user-login': {
                fieldName = 'login',
                fieldValue = element.value;
                break;
            }
            case 'id_fi-user-enabled': {
                fieldName = 'isActive',
                fieldValue = element.checked;
                break;
            }
            case 'id_fi-user-password_part-1': {
                const oldValue = this.props.userData.get('password');

                fieldName = 'password',
                fieldValue = element.value;
                if (fieldValue.length == 0 || 
                    (oldValue.length > 2) && fieldValue.length == 1) {
                    dispatch( {
                        type: 'USER_EDIT_DATA_CHANGED',
                            payload: {
                                fieldName: 'passwordConfirm',
                                fieldValue: ''
                            }
                    });
                    const { showConfirmPassword } = this.getPasswordBlockParams(fieldValue);
                    this.passwordValidationCallBack(false, 
                                                    fieldValue,
                                                    showConfirmPassword);
                    if (!showConfirmPassword) {
                        showElementValidityState(element, null);
                    }
                }
                break;                
            }
            case 'id_fi-user-password_part-2': {
                fieldName = 'passwordConfirm',
                fieldValue = element.value;
                break;
            }
            case 'id_fi-user-must-change-pwd': {
                fieldName = 'userMustChangePassword',
                fieldValue = element.checked;
                break;
            }
            case 'id_fi-user-last-name': {
                fieldName = 'lastName',
                fieldValue = element.value;
                break;
            }
            case 'id_fi-user-first-name': {
                fieldName = 'firstName',
                fieldValue = element.value;
                break;
            }
            case 'id_fi-user-chat-access': {
                fieldName = 'chatAccessEnabled',
                fieldValue = element.checked;
                break;
            }
            case 'id_fi-user-chat-name': {
                fieldName = 'chatVisibleName',
                fieldValue = element.value;
                break;
            }
            case 'id_fi-user-chat-group': {
                fieldName = 'chatGroupId',
                fieldValue = element.value == 'undefined' ?  null : + element.value;
                break;
            }
            default: {
                // process user group checkbox change
                if (elementId.includes('id_fi-user-group-')) {

                    const groupId = + element.getAttribute('data-group-id'),
                          userData = this.props.userData,
                          userGroups = userData.get('userGroups');

                    fieldName = 'userGroups',
                    fieldValue = userGroups;

                    if (element.checked) {
                        if (!userGroups.includes(groupId)) fieldValue = [...userGroups, groupId];
                    }
                    else {
                        if (userGroups.includes(groupId)) 
                           fieldValue = userGroups.filter(group => (group != groupId));
                    }
                }
            }
        }

        // dispatch if changes are detected 
        fieldName && dispatch( {
                        type: 'USER_EDIT_DATA_CHANGED',
                            payload: {
                                fieldName: fieldName,
                                fieldValue: fieldValue
                            }
                     });
    }

    onBtnClick(e) {
        e.preventDefault();
        const elementId = e.currentTarget.id;

        switch (elementId) {
            case 'id-fi_user-btn-save': {
                if (this.validate(true)) {
                    const { userData, dispatch } = this.props;
                    dispatch({
                        type: 'SAVE_USER_DATA',
                        payload: prepareForSave(userData)
                    });

                }
                break;
            }
            case 'id-fi_user-btn-cancel': {
                const { dispatch } = this.props;
                dispatch({
                    type: 'APP_CONTEXT_SET',
                    payload: {
                        window: 'usersList'
                    }
                });
                break;
            }
        }
    }
    
    renderFormHeader(userId, login, is_active, onChange, onBlur) {
        const loginElementName = 'id_fi-user-login' + (userId ? '__ro' : '');
        return (
            <div className='form-input__row'>
                <div className='form-input__block'>
                    <div className='fi-block__header'>Логин</div>
                    <input className='fi__input fi__element form-control' 
                            id={ loginElementName }
                            onChange={ onChange }
                            onBlur={ onBlur }
                            readOnly={ userId > 0 } 
                    value={ login }/>
                    <div className='fi-block__footer'>
                        { minLoginLength.toString() + 
                          ' символов минимум, латинские буквы в нижнем регистре и/или цифры' } 
                    </div>
                </div>
                <div className='form-input__block'>
                    <div className='form-input__element-group'>
                        <input type='checkbox' 
                               className='form-control 
                                          fi__element_override-form-control
                                          fi__input_checkbox'
                               onChange={ onChange }                 
                               id='id_fi-user-enabled' 
                               checked={ is_active }/>
                        <label className='fi__label_smallfont' 
                               htmlFor='id_fi-user-enabled'>Активный</label>
                    </div>
                </div>
            </div>
        );
    }

    getPasswordBlockParams(password = undefined, userId = undefined) {
        const password_ = (password == undefined) ? 
                           this.props.userData.get('password') : password,
              userId_ = (userId == undefined) ? 
                           this.props.userId : userId;

        const passwordEmpty = (!password_ || password_.length == 0),
              showConfirmPassword = (!userId_ || (!passwordEmpty && userId_));

        return { passwordEmpty, showConfirmPassword }
    }

    renderPasswordBlock(password, 
                        passwordConfirm, 
                        userMustChangePassword,
                        onChange,
                        onBlur,
                        userId) {
        
        const { passwordEmpty, 
                showConfirmPassword } = this.getPasswordBlockParams(password, userId),
              { passwordNotMatched, 
                passwordLength } = this.state;

        return (
            <div className='form-input__row__wrapper'> 
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Пароль</div>
                        <input className='fi__input fi__element form-control' 
                               id='id_fi-user-password_part-1'
                               value={ password }
                               type='password'
                               onChange={ onChange } 
                               onBlur={ onBlur }
                               />
                    </div>
                    { showConfirmPassword ? 
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Подтверждение пароля</div>
                        <input className='fi__input fi__element form-control' 
                               id='id_fi-user-password_part-2'
                               value={ passwordConfirm } 
                               type='password'
                               onChange={ onChange }
                               onBlur={ onBlur } />
                    </div> : null }
                </div>
                { showConfirmPassword && passwordNotMatched ? 
                <div className='form-input__row form-input__row__warning'>
                    Пароль и его подтверждение не совпадают
                </div> : null }
                { passwordLength && passwordLength < minPasswordLength ? 
                <div className='form-input__row form-input__row__warning'>
                    { 'Длина пароля должна быть не менее ' + 
                       minPasswordLength.toString() + ' символов' }
                </div> : null }
                { showConfirmPassword ? 
                <div className='form-input__row'>
                    <div className='form-input__block form-input__block-no-padding-top'>
                        <div className='form-input__element-group'>
                            <input type='checkbox' 
                                   className='form-control 
                                              fi__element_override-form-control 
                                              fi__input_checkbox'
                                   id='id_fi-user-must-change-pwd' 
                                   checked={ userMustChangePassword }
                                   onChange={ onChange } 
                                   disabled={ true }
                                   />
                            <label className='fi__label_smallfont 
                                              fi__label_noborder-wide' 
                                   htmlFor='id_fi-user-must-change-pwd'>
                                   Пользователь должен сменить пароль при следующем входе
                            </label>
                        </div>
                    </div>
                </div> : null }
                { (userId && !passwordEmpty) ? 
                <div className='form-input__row form-input__row__warning'>
                    Внимание! Пароль пользователя будет изменен!
                </div> : null }
            </div>
        );
    }

    renderUserName(lastName, firstName, onChange, onBlur) {
        return (
            <div className='form-input__row__wrapper'>
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Фамилия</div>
                        <input className='fi__input 
                                          fi__element form-control'
                               id='id_fi-user-last-name'
                               value={ lastName } 
                               onChange={ onChange } 
                               onBlur={ onBlur } />
                    </div>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>Имя</div>
                        <input className='fi__input 
                                          fi__element form-control' 
                                id='id_fi-user-first-name'
                                value={ firstName } 
                                onChange={ onChange } 
                                onBlur={ onBlur } />
                    </div>
                </div>
            </div>
        );
    }

    renderChatGroups(chatGroupsList) {
        return List().push(<option value={ 'undefined' } key={ 'undefined' }> { 'Не присвоена' }</option>)
                     .concat(chatGroupsList.map(value => {
            const groupId = value.get('groupId').toString(),
                  groupName =  value.get('groupName');
            return <option value={ groupId } key={ groupId }>{ groupName }</option>
        }));
    }

    renderChatSettings(chatAccessEnabled, 
                       chatVisibleName, 
                       chatGroupId,
                       chatGroupsList,  
                       onChange,
                       onBlur) {
        
        return (
            <div className='form-input__row__wrapper'>
                <div className='form-input__row 
                                form-input__row__block-title'>
                        Доступ к чату
                </div>
                <div className='form-input__row'>
                    <div className='form-input__block 
                                    form-input__block-no-padding-top'>
                        <div className='form-input__element-group'>
                            <input type='checkbox'
                                   className='form-control 
                                          fi__element_override-form-control
                                          fi__input_checkbox'
                                   id='id_fi-user-chat-access' 
                                   checked={ chatAccessEnabled } 
                                   onChange={ onChange } />
                            <label className='fi__label_smallfont
                                              fi__label_noborder-wide' 
                                   htmlFor='id_fi-user-chat-access'>
                                Доступ к чату разрешен
                            </label>
                        </div>
                    </div>
                </div>
                { chatAccessEnabled ? 
                <div className='form-input__row'>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>
                            Отображаемое имя
                        </div>
                        <input className='fi__input fi__element form-control' 
                               id='id_fi-user-chat-name'
                               value={ chatVisibleName }
                               onChange={ onChange } 
                               onBlur={ onBlur } 
                               />
                    </div>
                    <div className='form-input__block'>
                        <div className='fi-block__header'>
                            Группа чата
                        </div>
                        <select className='fi__input fi__element form-control' 
                                id='id_fi-user-chat-group'
                                onChange={ onChange }
                                value={ chatGroupId ? chatGroupId.toString() : 'undefined' }
                                onBlur={ onBlur } >
                                { this.renderChatGroups(chatGroupsList) }
                                
                        </select>
                    </div>
                </div> : null } 
            </div>
        );
    }

    renderGroup(groupId, groupName, isSelected, onChange) {
        const groupElementId='id_fi-user-group-' + groupId.toString();
        return (
            <li key={ groupElementId }>
                <div className='form-input__block 
                                form-input__block-no-padding-top'>
                    <div className='form-input__element-group'>
                        <input type='checkbox' 
                            className='form-control 
                                        fi__element_override-form-control 
                                        fi__input_checkbox'
                            id={ groupElementId }   
                            data-group-id={ groupId.toString() }
                            checked={ isSelected }
                            onChange={ onChange } />
                        <label className='fi__label_smallfont
                                          fi__label_noborder-wide' 
                               htmlFor={ groupElementId }>{ groupName }</label>
                    </div>
                </div>
            </li>
        );    
    }

    renderUsersGroups(userGroups, userGroupsList, onChange) {
        return (
            <div className='form-input__row'>
                <ul className='form-input__row__ul'>
                    { userGroupsList.map( value => {
                        const groupId = value.get('groupId'),
                              groupName = value.get('groupName'),
                              isSelected = userGroups.includes(groupId);

                        return this.renderGroup(groupId, 
                                                groupName,
                                                isSelected,
                                                onChange)
                    }) }
                </ul>
            </div>                        
        );
    }

    renderButtons() {
        return (
            <div className='main-block__buttons'>
                <button type='button' 
                        className='btn main-block__btn main-block__buttons_btn_mr2'
                        onClick={ this.onBtnClick.bind(this) }
                        id='id-fi_user-btn-save'>Сохранить</button>
                <button type='button' 
                        className='btn main-block__btn main-block__btn-cancel'
                        onClick={ this.onBtnClick.bind(this) }
                        id='id-fi_user-btn-cancel'>Отмена</button>
            </div>
        );
    }

    render() {
        const { userData, userId } = this.props;
        const userDataId = userData.get('userDataId');
            
        if (!this.dictionariesEmpty() && userDataId !== undefined) {
            const onChange = this.onChange.bind(this),
                  onBlur = this.onBlur.bind(this),
                  login = userData.get('login'),
                  isActive = userData.get('isActive'),
                  password = userData.get('password'),
                  passwordConfirm = userData.get('passwordConfirm'),
                  userMustChangePassword = userData.get('userMustChangePassword'),
                  lastName = userData.get('lastName'),
                  firstName = userData.get('firstName'),
                  chatAccessEnabled = userData.get('chatAccessEnabled'),
                  chatVisibleName = userData.get('chatVisibleName'),
                  chatGroupId = userData.get('chatGroupId'),
                  userGroups = userData.get('userGroups');
            
            const { dictionaries } = this.props,
                    chatGroupsList = dictionaries.get('chatGroups'),
                    userGroupsList = dictionaries.get('userGroups');
                
            return (
                <div className='main-block__body'>
                    <form id='id_form-user'>
                        { this.renderFormHeader(userId, login, isActive, onChange, onBlur) }
                        { this.renderPasswordBlock(password, 
                                                   passwordConfirm, 
                                                   userMustChangePassword,
                                                   onChange,
                                                   onBlur,
                                                   userId) }
                        { this.renderUserName(lastName, firstName, onChange, onBlur) }
                        { this.renderChatSettings(chatAccessEnabled, 
                                                  chatVisibleName, 
                                                  chatGroupId,
                                                  chatGroupsList,  
                                                  onChange,
                                                  onBlur) }
                        <div className='form-input__row 
                                        form-input__row__block-title'>
                             Группы пользователя
                        </div>
                        { this.renderUsersGroups(userGroups, 
                                                 userGroupsList,
                                                 onChange) }
                        { this.renderButtons() }
                    </form>
                </div>
            );
        }
        else return <div className='main-block__body'>Идет загрузка данных...</div>;
    }
}

UserEdit.propTypes = {
    userId: PropTypes.number
}

const mapStateToProps = function(state, ownProps) {
    const windowName = ownProps.windowName,
          rootState = state.get('root').data,
          windowContext = rootState.get('appContext').get('windowContext').get(windowName),
          windowContents = windowContext.get('windowContent');          
    
    const usersState = state.get('users').data;
          
    const userId = windowContents.get('userId')

    return {
        userId: userId,
        dictionaries: usersState.get('dictionaries'),
        userData: usersState.get('userData')
    }
}

export default connect(mapStateToProps)(UserEdit);