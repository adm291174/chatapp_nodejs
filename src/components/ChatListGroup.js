import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { capitalizeFirstLetter } from '../core/core_functions'
import ChatListRecord from './ChatListRecord';
import { Map } from 'immutable';
 
class ChatListGroup extends Component {
    constructor(props) {
        super(props);        
    }

    renderValues(itemsList, handleChatClick, activeChatId, chatIsActive) {
        /* 
            Use tis function to render ChatListRecords
        */ 
        

        const values = itemsList.map(value => {
            const { chatId, chatHistoryId, lastMessageTime, multiChat,
                    lastView, visibleToUser, visibleName, oUserId,
                    oUserName, oGroupId, oGroupName, oCssGroup, oIcon,
                    unreadMessagesCnt, lastMessageTimeDate, lastViewDate,
                    timeCategoryId, elementId } = value.toJS();
            return (
                <ChatListRecord 
                    key={elementId}
                    chatId={chatId} 
                    chatHistoryId={chatHistoryId}
                    lastMessageTime={lastMessageTime}
                    multiChat={multiChat}
                    lastView={lastView}
                    visibleToUser={visibleToUser}
                    visibleName={visibleName}
                    oUserId={oUserId}
                    oUserName={oUserName}
                    oGroupId={oGroupId}
                    oGroupName={oGroupName}
                    oCssGroup={oCssGroup}
                    oIcon={oIcon}
                    unreadMessagesCnt={unreadMessagesCnt}
                    lastMessageTimeDate={lastMessageTimeDate}
                    lastViewDate={lastViewDate}
                    timeCategoryId={timeCategoryId}
                    elementId={elementId}
                    handleChatClick={handleChatClick}
                    chatIsActive={chatIsActive}
                    activeChatId={activeChatId}
                />
            )
        });
        return values;

    }

    filterValues(itemsList, filterText) {
        /* 
           Filter list of group with filter value 
           call this routine if this.props.filterEnabled == true 
        */
        let _filter = filterText.toLowerCase();
        return itemsList.filter( value => {
            let userName = value.get('oUserName').toLowerCase();
            return (userName.indexOf(_filter) > -1)
        })
    } 

    onClick(e) {
        let key = e.target.getAttribute('component-key');
        let state = this.props.collapsedState.get(key);
        if (key) {
            this.props.onGroupCategoryChange(key, !state);
        }
    } 

    renderChatListGroup() {
        /* 
        Render ChatListGroup 
        params:
            get all params from this.props 

        return:
            React component ready for rendering or 
                  null if no need to render this group 
        */

        const { renderedGroup, 
                filterEnabled, 
                filterText, 
                collapsedState, 
                handleChatClick,
                activeChatId,
                chatIsActive } = this.props;

        const { groupName, key } = renderedGroup.toJS();
        const groupValues = renderedGroup.get('groupValues');
        const visibleList = filterEnabled ? this.filterValues(groupValues, filterText) : groupValues

        if (visibleList.size) {
            
            let collapsed = collapsedState.get(key)
            if (collapsed == undefined) collapsed = true;
            const elementId = 'id_' + key;
            const collapsedClassName = 'collapsed-group-w collapse ' + (collapsed ? '' : 'show');
            return (
                <div className='chat-list-group-w' key={key} >
                    <div data-toggle='collapse'
                        data-target={'#' + elementId } aria-expanded={collapsed}
                        aria-controls={ elementId } role='group'
                        className='chat-list-group-w__control' 
                        onClick={this.onClick.bind(this)} 
                        component-key={key}>
                            <div className='triangle-right'
                                 onClick={this.onClick.bind(this)} 
                                 component-key={key}></div>
                            <div className='triangle-down'
                                 onClick={this.onClick.bind(this)} 
                                 component-key={key}></div>
                                { capitalizeFirstLetter(groupName)} 
                       
                    </div>
                    <div className={ collapsedClassName } 
                            id={'id_' + key}>
                        <ul className='chat-group'>
                            { this.renderValues(visibleList, 
                                                handleChatClick, 
                                                activeChatId,
                                                chatIsActive) }
                        </ul>
                    </div>
                </div>
            );
        }
        else return null;
    } 

    render() {
        // return null;
        return this.renderChatListGroup();
    }
}

ChatListGroup.propTypes = {
    renderedGroup: PropTypes.instanceOf(Map).isRequired,
    filterEnabled: PropTypes.bool.isRequired,
    filterText: PropTypes.string,
    collapsedState: PropTypes.instanceOf(Map).isRequired,
    onGroupCategoryChange: PropTypes.func.isRequired,
    handleChatClick: PropTypes.func.isRequired,
    chatIsActive: PropTypes.bool.isRequired,
    activeChatId: PropTypes.number
}

export default ChatListGroup; 
