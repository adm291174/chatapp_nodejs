import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';


class FilterClientAddUser extends Component {

    render() {
        const { filterContext, onElementChange, 
                onFilterClearClick } = this.props;

        const filterValues = filterContext.get('filterValues');

        const name = filterValues.get('clients-adduser-filter__name');
        const queryUsersState = filterValues.get('clients-adduser-filter__query-user-state');

        return (
            <form id='id_form-adduser-filter'>
                <div className='main-block__filter'>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Фильтр по имени</div>
                        <div className='mbf-block__element-group'>
                            <input type='text' className='form-control' placeholder='' 
                                   id='id_form-adduser-filter-name' 
                                   onChange={ onElementChange }
                                   name='clients-adduser-filter__name' 
                                   value={ name }
                                   />
                            <div className='img-wrapper' 
                                 aria-label='Очистить'
                                 htmlFor='id_form-adduser-filter-name'
                                 onClick={ onFilterClearClick }>
                                <img src='static/images/filter-clear.png' alt='Filter clear' />
                            </div>
                        </div>
                    </div>
                    <div className='mbf__block'>
                        <div className='mbf-block__header'>Статус пользователя</div>
                        <div className='mbf-block__element'>
                            <select className='form-control' 
                                    id='id_form-adduser-filter-status'
                                    onChange={ onElementChange }
                                    name='clients-adduser-filter__query-user-state'
                                    value={ queryUsersState }>                                    
                                <option value='not-linked'>Не привязанные к клиенту</option>
                                <option value='all-users'>Все пользователи</option>
                            </select>
                        </div>
                    </div>
                </div>                        
            </form>                
        );
    }
}

FilterClientAddUser.propTypes = {
    filterContext: PropTypes.instanceOf(Map).isRequired,
    onElementChange: PropTypes.func.isRequired,
    onFilterClearClick: PropTypes.func.isRequired,
    getElementData: PropTypes.func.isRequired
}

export { FilterClientAddUser };