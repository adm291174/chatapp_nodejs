import { Map } from 'immutable';

const buyOrderType = '6CB87DBE93F1493B8A30AF6B4E43D8AA',
      supplyOrderType = 'BE65C9F496D14EEE8CA32697F0C323A0';

const orderStatusNewGuid = '3589BA40AB884E4186D5EEEB62A5BBA7',
      orderStatusNewDescription = 'Введен';


const orderTypeCommissionTypes = [
    {
        commisionTypeGuid: '1A80A84E62EC477B90BFE1163F2715E5',
        commissionTypeDescription: 'Простая',
        commissionPercent: undefined,
        commissionDisabled: false
    },
    {
        commisionTypeGuid: '5D0F93851D444FC397FC997228CFA538',
        commissionTypeDescription: 'Без комиссии',
        commissionPercent: 0.0,
        commissionDisabled: true
    }
];

const orderTypeCommissionsMap = function () {
    let result = Map();
    orderTypeCommissionTypes.forEach(value => {
        result = result.set(value.commisionTypeGuid, {
            description: value.commissionTypeDescription,
            commissionPercent: value.commissionPercent,
            commissionDisabled: value.commissionDisabled
        });
    })
    return result;
}

const orderTypeCommissions = orderTypeCommissionsMap();

const orderTypes = [
    {
        orderType: buyOrderType,
        orderTypeDescription: 'Покупка'
    },
    {
        orderType: supplyOrderType,
        orderTypeDescription: 'Поставка'
    }
];

const orderTypesMap = function () {
    let result = Map();
    orderTypes.forEach(value => {
        result = result.set(value.orderType, value.orderTypeDescription)
    })
    return result;
}

const orderTypesById = orderTypesMap()

const emptyOrderBodyData = Map({
    orderNumber: '',
    orderType: buyOrderType, 
    orderTypeDescription: orderTypesById.get(buyOrderType),
    orderStatusId: orderStatusNewGuid,
    orderStatusDescription: orderStatusNewDescription,
    orderDate: undefined,
    orderAccountId: null,
    orderAccountCurrencyId: undefined,
    orderAccountCurrencyCode: 'RUB',
    orderAccountNumber: undefined,
    orderClientId: undefined,
    orderClientName: '',
    orderSum: undefined, 
    orderCommissionType: orderTypeCommissionTypes[0].commisionTypeGuid, 
    orderCommissionDescription: orderTypeCommissionTypes[0].commissionTypeDescription, 
    orderCommissionPercent: orderTypeCommissionTypes[0].commissionPercent, 
    orderCommissionSum: undefined,
    orderSecondarySum: undefined
});


export { orderTypeCommissionTypes, orderTypeCommissions,
         orderTypes,
         orderTypesById, 
         emptyOrderBodyData, 
         buyOrderType, supplyOrderType };