'use strict';
// Common used constants for project 

const useHotComponent = false;

// Chat list constants 
const filterGroupNames = ['byTime', 'byGroup'];

// Date constants 
const monthName = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 
                   'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

// time constants 
const chatListUpdateInterval = 10000;
const chatListRefreshTimeout  = 10000;

const appVersion = '2.1.0.3',
      appDate = '2019-02-21'


export { filterGroupNames, 
         chatListUpdateInterval,
         chatListRefreshTimeout, 
         monthName,
         useHotComponent,
         appVersion,
         appDate };
