const getConfig = {
    timeout: 10000
}

const putConfig = {
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',
    timeout: 10000
}

const postHtmlConfig = {
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',    
    timeout: 10000,
    headers: {
        'Content-Type': 'multipart/form-data'
    }
}

const postConfig = {
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',    
    timeout: 10000 
}

const postReportConfig = {
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',    
    timeout: 30000 
}

const bunchMessagesCount = 10;

/* Coherent pairs of values 
   key: name of filter 
   values: object of following structure
   { 
       pairType: 'high' of 'low' - type of current processing value 
       pairField: name of coherent filter field
   }

*/
const coherentPairs = {
    'orders-filter__start-date': {
            pairType: 'low',
            pairField: 'orders-filter__end-date'
        },
    'orders-filter__end-date': {
            pairType: 'high',
            pairField: 'orders-filter__start-date'
       },
    'operations-filter__start-date': {
            pairType: 'low',
            pairField: 'operations-filter__end-date'
        },
    'operations-filter__end-date': {
            pairType: 'high',
            pairField: 'operations-filter__start-date'
       },
    'report-filter__start-date': {
            pairType: 'low',
            pairField: 'report-filter__end-date'
       },
    'report-filter__end-date': {
            pairType: 'high',
            pairField: 'report-filter__start-date'
       }
}

/* orders statuses for query */

const ordersListStatusQueryParams = {
    'edit_status':  [ '3589BA40AB884E4186D5EEEB62A5BBA7', '415BA5137A324E03880C517124FDAF55' ],
    'all_statuses': []
}


export { getConfig,
         putConfig, 
         postHtmlConfig, 
         postConfig, 
         postReportConfig,
         bunchMessagesCount, 
         coherentPairs,
         ordersListStatusQueryParams };

