import React from 'react';
import { Map, List } from 'immutable';

import { buyOrderType, supplyOrderType } from './order_constants';
import PickerFilterDate from '../elements/picker/PickerFilterDate';

import { getDateStr } from '../core/core_functions';


/* field types 
  for text filters - 'filter' 
  for readOnly – 'readOnly'
  for data retrieving - 'dataImpact'
  group elements = 'group' (Field can render itself with behaviour) 
*/  

const orderPickerFilterFields = List([
    Map({
        fieldId: 'id-order-picker-filter-name',
        fieldHeader: 'Имя клиента',
        fieldElement: (id, value, onChange) => {
                            return <input className='form-control'
                                          id={ id}
                                          value = { value } 
                                          onChange = { onChange } ></input>
                      },
        fieldType: 'filter'
    }),
    Map({
        fieldId: 'id-order-picker-client-type-ro',
        fieldHeader: 'Тип клиента',
        fieldElement: (id, value) => {
                        return <div className='pc-header__elem_bordered' id={ id }>{ value }</div>
                      },
        fieldType: 'readOnly'
    })
]);

const orderPickerListColumns = Map({
   keyField: 'accountId',
   filteredField: 'clientName',  
   listFields: [
       { 
           fieldName: 'accountNumber',
           className: 'td__account-number'
       },
       { 
           fieldName: 'clientName',
           className: ''
       }
   ]
});


const linkedOrdersPickerFilterFields = List([
    Map({
        fieldId: 'id-order-linked-orders-picker-filter-name',
        fieldHeader: 'Имя клиента',
        fieldElement: (id, value, onChange) => {
                            return <input className='form-control'
                                          id={ id}
                                          value = { value } 
                                          onChange = { onChange } ></input>
                      },
        fieldType: 'filter'
    }),
    Map({
        fieldId: 'id-order-linked-orders-picker-client-type',
        fieldHeader: 'Тип клиента',
        fieldElement: (id, value, onChange) => {
                        return (
                            <select className='form-control' id={ id } value={value}  
                                    onChange = { onChange }>
                                <option value={ buyOrderType }>Покупатель</option>
                                <option value={ supplyOrderType }>Поставщик</option>
                            </select>
                        );    
                      },
        fieldType: 'dataImpact'
    }),
    Map({
        fieldId: 'id-order-linked-orders-picker-date',
        fieldHeader: 'Дата',
        fieldElement: (id, value, onValueChange ) => {
                        return <PickerFilterDate id={ id } value = {value} 
                               onChange = { onValueChange }
                               key={ id }
                               nextElementId = 'id-order-linked-orders-picker-linked-status'
                               prevElementId = 'id-order-linked-orders-picker-client-type'
                        />  
                      },
        fieldType: 'group'
    }),
    Map({
        fieldId: 'id-order-linked-orders-picker-linked-status',
        fieldHeader: 'Признак привязки ордеров',
        fieldElement: (id, value, onChange) => {
                        return <div className='pc-header__elem-group_line' key={ id }>
                                <input type='checkbox' 
                                       className='form-control pc-header__elem pc-header__elem_free' 
                                       id={ id } 
                                       checked = { value } 
                                       onChange = { onChange }
                                       />
                                <label className='pc-header__elem pc-header__elem pc-header__elem_free' 
                                       htmlFor={ id } >Без привязки к ордерам</label>
                            </div>;
                    },
        fieldType: 'group'
    })
]);

const linkedOrdersPickerListColumns = Map({
   keyField: 'orderId',
   filteredField: 'orderClientName',  
   listFields: [
       { 
           fieldName: 'orderNumber',
           className: '',
           transform: null
       },
       { 
           fieldName: 'orderDate',
           className: '',
           transform: (value) => (getDateStr(value))
       },
       { 
           fieldName: 'orderSumForPrint',
           className: 'td__align-right',
           transform: null, 
       },
       { 
           fieldName: 'orderClientName',
           className: ''
       }
   ]
});


const operationPickerAccsFilterFieldsFunc = function(filterFieldId, clientTypeFieldId) {
    return (
        List([
        Map({
            fieldId: filterFieldId,
            fieldHeader: 'Имя клиента',
            fieldElement: (id, value, onChange) => {
                            return <input className='form-control'
                                          key={ id }
                                          id={ id }
                                          value = { value } 
                                          onChange = { onChange } ></input>
                        },
            fieldType: 'filter'
        }),
        Map({
            fieldId: clientTypeFieldId,
            fieldHeader: 'Тип клиента',
            fieldElement: (id, value) => {
                            return <div className='pc-header__elem_bordered' id={ id }>{ value }</div>
                        },
            fieldType: 'readOnly'
        })
    ])
)};

const operationPickerAccsListColumns = Map({
   keyField: 'accountId',
   filteredField: 'clientName',  
   listFields: [
       { 
           fieldName: 'accountNumber',
           className: 'td__account-number'
       },
       { 
           fieldName: 'clientName',
           className: ''
       }
   ]
});


const operationPickerPayReqsFilterFields = function(filterFieldId, clientNameFieldId) {
    return (
        List([
            Map({
                fieldId: filterFieldId,
                fieldHeader: 'Имя организации',
                fieldElement: (id, value, onChange) => {
                                return <input className='form-control'
                                            id={ id }
                                            key={ id } 
                                            value = { value } 
                                            onChange = { onChange } ></input>
                        },
            fieldType: 'filter'
            }),
            Map({
                fieldId: clientNameFieldId,
                fieldHeader: 'Имя клиента',
                isWide: true,
                fieldElement: (id, value) => {
                                return <div className='pc-header__elem_bordered' 
                                        id={ id } key={ id }>{ value }</div>
                            },
                fieldType: 'readOnly'
            })
        ]))
};

const operationPickerPayReqsListColumns = Map({
    keyField: 'requisitId',
    filteredField: 'orgName',  
    listFields: [
        { 
            fieldName: 'payReqDescription',
            className: 'pc-main__row',
            transform: ({ requisitId, orgTaxNumber, orgName, accountNumber, bankName}) => {
                return (
                <div key={ requisitId }><strong>{ orgTaxNumber }</strong>
                     <br/>{ orgName }<br/>
                     <strong>{ accountNumber }</strong>
                     <br/>{ bankName }</div>);
            }
        }
    ]
});


const operationPickerLOFilterFieldsFunc = function(orderDataFieldId, 
                                               orderTypeFieldId,
                                               prevElementId, 
                                               nextElementId) {
    return List([
        Map({
            fieldId: orderDataFieldId,
            fieldHeader: 'Дата',
            fieldElement: (id, value, onValueChange ) => {
                            return <PickerFilterDate id={ id } value = {value} 
                                onChange = { onValueChange }
                                key={ id }
                                nextElementId={ prevElementId }
                                prevElementId={ nextElementId }
                            />  
                        },
            fieldType: 'group'
        }),
        Map({
            fieldId: orderTypeFieldId,
            fieldHeader: 'Тип ордера',
            fieldElement: (id, value) => {
                              return <div className='pc-header__elem_bordered' 
                                     id={ id } key={ id }>{ value }</div>
                          },
            fieldType: 'readOnly'
        })
    ]);
}

const operationPickerLOListColumns = Map({
   keyField: 'orderId',
   filteredField: null,
   listFields: [
       { 
           fieldName: 'orderNumber',
           className: 'td__tight',
           transform: null
       },
       { 
           fieldName: 'orderDate',
           className: 'td__tight',
           transform: (value) => (getDateStr(value))
       },
       { 
           fieldName: 'orderSumForPrint',
           className: 'td__sum td__align-right',
           transform: null, 
       },
       { 
           fieldName: 'empty',
           className: '' 
       }

   ]
});


const operationPickerAccsForTPListColumns = Map({
    /* Columns list definiton for account picker (OperationTransactionPart) */
    keyField: 'accountId',
    filteredField: 'accountName',  
    listFields: [
        { 
            fieldName: 'accountNumber',
            className: 'td__account-number'
        },
        { 
            fieldName: 'accountName',
            className: ''
        }
    ]
});

const operationPickerAccsForSOPListColumns = Map({
    /* Columns list definiton for account picker (OperationSimplePart) */
    keyField: 'accountId',
    filteredField: 'accountName',  
    listFields: [
        { 
            fieldName: 'accountNumber',
            className: 'td__account-number'
        },
        { 
            fieldName: 'accountName',
            className: ''
        }
    ]
});


const statementPickerAccsColumns = Map({
    /* Columns list definiton for account picker (Statement) */
    keyField: 'accountId',
    filteredField: 'accountName',  
    listFields: [
        { 
            fieldName: 'accountNumber',
            className: 'td__account-number'
        },
        { 
            fieldName: 'accountName',
            className: ''
        }
    ]
});

function accPickerFilterFields(filterFieldName) {
    return List([
        Map({
            fieldId: filterFieldName,
            fieldHeader: 'Название счета',
            fieldElement: (id, value, onChange) => {
                            return <input className='form-control'
                                        id={ id}
                                        value = { value } 
                                        onChange = { onChange } ></input>
                    },
        fieldType: 'filter'
        })
    ])
}



export { orderPickerFilterFields, orderPickerListColumns,
         linkedOrdersPickerFilterFields, linkedOrdersPickerListColumns,
         operationPickerAccsFilterFieldsFunc, operationPickerAccsListColumns,
         operationPickerPayReqsFilterFields, operationPickerPayReqsListColumns,
         operationPickerLOFilterFieldsFunc, operationPickerLOListColumns,
         operationPickerAccsForTPListColumns, operationPickerAccsForSOPListColumns,
         statementPickerAccsColumns, accPickerFilterFields };
