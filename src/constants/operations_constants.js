import { Map } from 'immutable';

const transferOperationType = '32DE629C3E4F440FA9A320622602C8F2',
      supplyOperationType = '474082881D4047EA9A3E8546BA45FCFA',
      settlementOperationType = 'DDFB366FD5A04471A0EB18886E557C50',
      simpleOperationType = 'AB59D801D95347E1B775F6DE48DA573C',
      allTypes = '63937BD553A6461A8DA6325503A5BB16',
      debetOperationPart = '412260F6F8F84B149B7FFE6A775E5836',
      creditOperationPart = '8A69565F932C4794AC10FC41B1FD8435';

const operationStatuses = {
    activeOperation: {
        statusId: '5D50218A945F480E9C4665B7C37510BA',
        statusDescription: 'Действующая'
    },
    inactiveOperation: {
        statusId: '81E8E42BB1024E0683F1A6790B8A1E5B',
        statusDescription: 'Не действующая'
    },
    
}

const operationTypes = [
    {
        orderType: transferOperationType,
        orderTypeDescription: 'Перевод'
    },
    {
        orderType: supplyOperationType,
        orderTypeDescription: 'Поставка товара'
    },
    {
        orderType: settlementOperationType,
        orderTypeDescription: 'Расчеты с клиентом'
    },
    {
        orderType: simpleOperationType,
        orderTypeDescription: 'Простая операция'
    },
    {
        orderType: allTypes,
        orderTypeDescription: 'Все типы'
    }
];                                

const operationTypesMap = function () {
    let result = Map();
    operationTypes.forEach(value => {
        result = result.set(value.orderType, value.orderTypeDescription)
    })
    return result;
}

const operationTypesById = operationTypesMap();

const operationImages = 
    Map().set(transferOperationType, 'static/images/client_to_client.png')
         .set(supplyOperationType, 'static/images/from_client.png')
         .set(settlementOperationType, 'static/images/to_client.png')
         .set(simpleOperationType, '')


const transferPartComponentDefs = {
    payerComponentPart: {
        accountPicker: {
            elementName: 'Номер счета плательщика',
            elementId: 'id_fi-payer-account-number',
            btnElementId: 'id_fi-payer-account-btn',
            elementValueId: 'buyerAccountId',
            elementValue: 'buyerAccountNumber',
            pickerElementId: 'id_fi-payer-account-picker',
            pickerFilterFieldId: 'id_fi-payer-account-picker-filter',
            pickerClientTypeFieldId: 'id_fi-payer-account-picker-client-ro'
        },
        accountClient: {            
            elementId: 'id_fi-payer-account-number-label',
            elementName: 'Наименование плательщика',
            elementValueId: 'buyerClientId',
            elementValue: 'buyerClientName'
        },
        linkedToOrder: {
            elementId: 'id_fi-payer-linked-order',
            btnElementId: 'id_fi-payer-linked-order-btn',
            btnClearElementId: 'id_fi-payer-linked-order-clear-btn',
            elementName: 'Связан с ордером',
            pickerElementId: 'id_fi-payer-linked-order-picker',
            elementValueId: 'buyerOrderId',
            orderInfoElement: 'buyerOrderInfo',
            orderNumberValue: 'orderNumber',
            orderDateValue: 'orderDate',
            pickerOrderTypeFieldId: 'id_fi-payer-lo-picker-order-type-ro',
            pickerDateFieldId: 'id_fi-payer-lo-picker-date'
        },
        operationSum: {
            elementLabelId: 'id_fi-operation-sum-sign',
            elementId: 'id_fi-transfer-operation-sum',
            elementName: 'Сумма операции',
            elementSign: 'minus',
            elementValue: 'operationSum',
            readOnly: false
        },
        paymentRequisit: {
            elementId: 'id_fi-payer-pay-req',
            btnElementId: 'id_fi-payer-pay-req-btn',
            elementName: 'Реквизиты плательщика',
            pickerElementId: 'id_fi-payer-pay-req-picker',
            elementValueId: 'buyerPaymentRequisitId',
            elementValue: 'buyerPaymentRequisitInfo',
            ariaLabel: 'Выберите реквизиты плательщика',
            pickerClientTypeFieldId: 'id_fi-payer-payreq-picker-client-ro',
            pickerFilterFieldId: 'id_fi-payer-payreq-picker-filter'
        }
    },
    payeeComponentPart: {
        accountPicker: {
            elementName: 'Номер счета получателя',
            elementId: 'id_fi-payee-account-number',
            btnElementId: 'id_fi-payee-account-btn',
            pickerElementId: 'id_fi-payee-account-picker',
            elementValueId: 'supplierAccountId',
            elementValue: 'supplierAccountNumber',
            pickerFilterFieldId: 'id_fi-payee-account-picker-filter',
            pickerClientTypeFieldId: 'id_fi-payee-account-picker-client-ro'
        },
        accountClient: {            
            elementId: 'id_fi-payee-account-number-label',
            elementName: 'Наименование получателя',
            elementValueId: 'supplierClientId',
            elementValue: 'supplierClientName'
        },
        linkedToOrder: {
            elementId: 'id_fi-payee-linked-order',
            btnElementId: 'id_fi-payee-linked-order-btn',
            btnClearElementId: 'id_fi-payee-linked-order-clear-btn',
            elementName: 'Связан с ордером',
            pickerElementId: 'id_fi-payee-linked-order-picker',
            elementValueId: 'supplierOrderId',
            orderInfoElement: 'supplierOrderInfo',
            orderNumberValue: 'orderNumber',
            orderDateValue: 'orderDate',
            pickerOrderTypeFieldId: 'id_fi-payee-lo-picker-order-type-ro',
            pickerDateFieldId: 'id_fi-payee-lo-picker-date'
            
        },
        operationSum: {
            elementId: 'id_fi-operation-payee-sum',
            elementName: 'Сумма прихода',
            elementSign: 'plus',
            readOnly: true,
            elementValue: 'operationSum'
        },
        paymentRequisit: {
            elementId: 'id_fi-payee-pay-req',
            btnElementId: 'id_fi-payee-pay-req-btn',
            elementName: 'Реквизиты получателя',
            pickerElementId: 'id_fi-payee-pay-req-picker',
            elementValueId: 'supplierPaymentRequisitId',
            elementValue: 'supplierPaymentRequisitInfo',
            ariaLabel: 'Выберите реквизиты получателя',
            pickerClientTypeFieldId: 'id_fi-payee-payreq-picker-client-ro',
            pickerFilterFieldId: 'id_fi-payee-payreq-picker-filter'
        }
    }
}

const operaitionDateNextElement = Map().set(transferOperationType, 
                                            'id_fi-payer-account-btn');

const supplyMoneyPart = 'D594BF81B7734050B70A41BF253FDAC1',
      supplyCommissionPart = 'E384CA8AFC5541519BBE8548B1804D35',
      settlementMoneyPart = '8B95A2EDF9F5492CA12F8EBF42B18E11',
      settlementCommissionPart = '6B183861EE094691B36A8834B697BD7B';

const transactionPartNames = [
    'supplyMoneyPart', 'supplyCommissionPart', 'settlementMoneyPart', 'settlementCommissionPart'
];

const operationPartPickerIds = [
    'id_fi-oper-account-picker-' + supplyMoneyPart,
    'id_fi-oper-account-picker-' + supplyCommissionPart,
    'id_fi-oper-account-picker-' + settlementMoneyPart,
    'id_fi-oper-account-picker-' + settlementCommissionPart
];

const operationPartSumInputIds = [
    'id_fi-operation-sum-' + supplyMoneyPart,
    'id_fi-operation-sum-' + supplyCommissionPart,
    'id_fi-operation-sum-' + settlementMoneyPart,
    'id_fi-operation-sum-' + settlementCommissionPart
];

const operationPartPurposeIds = [
    'id_fi-oper-purpose-' + supplyMoneyPart,
    'id_fi-oper-purpose-' + supplyCommissionPart,
    'id_fi-oper-purpose-' + settlementMoneyPart,
    'id_fi-oper-purpose-' + settlementCommissionPart
]

const operationPartDefs = Map()
    .set(supplyMoneyPart, 
    {
        partHeader: 'Отражение поставки денежных средств',
        sumElementHeader: 'Сумма поставки денежных средств',
        accountPickerElementHeader: 'Корреспондирующий счет',
        purposeElementHeader: 'Назначение платежа поставки',
        sumSign: '+',
        accSelectorCodeParameter: '30F5E1DDF2B546699FC021D9D24B0455',
        pickerListName: 'supplyAccountsMoneyPart'
    })
    .set(supplyCommissionPart,
    {
        partHeader: 'Отражение комиссии продавцу',
        sumElementHeader: 'Сумма комиссии продавца',
        accountPickerElementHeader: 'Счет комиссии',
        purposeElementHeader: 'Назначение платежа комиссии',
        sumSign: '-',
        accSelectorCodeParameter: '976881FEA8CC4B35AEAB416A15D03BD6',
        pickerListName: 'supplyAccountsCommissionPart'
    })
    .set(settlementMoneyPart,
    {
        partHeader: 'Отражение расчетов с покупателем',
        sumElementHeader: 'Сумма расчета',
        accountPickerElementHeader: 'Корреспондирующий счет',
        purposeElementHeader: 'Назначение платежа',
        sumSign: '-',
        accSelectorCodeParameter: '3C87B643238E47DAB1E9493C0393B4EA',
        pickerListName: 'settlementAccountsMoneyPart'
    })
    .set(settlementCommissionPart,
    {
        partHeader: 'Отражение собственной комиссии',
        sumElementHeader: 'Сумма комиссии',
        accountPickerElementHeader: 'Счет комиссии',
        purposeElementHeader: 'Назначение платежа комиссии',
        sumSign: '+',
        accSelectorCodeParameter: '33AC6F619AD04D6A855A43A1A6C3A7D6',
        pickerListName: 'settlementAccountsCommissionPart'
    });


const commonPartedComponentsDefs = {
    /* definitions for Supply and Settlement components */ 
    supplyComponentPart: {
        accountPicker: {
            elementName: 'Номер счета поставщика',
            elementId: 'id_fi-pf-supplier-account-number',
            btnElementId: 'id_fi-pf-supplier-account-btn',
            elementValueId: 'supplierAccountId',
            elementValue: 'supplierAccountNumber',
            pickerElementId: 'id_fi-pf-supplier-account-picker',
            pickerFilterFieldId: 'id_fi-pf-supplier-account-picker-filter',
            pickerClientTypeFieldId: 'id_fi-pf-supplier-account-picker-client-ro',
            accSelectorCodeParameter: 'supplierAccount'
        },
        accountClient: {            
            elementId: 'id_fi-pf-supplier-account-number',
            elementName: 'Наименование поставщика',
            elementValueId: 'supplierClientId',
            elementValue: 'supplierClientName'
        },
        linkedToOrder: {
            elementId: 'id_fi-pf-supplier-linked-order',
            btnElementId: 'id_fi-pf-supplier-linked-order-btn',
            btnClearElementId: 'id_fi-pf-supplier-linked-order-clear-btn',
            elementName: 'Связан с ордером поставки',
            pickerElementId: 'id_fi-pf-supplier-linked-order-picker',
            elementValueId: 'supplierOrderId',
            orderInfoElement: 'supplierOrderInfo',
            orderNumberValue: 'orderNumber',
            orderDateValue: 'orderDate',
            pickerOrderTypeFieldId: 'id_fi-pf-supplier-lo-picker-order-type-ro',
            pickerDateFieldId: 'id_fi-pf-supplier-lo-picker-date'
        }
    },
    settlementComponentPart: {
        accountPicker: {
            elementName: 'Номер счета покупателя',
            elementId: 'id_fi-pf-buyer-account-number',
            btnElementId: 'id_fi-pf-buyer-account-btn',
            pickerElementId: 'id_fi-pf-buyer-account-picker',
            elementValueId: 'buyerAccountId',
            elementValue: 'buyerAccountNumber',
            pickerFilterFieldId: 'id_fi-pf-buyer-account-picker-filter',
            pickerClientTypeFieldId: 'id_fi-pf-buyer-account-picker-client-ro',
            accSelectorCodeParameter: 'buyerAccount'
        },
        accountClient: {            
            elementId: 'id_fi-pf-buyer-account-number',
            elementName: 'Наименование получателя товара',
            elementValueId: 'buyerClientId',
            elementValue: 'buyerClientName'
        },
        linkedToOrder: {
            elementId: 'id_fi-pf-buyer-linked-order',
            btnElementId: 'id_fi-pf-buyer-linked-order-btn',
            btnClearElementId: 'id_fi-pf-buyer-linked-order-clear-btn',
            elementName: 'Связан с ордером',
            pickerElementId: 'id_fi-pf-buyer-linked-order-picker',
            elementValueId: 'buyerOrderId',
            orderInfoElement: 'buyerOrderInfo',
            orderNumberValue: 'orderNumber',
            orderDateValue: 'orderDate',
            pickerOrderTypeFieldId: 'id_fi-pf-buyer-lo-picker-order-type-ro',
            pickerDateFieldId: 'id_fi-pf-buyer-lo-picker-date'
            
        }
    }
}

const PartPickerNames = [
    'supplyAccountsMoneyPart', 
    'supplyAccountsCommissionPart', 
    'settlementAccountsMoneyPart',
    'settlementAccountsCommissionPart'
];


const simpleOperationPartDefs = Map()
    .set(debetOperationPart, 
    {
        partHeader: 'Дебет',
        accountElementHeader: 'Счет по дебету',
        accountElementNameHeader: 'Наименование счета по дебету',
        accSelectorCodeParameter: '9E88E0CAA4414A4DBA2AB2692B7A52B2',
        pickerListName: 'supplyAccountsMoneyPart',
        clientElementHeader: 'Клиент счета по дебету'
    })
    .set(creditOperationPart,
    {
        partHeader: 'Кредит',
        accountElementHeader: 'Счет по кредиту',
        accountElementNameHeader: 'Наименование счета по кредиту',
        accSelectorCodeParameter: '9E88E0CAA4414A4DBA2AB2692B7A52B2',
        pickerListName: 'supplyAccountsCommissionPart',
        clientElementHeader: 'Клиент счета по кредиту'
    });


export { operationTypesById,
         transferOperationType,
         supplyOperationType,
         settlementOperationType,
         simpleOperationType,
         allTypes,
         debetOperationPart,
         creditOperationPart,
         operationTypes,
         operationImages,
         transferPartComponentDefs,
         operaitionDateNextElement,
         operationStatuses,
         operationPartDefs,
         commonPartedComponentsDefs,
         operationPartPickerIds,
         operationPartSumInputIds,
         transactionPartNames,
         supplyMoneyPart,
         supplyCommissionPart,
         settlementMoneyPart,
         settlementCommissionPart,
         PartPickerNames,
         operationPartPurposeIds,
         simpleOperationPartDefs };