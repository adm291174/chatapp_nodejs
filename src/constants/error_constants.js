// Error identfiers 


const messageCategory = {
    'default': '',
    'error': 'error-panel__record_error', 
    'info': 'error-panel__record_info',
    'alert': 'error-panel__record_alert',
    'success': 'error-panel__record_success',
}

const errorIdentifiers = {
    commonInfo: {
        guid: '15fcd710-9c58-4087-89eb-3954e1a502de',
        categoryId: 'info',
        oneInstance: true
    },    
    uniqueCommonInfo: {
        guid: '15fcd710-9c58-4087-89eb-3954e1a502de',
        categoryId: 'info',        
        oneInstance: true
    },    
    commonError: {
        guid: '1177f366-27a8-43c8-8d02-4ab65795273a', 
        categoryId: 'error',
        oneInstance: false
    },
    chatListFetchError: {
        guid: '03b27b14-4399-419d-8149-359c905afe34', 
        categoryId: 'error',
        oneInstance: true,
        concealDelay: 5000
    },
    chatMessageFetchError: {
        guid: 'a9423f0b-b8e0-43bf-bc97-bbd56e4a0268', 
        categoryId: 'error',
        oneInstance: true
    },
    dataUpdateError: {
        guid: '91481b08-ac3a-41ea-83d3-657520be2ac5',
        categoryId: 'error',
        oneInstance: true
    },
    chatMessageSaveError: {
        guid: '49ca3854-3da2-4457-959d-e8330fc85349', 
        categoryId: 'error',
        oneInstance: true
    },
    clientDataGetError: {
        guid: 'cc02f4cd-5bed-49a8-ad1e-96aa37f7784d', 
        categoryId: 'error',
        oneInstance: true
    },
    payReqDataGetError: {
        guid: '135dd806-27a6-486f-86b4-ef54cb85e44d', 
        categoryId: 'error',
        oneInstance: true
    },
    payReqDataSaveError: {
        guid: 'a420caa1-2f3f-41a4-960c-c8210f99b86a', 
        categoryId: 'error',
        oneInstance: true
    },
    clientDataSaveError: {
        guid: 'ce53ad71-1b54-40fd-9d4a-3e3f21d92cc7', 
        categoryId: 'error',
        oneInstance: true
    },
    clientGetUserAvailableList: {
        guid: '893ca0ab-baad-409e-bc0d-122e388c3b8b', 
        categoryId: 'error',
        oneInstance: true
    },
    clientSetUser: {
        guid: '84a881ec-e646-47d2-ae19-550d9845f386',
        categoryId: 'error',
        oneInstance: true
    },
    clientListGetError: {
        guid: '1bb454ea-b387-4321-9cf5-448103ff85a3',
        categoryId: 'error',
        oneInstance: true
    },
    ordersListGetError: {
        guid: '2d4b2e38-8ce2-4e15-9175-65bfdb9e2e8d',
        categoryId: 'error',
        oneInstance: true
    },
    orderEditAccountGetError: {
        guid: '73bf3a1e-7ee6-4d0d-aa3f-dd11f6d272a7',
        categoryId: 'error',
        oneInstance: true
    },
    orderDataGetError: {
        guid: 'ef6aa6af-15d1-47e9-b43e-d3128044a25e',
        categoryId: 'error',
        oneInstance: true
    },
    orderDataSaveError: {
        guid: '01919e89-f3f3-4e9a-bbd5-ad64d47972a1',
        categoryId: 'error',
        oneInstance: true
    },
    orderEditOrdersForLinkGetError: {
        guid: '2bd72868-c863-4fc9-bfbc-969cc95dc15a',
        categoryId: 'error',
        oneInstance: true
    },
    orderActionExecuteError: {
        guid: '73c0dc72-8d48-4b68-910e-bfc2acf7affe',
        categoryId: 'error',
        oneInstance: true
    },
    operationsListGetError: {
        guid: '2ec7b12a-e643-45ef-847a-b19d0a01eadf',
        categoryId: 'error',
        oneInstance: true
    },
    operationsAccsPickerGetError: {
        guid: 'cbc90ca1-2d7a-4b0e-8937-4b9341d2ecdf',
        categoryId: 'error',
        oneInstance: true
    },
    operationsPayReqsPickerGetError: {
        guid: 'abaa23d7-b34b-4d65-9b75-d94b90fe31b2',
        categoryId: 'error',
        oneInstance: true
    },
    operationsOrdersPickerGetError: {
        guid: '8a390bca-636f-413a-841d-043c0331e9b2',
        categoryId: 'error',
        oneInstance: true
    },
    operationGetError: {
        guid: '5a59a815-8ada-435b-90bf-b08a190d40ff',
        categoryId: 'error',
        oneInstance: true
    },
    operationSaveError: {
        guid: 'b01b839f-63a9-4f7d-87d5-499f5fbb805f',
        categoryId: 'error',
        oneInstance: true
    },
    statementAccsGetError: {
        guid: '09a776ea-8ca9-4198-a17d-881cdfd3f87d',
        categoryId: 'error',
        oneInstance: true
    },
    statementGetError: {
        guid: '85745308-df36-4218-be29-921d57fec71c',
        categoryId: 'error',
        oneInstance: true
    },
    reportGetError: {
        guid: '5ce924f0-8adf-4fc7-b3aa-f7c7cf664033',
        categoryId: 'error',
        oneInstance: true
    },
    reportFileGetFailed: {
        guid: 'e299fc64-da41-4c53-8eb3-3203494877ec',
        categoryId: 'error',
        oneInstance: true
    },
    userListGetFailed: {
        guid: 'd5891fe6-c617-4998-a1b2-554700b36b61',
        categoryId: 'error',
        oneInstance: true        
    },
    userDictionaryGetFailed: {
        guid: 'ea423618-5d19-452f-b44c-0809de7f1dfe',
        categoryId: 'error',
        oneInstance: true        
    },
    userDataGetFailed: {
        guid: '15a2fa7f-a214-4c37-99be-61d9c1cc92fd',
        categoryId: 'error',
        oneInstance: true        
    },
    userDataSaveFailed: {
        guid: '5eac2731-c3f2-433e-bbcd-6882c971393d',
        categoryId: 'error',
        oneInstance: true        
    },    
    // messages contstants 
    messageInfo: {
        guid: 'deff6fd4-08df-4769-b012-09d8e18e5c32',
        categoryId: 'info',
        oneInstance: false,
        concealDelay: 3000
    },
    messageError: {
        guid: 'bf814dcd-b13f-459a-9282-65b13354a556',
        categoryId: 'error',
        oneInstance: false
    },
    messageAlert: {
        guid: 'b53d3230-62b8-44ed-9308-7a6e579d815f',
        categoryId: 'alert',
        oneInstance: false
    },
    messageSuccess: {
        guid: '19324911-420c-4742-84fe-8c5a9313010a',
        categoryId: 'success',
        oneInstance: false,
        concealDelay: 3000
    }
}

const codeOperationDescriptions = {
    'CHAT_INFO_DATA_GET_FAILED': 'Получение данных чата',
    'CHAT_MESSAGE_SAVE_FAILED': 'Сохранение нового сообщения чата',
    'DATA_CHATLIST_GET_FAILED': 'Получение данных списка чатов',
    'CLIENT_DATA_GET_FAILED': 'Получение данных клиента',
    'PAYREQ_DATA_GET_FAILED': 'Получение данных платежного реквизита',
    'PAYREQ_DATA_SAVE_FAILED': 'Сохранение данных платежного реквизита',
    'CLIENT_DATA_SAVE_FAILED': 'Сохранение данных клиента',
    'CLIENT_USERS_AVAILABLE_GET_FAILED': 'Получение списка доступных пользователей',
    'CLIENT_ADDUSER_SAVE_FAILED': 'Сохранение пользователя для клиента',
    'CLIENTS_LIST_DATA_GET_FAILED': 'Получение данных списка клиентов',
    'ORDERS_LIST_DATA_GET_FAILED': 'Получение данных списка ордеров',
    'ORDER_EDIT_PICKER_DATA_ACCS_GET_FAILED': 'Получение данных счетов для ордера',
    'ORDER_EDIT_PICKER_LO_DATA_GET_FAILED': 'Получение данных доступных ордеров для ордера',
    'ORDER_DATA_GET_FAILED': 'Получение данных ордера',
    'ORDER_DATA_SAVE_FAILED': 'Сохранение данных ордера',
    'ORDER_PROCESS_ACTION_FAILED': 'Выполнение операции с ордером',
    'OPERATIONS_LIST_DATA_GET_FAILED': 'Получение списка операций',
    'OPERATIONS_PICKER_DATA_ACCS_GET_FAILED': 'Получение списка счетов для выбора (Редактирование операции)',
    'OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED': 'Получение списка платежных реквизитов для выбора (Редактирование операции)',
    'OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED': 'Получение списка ордеров для выбора (Редактирование операции)',
    'OPERATION_DATA_GET_FAILED': 'Получение данных операции',
    'OPERATION_DATA_SAVE_FAILED': 'Сохранение данных операции',
    'STATEMENT_PICKER_DATA_ACCS_GET_FAILED': 'Получения списка счетов для выписки',
    'STATEMENT_DATA_GET_FAILED': 'Получение данных выписки',
    'REPORTS_DATA_GET_FAILED': 'Получение данных отчета',
    'REPORT_FILE_GET_FAILED': 'Загрузка файла отчета',
    'USERS_LIST_DATA_GET_FAILED': 'Получение данных списка пользователей',
    'USERS_DICTIONARY_DATA_GET_FAILED': 'Получение справочников редактирования пользователей',
    'USER_DATA_GET_FAILED': 'Получение данных пользователя',
    'USER_DATA_SAVE_FAILED': 'Сохранение данных пользователя'
}

const mapTypeToIdentifiers = {
    // fill all known action types here and map them to errorIdentifiers  
    'CHAT_INFO_DATA_GET_FAILED': errorIdentifiers.chatMessageFetchError,
    'CHAT_MESSAGE_SAVE_FAILED': errorIdentifiers.chatMessageSaveError,
    'DATA_CHATLIST_GET_FAILED': errorIdentifiers.chatListFetchError,
    'CLIENT_DATA_GET_FAILED': errorIdentifiers.clientDataGetError,
    'PAYREQ_DATA_GET_FAILED': errorIdentifiers.payReqDataGetError,
    'PAYREQ_DATA_SAVE_FAILED': errorIdentifiers.payReqDataSaveError,
    'CLIENT_DATA_SAVE_FAILED': errorIdentifiers.clientDataSaveError,
    'CLIENT_USERS_AVAILABLE_GET_FAILED': errorIdentifiers.clientGetUserAvailableList,
    'CLIENT_ADDUSER_SAVE_FAILED': errorIdentifiers.clientSetUser,
    'CLIENTS_LIST_DATA_GET_FAILED': errorIdentifiers.clientListGetError,
    'ORDERS_LIST_DATA_GET_FAILED': errorIdentifiers.ordersListGetError,
    'ORDER_EDIT_PICKER_DATA_ACCS_GET_FAILED': errorIdentifiers.orderEditAccountGetError,
    'ORDER_EDIT_PICKER_LO_DATA_GET_FAILED': errorIdentifiers.orderEditOrdersForLinkGetError,
    'ORDER_DATA_GET_FAILED': errorIdentifiers.orderDataGetError,
    'ORDER_DATA_SAVE_FAILED': errorIdentifiers.orderDataSaveError,
    'ORDER_PROCESS_ACTION_FAILED': errorIdentifiers.orderActionExecuteError,
    'OPERATIONS_LIST_DATA_GET_FAILED': errorIdentifiers.operationsListGetError,
    'OPERATIONS_PICKER_DATA_ACCS_GET_FAILED': errorIdentifiers.operationsAccsPickerGetError,
    'OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED': errorIdentifiers.operationsPayReqsPickerGetError,
    'OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED': errorIdentifiers.operationsOrdersPickerGetError,
    'OPERATION_DATA_GET_FAILED': errorIdentifiers.operationGetError,
    'OPERATION_DATA_SAVE_FAILED': errorIdentifiers.operationSaveError,
    'STATEMENT_PICKER_DATA_ACCS_GET_FAILED': errorIdentifiers.statementAccsGetError,
    'STATEMENT_DATA_GET_FAILED': errorIdentifiers.statementGetError,
    'REPORTS_DATA_GET_FAILED': errorIdentifiers.reportGetError,
    'REPORT_FILE_GET_FAILED': errorIdentifiers.reportFileGetFailed,
    'USERS_LIST_DATA_GET_FAILED': errorIdentifiers.userListGetFailed,
    'USERS_DICTIONARY_DATA_GET_FAILED': errorIdentifiers.userDictionaryGetFailed,
    'USER_DATA_GET_FAILED': errorIdentifiers.userDataGetFailed,
    'USER_DATA_SAVE_FAILED': errorIdentifiers.userDataSaveFailed
}

const mapTypeToContext = {
    // fill all known action types here and map them to errorIdentifiers  
    'CHAT_INFO_DATA_GET_FAILED': 'CHAT_INFO_DATA_GET',
    'CHAT_MESSAGE_SAVE_FAILED': 'CHAT_MESSAGE_SAVE',
    'DATA_CHATLIST_GET_FAILED': 'DATA_СHATLIST_INFO',
    'CLIENT_DATA_GET_FAILED': 'GET_CLIENT_DATA',
    'PAYREQ_DATA_GET_FAILED': 'GET_PAYREQ_DATA',
    'PAYREQ_DATA_SAVE_FAILED': 'PAYREQ_DATA_SAVE',
    'CLIENT_DATA_SAVE_FAILED': 'CLIENT_DATA_SAVE',
    'CLIENT_USERS_AVAILABLE_GET_FAILED': 'GET_CLIENT_USERS_FORADD',
    'CLIENT_ADDUSER_SAVE_FAILED': 'CLIENT_ADDUSER_SAVE',
    'CLIENTS_LIST_DATA_GET_FAILED': 'GET_CLIENTS_LIST_DATA',
    'ORDERS_LIST_DATA_GET_FAILED': 'GET_ORDERS_LIST_DATA',
    'ORDER_EDIT_PICKER_DATA_ACCS_GET_FAILED': 'GET_ORDER_EDIT_PICKER_DATA_ACCS',
    'ORDER_EDIT_PICKER_LO_DATA_GET_FAILED': 'GET_ORDER_EDIT_PICKER_DATA_LO',
    'ORDER_DATA_GET_FAILED': 'GET_ORDER_DATA',
    'ORDER_DATA_SAVE_FAILED': 'ORDER_DATA_SAVE',
    'ORDER_PROCESS_ACTION_FAILED': 'ORDER_PROCESS_ACTION',
    'OPERATIONS_LIST_DATA_GET_FAILED': 'GET_OPERATIONS_LIST_DATA',
    'OPERATIONS_PICKER_DATA_ACCS_GET_FAILED': 'GET_OPERATION_PICKER_DATA_ACCS',
    'OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED': 'GET_OPERATION_PICKER_DATA_PAYREQ',
    'OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED': 'OPERATIONS_PICKER_DATA_ORDERS',
    'OPERATION_DATA_GET_FAILED': 'GET_OPERATION_DATA',
    'OPERATION_DATA_SAVE_FAILED': 'SAVE_OPERATION_DATA',
    'STATEMENT_PICKER_DATA_ACCS_GET_FAILED': 'GET_STATEMENT_PICKER_DATA_ACCS',
    'STATEMENT_DATA_GET_FAILED': 'GET_STATEMENT_DATA',
    'REPORTS_DATA_GET_FAILED': 'GET_REPORTS_DATA',
    'REPORT_FILE_GET_FAILED': 'LOAD_REPORT_FILE',
    'USERS_LIST_DATA_GET_FAILED': 'GET_USERS_LIST_DATA',
    'USERS_DICTIONARY_DATA_GET_FAILED': 'GET_USERS_DICTIONARY_DATA',
    'USER_DATA_GET_FAILED': 'GET_USER_DATA',
    'USER_DATA_SAVE_FAILED': 'SAVE_USER_DATA'
}

export { codeOperationDescriptions, 
         errorIdentifiers, 
         mapTypeToContext,
         mapTypeToIdentifiers,
         messageCategory};
