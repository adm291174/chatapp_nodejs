module.exports.getRandomInt = function (min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}


module.exports.checkSession = function(req, res, sessions, returnHtth401 = true) {
    // return session if session is valid or null if session is absent 
    // if returnHtth401 == true - return response whith code 401 
    const sessionid = req.cookies.sessionid,
          session = sessions.checkUser(sessionid);
    
    if (!session && returnHtth401) {
        res.status(401).json({detail: 'Требуется авторизация'});
    }
    return session;
}
