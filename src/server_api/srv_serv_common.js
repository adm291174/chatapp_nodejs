var pathModule = require('path');
var fs = require('fs');

var { checkSession } = require('./srv_func');

var servStateInfo = function(app, path, sessions) {
    app.get(path, function(req, res) {
        const session = checkSession(req, res, sessions);

        if (session) {
            const sessionId = session.sessionId;

            var response = {userId: 12,
                            login: 'administrator',
                            userName: 'Иван Иванов',
                            userRoles: ['admin', 'manager', 'appchat_users'],
                            hasChatAccess: true,
                            isClient: false,
                            isManager: true,
                            isAdmin: true,
                            isUserAdmin: true,
                            adminAppUrl: '/admin',
                            reportsGroups: [
                                {
                                    reportGroupId: 1,
                                    reportGroupName: 'Бухгалтерские отчеты',
                                    reportGroupPriority: 10
                                },
                                {
                                    reportGroupId: 2,
                                    reportGroupName: 'Оперативные отчеты',
                                    reportGroupPriority: 20
                                }
                            ],
                            reports: [
                                {
                                    reportId: '27304D4C12FE45F9B8134999CED6699E',
                                    reportName: 'Бухгалтерский баланс на дату', 
                                    reportGroupId: 1,
                                    reportOrder: 0

                                },
                                {
                                    reportId: '21cad56976ac47dcb2febc54e1207cbc',
                                    reportName: 'Оборотная ведомость за период', 
                                    reportGroupId: 1,
                                    reportOrder: 0

                                },
                                {
                                    reportId: 'bf02a8ecea2f40ba9bb0b51b264b6a7d',
                                    reportName: 'Статистика по клиенту',
                                    reportGroupId: 2,
                                    reportOrder: 100

                                },
                                {
                                    reportId: 'dfca941128ae48ec9ab9d6a360ebe421',
                                    reportName: 'Отчет о прибылях и убытках',
                                    reportGroupId: 2,
                                    reportOrder: 10

                                }
                            ]
            };
            const firstCall = sessions.getSessionValue(sessionId, 'firstCall');
            if (firstCall != 0) {
                if (!firstCall) sessions.setSessionValue(sessionId, 'firstCall', 1);
            }
            res.status(200);
            res.json(response);
        }
    });
}

var servMessagesQuery = function(app, path, sessions) {
    app.get(path, function(req, res) {
        const session = checkSession(req, res, sessions);

        if (session) {
            const sessionId = session.sessionId;

            const firstCall = sessions.getSessionValue(sessionId, 'firstCall');
            // console.log('firstCall: ', firstCall, 
            //            'sessionId: ', req.cookies.sessionid);

            const error_message_response_data = [
                {
                    message: 'Это ошибка!',
                    level: 'error'
                },
                {
                    message: 'Это предупреждение!',
                    level: 'alert',
                    concealDelay: 5000
                },
                {
                    message: 'Просто сообщение',
                    level: 'info',
                    concealDelay: 2500
                }
            ];

            
            var filePath = pathModule.dirname(require.main.filename);
            var buffer = fs.readFileSync(filePath + '\\src\\server_api\\sysconf.json');
            const systemFileInfo = JSON.parse(buffer.toString());

            // res.status(400).send('Bad request');
            if (firstCall == 1) {
                sessions.setSessionValue(sessionId, 'firstCall', 0);
            }
            res.status(200);
            res.json({
                systemStatus: systemFileInfo.systemStatus,
                needLogin: systemFileInfo.needLogin,
                messages: (firstCall != 1) ? [] : error_message_response_data
            });
        }
    });
}

module.exports.servCommon = function(app, sessions) {
    servStateInfo(app, '/api/v2/get_user_info', sessions);
    servMessagesQuery(app, '/api/v2/status/get', sessions);
}


