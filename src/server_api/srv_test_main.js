// main calls handler, including simple authorization 
const uuidv4 = require('uuid/v4');
const { Map } = require('immutable');

const { report_files_test_data } = require('../test_data/test_data_reports');

/* sessions handler block */ 

const maxSessionLength = 24 * 60 * 60 * 1000; // 24 hours in ms shift value 

/* class for users authorization and session management 
   test use only  

   typical scenario:
   - check user session. If <checkUser> returns sessionId, allow user access
   - authorize if sessionId == null (with <authorize> method)
   - on every method call check user session with <checkUser> 
*/

class Sessions {
    
    constructor(usersDatabase) {
        // expected array of {userLogin, userPassword} objects 
        this.usersDatabase = usersDatabase
        this.sessions = Map();
    } 
    
    authorize(login, password) {
        /* authorize test user 

           check login and password here, 
           :return 
               new sessionId if authorization successful
               or null if authorization failed 

           sessionId should be stored in this.sessions
        */
        let _this = this,
            sessionId = null;

        if (this.usersDatabase) {
            this.usersDatabase.forEach( ({ userLogin, userPassword }) => {
                if (userLogin == login && password == userPassword) {
                    sessionId = _this.generateSession(login);
                }
            });
        }
        return sessionId;
    }
    
    logout(sessionId) {
        /* process logout if this function is called 
           return true if logout successful 
        */
        if (this.sessions.has(sessionId)) {
            this.sessions = this.sessions.delete(sessionId)
            return true;
        }
        return false;
    }

    checkUser(sessionId) {
        /* check whether user session is valid 
           return session value if session is valid or null if session is invalid or expired */ 
        if (this.sessions.has(sessionId)) {
            const session = this.sessions.get(sessionId);
            if (session.maxValidTime && Date.now() < session.maxValidTime) {
                return session;
            }
        }
        return null;        
    }

    generateSession(userName) {
        /* generate sessionId, store sessionId and user parameters (userName, csrfToken in storage ) */
        const sessionId = 'express-srv-sess-' + uuidv4();
        this.sessions = this.sessions.set(sessionId, {
            userName: userName,
            csrfToken: 'csrf-token-' + uuidv4(),
            maxValidTime: Date.now() + maxSessionLength,
            sessionId: sessionId
        });
        return sessionId;
    }

    setSessionValue(sessionId, parameterName, parameterValue) {
        let session = this.sessions.get(sessionId);
        if (session) {
            session[parameterName] = parameterValue;
            this.sessions = this.sessions.set(sessionId, 
                                            session);
        }
    } 

    getSessionValue(sessionid, parameterName) {
        const session = this.sessions.get(sessionid);
        if (session)
            if (session.hasOwnProperty(parameterName))
            return session[parameterName];
        else return null;
    }
}


var mainCallsSrv = function(app, sessions, rootDir) {
    servAccounts(app, '^/accounts/.*$', sessions, rootDir);
    servDownload(app, 
                 '^/download/files/([A-Fa-f0-9]{32})/$', 
                 sessions,
                 rootDir + '/sample-files'
                 );
}

var servAccounts = function(app, path, sessions, rootDir) {
    app.get('/accounts/login/', function(req, res) {
        res.clearCookie('sessionid');
        res.cookie('csrftoken', 'XaU0mbOVU3iY8TT5eXJtq75LFftvCPFWQRanodZJerqJpBZ2Fq1UDlAGdAHXJbjv');
        res.sendFile(rootDir + '/htmls/login.html');
    });
    app.get('/accounts/logout/', function(req, res) {
        res.clearCookie('sessionid');
        res.cookie('csrftoken', 'XaU0mbOVU3iY8TT5eXJtq75LFftvCPFWQRanodZJerqJpBZ2Fq1UDlAGdAHXJbjv');
        res.sendFile(rootDir + '/htmls/logout.html');
    });
    app.post('/accounts/login/', function(req, res) {
        const { login, password } = req.body;
        if (login && password) {
            const sessionId = sessions.authorize(login, password);
            if (sessionId) {
                const sessionInfo = sessions.checkUser(sessionId);
                res.cookie('sessionid', sessionId);
                res.cookie('csrftoken', sessionInfo.csrfToken);
                res.set('Location', '/');
                res.status(302);
                res.send(null);
                return;
            }
        }
        // login failed ... delay response for 3 seconds
        setTimeout(()=>{
            res.status(403);
            res.sendFile(rootDir + '/htmls/login_error.html');
        }, 3000);        
    });
    app.post('/accounts/logout/', function(req, res) {
        const sessionId = req.cookies.sessionid;
        if (sessionId) {
            sessions.logout(sessionId);
        }
        res.set('Location', '/');
        res.status(302);
        res.send(null);
    });
}

var downloadFile = function(returnBody, path, sessions, storageFilePath) {
    return function(req, res) {
        const regexp = new RegExp(path),
            match = regexp.exec(req.originalUrl);
        if (match) {
            const sessionid = req.cookies.sessionid,
                  session = sessions.checkUser(sessionid);

            if (session) {
                if (session.userName == 'test_user') {
                    res.status(500);
                    res.send('Internal server error');
                    return;
                }
                const donloadedFileGuid = match[1],
                      reportFileInfo = report_files_test_data[donloadedFileGuid];
                
                if (reportFileInfo) {
                    const realFileName = reportFileInfo.realFileName,
                        storageFileName = storageFilePath + '/' + reportFileInfo.storageFileName;
                    if (returnBody) {
                        res.download(storageFileName, realFileName);
                        return;
                    }
                    else {
                        res.send('');
                        return;
                    }
                }                
            }
            else {
                res.status(403);
                res.send('');
                return;
            }
        }
        res.status(400);
        res.send('Invalid download link');
    }
}

var servDownload = function(app, path, sessions, storageFilePath) {
    app.head(path, downloadFile(false, path, sessions, storageFilePath));
    app.get(path, downloadFile(true, path, sessions, storageFilePath));
}

module.exports.Sessions = Sessions;
module.exports.mainCallsSrv = mainCallsSrv;
