// serv functions import 
var { servCommon } = require('./srv_serv_common')
var { servAccounts } = require('./srv_serv_accounts');
var { servChat } = require('./srv_serv_chat');
var { servUsers } = require('./srv_serv_users');
var { servOrders } = require('./srv_serv_orders');
var { servOperations } = require('./srv_serv_operations');
var { servReports } = require('./srv_serv_reports');

String.prototype.format= function () {
    var args = arguments;
    return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function(m, n) { 
        if (m =='{{') { return '{'; }
        if (m =='}}') { return '}';}
        return args[n];});
}

var apiCallsSrv = function(app, sessions) {
    servCommon(app, sessions);
    servChat(app);
    servUsers(app);
    servOrders(app);
    servOperations(app);
    servAccounts(app);
    servReports(app);
}

module.exports.apiCallsSrv = apiCallsSrv;