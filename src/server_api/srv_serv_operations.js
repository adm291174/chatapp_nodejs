const assert = require('assert'); 

var { ordersList_data2,
      operationsList_data1,
      operationsList_data2,
      operationsPickerAccount_data1 } = require('../test_data/test_data_operations');

var { test_data_payrequsits } = require('../test_data/test_data_payreq');
var { extractDate } = require('../test_data/test_data_functions');


var servOperationsList = function(app, path) {
    app.put(path, function(req, res) {
        const response_data = operationsList_data1()
        const { startDate, endDate, operationType } = req.body;

        const startDateFixed = extractDate(startDate),
              endDateFixed = extractDate(endDate)

        assert(startDateFixed instanceof Date);
        assert(endDateFixed instanceof Date);
        if (operationType != null)
            assert(typeof(operationType) == 'string' && operationType.length == 32);


        const filteredResponseData = 
            response_data.filter(value => {
            const operationDate = extractDate(value.operationDate);
            return (operationDate >= startDateFixed && 
                    operationDate <= endDateFixed)
                    })
                    .filter(value => {
                        if (operationType) {
                            return operationType == value.operationType;
                        }
                        else return true;
                    })
                .map(value => {
                    const { operationId, operationNumber, operationDate,
                            status, operationStatusDescription, clients,
                            operationClientId, operationClientName, operationSum,
                            operationCurrencyCodeId, operationCurrencyCode, 
                            operationType, operationTypeDescription } = value;
                    return { operationId, operationNumber, operationDate,
                             status, operationStatusDescription, clients,
                             operationClientId, operationClientName, operationSum,
                             operationCurrencyCodeId, operationCurrencyCode, 
                             operationType, operationTypeDescription }
                });
            
        //res.status(400).send('Bad request');
        res.status(200);
        res.json(filteredResponseData);
    });
}

var servAccsForOperation = function(app, path) {
    app.put(path, function(req, res) {
        
        const response_data = operationsPickerAccount_data1();
        const { currencyId, accountType } = req.body,
               filterAccounts = [ 'buyerAccount', 'supplierAccount']
                                .includes(accountType);


        const filteredResponseData = response_data.filter( value => {
                  return !filterAccounts ||
                          (accountType ? value.accountType == accountType : true) })
                  .filter(value => {
                      return currencyId ? value.accountCurrencyId == currencyId : true 
                  });

        // res.status(400).send('Bad request');
        res.status(200);
        res.json(filteredResponseData);
    });
}

var servPayReqsForOperation = function(app, path) {
    app.put(path, function(req, res) {
        
        const response_data = test_data_payrequsits;
        const { clientId } = req.body;

        assert(typeof(clientId) == 'number');
        
        // res.status(400).send('Bad request');
        res.status(200);
        res.json(response_data.map(({ requisitId, orgTaxNumber, accountNumber, orgName, bankName }) => 
                                   ({ requisitId, orgTaxNumber, accountNumber, orgName, bankName })));
    });
}

var servOrdersForOperation = function(app, path) {
    app.put(path, function(req, res) {
        
        const response_data = ordersList_data2();
        const { accountId, orderType, startDate, endDate, linkedOrderId } = req.body;

        assert(typeof(accountId) == 'number');
        assert(accountId > 0);
        assert(typeof(orderType) == 'string' && orderType.length == 32);
        
        const startDateFixed = extractDate(startDate),
              endDateFixed = extractDate(endDate)

        assert(startDateFixed instanceof Date);
        assert(endDateFixed instanceof Date);

        assert(linkedOrderId == null || typeof(linkedOrderId) == 'number');
        
        const filteredResponseData = response_data.filter((value) => (value.orderId == 999 || value.accountId == accountId))
                                             .filter((value) => (extractDate(value.orderDate) >= startDateFixed &&
                                                                 extractDate(value.orderDate) <= endDateFixed))
                        .filter((value) => (value.orderTypeGuid == orderType))
                        .filter((value) => (value.orderTypeGuid == orderType)) 
                        .filter((value) => (linkedOrderId == null || value.orderId == 999)); 

        // res.status(400).send('Bad request');
        res.status(200);
        res.json(filteredResponseData.map(({ orderId, orderDate, orderNumber, orderSum, 
                                             accountCurrencyId, accountCurrencyCode }) => 
            ({ orderId, orderDate, orderNumber, orderSum, 
               orderCurrencyId: accountCurrencyId, orderCurrencyCode: accountCurrencyCode })));
    });
}

var servOperationGet = function(app, path) {
    app.put(path, function(req, res) {
        
        const response_data = operationsList_data2();
        const { operationId } = req.body;

        assert(typeof(operationId) == 'number');
        assert(operationId > 0);
        
        const filteredResponseData = response_data.filter((value) => (value.operationId == operationId));

        if (filteredResponseData.length == 0) {
            res.status(400).send('Bad request');
        }
        else {
            res.status(200);
            res.json(filteredResponseData[0]);
        }
    });
}


var servOperationSave = function(app, path) {
    app.post(path, function(req, res) {
        const { operationId } = req.body;
        if (operationId !== null) {
            assert(typeof(operationId) == 'number');
            assert(operationId > 0);
        }        
        if (operationId == 300) {
            res.status(400).send('Bad request');
        }
        else {
            res.status(200);
            res.json( operationId ? { operationId } : { operationId : 200} );
        }
    });
}

module.exports.servOperations = function(app) {
    servOperationsList(app, '/api/v2/operations/list');
    servAccsForOperation(app, '/api/v2/operations/get_acc_picker_data');
    servPayReqsForOperation(app, '/api/v2/operations/get_payreq_picker_data');
    servOrdersForOperation(app, '/api/v2/operations/get_orders_picker_data');
    servOperationGet(app, '/api/v2/operations/get_operation');
    servOperationSave(app, '/api/v2/operations/save_operation');
}