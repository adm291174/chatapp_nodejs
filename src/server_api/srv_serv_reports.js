var { test_data_reports } = require('../test_data/test_data_reports');
var { ID_REPORT_BALANCE } = require('../constants/report_constants');

var servReportGet = function(app, path) {
    app.post(path, function(req, res) {
        const { reportId, reportOutput } = req.body;
        switch (reportId) {
            case ID_REPORT_BALANCE: {// get balance report here 
                if (reportOutput == 'json') {
                    setTimeout(()=>{
                        res.status(200);                    
                        res.json({ reportData: test_data_reports[ID_REPORT_BALANCE], 
                                reportFileName: null });
                    }, 3000);        
                }
                else if (reportOutput == 'file'){
                    res.status(200);      
                    res.json({ reportData: null, reportFileName: ID_REPORT_BALANCE});
                }
                else {
                    res.status(400);
                    res.send('Wrong reportOutput value');
                }
                return;                
            }
            default: {
                res.status(400);
                res.send('Wrong reportId value');
            } 
        }
    });
}

module.exports.servReports = function(app) {
    servReportGet (app, '/api/v2/reports/get_report');
}