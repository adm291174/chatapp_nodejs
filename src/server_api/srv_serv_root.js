var servRoot = function(app, sessions, rootPath) {
    app.get('/', function(req, res) {
        // console.log('Cookies:', req.cookies);
        // get user session 
        const sessionId = req.cookies.sessionid;
        if (sessionId) {
            // check sessionId 
            const userSession = sessions.checkUser(sessionId);
            if (userSession) {
                res.cookie('sessionid', sessionId);
                res.cookie('csrftoken', userSession.csrfToken);
                res.sendFile(rootPath + '/index.html');
                return;
            }
        }
        // redirect to login page
        res.set('Location', '/accounts/login/');
        res.status(302);
        res.send(null);    
    });
}

module.exports.servRoot = servRoot;