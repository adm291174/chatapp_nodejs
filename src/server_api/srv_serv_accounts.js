var { test_data_clients, test_data_accounts } = require('../test_data/test_data_clients');
var { test_data_payrequsits } = require('../test_data/test_data_payreq');
var { operationsPickerAccount_data1 } = require('../test_data/test_data_operations');


var servClientsList = function(app, path) {
    app.put(path, function(req, res) {
        let filterText = req.body.filterText.toLowerCase();
        let response_data = test_data_clients.filter(value => {
            const clientName = value.clientName.toLowerCase();
            return (clientName.indexOf(filterText) !== -1); 
        }).map(value => ({ clientId: value.clientId, clientName: value.clientName }) );
        response_data.sort((a, b) => {
            return (a.clientName < b.clientName) ? -1 : 1;
        });
        res.status(200);
        res.json(response_data);
    });
}

var servAccountsList = function(app, path) {
    app.put(path, function(req, res) {
        let clientsList = req.body.clients; // client Ids here 
        if (clientsList instanceof Array) {
            let response_data_pre = clientsList.map(clientId => {
                return test_data_accounts.filter(account => (account.clientId == clientId));
            });
            
            let result = []
            response_data_pre.forEach(values => {result.push(...values)})
            res.status(200);
            res.json(result);
        }
        else {
            res.status(400).send('Bad request');
        }
    });
}

var servClientGetInfo = function(app, path) {
    app.put(path, function(req, res) {
        let clientId = req.body.clientId; // client Ids here 

        let response_data = test_data_clients.filter(value => {
            return (clientId == value.clientId); 
        });
        
        res.status(200);
        if (response_data.length) {
            res.json(response_data[0]);
        }
        else 
            res.json(null);
    });
} 

var servClientSaveInfo = function(app, path) {
    app.post(path, function(req, res) {
        let { clientId, clientName } = req.body; // client Ids here 
        if (clientId || (!clientName) && (clientName.length == 0)) {
            if (clientId == 800) {
                res.status(400).send('Bad request');
            }
            else {
                let response_data = {clientId: clientId} 
                res.status(200);
                res.json(response_data);
            }
        }
        else {
            let response_data = {clientId: 400} 
            res.status(200);
            res.json(response_data);
        }
    });
} 


var servClientGetPayRequisits = function(app, path) {
    app.put(path, function(req, res) {
        let clientId = req.body.clientId; // client Id here 

        let response_data = test_data_payrequsits.filter(value => {
            return (clientId == value.clientId); 
        });
        
        res.status(200);
        res.json(response_data);
    });
} 

var servClientGetPayRequisit = function(app, path) {
    app.put(path, function(req, res) {
        let requisitId = req.body.requisitId; // requisit ID here 

        let response_data = test_data_payrequsits.filter(value => {
            return (requisitId == value.requisitId); 
        });
        
        if (response_data.length == 1) {
            if (requisitId == 20) {
                res.status(400).send('Bad request');
                return;           
            }

            res.status(200);    
            res.json(response_data[0]);
        }
        else {
            res.status(200);    
            res.json({});
        }
    });
}

var servClientSavePayRequisit = function(app, path) {
    app.post(path, function(req, res) {
        let requisitId = req.body.requisitId, // requisit ID here 
            clientId = req.body.clientId,
            orgTaxNumber = req.body.orgTaxNumber,
            orgTaxReasonCode = req.body.orgTaxReasonCode,
            orgName = req.body.orgName,
            accountNumber = req.body.accountNumber,
            bankCode = req.body.bankCode,
            bankName = req.body.bankName,
            bankCorrAccNumber = req.body.bankCorrAccNumber,
            enabled = req.body.enabled;

        console.log('Server: client requisit saving operation');
        console.log('Server: values [ requisitId: {0}, clientId: {1}, orgTaxNumber: {2} \
orgTaxReasonCode: {3}, orgName: {4}, accountNumber: {5}, bankCode: {6}, bankName: {7}, \
bankCorrAccNumber: {8}, enabled: {9} ]'.format(
                     requisitId, clientId, orgTaxNumber,
                     orgTaxReasonCode, orgName, accountNumber,
                     bankCode, bankName, bankCorrAccNumber,
                     enabled));
        let response_data = { requisitId: requisitId ? requisitId : 30 };
        if (clientId == 600) {
            res.status(400).send('Bad request');            
        }
        else {
            res.status(200);    
            res.json(response_data);
        }
    });
}


var servClientSaveUser = function(app, path) {
    app.post(path, function(req, res) {
        let clientId = req.body.clientId,
            userId = req.body.userId,
            enable = req.body.enable;
        console.log('Server: client user saving operation');
        console.log('Server: values [ clientId: {0}, userId: {1}, enable: {2} ]'.format(
                     clientId, userId, enable));

        if (clientId == 500) {
            res.status(400).send('Bad request');            
        }
        else {
            res.status(200);    
            res.json({});
        }
    });
}



var servStatementAccPicker = function(app, path) {
    app.get(path, function(req, res) {
        
        const response_data = operationsPickerAccount_data1();

        // res.status(400).send('Bad request');
        res.status(200);
        res.json(response_data);
    });
}


module.exports.servAccounts = function(app) {
    servClientsList(app, '/api/v2/clients/list');
    servAccountsList(app, '/api/v2/clients/accounts');
    servClientGetInfo(app, '/api/v2/clients/get_info');
    servClientGetPayRequisits(app, '/api/v2/clients/get_pay_reqs_list');
    servClientGetPayRequisit(app, '/api/v2/clients/get_pay_req');
    servClientSavePayRequisit(app, '/api/v2/clients/save_pay_req');
    servClientSaveInfo(app, '/api/v2/clients/save_info');
    servClientSaveUser(app, '/api/v2/clients/set_user');
    servStatementAccPicker(app, '/api/v2/statement/get_acc_picker_data');

}