var { apiChatList_data1 } = require('../test_data/test_data_chats');
var { chatMessageInfo_data1, chatMessageInfo_data2, chatMessageContent_data1 } = require('../test_data/test_data_messages');


var servChatList = function(app, path) {
    app.get(path, function(req, res) {
        res.status(200);
        res.json(apiChatList_data1());
    });
}

var servChatInfo = function(app, path) {
    app.put(path, function(req, res) {
        let chatHistoryId = req.body.chat_history_id;

        var response = chatHistoryId == 100 ? 
                       chatMessageInfo_data2(chatHistoryId) : 
                       chatMessageInfo_data1(chatHistoryId);
        res.status(200);
        res.json(response);
    });
}

var servChatContent = function(app, path) {
    app.put(path, function(req, res) {
        let chatHistoryId = req.body.chat_history_id;
        let { command, count, items } = req.body;
        if (count == undefined) count = 10;
        if (command == undefined) command = 'before';
        let response_data = chatHistoryId == 100 ? chatMessageContent_data2(chatHistoryId) : chatMessageContent_data1(chatHistoryId);
        if (command == 'before') {
            const boardItem = items.length == 1 ? items[0] : 10000000000;
            let response = [];
            let counter = 0;
            response_data.reverse().forEach(value => {
                if (counter < count && value.id < boardItem)  {
                    response.push(value);
                    counter ++;
                }
            })
            response = response.reverse();
            
            res.status(200);
            res.json(response);
        }
        if (command == 'after') {
            const boardItem = items.length == 1 ? items[0] : 10000000000;
            var response = [];
            var counter = 0;
            response_data.forEach(value => {
                if (counter < count && value.id > boardItem)  {
                    response.push(value);
                    counter ++;
                }
            })            
            res.status(200);
            res.json(response);
        }
        
    });
}

module.exports.servChat = function(app) {
    servChatList(app, '/api/chats');
    servChatInfo(app, '/api/messages/info');
    servChatContent(app, '/api/messages/content');
}

