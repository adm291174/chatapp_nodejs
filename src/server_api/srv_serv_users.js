// serv users dummy server functionality 
var assert = require('assert');

var { test_data_users, 
      test_data_users_avail,
      test_data_users_list,
      test_data_users_dictionaries } = require('../test_data/test_data_users');


var servUsersAvailableListGet = function(app, path) {
    app.put(path, function(req, res) {
        
        let response_data = test_data_users_avail
        const { clientId, onlyAvailable } = req.body;

        if (clientId == 600) {
            res.status(400).send('Bad request');            
        }
        else {
            const filteredResponseData = response_data.filter((value) => {
                return onlyAvailable ? value.available == true : true 
            }).map((value) => {
                return { userId: value.userId, userLogin: value.userLogin, userName: value.userName }
            });
            
            res.status(200);
            res.json(filteredResponseData);
        }
    });
}

var servUsersListGet = function(app, path) {
    app.put(path, function(req, res) {
        
        let response_data = test_data_users
        
        res.status(200);
        if (response_data.length) {
            res.json(response_data);
        }
        else 
            res.json(null);
    });
}


var servUsersGet = function(app, path) {
    app.get(path, function(req, res) {
        const response_data = test_data_users_list.map(value => {
            const { userId, login, lastName, firstName, isActive } = value,
                    userName = lastName + 
                        (firstName && firstName.length > 0) ? firstName : '';

            return { userId, login, userName, isActive };
        });

        // res.status(400).send('Bad request');
        res.status(200);
        res.json(response_data);
    });
}

var servUsersDictionaries = function(app, path) {
    app.get(path, function(req, res) {
        const response_data = test_data_users_dictionaries;

        res.status(200);
        res.json(response_data);
    });
}

var servUserGet = function(app, path) {
    // get user information for editUser form 
    app.put(path, function(req, res) {
        
        const usersData = test_data_users_list,
              userId = req.body.userId;

        if (userId) {
            const userData = usersData.find(value => value.userId == userId);
            if (userData) {
                 const { userId, isActive, login,
                         userPassword, userMustChangePwd, lastName,
                         firstName, chatAccessEnabled, chatVisibleName, 
                         chatParameters, chatGroupId, userGroups } = userData;

                setTimeout(() => { res.status(200).json({
                    userId, isActive, userLogin: login,
                    userPassword, userMustChangePwd, lastName,
                    firstName, chatAccessEnabled, chatVisibleName, 
                    chatParameters, chatGroupId, userGroups
                }); }, 2000);
                return;
            }
        }
        res.status(400).send('{detail: "Неверный идентификатор пользователя"}');
    });
}

var servUserSave = function(app, path) {
    // get user information for editUser form 
    app.post(path, function(req, res) {
        
        const { userId, isActive, login,
                userPassword, userMustChangePwd, lastName,
                firstName, chatAccessEnabled, chatVisibleName, 
                chatParameters, chatGroupId, userGroups } = req.body,
                userLogin = login;

        assert.equal(userGroups instanceof(Array), true, 'Wrong type of passed variable: userGroups')

        if (userId) { // update user 
            if ((Math.random(1)*10)>5)  {
                setTimeout(() => { res.status(200).json({ userId: userId}); }, 2000);
                return;
            }
            else {
                res.status(500).send('{detail: "Ошибка обработки на сервере (stub error)"}');
            }
        }
        else { // new user 
            const usersData = test_data_users_list,
                  userLoginLowerCase = userLogin.toLowerCase();
                  
            const userData = usersData.find(value => value.login.toLowerCase() == userLoginLowerCase);
            if (userData) {
                setTimeout(() => { res.status(500).send('{ "detail": "Логин пользователя уже сущестует"}'); }, 1000);
                return;
            }
            if ((Math.random(1)*10)>5)  {
                setTimeout(() => { res.status(200).json({ userId: 1200}); }, 1000);
                return;
            }
            else {
                setTimeout(() => { res.status(500).send('{ "detail": "Ошибка при сохранении пользователя"}'); }, 1000);
                return;
            }
        }
        
    });
}


module.exports.servUsers = function(app) {
    servUsersListGet(app, '/api/v2/clients/get_users');
    servUsersAvailableListGet(app, '/api/v2/clients/get_users_for_addition');
    servUsersGet(app, '/api/v2/users/get');
    servUsersDictionaries(app, '/api/v2/users/get_params');
    servUserGet(app, '/api/v2/users/get_user');
    servUserSave(app, '/api/v2/users/save_user');
}
