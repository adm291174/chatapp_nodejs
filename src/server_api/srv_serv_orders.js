const assert = require('assert'); 

var { ordersList_data1, 
      ordersList_data2,
      ordersPickerAccount_data1, 
      validOrders,
      actionsForOrder } = require('../test_data/test_data_orders'); 

var { extractDate, getShiftDays } = require('../test_data/test_data_functions');

var { operations_data_for_order_1 } = require('../test_data/test_data_operations');
var { getRandomInt } = require('./srv_func');

var servOrderGet = function(app, path) {
    app.put(path, function(req, res) {
        let { orderId } = req.body;
        
        assert(orderId && (typeof(orderId) == 'number'));
        
        console.log('Server: order getting operation, orderId', orderId);
        
        if (!validOrders.includes(orderId)) { 
            res.status(400).send('Bad request');
        }
        else {
            const ordersList = ordersList_data1(),
                  orderIndex = ordersList.findIndex(value => (value.orderId == orderId));
            
            if (orderIndex != -1) {
                const { orderNumber, orderDate, statusGuid, orderStatusDescription, 
                        orderClientId, orderClientName, orderSum, 
                        orderTypeGuid, orderTypeDescription, accountId, accountNumber,
                        accountCurrencyId, accountCurrencyCode, commissionType,
                        commissionDescription, commissionPercent, commissionSum, 
                        secondarySum, canBeEdited} = ordersList[orderIndex],

                    result = { 
                            orderId: orderId,
                            orderNumber: orderNumber,
                            orderStatusId: statusGuid,
                            orderStatusDescription: orderStatusDescription,
                            orderDate: orderDate,
                            orderType: orderTypeGuid,
                            orderTypeDescription: orderTypeDescription,
                            accountId: accountId,
                            accountNumber: accountNumber,
                            accountCurrencyId: accountCurrencyId,
                            accountCurrencyCode: accountCurrencyCode,
                            clientId: orderClientId,
                            clientName: orderClientName,
                            orderSum: orderSum,
                            commissionType: commissionType,
                            commissionDescription: commissionDescription, 
                            commissionPercent: commissionPercent,
                            commissionSum: commissionSum,
                            secondarySum: secondarySum,
                            canBeEdited: canBeEdited
                    } ;

                res.status(200);
                res.json(result);
            }
            else {
                res.status(400).send('Bad request');
            }
        }
    });
}

var servOrderGetLinkedOrders = function(app, path) {
    app.put(path, function(req, res) {
        let { orderId } = req.body;

        const result = ordersList_data1().map(value => {
            const { orderId, orderNumber, orderDate, statusGuid, 
                    orderStatusDescription, orderClientId, orderClientName,
                    orderSum, orderTypeGuid, orderTypeDescription, 
                    accountCurrencyId, accountCurrencyCode, 
                    accountId, accountNumber } = value;

            return {
                orderId: orderId,
                orderNumber: orderNumber,
                orderStatusId: statusGuid,
                orderStatusDescription: orderStatusDescription,
                orderDate: orderDate,
                orderType: orderTypeGuid,
                orderTypeDescription: orderTypeDescription,
                accountCurrencyId: accountCurrencyId,
                accountCurrencyCode: accountCurrencyCode,
                clientId: orderClientId,
                clientName: orderClientName,
                orderSum: orderSum,
                orderAccountId: accountId,
                orderAccountNumber: accountNumber
            }
        }).slice(0, 3);
            
        if (orderId == 9201) {
                res.status(400).send('Bad request');
            }
        else {
            res.status(200);
            if (orderId == 9200) 
                res.json([]);
            else 
               res.json(result);
        }
    });
}

var servOrderGetLinkedOperations = function(app, path) {
    app.put(path, function(req, res) {
        let { orderId } = req.body;
        assert(orderId && (typeof(orderId) == 'number'));
        
        if (orderId == 9201) {
                res.status(400).send('Bad request');
            }
        else {
            res.status(200);
            if (orderId == 9200) res.json([]);
            else res.json(operations_data_for_order_1());
        }
    });
}


var servOrderActionsList = function(app, path) {
    app.put(path, function(req, res) {
        let { orderId } = req.body;
        assert(orderId && (typeof(orderId) == 'number'));
        
        res.status(200);    
        if (orderId == 9200) 
            res.json([]);
        else 
            res.json(actionsForOrder);
    });
}

var servOrderGetOrdersForLink = function(app, path) {
    app.put(path, function(req, res) {
        let { orderType, startDate, endDate, onlyNotLinked } = req.body,
              startDateValue = extractDate(startDate),
              endDateValue = extractDate(endDate);


        const result = ordersList_data2()
                            .filter((value) => (!onlyNotLinked || value.linked))
                            .filter((value) => (extractDate(value.orderDate) >= startDateValue &&
                                                extractDate(value.orderDate) <= endDateValue))
                            .filter((value) => ( orderType == value.orderTypeGuid))
                            .map(value => {
            const { orderId, orderNumber, orderDate, statusGuid, 
                    orderStatusDescription, orderClientId, orderClientName,
                    orderSum, orderTypeGuid, orderTypeDescription, 
                    accountCurrencyId, accountCurrencyCode } = value;

            return {
                orderId: orderId,
                orderNumber: orderNumber,
                orderStatusId: statusGuid,
                orderStatusDescription: orderStatusDescription,
                orderDate: orderDate,
                orderType: orderTypeGuid,
                orderTypeDescription: orderTypeDescription,
                orderCurrencyId: accountCurrencyId,
                orderCurrencyCode: accountCurrencyCode,
                clientId: orderClientId,
                clientName: orderClientName,
                orderSum: orderSum

            }
        });
            
        if (startDateValue < getShiftDays(10)) {
                res.status(400).send('Bad request');
            }
        else {
            res.status(200);
            res.json(result);
        }
    });
}

var servOrderActionExecute = function(app, path) {
    app.post(path, function(req, res) {
        let { orderId, actionId } = req.body;
        assert(orderId && (typeof(orderId) == 'number'));
        assert(actionId && (typeof(actionId) == 'string'));
        
        if (orderId == 9199) 
            res.status(400).send('Bad request');
        else {
            res.status(200);
            res.json({});
        }
    });
}

var servOrdersList = function(app, path) {
    app.put(path, function(req, res) {
        
        const response_data = ordersList_data1()
        const { startDate, endDate, status } = req.body;

        const startDateFixed = extractDate(startDate),
              endDateFixed = extractDate(endDate)

        assert(startDateFixed instanceof Date);
        assert(endDateFixed instanceof Date);
        assert(status instanceof Array);


        const filteredResponseData = 
                response_data.filter(value => {
                const orderDate = extractDate(value.orderDate);
                return (orderDate >= startDateFixed && 
                        orderDate <= endDateFixed)
                        })
                        .filter(value => {
                            if (status.length) {
                                return status.includes(value.statusGuid);
                            }
                            else return true;
                        })
                    .map(value => {
                        const { orderId, orderNumber, orderDate, 
                                statusGuid, orderStatusDescription, orderClientId,
                                orderClientName, orderSum, orderCurrencyCode, 
                                orderTypeGuid, orderTypeDescription } = value;
                        return { orderId, orderNumber, orderDate, 
                                statusGuid, orderStatusDescription, orderClientId,
                                orderClientName, orderSum, orderCurrencyCode, 
                                orderTypeGuid, orderTypeDescription }

                    });
            
        //res.status(400).send('Bad request');
        res.status(200);
        res.json(filteredResponseData);
    });
}

var servAccsForOrderList = function(app, path) {
    app.put(path, function(req, res) {
        
        const response_data = ordersPickerAccount_data1();
        const { orderType } = req.body;

        const filteredResponseData = orderType 
                ? response_data.filter(value => {
                    return (value.orderType == orderType)}) 
                    : response_data
            
        // res.status(400).send('Bad request');
        res.status(200);
        res.json(filteredResponseData);
    });
}

var servOrderSave = function(app, path) {
    app.post(path, function(req, res) {
        let { orderId, orderType, accountId, 
              accountCurrencyId, clientId, orderSum, 
              commissionType, commissionPercent, commissionSum, 
              secondarySum, 
              addOrders, removeOrders } = req.body;
        
        orderId && assert(typeof(orderId) == 'number');
        assert(typeof(orderType) == 'string' && orderType.length == 32);
        assert(typeof(accountId) == 'number');
        assert(typeof(accountCurrencyId) == 'number');
        assert(typeof(clientId) == 'number');
        assert(typeof(orderSum) == 'number');
        assert(typeof(commissionType) == 'string' && orderType.length == 32);
        assert(typeof(commissionPercent) == 'number');
        assert(typeof(commissionSum) == 'number');
        assert(typeof(secondarySum) == 'number');
        assert(addOrders instanceof Array);
        assert(removeOrders instanceof Array);
        
        (addOrders.length > 0) && addOrders.forEach(value => { assert(typeof(value) == 'number') });
        (removeOrders.length > 0) && removeOrders.forEach(value => { assert(typeof(value) == 'number') });
        
        console.log('Server: order saving operation');
        
        if (clientId == 897) { 
            res.status(400).send('Bad request');            
        }
        else {
            const orderPosition = getRandomInt(0, validOrders.length-1);
            res.json({orderId: validOrders[orderPosition]});
        }
    });
}


module.exports.servOrders = function(app) {
    servOrderSave(app, '/api/v2/orders/save_order_data');
    servOrderGet(app, '/api/v2/orders/get_order_data');
    servOrderGetLinkedOrders(app, '/api/v2/orders/get_order_linked_orders');
    servOrderGetLinkedOperations(app, '/api/v2/orders/get_order_linked_operations');
    servOrderActionsList(app, '/api/v2/orders/get_order_actions');
    servOrderGetOrdersForLink(app, '/api/v2/orders/get_lo_picker_data');
    servOrderActionExecute(app,'/api/v2/orders/execute_action');
    servOrdersList(app, '/api/v2/orders/list');
    servAccsForOrderList(app, '/api/v2/orders/get_acc_picker_data');
}