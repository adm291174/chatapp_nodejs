import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
// rendered Components 
import ChatList from '../components/ChatList';
import Chat from '../components/Chat';
import AppWindow from '../components/AppWindow';

import RefreshIndicator from '../elements/RefreshIndicatorLoad';


class MainContainer extends Component {
    constructor(props) {
        super(props);
    }

    renderChat(chatInfo) {
        return chatInfo.get('isActive') ? 
                  (<Chat chatHistoryId={chatInfo.get('chatHistoryId')} />)
               : null

    }
    
    renderAppWindow(activeWindow, chatIsActive) {
        if (!chatIsActive && activeWindow != undefined) {
            return (<AppWindow windowName={ activeWindow } />);
        }
        else return null;
    }

    renderRefreshIndicator(refreshStatus) {
        // show refresh indicator if refreshStatus == true
        return RefreshIndicator(refreshStatus);
    }

    render() {
        const { appContext, isVisible, isRefreshData } = this.props;
        const chatInfo = appContext.get('chatContext');
        const chatIsActive = chatInfo.get('isActive');
        const activeWindow = appContext.get('activeWindow');
        return <div className='main'>
            <div className={ 'main-block' + (!isVisible ? ' main-block__hidden': '') }>
                { /* <div> Active page: { chatIsActive
                                     ? 'Chat, chatHistoryId: ' + String(chatInfo.get('chatHistoryId')) 
                                     : appContext.get('active') } 
                </div> */ } 
                { this.renderAppWindow(activeWindow, chatIsActive) }
                { this.renderChat(chatInfo) }
            </div>
            { /* <input className='main__checkbox' id='id_main__checkbox' type='checkbox'></input> */}
            <ChatList />
            { this.renderRefreshIndicator(isRefreshData) }
                <footer className='footer'></footer>
        </div>
    }
}

MainContainer.propTypes = {
    isVisible: PropTypes.bool
}

const mapStateToProps = (state) => {
    const chatListData = state.get('chatList').data;
    const rootReducerData = state.get('root').data
    return {
        appContext: rootReducerData.get('appContext'),        
        isVisible: !(chatListData.get('isOverlap') && chatListData.get('isVisible')),
        isRefreshData: rootReducerData.get('isRefreshData')
    };
}

export default connect(mapStateToProps)(MainContainer);