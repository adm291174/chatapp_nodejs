// import React, { Component, PropTypes } from 'react'
import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from './Header';
import StubPanel from './StubPanel';
import MainContainer from './MainContainer';
import { Map } from 'immutable';
import ErrorBoundary from '../components/ErrorBoundary';
import ErrorPanel from './ErrorPanel';
import { chatListUpdateInterval } from '..//constants/main_constants';


import * as appActions from '../actions/App';


class App extends Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
            timerId: undefined
        }
        this.getUser = props.appActions.actionAppSetUsersData;
        this.getChatListInfo = props.appActions.actionGetChatListData;
    }

    startChatListUpdateTimer() {
        this.getChatListInfo();
        return setInterval( () => this.getChatListInfo.bind(this), chatListUpdateInterval);
    }
    
    componentDidMount() {
        const hasChatAccess = this.props.hasChatAccess;
        if (hasChatAccess) {
            const timerId = this.startChatListUpdateTimer();
            this.setState({ timerId: timerId });
        }
    }

    componentDidUpdate(prevProps) {
        const userInfo = this.props.userInfo;
        if (!userInfo.equals(prevProps.userInfo)) {
            const hasChatAccess = userInfo.get('hasChatAccess');
            const prevChatAccess = prevProps.userInfo.get('hasChatAccess');
            if (hasChatAccess && !prevChatAccess) {
                const timerId = this.state.timerId; 
                if (!timerId) {
                    const newTimerId = this.startChatListUpdateTimer();
                    this.setState( (state) => { return Object.assign({}, state, {timerId: newTimerId})});
                }
            }
            if (!hasChatAccess && prevChatAccess) {
                const timerId = this.state.timerId; 
                if (timerId) clearInterval(timerId);
                    this.setState( (state) => { return Object.assign({}, state, {timerId: undefined})});
            }
        }
        
    }    

    componentWillUnmount() {
        const timerId = this.state.timerId; 
        if (timerId) clearInterval(timerId);
    }


    renderWorkApp(userInfo, dispatch, appContext) {
        return (
            <div className='app-wrapper'>
            <ErrorBoundary>
                <Header userInfo={ userInfo }
                        dispatch={ dispatch } 
                />
                <StubPanel />
                <MainContainer userId={ userInfo.get('userId') }
                               appContext={ appContext } 
                               />
                <ErrorPanel />
                
            </ErrorBoundary>
            </div>
        )
    }

    renderMaintenanceApp() {
        return (
            <div className='app-wrapper'>
                <div className='main'>
                    <div className='main__maintain-block'>
                        <img width='100%' src='static/images/maintenance.png'/>
                        <h1>Проводится техническое обслуживание</h1>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { userInfo, appContext, dispatch, systemStatus } = this.props,
              onMaintenance = (systemStatus == 'maintenance');
        
        if (onMaintenance) { 
            return this.renderMaintenanceApp()
            
        }
        else {
            return this.renderWorkApp(userInfo, dispatch, appContext)
        }
    }
}


function mapStateToProps (state) {
    let root = state.get('root'),
        rootData = root.data;

    return {
        userInfo: Map( { 
                    userName: rootData.get('userName'),
                    userId: rootData.get('userId'),
                    userRoles: rootData.get('userRoles'),
                    login: rootData.get('login'),
                    hasChatAccess: rootData.get('hasChatAccess'),
                    isManager: rootData.get('isManager'),
                    isClient: rootData.get('isClient'),
                    isAdmin: rootData.get('isAdmin'),
                    isUserAdmin: rootData.get('isUserAdmin'),
                    adminAppUrl: rootData.get('adminAppUrl'),
                    reportsGroups: rootData.get('reportsGroups'),
                    reports: rootData.get('reports'),
                    appVersion: rootData.get('appVersion'),
                    appDate: rootData.get('appDate')
                  }),            
        appContext: root.data.get('appContext'),
        systemStatus: rootData.get('systemStatus')
    }
}


function mapDispatchToProps (dispatch) {
return {
          appActions: bindActionCreators(appActions, dispatch),
          dispatch: dispatch
       }
} 

App.propTypes = {
    userInfo: PropTypes.instanceOf(Map).isRequired
}

export default (connect(mapStateToProps, mapDispatchToProps)(App));