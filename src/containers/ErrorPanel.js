import React, { Component } from 'react'
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { messageCategory } from '../constants/error_constants';

/* pure component depends on errorContext only 

    errorContext contains properties 
        errorsList: list of errors to show
        every record contains Map with following properties: 
            messageId: string,
            errorMessage: string
            errorCategory: error, info, default
*/


class ErrorPanel extends Component {

    onPanelClick(event) {
        event.preventDefault();
        this.props.dispatch( { type: 'ERROR_PANEL_EVENT_CLICK',
                               payload: null });
    }

    renderList(errorsList) {
        return (errorsList.map(errorMessageItem => {
            const messageId = errorMessageItem.get('messageId'),
                  errorMessage = errorMessageItem.get('errorMessage'),
                  errorCategory = errorMessageItem.get('errorCategory');
                                                               
            const errorCategoryClass = messageCategory.hasOwnProperty(errorCategory) ? messageCategory[errorCategory] : '';
            return (
                <article className={ 'error-panel__record ' + errorCategoryClass}
                 key={ messageId }>{ errorMessage }</article>
             )}));
    }

    render() {
        const { errorsList } = this.props;
        if (errorsList.size) {
            return (
                <div className='error-panel' onClick={this.onPanelClick.bind(this)}>
                    { this.renderList(errorsList) }
                </div>
            )
        }
        else return null;
    }
}


ErrorPanel.propTypes = {
    errorsList: PropTypes.instanceOf(List)
}


function mapStateToProps (state) {
    let error = state.get('error')
    return {
        errorsList: error.errorContext.get('errorsList')
    }
}


export default (connect(mapStateToProps)(ErrorPanel));
