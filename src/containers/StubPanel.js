import React, { Component } from 'react';
import { connect } from 'react-redux';
import $ from 'jquery';
import { fixChatElements } from '../core/handler_functions';


class StubPanel extends Component {
    constructor(props) {
        super(props);
    }

    componentDidUpdate() {
        fixChatElements(this.props.dispatch);
    }

    render() {
        let navBarHeight = $('header nav').outerHeight();
        return <div className='main-panel' id='id_main-panel'
                    style={ navBarHeight ? {height: navBarHeight + 'px'} : {} }>Panel</div>
    }
}

function mapStateToProps(state) {
    let uiReducer = state.get('uiReducer');
    return uiReducer.data.toJS();
}

export default (connect(mapStateToProps)(StubPanel));
