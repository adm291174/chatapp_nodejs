import React, { Component } from 'react'
import AppMenu from '../components/AppMenu'
import HeaderButtons from '../components/HeaderButtons'
import ChatListBtn from '../components/ChatListBtn'
import { PropTypes } from 'prop-types';
import { Map } from 'immutable';
import { Navbar } from 'react-bootstrap';
import { processUIData } from '../core/handler_functions';


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuCollapsed: true
        }
    }


    componentDidMount() {
        // Fix for expanding react navbar component 
        const element = document.getElementById('id_header-navbar');
        element.classList.remove('navbar-expand');
    }

    onClick() {
        this.setState({ menuCollapsed: !this.state.menuCollapsed });
    }

    onItemPick() {
        !this.state.menuCollapsed && this.setState({ menuCollapsed: !this.state.menuCollapsed });
        
    }

    componentDidUpdate(prevProps, prevState) {
        if (!prevState.menuCollapsed && this.state.menuCollapsed) {
            // Stub for resize markup issues 
            const _this = this;
            setTimeout(() => { if (_this.state.menuCollapsed) {
                                  processUIData(_this.props.dispatch); }}, 450);
        }
    }

    renderNavBar() {
        const { userInfo, dispatch } = this.props,
              { menuCollapsed } = this.state;

        return (
            <Navbar fixed='top' bg='dark' variant='dark' 
                    className='header-navbar' id='id_header-navbar'> 
                <Navbar.Toggle aria-controls='responsive-navbar-nav' 
                               className='header-navbar__toggler'
                               onClick={ this.onClick.bind(this) } 
                               />
                <HeaderButtons btnClickHandler={ this.onItemPick.bind(this) } 
                               userInfo={ userInfo }                               
                /> 
                <AppMenu dispatch={ dispatch }
                         userInfo={ userInfo }
                         menuCollapsed={ menuCollapsed } 
                         onItemPick={ this.onItemPick.bind(this) }
                />
                <ChatListBtn visible={ menuCollapsed }/>  
            </Navbar>
            
        );        


        /*  
        return <nav className='header-navbar navbar navbar-dark bg-dark fixed-top'>
            <button className='navbar-toggler header-navbar__toggler' type='button' 
                    data-toggle='collapse' data-target='#navbarSupportedContent' 
                    aria-controls='navbarSupportedContent' aria-expanded='false'
                    aria-label='Toggle navigation'>
                <span className='navbar-toggler-icon'></span>
            </button>
            <HeaderButtons btnClickHandler={ actionSubmitButton } /> 
            <AppMenu />
            <ChatListBtn />  
        </nav> */ 
         
    } 

    render() {
        return <header className='header'>
                   { this.renderNavBar() } 
               </header>
        
    }
}

Header.propTypes = {
    userInfo: PropTypes.instanceOf(Map).isRequired,
    dispatch: PropTypes.func.isRequired
}
                        
export default Header;