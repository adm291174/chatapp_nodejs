/* Core functions for ChatList */ 
import { Map, List } from 'immutable';
import { sortListOfMaps } from './core_functions';


function getGroupedChatList(chatList, category, categoryNameGet) {
    /* function for getting chat groups for chat list formation by category  
    params:
        chatList: immutable List with ChatListItemObj.getChatListItemObj() objects 
        category: name of category for goruping 
                  for example: oGroupId or timeCategoryId
        categoryNameGet: function for getting name of grouping category for visualisation,
                         this avoids extracting group name from ChatListItemObj properties 
                         for faster rendering 
    returns: 
        immutable Map object with keys as groupId, storing their names and lists with elements 
    */
    let result = Map();
    chatList.forEach( value => {
        let categoryId = value.get(category);
        let categoryName = categoryNameGet(value);
        if (!result.has(categoryId)) {
            result = result.set(categoryId, Map({
                                                name: categoryName,
                                                values: List().push(value)
                                            }));
        }
        else {
            result = result.update(categoryId, 
                                   groupRecordValue => (
                                        groupRecordValue.update('values', _list => _list.push(value)) 
                                   ))
        }
    });
    return result;
}

function extractGroups(groupsMap, sortItemField, sortByName = false, keyPrefix = '', reverse = false) {
    /*
       Fill immutable List with valid groups information 
       params: 
            groupsMap: Map of Lists of grouped values,
            sortItemField: filed name for sorting in group,
            sortByName: if set, sort result by Name, else sort by ID 
            keyPrefix: prefix for React element keys generation 
            reverse: if reverse order for sorting is needed (for elements sorting)
               

       returns: ordered List, every list element contains Map with:
            groupId: ID of group
            groupName: name of group for visualisation 
            groupValues: ordered List of Map(ChatListItem)
    */ 

    let result = List();
    for (let key of groupsMap.keys()) {
        let groupName = groupsMap.get(key).get('name');
        // Sort group items list 
        let groupItemsList = groupsMap.get(key)
                                        .get('values')
                                        .sort(sortListOfMaps(sortItemField, false, reverse));

        let groupItem = Map({groupId: key, 
                             groupName: groupName,
                             key: keyPrefix + '-' + key,
                             groupValues: groupItemsList});
        result = result.push(groupItem)
    }
    // sort groups List, convert to Int for groupId for distinct results 
    return sortByName 
           ? result.sort(sortListOfMaps('groupName', false, false)) 
           : result.sort(sortListOfMaps('groupId', true, false));
    
}

export { getGroupedChatList, extractGroups }