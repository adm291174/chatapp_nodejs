// Core functions module 

import { Map } from 'immutable';
import { monthName } from '../constants/main_constants'

// import moment from 'moment';

// Browser objects functions 

/* getCookie function */ 

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

/* CSRF token get function */ 

function getCSRFtoken() {
   var csrftoken = getCookie('csrftoken');
   return csrftoken;
}


// Element manipulation functions 

function extractFileName(fullFileName) {
    // Fix file name from information 
    let re = new RegExp('.*/([/]{0}.*)$');
    let matchStr = re.exec(fullFileName.replace(/\\/g,'/'));
    if (matchStr) return matchStr[1]; else return '';
}


function getMessageElementId(MessageId) {
   return 'id_message-' + MessageId.toString();
}


function getUserId() {
/* getUserId
   Extract userId from data attributes of #id_app-root element 
*/
    return +document.getElementById('id_app-root').getAttribute('data-user-id');
}


function hideElement(element) {
    // hide DOM element with class style setting 
    element.classList.add('element-hidden');
}

function showElement(element) {
    // show DOM element with class style setting 
    if (element.classList.contains('element-hidden'))
        element.classList.remove('element-hidden');
}

/* Time and date conversion functions */ 

function getDateFromJSON(strValue) {
    if (strValue) return new Date(Date.parse(strValue))
    else return null;
}

function getDateTimeStr(dateValue, shortFormat) {
    if (typeof shortFormat == 'undefined') shortFormat = false;
    let month = dateValue.getMonth()+1;
    let day = dateValue.getDate();
    let hours = dateValue.getHours();
    let minutes = dateValue.getMinutes();
    let seconds = dateValue.getSeconds();
    return ((day < 10) ? '0' + day : day) + '.' +
           ((month < 10) ? '0' + month : month) + '.' +
           dateValue.getFullYear() + ' ' +
           ((hours < 10) ? '0' + hours : hours) + ':' +
           ((minutes < 10) ? '0' + minutes : minutes) +
           ( (!shortFormat) ? ':' +((seconds < 10) ? '0' + seconds : seconds)  : '');
    /* return dateValue.toLocaleDateString([userLang, 'en'], dateTimeOptions) + ' ' +
           dateValue.toLocaleTimeString([userLang, 'en'], dateTimeOptions) */
}

function getDateStr(dateValue) {
    /* return DD.MM.YYYY string for visualization purposes */
    if (dateValue && (dateValue instanceof Date)) {
        let month = dateValue.getMonth()+1;
        let day = dateValue.getDate();
        return ((day < 10) ? '0' + day : day) + '.' +
            ((month < 10) ? '0' + month : month) + '.' +
            dateValue.getFullYear();
    }
    else return '';
    /* return dateValue.toLocaleDateString([userLang, 'en'], dateTimeOptions) + ' ' +
           dateValue.toLocaleTimeString([userLang, 'en'], dateTimeOptions) */
}

function getDateStrForQuery(dateValue) {
    /* return YYYY-MM-DD string to pass to AJAX query */ 
    if (dateValue && (dateValue instanceof Date)) {
        let month = dateValue.getMonth()+1;
        let day = dateValue.getDate();
        return dateValue.getFullYear().toString() + '-' + 
            ((month < 10) ? '0' + month : month).toString() + '-' + 
            ((day < 10) ? '0' + day : day);
    }
    else return '';
    /* return dateValue.toLocaleDateString([userLang, 'en'], dateTimeOptions) + ' ' +
           dateValue.toLocaleTimeString([userLang, 'en'], dateTimeOptions) */
}

function getDateTimeStrForInput(dateValue) {
    let month = dateValue.getMonth()+1;
    let day = dateValue.getDate();
    let hours = dateValue.getHours();
    let minutes = dateValue.getMinutes();
    let seconds = dateValue.getSeconds();
    return dateValue.getFullYear() + '-' +
           ((month < 10) ? '0' + month : month) + '-' +
           ((day < 10) ? '0' + day : day) + ' ' +
           ((hours < 10) ? '0' + hours : hours) + ':' +
           ((minutes < 10) ? '0' + minutes : minutes) + ':' +
           ((seconds < 10) ? '0' + seconds : seconds);
    /* return dateValue.toLocaleDateString([userLang, 'en'], dateTimeOptions) + ' ' +
           dateValue.toLocaleTimeString([userLang, 'en'], dateTimeOptions) */
}


function getDateTimeStrWithStrMonth(messageTime, showTodayValue = false) {
    /* if showTodayValue is set to <true> then we check the date for Today */ 
    const getValues = (date) => {
        const [dayOfDate, monthOfDate, yearOfDate] = [ date.getDate(), date.getMonth(), date.getFullYear()];
        return Map({ dayOfDate, monthOfDate, yearOfDate });
    }; 
    
    const [ messageDate, todayDate ]  = [ getValues(messageTime), getValues(new Date()) ];

    if (showTodayValue) {
        if (messageDate.equals(todayDate)) {
            return 'Сегодня'
        }
    }
    return (
        messageTime.getDate().toString() + ' ' +
        monthName[messageTime.getMonth()] + (
        messageDate.get('yearOfDate') == todayDate.get('yearOfDate') ?  '' : (' ' + messageTime.getFullYear().toString()))
    );
}


function getTimeStr(messageTime) {
    let hours = messageTime.getHours();
    let minutes = messageTime.getMinutes();
    return  ((hours < 10) ? '0' + hours : hours) + ':' +
             ((minutes < 10) ? '0' + minutes : minutes);

}


function makeClearDate(dateValue) {
    /* get clear date (YYYY-MM-DD in local time) */
    return new Date(
        dateValue.getFullYear(),
        dateValue.getMonth(),
        dateValue.getDate(),
        0,
        0,
        0
    );
}

function getCurrentDate() {
    /* get current clear Date */ 
    var currentTime = new Date();
    return makeClearDate(currentTime);
}

function getDateFromStr( strDate, strictFormat = null ) {
    /* 
        parse Date for string YYYY-MM-DD 
                or for string DD.MM.YYYY
        
        pass strictFormat for parsing only with this format 
    */ 
    
    let parsedYear = null,
        parsedMonth = null,
        parsedDay = null,
        parsedDate = null,
        parsedFormat = null; 
    
        
    if (strictFormat != 'DD.MM.YYYY') {
        const re = new RegExp('^([0-9]{4})-([0-9]{2})-([0-9]{2})$');
    
        const matchStr = re.exec(strDate);
        
        if (matchStr) {
            parsedYear = +matchStr[1], 
            parsedMonth = +matchStr[2],
            parsedDay = +matchStr[3],
            parsedFormat = 'YYYY-MM-DD'; 
            parsedDate = new Date(parsedYear, parsedMonth-1, parsedDay);
        }
    }

    if (!parsedFormat && strictFormat != 'YYYY-MM-DD') {
        const re = new RegExp('^([0-9]{2})\\.([0-9]{2})\\.([0-9]{4})$');
        const matchStr = re.exec(strDate);
        if (matchStr) {
            parsedYear = +matchStr[3], 
            parsedMonth = +matchStr[2],
            parsedDay = +matchStr[1],
            parsedDate = new Date(parsedYear, parsedMonth-1, parsedDay);
            parsedFormat = 'DD.MM.YYYY';
        }
    }
    if (parsedFormat && parsedDate) {
        // check whether date parsed correctly 
        const monthStrPre = '00' + (parsedDate.getMonth() + 1).toString(), 
              dayStrPre = '00' + parsedDate.getDate().toString();

        const formattedDate = (parsedFormat == 'YYYY-MM-DD') ? 
                                  
                            parsedDate.getFullYear().toString() + '-' +
                            monthStrPre.substr(monthStrPre.length-2, 2) + '-' + 
                            dayStrPre.substr(dayStrPre.length-2, 2) :
                                
                            dayStrPre.substr(dayStrPre.length-2, 2) + '.' + 
                            monthStrPre.substr(monthStrPre.length-2, 2) + '.' + 
                            parsedDate.getFullYear().toString();

        if (formattedDate == strDate) return parsedDate;
    }
    return null;    
}

// Object for get Category for Chat Item 
var messageTimeCategory = {
    today: 1,
    yesterday: 2,
    thisWeek: 3,
    earlier: 4,
    nochat: 5,
    properties: {
        1: {value: 'today'},
        2: {value: 'yesterday'},
        3: {value: 'this week'},
        4: {value: 'earlier'},
        5: {value: 'no conversation'}
    },
    propertiesRus: {
        1: {value: 'Сегодня'},
        2: {value: 'Вчера'},
        3: {value: 'На этой неделе'},
        4: {value: 'Давно'},
        5: {value: 'Без переписки'}
    },
    currentTime: () => new Date(),
    getCurrentTimeCategoryId: function(processedTime) {
        // assume that processedTime is Date object
        let currentTime = this.currentTime(); 
        if (!processedTime) return this.nochat;
        if (! (processedTime instanceof Date)) 
           throw ('getCurrentTimeCategory: argument is not of Date type');
        let processedTimeEpoch = processedTime.getTime();
        let todayBoard = new Date(
                            currentTime.getFullYear(),
                            currentTime.getMonth(),
                            currentTime.getDate(),
                            0,
                            0,
                            0
                         );
        let todayBoardEpoch = todayBoard.getTime();
        if (processedTimeEpoch >= todayBoardEpoch) {
            return this.today;
        }
        let yesterdayBoardEpoch = todayBoardEpoch - 86400000;
        if (processedTimeEpoch >= yesterdayBoardEpoch) {
            return this.yesterday;
        }
        // determine this week conversations
        let dayOfWeek = todayBoard.getDay();
        if (dayOfWeek == 0) dayOfWeek = 7;
        let startDayOfWeekEpoch = todayBoardEpoch - (dayOfWeek - 1) * 86400000;
        if (processedTimeEpoch >= startDayOfWeekEpoch) {
            return this.thisWeek;
        }
        else return this.earlier;
    },
    getTimeCategoryName(timeCategoryId) {
        return this.propertiesRus[timeCategoryId].value;
    }
}

// String Functions 
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function escapeHtml(string) {

 /* escape-html
 * Copyright(c) 2012-2013 TJ Holowaychuk
 * Copyright(c) 2015 Andreas Lubbe
 * Copyright(c) 2015 Tiancheng 'Timothy' Gu
 * MIT Licensed
 */

  var matchHtmlRegExp = /[''&<>]/;
  var str = '' + string;
  var match = matchHtmlRegExp.exec(str);

  if (!match) {
    return str;
  }

  var escape;
  var html = '';
  var index = 0;
  var lastIndex = 0;

  for (index = match.index; index < str.length; index++) {
    switch (str.charCodeAt(index)) {
      case 34: // '
        escape = '&quot;';
        break;
      case 38: // &
        escape = '&amp;';
        break;
      case 39: // '
        escape = '&#39;';
        break;
      case 60: // <
        escape = '&lt;';
        break;
      case 62: // >
        escape = '&gt;';
        break;
      default:
        continue;
    }

    if (lastIndex !== index) {
      html += str.substring(lastIndex, index);
    }

    lastIndex = index + 1;
    html += escape;
  }

  return lastIndex !== index
    ? html + str.substring(lastIndex, index)
    : html;
}

function replaceCr(value) {
    let newValue = value.replace(new RegExp('\\r\\n', 'g'), '<br/>');
    return newValue.replace(new RegExp('\\n', 'g'), '<br/>');
}

// Formattimg functions 

function floatPrettyPrint(number, 
                          thousand_separator = ' ', 
                          trimFractionPart = true) {
    /* formatting of float number for nice visualisation 

       transform 100897654.12 -> 100 897 654.12 
    
    */
    
    if (number === undefined || number === null ) return '';
    
    const fractionPart = Math.abs(number) - Math.floor(Math.abs(number)), 
        integerPart = Math.floor(Math.abs(number))
                            .toString().split('').reverse().join('')
                            .match(/\d{1,3}/g)
                            .join(thousand_separator).split('').reverse().join('');

    const isNegative = number < 0,
          fractionPartStr = trimFractionPart ? 
          ((fractionPart > 0) ? '.' + ('00' + Math.round((fractionPart*100)).toString()).slice(-2) : '') :
          ('.' + ('00' + Math.round((fractionPart*100)).toString()).slice(-2));

    return (isNegative ? '-' : '') + 
        integerPart + fractionPartStr;
        
}

function floatPrettyPrintForInput(x, decimalPoints) { 
    /* make string for html input element 
       param: x (number)
       param: decimalPoints - number of digits in fraction part 

       returns: string 
    */ 
    if (!x && x != 0) {
        return '';
    }
    else {
        var nbDec = (decimalPoints) ? Math.pow(10, decimalPoints) : 100;
        var a = Math.abs(x), 
            e = Math.floor(a),
            d = Math.round((a - e) * nbDec); 
        if (d == nbDec) { d = 0; e++; }
        var signStr = (x<0) ? '-' : '';
        var decStr = d.toString(); 
        var tmp = 10; 
        while ( tmp < nbDec && d * tmp < nbDec) {
            decStr = '0' + decStr; 
            tmp *= 10;
        }
        var eStr = e.toString();
        return signStr + eStr + ((parseInt(decStr) == 0) ? '' : '.' + decStr);
    }
}


// alternative round function 
const roundAlt = function(x, decimalPoints = 2) {
    var sign = (x >= 0) ? 1 : -1,
        powValue = Math.pow(10, decimalPoints),
        absValue = Math.abs(x),
        roundedAbsValue = (Math.round(absValue * powValue) / powValue).toFixed(decimalPoints);
    return sign * roundedAbsValue;
}


// immutable support functions 
function sortListOfMaps(sortItemField, convertToInt = false, reverse = false) {
    return function (a, b) {
        let checkForSort = function (a, b) {
            let _a = convertToInt ? +a.get(sortItemField) : a.get(sortItemField);
            let _b = convertToInt ? +b.get(sortItemField) : b.get(sortItemField);
            if (_a < _b) return -1;
            if (_a > _b) return 1;
            if (_a == _b) return 0;
        }

        return checkForSort(a, b) * (reverse ? -1 : 1);
    }
}



function fillIfEmptyMap(a = Map({})) {
    return a;
}

function isIExplorer() {
    /* detect whether browser is Internet Explorer 
       Source: https://msdn.microsoft.com/en-us/library/dn433245(v=vs.85).aspx
    */
    const getContextResult = document.body.msGetInputContext && document.body.msGetInputContext();
    return (getContextResult !== undefined)
}

export { 
    // Browser objects functions 
    getCookie, getCSRFtoken, isIExplorer,
    // Element manpulation functions 
    extractFileName, getMessageElementId, getUserId, hideElement, showElement,  
    // Date and time functions 
    getDateFromJSON, messageTimeCategory, getDateStr, getTimeStr, 
    getDateTimeStrForInput, getDateTimeStrWithStrMonth, getDateTimeStr, 
    getCurrentDate, getDateFromStr, getDateStrForQuery, 
    // String functions 
    capitalizeFirstLetter, 
    escapeHtml, 
    floatPrettyPrint, floatPrettyPrintForInput, 
    replaceCr, 
    // Conversion function 
    roundAlt, 
    // immutable support functions 
    fillIfEmptyMap, sortListOfMaps
}
