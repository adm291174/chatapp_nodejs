/* ChatList objects for data storing and conversion */ 

import { getDateFromJSON, messageTimeCategory } from './core_functions';
import { Map, List } from 'immutable';

/* 
   Class for storing and conversion functions 
   Time category is set inside at the construction time
*/

class ChatListItemObj {
    constructor (o) {
        this.chatId = o.chat_id;
        this.chatHistoryId = o.chat_history_id;
        this.lastMessageTime = o.last_message_time;
        this.multiChat = o.multichat;
        this.lastView = o.last_view;
        this.visibleToUser = o.visible_to_user;
        this.visibleName = o.visible_name;
        this.userId = o.user_id;
        this.oUserId = o.o_user_id;
        this.oUserName = o.o_user_name;
        this.oGroupId = o.o_group_id;
        this.oGroupName = o.o_group_name;
        this.oCssGroup = o.o_css_group;
        this.oIcon = o.o_icon;
        this.unreadMessagesCnt = o.unread_messages;
        this.lastMessageTimeDate = getDateFromJSON(this.lastMessageTime);
        this.lastViewDate = getDateFromJSON(this.lastView);
        this.timeCategoryId = messageTimeCategory.getCurrentTimeCategoryId
                                     .bind(messageTimeCategory)(this.lastMessageTimeDate);
        this.elementId = this.getChatItemElementId(this.chatHistoryId);
    }

    getChatItemElementId (id) {
        return 'id_chat-item-' + id.toString();
    }

    getChatHistoryObject() {
        return {
            chat_id: this.chatId,
            chat_history_id: this.chatHistoryId,
            last_message_time: this.lastMessageTime,
            multichat: this.multiChat,
            last_view: this.lastView,
            visible_to_user: this.visibleToUser,
            visible_name: this.visibleName,
            user_id: this.userId,
            o_user_id: this.oUserId,
            o_user_name: this.oUserName,
            o_group_id: this.oGroupId,
            o_group_name: this.oGroupName,
            o_css_group: this.oCssGroup,
            o_icon: this.oIcon,
            unread_messages: this.unreadMessagesCnt
        }    
    }

    getChatListItemObj() {
        return { 
            chatId: this.chatId,
            chatHistoryId: this.chatHistoryId, 
            lastMessageTime: this.lastMessageTime,
            multiChat: this.multiChat,
            lastView: this.lastView,
            visibleToUser: this.visibleToUser,
            visibleName: this.visibleName,
            userId: this.userId,
            oUserId: this.oUserId,
            oUserName: this.oUserName,
            oGroupId: this.oGroupId,
            oGroupName: this.oGroupName,
            oCssGroup: this.oCssGroup,
            oIcon: this.oIcon,
            unreadMessagesCnt: this.unreadMessagesCnt,
            lastMessageTimeDate: this.lastMessageTimeDate,
            lastViewDate: this.lastViewDate,
            timeCategoryId: this.timeCategoryId,
            elementId: this.elementId
        }
    }
    
    getChatListItemMap() {
        return Map(this.getChatListItemObj());
    }

}

// data conversion routines
var convertChatList = function(data) {
    return List(data.map(value => (new ChatListItemObj(value)).getChatListItemMap()));
}


export { ChatListItemObj, convertChatList }