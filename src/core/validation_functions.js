
const testRe = function(reValue, stringValue) {
    let re = new RegExp(reValue);
    let matchStr = re.exec(stringValue);
    if (matchStr) return true; else return false;
}

const testOnlyDigits = function(stringValue) {
    return testRe('^[0-9]{1,}$', stringValue);
};

const validateByRules = function (elementValue, rules) {
    /*  function for element values validation 
        Use for group validation by rules descriptions 

        Following rules are available: 

        noEmpty : empty fieldis not allowed 
        strictLength: value must be exactly the value or one of the array values 
        onlyDigits: value must contain only digits 
        onlyDigitsOrEmpty: value must contain only digits or could be empty 
        reCheck: check for regular expression 

        return true if element is valid or false if any of rules are 


        rule description: {
            name: "rule name",
            length: const or Array of const (for <strictLength> rule)
            reValue: check to match for this regular expression

    */

    let validateResult = true;
    rules.forEach(rule => {
        const ruleName = rule.name;
        switch (ruleName) {
            // validate for non empty string 
            case 'noEmpty': {
                validateResult = validateResult ? (elementValue.trim().length > 0) : validateResult;
                break;
            }
            case 'strictLength': {
                // validate for certain length 
                let lengthValues = rule.length;
                if (lengthValues) {
                    if (typeof(rule.length) == 'number')
                        lengthValues = [ lengthValues ];
                    let currentState = false;
                    lengthValues.forEach(value => { 
                        currentState = currentState ? currentState : (elementValue.length == value);                        
                    }); 
                    validateResult = currentState;
                }    
                break;
            }
            case 'onlyDigits': {
                // validate for digits only  
                validateResult = validateResult ? testOnlyDigits(elementValue) : validateResult;
                break;
            }
            case 'onlyDigitsOrEmpty': {
                // validate for digits or empty string 
                validateResult = validateResult 
                                    ? ((elementValue.trim().length == 0) || testOnlyDigits(elementValue)) : validateResult;
                break;
            }
            case 'reCheck': {
                // validate matching certain regular expression
                validateResult = testRe(rule.reValue, elementValue);
            }
        }
    });
    return validateResult;
}


var validateNumberRe = function(strNumber) {
    let re = new RegExp('^[-]{0,1}[0-9]{1,}[.]{0,1}[0-9]{0,}$');
    let matchStr = re.exec(strNumber.replace(/ /g,''));
    if (matchStr) return matchStr[0]; else return undefined;
}

function showElementValidityState (element, isValid) {
    if (isValid == null) { 
        // clear element validity signs 
        element.classList.remove('is-valid');
        element.classList.remove('is-invalid');
        return;
    }
    if (isValid) {
        element.classList.add('is-valid');
        element.classList.remove('is-invalid');
    }
    else {
        element.classList.remove('is-valid');
        element.classList.add('is-invalid');
    }
}

function showLabelValidityState (element, isValid, invalidClassName) {
    if (isValid) {
        element.classList.remove(invalidClassName);
    }
    else {
        element.classList.add(invalidClassName);
    }
}

function validateComponent(elementRef, needToSetFocus = true, prevElementState) {
    
    /* function for validating React component with its validate method */ 
    let validationState = prevElementState;

    if (elementRef.current) {
        const referredElement = elementRef.current.validate ? 
                                elementRef.current : 
                                null;
        if (referredElement) {
            const currentElementState = referredElement.validate.call(referredElement, 
                                                                      needToSetFocus && validationState);
            if (!currentElementState) {
                validationState = false;                    
            }
        }
    }
    return validationState
}

function validateElement(elementId, 
                         validationRules, 
                         prevValidationState,
                         needToShowState = true, 
                         needToSetFocus = true) {

    const element = document.getElementById(elementId);
    if (!element) return true;
    const elementValue = element.value;

    const validationState = validateByRules(elementValue, validationRules);
    if (needToShowState) showElementValidityState(element, validationState);
    if (!validationState && needToSetFocus && prevValidationState) element.focus();
    return validationState;
}

function ValidateDatePicker(elementRef, needToSetFocus = true, prevState) {
    if (!elementRef.current) return true; 
    const elementState = elementRef.current && elementRef.current.checkAndSetValidity();
    if (prevState && !elementState && needToSetFocus) {
        elementRef.current.setFocus();
    }
    return elementState;
}


const validateMixElement = function (validatedElement, 
                                     validationState = true, 
                                     needToSetFocus = true, 
                                     needToShowState = true) {
    
    const { validationType, element, rules } = validatedElement;
    let localState = null;
    if (validationType == 'component') {
        localState = validateComponent(element, needToSetFocus, validationState) ;
    }
    if (validationType == 'element') {
        localState = validateElement(element, 
                                        rules, 
                                        validationState, 
                                        needToShowState, 
                                        needToSetFocus);
    }
    if (validationType == 'datepicker') {
        localState = ValidateDatePicker(element, needToSetFocus, validationState);
    }
    if (validationType == 'function') {
        const { functionLink, callBackFunction } = validatedElement;                   
        
        if (functionLink) {
            localState = functionLink(needToSetFocus, 
                                        validationState,
                                        callBackFunction);
        }
    }
    validationState = validationState ? localState : validationState;
    return validationState;
}

function validateMix(validationList, 
                     needToSetFocus = true, 
                     needToShowState = true) {
    
    let validationState = true;
    validationList.forEach(validatedElement => {
        validationState = validateMixElement(validatedElement,
                                             validationState,
                                             needToSetFocus,
                                             needToShowState);
    });
    return validationState;
}

export {
    validateByRules, 
    validateNumberRe,
    showElementValidityState,
    showLabelValidityState,
    validateComponent,
    validateElement, 
    validateMix,
    validateMixElement
}