import { Map } from 'immutable';
import $ from 'jquery';
import { showElement, hideElement, escapeHtml, extractFileName } from '../core/core_functions';

const chatFooterHeight = 139; // Chat Footer height, change here to manage 

// Determine on which screen width (and less) ChatList should overlap other contents 
const chatListBoardValue = 680; 

const getUIData = () => Map({
        screenWidth: typeof window === 'object' ? window.innerWidth : null, 
        screenHeight: typeof window === 'object' ? window.innerHeight : null,
        timestamp: + new Date()
});

const fixChatElements = function() {
    // fix Close Button 
    const navBarElement = document.getElementById('id_header-navbar');
    navBarElement.classList.remove('navbar-expand');

    // Fix chat elements 
    let chat = document.getElementById('id_chat');
    let mainPanel = document.getElementById('id_main-panel');
    let closeButton = document.getElementById('id_chat-close-btn');
    let chatFooter = document.getElementById('id_chat-footer');
    let chatListBlock = document.getElementById('id_chat-list-block');
    const { screenHeight } = getUIData().toJS();

    if (chatListBlock && chat) {
        let mainPanelHeight = $('#id_main-panel').outerHeight();
        $('#id_chat-list-block').height(screenHeight - mainPanelHeight - 2);
    }
    else {
        $('#id_chat-list-block').css({'height': 'auto'});
    }

    if (chat && mainPanel) {
        // let mainPanelHeight = $('#id_main-panel').outerHeight();
        let chatOffsetY =  $('#id_chat').offset().top;        
        $('#id_chat-wrapper').height(screenHeight - chatOffsetY - chatFooterHeight);
    }
    if (mainPanel && chat) {
        // let mainPanelHeight = $('#id_main-panel').outerHeight();
        let chatOffsetY =  $('#id_chat').offset().top;
        let chatWidth = $('#id_chat').outerWidth();
        if (chatFooter && chat) {
            $('#id_chat-footer').width(chatWidth);
        }
        if (closeButton) {
            $('#id_chat-close-btn').css('top', String(chatOffsetY + 10) + 'px');
            $('#id_chat-close-btn').css('left', String(chatWidth - 35) + 'px');
        }
    }
};


var processUIData = (dispatch) => {
        const data = getUIData();
        dispatch({type: 'EVENT_SCREEN_RESIZE', payload: data});
    }

var getChatListUIState = (data) => {
    // true if screen is wider then chatListBoardValue
    return (data.get('screenWidth') > chatListBoardValue);
}
var onBasePageLoad = (dispatch) => {
    /* Base page load function */ 

    window.addEventListener('resize', () => {
        processUIData(dispatch);
    });

    // Start functions - execute them on Base page load 
    processUIData(dispatch);
    // Get UserInfo data 
    dispatch({type: 'GET_USER_INFO_DATA', payload: null });
    
    // start timer for message disposal 
    setInterval(() => {
        dispatch({type: 'ERROR_MESSAGE_DISPOSAL', payload: null});
    }, 2000);

    setInterval(() => {
        dispatch({type: 'PROCESS_SYSTEM_STATUS', payload: null});
    }, 10000);
    // get messages 
    dispatch({type: 'PROCESS_SYSTEM_STATUS', payload: null});
}

var setChatFileName = function(fileName) {
    var fileNameElement = document.getElementById('id_file-name-label');
    var fileBtnClear = document.getElementById('id_chat-form-btn-clear');
    if (fileNameElement && fileBtnClear) {
        if (fileName) {
                fileNameElement.textContent = escapeHtml(extractFileName(fileName));
                showElement(fileBtnClear);
            }
            else {
                fileNameElement.textContent = 'Выберите файл';
                hideElement(fileBtnClear);
        }
    } 
}



export { 
          // Event Handlers 
          // handleResize, 
          // Service functions 
          fixChatElements,
          getChatListUIState,
          getUIData,
          processUIData,
          onBasePageLoad,
          setChatFileName  }
