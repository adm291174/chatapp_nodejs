// module for user management functions 
import { showElementValidityState } from './validation_functions';
import { Map } from 'immutable';


const mustChangePwdInitial = false

const fillEmptyUserEditData = function() {
    return Map({
        userDataId: undefined,
        login: '',
        isActive: true,
        password: '',
        passwordConfirm: '',
        userMustChangePassword: mustChangePwdInitial,
        lastName: '',
        firstName: '',
        chatAccessEnabled: false,
        chatVisibleName: '',
        chatGroupId: null,
        userGroups: []
    });
}

const minPasswordLength = 8;

const prepareForSave = function(userData) {
    // function for data preparation before save (data clean depends on options) 
    const userId = userData.get('userDataId'),
          login = userData.get('login'),
          isActive = userData.get('isActive'),
          password = userData.get('password'),
          userMustChangePassword = userData.get('userMustChangePassword'),
          lastName = userData.get('lastName'),
          firstName = userData.get('firstName'),
          chatAccessEnabled = userData.get('chatAccessEnabled'),
          chatVisibleName = userData.get('chatVisibleName'),
          chatGroupId = userData.get('chatGroupId'),
          userGroups = userData.get('userGroups');

    
    return {
        userId: userId,
        isActive: isActive,
        userLogin: login, 
        userPassword: (password.length > 0) ? password : null,
        userMustChangePwd: (password.length > 0) ? userMustChangePassword : false,
        lastName: lastName,
        firstName : firstName,
        chatAccessEnabled: chatAccessEnabled,
        chatVisibleName: chatVisibleName,
        chatGroupId: chatGroupId,
        userGroups: userGroups
    };    
}

const validatePasswordForm = function(needToSetFocus, 
                                      prevState,
                                      callBackFunc) {
    /* validate function for password form 
       if password is filled, password and it's confirmation must match */

    const passwordElement = document.getElementById('id_fi-user-password_part-1'),
          passwordConfirmElement = document.getElementById('id_fi-user-password_part-2');

    if (passwordElement && passwordConfirmElement) {
        let validationState = false;

        validationState = (passwordElement.value == passwordConfirmElement.value);
        
        if (validationState && (passwordElement.value.length == 0)) {
            validationState = false;
            callBackFunc && callBackFunc(false);
        }
        else callBackFunc && callBackFunc(!validationState, passwordElement.value);
        

        if (prevState && !validationState && needToSetFocus) {
            passwordElement.focus();
        }
        
        // check password length 
        if (minPasswordLength && passwordElement.value.length < minPasswordLength) {
            validationState = false;
        }

        showElementValidityState(passwordElement, validationState);
        showElementValidityState(passwordConfirmElement, validationState);        
        return validationState;
    }
    else {
        showElementValidityState(passwordElement, null);
        callBackFunc && callBackFunc(true, '');
        return true;  // conceive everything is ok 
    }
}

export { prepareForSave, 
         validatePasswordForm, 
         minPasswordLength, 
         fillEmptyUserEditData,
         mustChangePwdInitial };
