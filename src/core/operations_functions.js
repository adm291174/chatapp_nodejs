import { Map, List } from 'immutable';
import { transferOperationType, supplyOperationType,
         settlementOperationType, simpleOperationType,
         supplyMoneyPart,
         supplyCommissionPart,
         settlementMoneyPart,
         settlementCommissionPart,
         debetOperationPart, 
         creditOperationPart } from '../constants/operations_constants';
import { getCurrentDate, getDateStrForQuery } from './core_functions';

const fillEmptyOperationData = function () {
    return Map(
    {
        operationDataId: null,
        operationType: undefined,
        operationTypeDescription: 'Не определено',
        operationNumber: '',
        operationDate: getCurrentDate(),
        operationSum: null,
        operationStatus: false,    
            
        transferData: fillEmptyTransferData(),
        supplyData: fillEmptySupplyData(), 
        settlementData: fillEmptySettlementData(),
        simpleOperationData: fillEmptySimpleOperationData(),
        transferPickerData: Map({
            transferPayerAccounts: List(),
            transferPayerOrders: List(),
            transferPayerPayReqs: List(),
            transferPayeeAccounts: List(),
            transferPayeeOrders: List(),
            transferPayeePayReqs: List()
        }),
        supplyPickerData: Map({
            supplyAccounts: List(),
            supplyLinkedOrdersData: List(),
            supplyAccountsMoneyPart: List(),
            supplyAccountsCommissionPart: List()
        }),
        settlementPickerData: Map({
            settlementAccounts: List(),
            settlementLinkedOrdersData: List(),
            settlementAccountsMoneyPart: List(),
            settlementAccountsCommissionPart: List() 

        }),
        simpleOperationPickerData: Map({
            accountsDebet: List(),
            accountsCredit: List()
        })
    });
}

const fillEmptyTransferData = function() {
    return Map({
        buyerOrderId: null,
        buyerOrderInfo: Map({
            orderNumber: null,
            orderDate: null
        }),
        buyerClientId: null,
        buyerClientName: '',
        buyerAccountId: null,
        buyerAccountNumber: '',
        buyerPaymentRequisitId: null,
        buyerPaymentRequisitInfo: Map({
            payRequisitClientId: null,
            payReqTaxNumber: '',
            payReqOrgName: '',
            payReqAccountNumber: '',
            payReqBankName: ''
        }),
        supplierOrderId: null,
        supplierOrderInfo: Map({
            orderNumber: null,
            orderDate: null
        }),
        supplierClientId: null,
        supplierClientName: '',
        supplierAccountId: null,
        supplierAccountNumber: '',
        supplierPaymentRequisitId: null,
        supplierPaymentRequisitInfo: Map({
            payRequisitClientId: null,
            payReqTaxNumber: '',
            payReqOrgName: '',
            payReqAccountNumber: '',
            payReqBankName: ''        
        }),
        paymentDocumentNumber: '',
        paymentDocumentDate: getCurrentDate(),
        paymentDocumentPurpose:  '',
        operationCurrencyId: null,
        operationCurrencyCode: null
    })
}

const fillEmptySupplyData = function() {
    return Map({
        supplierOrderId: null,
        supplierOrderInfo: Map({
            orderClientId: null,
            orderNumber: '',
            orderDate: getCurrentDate()
        }),
        supplierClientId: null,
        supplierClientName: '',
        supplierAccountId: null,
        supplierAccountNumber: '',
        supplyMoneyPart: Map({
            operationPartType: supplyMoneyPart,
            operationPartAccountId: null,
            operationPartAccountNumber: '',
            operationPartAccountName: '',
            operationPartSum: null,
            operationPartPurpose: ''
        }),
        supplyCommissionPart: Map({
            operationPartType: supplyCommissionPart,
            operationPartAccountId: null,
            operationPartAccountNumber: '',
            operationPartAccountName: '',
            operationPartSum: null,
            operationPartPurpose: ''
        }),
        operationCurrencyId: null,
        operationCurrencyCode: null
    });
}

const fillEmptySettlementData = function() {
    return Map({
        buyerOrderId: null,
        buyerOrderInfo: Map({
            orderClientId: null,
            orderNumber: '',
            orderDate: getCurrentDate()
        }),
        buyerClientId: null,
        buyerClientName: '',
        buyerAccountId: null,
        buyerAccountNumber: '',
        settlementMoneyPart: Map({
            operationPartType: settlementMoneyPart,
            operationPartAccountId: null,
            operationPartAccountNumber: '',
            operationPartAccountName: '',
            operationPartSum: null,
            operationPartPurpose: ''
        }),
        settlementCommissionPart: Map({
            operationPartType: settlementCommissionPart,
            operationPartAccountId: null,
            operationPartAccountNumber: '',
            operationPartAccountName: '',
            operationPartSum: null,
            operationPartPurpose: ''
        }),
        operationCurrencyId: null,
        operationCurrencyCode: null        
    });
}

const fillEmptySimpleOperationData = function() {
    return Map({
        debetOperationPart: Map({
            operationPartType: debetOperationPart,
            accountId: null,
            accountNumber: '',
            accountName: '',
            clientId: null,
            clientName: '',
        }),
        creditOperationPart: Map({
            operationPartType: creditOperationPart,
            accountId: null,
            accountNumber: '',
            accountName: '',
            clientId: null,
            clientName: '',
        }),
        operationPurpose: '',
        operationCurrencyId: null,
        operationCurrencyCode: null
    });
}

const prepareForSave = function(operationData) {
    
    const operationType =  operationData.get('operationType'),
          transferData = operationData.get('transferData'),
          supplyData = operationData.get('supplyData'),
          settlementData = operationData.get('settlementData'),
          simpleOperationData = operationData.get('simpleOperationData');
    
    let customOperationInfo = null,
        operationCurrencyId = null,
        operationSum = null;

    switch (operationType) {
        case transferOperationType: {
            customOperationInfo = {
                buyerOrderId: transferData.get('buyerOrderId'),
                buyerClientId: transferData.get('buyerClientId'),
                buyerAccountId: transferData.get('buyerAccountId'),
                buyerPaymentRequisitId: transferData.get('buyerPaymentRequisitId'),
                supplierOrderId: transferData.get('supplierOrderId'),
                supplierClientId: transferData.get('supplierClientId'),
                supplierAccountId: transferData.get('supplierAccountId'),
                supplierPaymentRequisitId: transferData.get('supplierPaymentRequisitId'),
                paymentDocumentNumber: transferData.get('paymentDocumentNumber'),
                paymentDocumentDate: getDateStrForQuery(transferData.get('paymentDocumentDate')),
                paymentDocumentPurpose: transferData.get('paymentDocumentPurpose')
                
            };
            operationCurrencyId = transferData.get('operationCurrencyId');
            operationSum = operationData.get('operationSum');
            break;
        }
        case supplyOperationType: {
            const supplyMoneyPart = supplyData.get('supplyMoneyPart'),
                  supplyCommissionPart = supplyData.get('supplyCommissionPart');

            customOperationInfo = {
                supplierOrderId: supplyData.get('supplierOrderId'),
                supplierClientId: supplyData.get('supplierClientId'),
                supplierAccountId: supplyData.get('supplierAccountId'),
                supplyMoneyPart: {
                    operationPartType: supplyMoneyPart.get('operationPartType'),
                    operationPartAccountId: supplyMoneyPart.get('operationPartAccountId'),
                    operationPartSum: supplyMoneyPart.get('operationPartSum'),
                    operationPartPurpose: supplyMoneyPart.get('operationPartPurpose')
                },
                supplyCommissionPart: {
                    operationPartType: supplyCommissionPart.get('operationPartType'),
                    operationPartAccountId: supplyCommissionPart.get('operationPartAccountId'),
                    operationPartSum: supplyCommissionPart.get('operationPartSum'),
                    operationPartPurpose: supplyCommissionPart.get('operationPartPurpose')
                }

            };
            operationCurrencyId = supplyData.get('operationCurrencyId');
            operationSum = supplyMoneyPart.get('operationPartSum');
            break;
        }
        case settlementOperationType: {
            const settlementMoneyPart = settlementData.get('settlementMoneyPart'),
                  settlementCommissionPart = settlementData.get('settlementCommissionPart');

            customOperationInfo = {
                buyerOrderId: settlementData.get('buyerOrderId'),
                buyerClientId: settlementData.get('buyerClientId'),
                buyerAccountId: settlementData.get('buyerAccountId'),
                settlementMoneyPart: {
                    operationPartType: settlementMoneyPart.get('operationPartType'),
                    operationPartAccountId: settlementMoneyPart.get('operationPartAccountId'),
                    operationPartSum: settlementMoneyPart.get('operationPartSum'),
                    operationPartPurpose: settlementMoneyPart.get('operationPartPurpose')
                },
                settlementCommissionPart: {
                    operationPartType: settlementCommissionPart.get('operationPartType'),
                    operationPartAccountId: settlementCommissionPart.get('operationPartAccountId'),
                    operationPartSum: settlementCommissionPart.get('operationPartSum'),
                    operationPartPurpose: settlementCommissionPart.get('operationPartPurpose')
                }
            };
            operationCurrencyId = settlementData.get('operationCurrencyId');
            operationSum = settlementMoneyPart.get('operationPartSum');
            break;
        } 
        case simpleOperationType: {
            customOperationInfo = {
                debetOperationPart: {
                    accountId: simpleOperationData.get('debetOperationPart').get('accountId')
                },
                creditOperationPart: {
                    accountId: simpleOperationData.get('creditOperationPart').get('accountId')
                },
                operationPurpose: simpleOperationData.get('operationPurpose')
            };
            operationCurrencyId = simpleOperationData.get('operationCurrencyId');
            operationSum = operationData.get('operationSum');
            break;
        }
    }
    
    return {
        operationId: operationData.get('operationDataId'),
        operationType: operationType,
        operationDate: getDateStrForQuery(operationData.get('operationDate')),
        operationSum: operationSum,
        operationCurrencyId: operationCurrencyId,
        operationStatus: operationData.get('operationStatus'),
        transferData: (operationType == transferOperationType) ? customOperationInfo : null,
        supplyData: (operationType == supplyOperationType) ? customOperationInfo : null,
        settlementData: (operationType == settlementOperationType) ? customOperationInfo : null,
        simpleOperationData: (operationType == simpleOperationType) ? customOperationInfo : null
    }
}

export { fillEmptyOperationData, 
         fillEmptyTransferData, 
         fillEmptySupplyData, 
         fillEmptySettlementData,
         fillEmptySimpleOperationData,
         prepareForSave } 