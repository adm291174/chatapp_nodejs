import { Map } from 'immutable';
// import { apiChatList_data1 } from '../test_data/test_data_chats'
import { convertChatList } from '../core/objects_chatlist';
import { getChatListUIState, getUIData } from '../core/handler_functions';


const initialState = {description: 'chatlist reducer state',
                      data: Map({
                          // chatListData: convertChatList(apiChatList_data1) // chatListData for further rendering 
                          chatListData: convertChatList([]),
                          isVisible: undefined,
                          isOverlap: !getChatListUIState(getUIData()) 
                      })};

function chatlistReducer(state = initialState, action) {
    switch (action.type) {
        // User info data retrieved 
        case 'DATA_СHATLIST_INFO': {
            return Object.assign({}, 
                                 state, 
                                 {data: state.data.set('chatListData', action.payload)});
        }
        case 'EVENT_SCREEN_RESIZE': {
            return state = Object.assign({},
                                         state,
                                         {data: state.data.set('isOverlap', 
                                                               !getChatListUIState(action.payload))}
                                        );
        }
        case 'CHAT_LIST_STATE_CHANGE': {
            return state = Object.assign({},
                                         state,
                                         {data: state.data.merge(action.payload)}
                                        );
        }
    default:
        return state;
    }

}

export default chatlistReducer;