import { Map, List } from 'immutable';
import { setChatFileName } from '../core/handler_functions';


const initialState = {
    description: 'Chat data section',
    data: Map({}) // should contain chatHistoryId key, every record contains 2 positions: 
    // messageList: orderder List of messages, 
    // chatInfo: actual chat info 
};

function getChatHistoryData(state, chatHistoryId) {
    let chatHistoryData = chatHistoryId ? state.data.get(chatHistoryId) : undefined;
    if (!chatHistoryData) {
        chatHistoryData = Map({
                messageList: List(), 
                chatInfo: Map(),
                formIsSubmitting: false,
                lastSaveWasFine: undefined,
                lastSaveResult: undefined,
                lastQueryTime: undefined,
                messageIsSaving: false,
                messageText: ''});
    }
    return chatHistoryData;
}


function chatReducer(state = initialState, action) {
    switch (action.type) {
        // User info data retrieved 
        case 'DATA_СHAT_INFO': {
            console.log('action DATA_СHAT_INFO reducing');
            const  {chatHistoryId, messageList, chatInfo, lastQueryTime } = action.payload;
            if (chatHistoryId) {
                let chatHistoryData = getChatHistoryData(state, chatHistoryId)
                if (chatInfo) {
                    chatHistoryData = chatHistoryData.merge(Map({chatInfo: chatInfo}));
                }
                if (messageList) {
                    chatHistoryData = chatHistoryData.merge(Map({messageList: messageList}));
                }
                chatHistoryData = chatHistoryData.set('lastQueryTime', lastQueryTime);
                
                return Object.assign({}, 
                                    state, 
                                    {data: state.data.set(chatHistoryId, chatHistoryData)});
            }
            return state;    
        }
        // process form message save  (start state)
        case 'CHAT_MESSAGE_SAVE': {
            console.log('action ' + action.type + ' reducing');
            const  {chatHistoryId} = action.payload;
            if (chatHistoryId) {
                let chatHistoryData = getChatHistoryData(state, chatHistoryId);
                return Object.assign({}, 
                                     state, 
                                     {data: state.data.set(chatHistoryId, 
                                                           chatHistoryData.set('formIsSubmitting', true))});
            }
            return state;
        }
        // process form message save (finish state)
        case 'CHAT_MESSAGE_SAVE_SUCCESS':
        case 'CHAT_MESSAGE_SAVE_FAILED': {
            console.log('action ' + action.type + ' reducing');            
            const chatHistoryId = action.chatHistoryId;
            if (chatHistoryId) {
                let chatHistoryData = getChatHistoryData(state, chatHistoryId);
                !action.error && setTimeout(() => { setChatFileName('') }, 500);
                return Object.assign({}, 
                                          state, 
                                          {data: state.data.set(chatHistoryId, 
                                                           chatHistoryData.set('formIsSubmitting', false)
                                                                          .set('lastSaveWasFine', !action.error)
                                                                          .set('lastSaveResult', action.payload)
                                                                          .set('messageText',
                                                                               !action.error ? '' : chatHistoryData.get('messageText')
                                                                          )     
                                                                          )});
            }                                                              
            return state;
        }
        case 'CHAT_MESSAGE_TEXT_UPDATE': {
            const chatHistoryId = action.chatHistoryId;
            if (chatHistoryId) {
                let chatHistoryData = getChatHistoryData(state, chatHistoryId);
                const messageText = action.payload;
                return Object.assign({}, 
                                     state, 
                                    {data: state.data.set(chatHistoryId, 
                                                          chatHistoryData.set('messageText', messageText))});
            }
            return state;
        }

    default:
        return state;
    }
}

export default chatReducer;