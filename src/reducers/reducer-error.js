import { Map, List } from 'immutable';



const initialState = {description: 'error reducer state',
                      errorContext: Map({
                          errorsList: List()
                          // errorsList: List([Map({ messageId: 'TEST', errorMessage: 'Ошибка сохранения', errorCategory: 'error' })])
                      })};

function searchMessage(errorsList, messageId) {
    return errorsList.findIndex(messageValue => (messageValue.get('messageId') == messageId));
}


function errorReducer(state = initialState, action) {
    switch (action.type) {
        // User info data retrieved 
        case 'ERROR_PANEL_EVENT_CLICK': {
            // dumb click reducer: clear errors list
            const errorsList = state.errorContext.get('errorsList'); 
            return Object.assign(
                {}, 
                state, 
                { errorContext: state.errorContext.set('errorsList', errorsList.clear()) }
            );
        }
        case 'ERROR_MESSAGE_ADD': {
            // add event to errorsList 
            const { messageId, errorMessage, errorCategory, concealTime } = action.payload;
            const errorsList = state.errorContext.get('errorsList'); 
            // add new messsage, update existing message 
            const messageIndex = searchMessage(errorsList, messageId);
            return Object.assign(
                  {}, 
                  state, 
                  { errorContext: state.errorContext.set('errorsList', 
                                                          (messageIndex == -1)
                                                          ? errorsList.push(
                                                                         Map({ messageId, 
                                                                               errorMessage, 
                                                                               errorCategory,
                                                                               concealTime }))
                                                          : errorsList.set(messageIndex, 
                                                                         Map({ messageId, 
                                                                               errorMessage, 
                                                                               errorCategory,
                                                                               concealTime })))}
            );

        }
        case 'ERROR_MESSAGE_REMOVE': {
            // add event to errorsList 
            const {messageId } = action.payload;
            const errorsList = state.errorContext.get('errorsList');
            // remove existing message 
            const messageIndex = searchMessage(errorsList, messageId);
            if (messageIndex != -1) {
                return Object.assign(
                    {}, 
                    state, 
                    { errorContext: state.errorContext.set('errorsList', 
                                                            errorsList.delete(messageIndex))}
                );
            }
            return state;               
            
        }
        case 'ERROR_MESSAGE_DISPOSAL': {
            const errorsList = state.errorContext.get('errorsList'),
                  boardTime = Date.now(); 
            // add new messsage, update existing message 
            // console.log('Reducing message disposal');

            const newErrorsList = errorsList.filter(value => {
                const concealTime = value.get('concealTime');
                return (!concealTime || concealTime > boardTime)
            })
            
            if (!newErrorsList.equals(errorsList)) {
                return Object.assign(
                        {}, 
                        state, 
                        { errorContext: state.errorContext.set('errorsList', 
                                                                newErrorsList)}
                    );
            }
            else return state;
        }
        default: {
            return state;
        }
    }
}

export default errorReducer;