import { Map } from 'immutable';


const initialState = {
    description: 'Reports data reducer',
    data: Map()
};


function reportReducer(state = initialState, action) {
    switch (action.type) {
        // process reports data get 
        case 'REPORTS_DATA': {
            const { reportId, reportData } = action.payload,
                  data = state.data;

            return Object.assign({}, 
                   state, {
                       data: data.set(
                           reportId, 
                           Map({
                               reportData: reportData,
                               dataProcessing: false
                           }))
                   });
        }
    } 
    return state;
}


export default reportReducer;