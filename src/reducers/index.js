// Main reducer module 

import { Map, List } from 'immutable';
// import { getUserId } from '../core/core_functions'
import { combineReducers } from 'redux-immutable';
import chatList from './reducer-chatlist';
import chat from './reducer-chat';
import error from './reducer-error';
import clients from './reducer-clients';
import orders from './reducer-orders';
import operations from './reducer-operations';
import reports from './reducer-reports';
import { appVersion, appDate } from '../constants/main_constants';
import users from './reducer-users';

import { uiReducer } from './reducer-ui';
import { coherentPairs } from '../constants/logic_constants';


import { saveCurrentContext, 
         setNewContext,
         initialWindowContext, 
         extractAndSetContent } from '../logic/context';


var setUserState = ({ userId, login, userName, userRoles, hasChatAccess,
                    isManager, isClient, isAdmin, isUserAdmin, adminAppUrl,
                    reportsGroups, reports } ) => {
    return Map({userId: userId,
        login: login,
        userName: userName,
        userRoles: List(userRoles),
        hasChatAccess: hasChatAccess,
        isManager: isManager,
        isClient: isClient,
        isAdmin: isAdmin,
        isUserAdmin: isUserAdmin,
        adminAppUrl: adminAppUrl,
        reportsGroups: reportsGroups,
        reports: reports,
        appVersion: appVersion,
        appDate: appDate
    });
}

const initialUserState = {
    userId: null, 
    login: undefined,
    userName: 'Информация о пользователе отсутствует',
    userRoles: [],
    hasChatAccess: false,
    isManager: false,
    isClient: false,
    isAdmin: false,
    isUserAdmin: false,
    adminAppUrl: '',
    reportsGroups: [],
    reports: []
}

const initialState = { 
    description: 'root reducder state data',
    data: setUserState(initialUserState)
              .merge(Map({appContext: Map({activeWindow: undefined, 
                                           windowContext: initialWindowContext,
                                           chatContext: Map({
                                               isActive: false,
                                               chatHistoryId: undefined 
                                            })
                                           })
                        }))
              .merge({systemStatus: 'work',
                      needLogin: false })
              .merge({isRefreshData: false})
}; 


function root(state = initialState, action) {
    switch (action.type) {
        // process system status change 
        case 'SYSTEM_STATUS': {
            const data = state.data;
            return {data: data.merge({ systemStatus: action.payload})}
        }
        // User info data retrieved 
        case 'DATA_USER_INFO': {
            // console.log('root reducer: action DATA_USER_INFO reducing');
            const data = state.data;
            return {data: data.merge(setUserState(action.payload))}
        }
        case 'APP_CONTEXT_SET': {
            return processAppContextChange(action, state);
        }
        case 'WINDOW_TITLE_SET': {
            return windowHeaderChanged(action, state);
        }
        case 'FILTER_DATA_CHANGED': {
            return processFilterDataChange(action, state);
        }
        case 'FILTER_MASS_SET': {
            return processFilterMassChange(action, state);
        }
        // process window filed changes -> pass to windowContext 
        case 'WINDOW_DATA_CHANGED': {
            return processWindowDataChanged(action, state);
        }
        case 'WINDOW_DATA_MASS_CHANGED': {
            return processWindowMassContentChange(action, state);
        }
        // Process cleintEdit data flow -> pass to windowContext 
        case 'CLIENT_MAIN_DATA': 
        case 'CLIENT_PAYREQ_DATA':
        case 'CLIENT_ACCOUNT_DATA':
        case 'CLIENT_USERS_DATA': {
            return processClientEditData(action, state);
        }
        case 'PAYREQ_DATA': {
            return extractAndSetContent(action, state, 'paymentRequsitEdit');
        }
        case 'CLIENT_USERS_AVAILABLE_DATA': {
            return extractAndSetContent(action, state, 'clientEditAddUser');
        }
        case 'ORDER_EDIT_PICKER_DATA': {
            return extractAndSetContent(action, state, 'orderEdit');
        }
        case 'SET_REFRESH_FLAG': {
            const { payload } = action,
                  data = state.data;
            return {data: data.merge({ isRefreshData: payload.isRefreshing}) };            
        }
        case 'USERS_LIST_DATA': {
            return processWindowDataChanged(action, state);            
        }
    default:
        return state;
    }
}

const processAppContextChange = function(action, state) {
    const { window, context, chatClose } = action.payload;
    const data = state.data;
    let savedContext = saveCurrentContext(data.get('appContext'), window);
    const newContext = setNewContext(savedContext, window, context, chatClose);
    return Object.assign({},
                         state, 
                        {data: data.merge({appContext: newContext})});

}

const processFilterDataChange = function(action, state) {
    // pass subFilterId for conplicated filter sets (reports implementation) 
    const {windowName, filterElementName, filterData, subFilterId} = action.payload;
    const data = state.data;
    const appContext = data.get('appContext'),
          currentWindowContext = appContext.get('windowContext').get(windowName),
          currentFilterSet = currentWindowContext.get('windowFilter'),
          identifiedFilterValues = subFilterId ? currentFilterSet.get('identifiedFilterValues') : null,
          subFilterValue = subFilterId ? identifiedFilterValues.get(subFilterId) : null, 
          filterValues = subFilterId ? subFilterValue.get('filterValues') : currentFilterSet.get('filterValues');

    const newFilterValues = filterValues.set(
                                filterElementName,
                                filterData
                            )
                            .merge(processLinkedElementsChange(filterValues,
                                                               filterElementName,
                                                               filterData))

    const newAppContext = appContext.set(
        'windowContext', 
        appContext.get('windowContext').set(
            windowName, 
            currentWindowContext.set(
                'windowFilter', 
                subFilterId ? 
                currentFilterSet.set(
                    'identifiedFilterValues', 
                        identifiedFilterValues.set(
                            subFilterId, subFilterValue.set(
                                'filterValues', newFilterValues             
                            )
                        )
                     
                ) : 
                currentFilterSet.set('filterValues',
                    newFilterValues  
                ) 
            )
        )
    );
    return Object.assign({},
                            state, 
                            {data: data.merge({appContext: newAppContext})});
}

const processFilterMassChange = function(action, state) {
    // expecting Map to merge with 
    const {windowName, filterValues} = action.payload;
    const data = state.data;
    const appContext = data.get('appContext');
    const currentWindowContext = appContext.get('windowContext').get(windowName);
    const currentFilterSet = currentWindowContext.get('windowFilter');
    const currentfilterValues = currentFilterSet.get('filterValues');
    const newAppContext = appContext.set(
        'windowContext', 
        appContext.get('windowContext').set(
            windowName, 
            currentWindowContext.set(
                'windowFilter',
                currentFilterSet.set(
                    'filterValues', 
                    currentfilterValues.merge(filterValues)
                )
            )
        )
    )
    return Object.assign({},
                            state, 
                            {data: data.merge({appContext: newAppContext})});
}

const processLinkedElementsChange = function (filterValues,
                                              filterElementName, 
                                              filterData) {
    /* 
       function for coherent setting filter values 
       return merged contents, or empty object if nothing should be changed 

    */
    if (coherentPairs.hasOwnProperty(filterElementName)) {
        const { pairType, pairField } = coherentPairs[filterElementName];
        const pairFieldValue = filterValues.get(pairField);
        switch (pairType) {
            // check whether we set lower value more then higher value 
            case 'low': {
                if (pairFieldValue < filterData) {
                    return Map({}).set(pairField, filterData);
                }
                break;
            }
            // check whether we set higher value less then lower value 
            case 'high': {
                if (pairFieldValue > filterData) {
                    return Map({}).set(pairField, filterData);
                }
                break;
            }
        }
    }
    return {};
}

const processWindowDataChanged = function(action, state) {
    const { windowName, fieldName, fieldValue } = action.payload;
    const data = state.data;
    const appContext = data.get('appContext');
    const windowContext = appContext.get('windowContext');
    const currentWindowContext = windowContext.get(windowName);
    const windowContents = currentWindowContext.get('windowContent');

    return Object.assign({},
                            state, 
                            {data: data.set('appContext', 
                                    appContext.set('windowContext',
                                        windowContext.set(windowName,  
                                            currentWindowContext.set('windowContent',
                                            windowContents.set(fieldName, fieldValue)
                                        )
                                        )   
                                    )
                            )});
        
    
}

const processClientEditData = function(action, state) {
    /* change client edif data */
    const { clientId, windowContent } = action.payload;
    const windowName = 'clientEdit';
    const data = state.data;
    const appContext = data.get('appContext');
    const windowContext = appContext.get('windowContext');
    const currentWindowContext = windowContext.get(windowName);
    const currentWindowContent = currentWindowContext.get('windowContent');

    return Object.assign({},
                            state, 
                            {data: data.set('appContext', 
                                    appContext.set('windowContext',
                                        windowContext.set(windowName,  
                                            currentWindowContext.set('windowContent',
                                               currentWindowContent.set('clientId', clientId)
                                               .merge(windowContent)
                                           )
                                        )   
                                    )
                            )});

}

const processWindowMassContentChange = function(action, state) {
    const { windowName, windowContent } = action.payload;
    const data = state.data,
          appContext = data.get('appContext'),
          windowContext = appContext.get('windowContext'),
          currentWindowContext = windowContext.get(windowName),
          currentWindowContent = currentWindowContext.get('windowContent');

    return Object.assign({},
                            state, 
                            {data: data.set('appContext', 
                                    appContext.set('windowContext',
                                        windowContext.set(windowName,  
                                            currentWindowContext.set('windowContent',
                                               currentWindowContent.merge(windowContent)
                                           )
                                        )   
                                    )
                            )});

}

const windowHeaderChanged = function(action, state) {
    /* 
         
         set new Window title 
         required for Edit objects with data processed in reducers other than ROOT

    */
    const { windowName, windowTitle } = action.payload;

    const data = state.data;
    const appContext = data.get('appContext'), 
          windowContext = appContext.get('windowContext'),
          currentWindowContext = windowContext.get(windowName);
    
    return Object.assign({},
                            state, 
                            {data: data.set('appContext', 
                                    appContext.set('windowContext',
                                        windowContext.set(windowName,  
                                            currentWindowContext.set('windowTitle',
                                               windowTitle
                                           )
                                        )   
                                    )
                            )});  
}


const rootReducer = combineReducers( { root, 
                                       chatList, 
                                       uiReducer, 
                                       chat, 
                                       error, 
                                       clients,
                                       orders,
                                       operations,
                                       reports,
                                       users } );

export default rootReducer