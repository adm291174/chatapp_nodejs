// Reducer for UI changes actions (size of window, etc)
import { getUIData } from '../core/handler_functions'


const initialState = {
    data: getUIData()
};

function uiReducer(state = initialState, action) {
    switch (action.type) {
        case 'EVENT_SCREEN_RESIZE':
            return Object.assign({}, state, {data: action.payload});
    }
    return state;
}

export { getUIData, uiReducer } 