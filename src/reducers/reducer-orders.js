import { Map, List } from 'immutable';
import { getDateFromStr } from '../core/core_functions';

import { emptyOrderBodyData } from '../constants/order_constants';


const initialState = {
    description: 'Orders data section',
    data: Map({
        ordersList: List(),
        orderData: Map({
            orderDataId: null,
            orderBodyData: emptyOrderBodyData,
            orderLinkedOrders: List(),
            orderLinkedOperations: List(),
            orderActions: List()
        })
    }) 
};


function ordersReducer(state = initialState, action) {
    switch (action.type) {
        // Orders list data retrieved 
        case 'ORDERS_LIST_DATA': {
            const  { ordersList } = action.payload;
            
            return Object.assign({}, 
                                state, 
                                { data: state.data.set('ordersList', 
                                        processOrdersList(ordersList)) }); 
        }
        case 'ORDER_DATA': {
            return processOrderData(state, action);
        }
        case 'ORDER_EDIT_DATA_CHANGED': {
            return processOrderEditDataChanged(state, action);
        }
        default:
            return state;
    }
}

function processOrderData(state, action) {
    /* 
        Pass list of affected items in <action.payload>
        This routine coukd be used for mass order body editions! 

        affectedRecord is simple list of objects:
            orderDataName: string 
            orderDataValue: suitable data for state update 

    */
    const orderDataRecords = action.payload;

    let orderData = state.data.get('orderData');

    orderDataRecords.forEach(affectedRecordInfo => {
        const { orderDataName, orderDataValue } = affectedRecordInfo;
        if (orderDataName == 'orderBodyData') {
            const orderBodyData = orderData.get('orderBodyData');
            orderData = orderData.set('orderBodyData', 
                                       orderBodyData.merge(orderDataValue));
        }
        else 
            orderData = orderData.set(orderDataName, orderDataValue);
    })

    return Object.assign({}, 
                        state, 
                        { data: state.data.set(
                            'orderData',
                            orderData)
                        });
}

function processOrderEditDataChanged(state, action) {
    const { fieldName, 
            fieldValue } = action.payload;

    const orderData = state.data.get('orderData'),
          orderBodyData = orderData.get('orderBodyData');

    return Object.assign({}, 
                        state, 
                        { data: state.data.set(
                                    'orderData',
                                        orderData.set(
                                            'orderBodyData', 
                                            orderBodyData.set(
                                                fieldName,
                                                fieldValue
                                            )
                                        ) 
                                )   
                        });
}

function processOrdersList(ordersList) {
    return List(
        ordersList.map( (value) => {
            return Map({
                orderId: + value.orderId,
                orderNumber: value.orderNumber,
                orderDate: getDateFromStr(value.orderDate),
                statusGuid: value.order_status_edit, 
                orderStatusDescription: value.orderStatusDescription, 
                orderClientId: + value.orderClientId,
                orderClientName: value.orderClientName,
                orderSum: value.orderSum,
                orderCurrencyCode: value.orderCurrencyCode,
                orderTypeGuid: value.orderTypeGuid,
                orderTypeDescription: value.orderTypeDescription
            })
        })
    );
}

export default ordersReducer;