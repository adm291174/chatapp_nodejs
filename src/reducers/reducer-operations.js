import { Map, List } from 'immutable';
import { getDateFromStr } from '../core/core_functions';
import { fillEmptyOperationData } from '../core/operations_functions'


const initialState = {
    description: 'Operations data section',
    data: Map({
        operationsList: List(),
        operationData: fillEmptyOperationData()
    }) 
};

function operationsReducer(state = initialState, action) {
    switch (action.type) {
        // Orders list data retrieved 
        case 'OPERATIONS_LIST_DATA': {
            const  { operationsList } = action.payload;
            
            return Object.assign({}, 
                                state, 
                                { data: state.data.set('operationsList', 
                                        processOperationsList(operationsList)) }); 
        }
        case 'OPERATION_EDIT_DATA_CHANGED': {
            return processOperationEditDataChanged(state, action);
        }
        case 'OPERATION_DATA': {
            return processOperationData(state, action);
        }
        case 'OPERATIONS_PICKER_DATA_ACCS':
        case 'OPERATIONS_PICKER_DATA_PAYREQS':
        case 'OPERATIONS_PICKER_DATA_ORDERS': {
            return processOperationsPickerChange(state, action);
        }
        default:
            return state;
    }
}

function processOperationsList(ordersList) {
    return List(
        ordersList.map( (value) => {
            return Map({
                operationId: + value.operationId,
                operationNumber: value.operationNumber,
                operationDate: getDateFromStr(value.operationDate),
                status: value.status, 
                operationStatusDescription: value.operationStatusDescription, 
                clients: List(value.clients), //   object with { operationClientId, operationClientName }
                operationSum: value.operationSum,
                operationCurrencyCodeId: value.operationCurrencyCodeId,
                operationCurrencyCode: value.operationCurrencyCode,
                operationType: value.operationType,
                operationTypeDescription: value.operationTypeDescription
            })
        })
    );
}


function processOperationEditDataChanged(state, action) {

    const { operationPart,
            values } = action.payload;

    const operationData = state.data.get('operationData')
    let editPartValue = operationPart ? operationData.get(operationPart) : operationData;
    
    values.forEach( ({fieldName, fieldValue}) => {
        editPartValue = editPartValue.set(fieldName, fieldValue);
    })

    if (operationPart) {
        return Object.assign({}, 
                        state, 
                        { data: state.data.set(
                                    'operationData',
                                        operationData.set(
                                            operationPart, 
                                            processOperationPartChanges(operationData,
                                                                        operationPart,
                                                                        editPartValue)
                                            )
                                    ) 
                        });
    }  
    else {
        return Object.assign({}, 
                      state, 
                      { data: state.data.set(
                                  'operationData',
                                  editPartValue    
                              )   
                      });
    }
}

function processOperationPartChanges(operationData, operationPart, editPartValue) {
    /* process account changes here, avoiding use of componentDidUpdate 
       return new editPartValue 
    */
    if (operationPart == 'transferData') {
        const oldTransferData = operationData.get(operationPart),
              buyerAccountId = oldTransferData.get('buyerAccountId'),
              supplierAccountId = oldTransferData.get('supplierAccountId');

        // check whether new buyerAccount is set 
        const newBuyerAccountId = editPartValue.get('buyerAccountId'),
              newSupplierAccountId = editPartValue.get('supplierAccountId');
        if (newBuyerAccountId) {
            if ((newBuyerAccountId != buyerAccountId) && 
                (supplierAccountId == newSupplierAccountId)) {
                return editPartValue.merge(Map({
                    buyerOrderId: null,
                    buyerOrderInfo: Map({
                        orderClientId: null,
                        orderNumber: null,
                        orderDate: null
                    }),
                    buyerPaymentRequisitId: null,
                    buyerPaymentRequisitInfo: Map({
                        payRequisitClientId: null,
                        payReqTaxNumber: '',
                        payReqOrgName: '',
                        payReqAccountNumber: '',
                        payReqBankName: ''
                    }),
                    supplierClientId: null,
                    supplierClientName: '',
                    supplierAccountId: null,
                    supplierAccountNumber: '',
                    supplierOrderId: null,
                    supplierOrderInfo: Map({
                        orderClientId: null,
                        orderNumber: null,
                        orderDate: null
                    }),
                    supplierPaymentRequisitId: null,
                    supplierPaymentRequisitInfo: Map({
                        payRequisitClientId: null,
                        payReqTaxNumber: '',
                        payReqOrgName: '',
                        payReqAccountNumber: '',
                        payReqBankName: ''
                    }) 
                }));
            }
        }    
        if (newSupplierAccountId && (supplierAccountId != newSupplierAccountId)) {
            return editPartValue.merge(Map({
                supplierOrderId: null,
                supplierOrderInfo: Map({
                    orderClientId: null,
                    orderNumber: null,
                    orderDate: null
                }),
                supplierPaymentRequisitId: null,
                supplierPaymentRequisitInfo: Map({
                    payRequisitClientId: null,
                    payReqTaxNumber: '',
                    payReqOrgName: '',
                    payReqAccountNumber: '',
                    payReqBankName: ''
                }) 
            }));
        }
    }    
    return editPartValue;
}

function processOperationData(state, action) {
    /* 
        Pass list of affected items in <action.payload>
        This routine coukd be used for mass order body editions! 

        affectedRecord is simple list of objects:
            orderDataName: string 
            orderDataValue: suitable data for state update 

    */
    const operationData= action.payload;

    const stateOperationData = state.data.get('operationData');
    
    return Object.assign({}, 
                         state, 
                        { data: state.data.set(
                            'operationData',
                            stateOperationData.merge(operationData))
                        });
}

function processOperationsPickerChange(state, action) {
    const { pickerName, data } = action.payload;
    
    let operationPart = null;
    switch (pickerName) {
        case 'transferPayerAccounts':
        case 'transferPayerOrders':
        case 'transferPayerPayReqs':
        case 'transferPayeeAccounts':
        case 'transferPayeeOrders':
        case 'transferPayeePayReqs': {
            operationPart = 'transferPickerData';
            break;
        }
        case 'supplyAccounts':
        case 'supplyLinkedOrdersData':
        case 'supplyAccountsMoneyPart':
        case 'supplyAccountsCommissionPart': {
            operationPart = 'supplyPickerData';
            break;
        }
        case 'settlementAccounts':
        case 'settlementLinkedOrdersData':
        case 'settlementAccountsMoneyPart':
        case 'settlementAccountsCommissionPart': {
            operationPart = 'settlementPickerData';
            break;
        }
        case 'accountsDebet':
        case 'accountsCredit': {
            operationPart = 'simpleOperationPickerData';
            break;
        }
    }

    const newAction = { payload: {
        operationPart: operationPart,
        values: [
            { fieldName: pickerName,
              fieldValue: data } 
        ]}};
    return processOperationEditDataChanged(state, newAction);    
}


export default operationsReducer;
