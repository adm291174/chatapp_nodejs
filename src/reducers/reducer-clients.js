import { Map, List } from 'immutable';

const initialState = {
    description: 'Clients data section',
    data: Map({
        clientsList: List(),
        accountsList: Map()
    }) 
};

function clientsReducer(state = initialState, action) {
    switch (action.type) {
        // Clients list data retrieved 
        case 'CLIENTS_LIST_DATA': {
            const  { clientsInfo, accountsInfo } = action.payload;
                
            return Object.assign({}, 
                                state, 
                                {data: state.data.set('clientsList', clientsInfo).
                                                  set('accountsList', accountsInfo)});            
       }

    default:
        return state;
    }
}


export default clientsReducer;