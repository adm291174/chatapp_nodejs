import { Map, List } from 'immutable';
import { fillEmptyUserEditData, mustChangePwdInitial } from '../core/user_functions';


const initialState = {
    description: 'Users data section',
    data: Map({
        dictionaries: Map({}),
        userData: fillEmptyUserEditData()  // if empty, load them via API call (in editUser component)
    })
};


function usersReducer(state = initialState, action) {
    switch (action.type) {
        case 'USERS_DICTIONARY_DATA': {
            const { chatGroups, userGroups} = action.payload;
            const dictionaries = state.data.get('dictionaries');
            return Object.assign({}, 
                                 state, 
                                 { data: state.data.set('dictionaries', 
                                            dictionaries.set('chatGroups', 
                                                processUserDictionaryData(chatGroups))
                                            .set('userGroups',
                                                processUserDictionaryData(userGroups))
                                    )
                                 }); 
        }
        case 'USER_EDIT_DATA_CHANGED': {
            return processUserEditDataChanged(state, action);
        }
        case 'USER_DATA': {
            return processUserData(state, action);
        }
        case 'USER_DATA_MASS_CHANGE': {
            return processUserEditMassChange(state, action);
        }
        default: {
            return state
        }
    }
}

const processUserDictionaryData = function(listData) {
    return List(listData.map(value => {
        return Map({
            groupId: value.groupId,
            groupName: value.groupName
        })
    }));
}


function processUserEditDataChanged(state, action) {
    const { fieldName,
            fieldValue } = action.payload;

    const userData = state.data.get('userData');

    return Object.assign({}, 
                        state, 
                        { data: state.data.set(
                                    'userData',
                                        userData.set(
                                            fieldName,
                                            fieldValue
                                        ) 
                                )   
                        });
}

function processUserEditMassChange(state, action) {

    const userData = state.data.get('userData'),
          newData = action.payload;

    return Object.assign({}, 
                        state, 
                        { data: state.data.set(
                                    'userData',
                                        userData.merge(
                                            newData
                                        ) 
                                )   
                        });
}


function processUserData(state, action) {
    const { userId, userLogin, isActive, lastName, firstName,
            chatAccessEnabled, chatVisibleName, chatGroupId, userGroups } = action.payload,
          userDataId = userId,
          password = '',
          passwordConfirm = '';
    
    return processUserEditMassChange(
                state, 
                { payload: Map({
                        userDataId: userDataId,
                        login: userLogin,
                        password: password,
                        passwordConfirm: passwordConfirm,
                        userMustChangePassword: mustChangePwdInitial,
                        isActive: isActive,
                        lastName: lastName,
                        firstName: firstName,
                        chatAccessEnabled: chatAccessEnabled,
                        chatVisibleName: chatVisibleName ? chatVisibleName: '',
                        chatGroupId: chatGroupId,
                        userGroups: userGroups
                    })
                });
}

export default usersReducer;