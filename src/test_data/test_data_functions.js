module.exports.extractDate = function(stringValue) {
    /* extract date from YYYY-MM-DD string and return Date object */ 
    let re = new RegExp('^([0-9]{4})-([0-9]{2})-([0-9]{2})$');
    let matchStr = re.exec(stringValue);
    if (matchStr) {
        return new Date(+ matchStr[1], + matchStr[2] -1, + matchStr[3]);
    } 
    else return null;
};

module.exports.getDateStr = function(dateValue) {
    /* make clear date string for record set */ 
    if (dateValue && (dateValue instanceof Date)) {
        let month = dateValue.getMonth()+1;
        let day = dateValue.getDate();
        return dateValue.getFullYear() + '-' + 
               ((month < 10) ? '0' + month : month) + '-' + 
               ((day < 10) ? '0' + day : day);
    }
    else return '';
    /* return dateValue.toLocaleDateString([userLang, 'en'], dateTimeOptions) + ' ' +
           dateValue.toLocaleTimeString([userLang, 'en'], dateTimeOptions) */
}

var makeClearDate = function(dateValue) {
    /* get clear date (YYYY-MM-DD in local time) */
    return new Date(
        dateValue.getFullYear(),
        dateValue.getMonth(),
        dateValue.getDate(),
        0,
        0,
        0
    );
}

var getCurrentDate = function() {
    /* get current clear Date */ 
    var currentTime = new Date();
    return makeClearDate(currentTime);
}

module.exports.getCurrentDate = getCurrentDate;

module.exports.getShiftDays = function(shiftDays) {
    /* get date shifted on shiftDays days */  
    return makeClearDate(new Date(new Date() - shiftDays * 3600000 * 24));
}
