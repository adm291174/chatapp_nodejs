var { ID_REPORT_BALANCE } = require('../constants/report_constants');

var test_data_reports = {};

test_data_reports[ID_REPORT_BALANCE] = {
        active: {
            groupName: 'Итого актив:',
            beginAmount: 100.00,
            debitTurnover: 50.50,
            creditTurnover: 40.50,
            endAmount: 90.00,
        },
        passive: {
            groupName: 'Итого пассив:',
            beginAmount: 100.00,
            debitTurnover: 50.50,
            creditTurnover: 40.50,
            endAmount: 90.00,
        },
        accounts: [
            {
                baccGroupId: 100,
                baccGroupCode: '407',
                baccGroupName: 'Счета клиентов - покупателей',
                isActive: false,
                beginAmount: 100.00,
                debitTurnover: 50.50,
                creditTurnover: 40.50,
                endAmount: 90.00,
                accounts: [
                    {
                        accountId: 1,
                        accountNumber: '407-RUB-928718', 
                        accountName: 'Счет ООО Ромашка', 
                        beginAmount: 0.00, 
                        debitTurnover: 30.50, 
                        creditTurnover: 30.50, 
                        endAmount: 0.00
                    },
                    {
                        accountId: 2,
                        accountNumber: '407-RUB-782771', 
                        accountName: 'Счет ООО Длинное наименование клиента для теста', 
                        beginAmount: 100.00, 
                        debitTurnover: 20.50, 
                        creditTurnover: 10.50, 
                        endAmount: 90.00
                    }
                ]
            },
            {
                baccGroupId: 200,
                baccGroupCode: '800',
                baccGroupName: 'Счета клиентов - поставщиков',
                isActive: true,
                beginAmount: 10000082.99,
                debitTurnover: 2321230.50,
                creditTurnover: 4231230.50,
                endAmount: 21312330.00,
                accounts: [
                    {
                        accountId: 3,
                        accountNumber: '800-RUB-928718', 
                        accountName: 'Счет ООО Ромашка (поставщик)', 
                        beginAmount: 100000.00, 
                        debitTurnover: 30.50, 
                        creditTurnover: 30.50, 
                        endAmount: 100000.00
                    },
                    {
                        accountId: 4,
                        accountNumber: '800-RUB-782771', 
                        accountName: 'Счет ООО Длинное наименование клиента для теста', 
                        beginAmount: 100.00, 
                        debitTurnover: 20.50, 
                        creditTurnover: 10.50, 
                        endAmount: 90.00
                    }
                ]
            }            
        ]
}
         

var report_files_test_data = {}

report_files_test_data[ID_REPORT_BALANCE] = {
    'realFileName': 'Баланс на дату.xlsx',
    'storageFileName': ID_REPORT_BALANCE + '.bin'
}

module.exports.test_data_reports = test_data_reports;
module.exports.report_files_test_data = report_files_test_data;
