module.exports.test_data_users = [
    {userId: 1200,
     userLogin: 'administrator',
     userName: 'Администратор системы',
     userPassword: '123456_administrator'
    },
    {userId: 1201,
     userLogin: 'test_user',
     userName: 'Тестовый пользователь',
     userPassword: '123456_test_user'
    },
    {userId: 1203,
     userLogin: 'MishaV',
     userName: 'Волков Михаил',
     userPassword: '123456_mishav'
    },
    {userId: 1400,
     userLogin: 'alex',
     userName: 'Саша'
    },
    {userId: 10,
     userLogin: 'anuta',
     userName: 'Рыжкова Анна Александровна',
     userPassword: '123456_anuta'
    }
]

module.exports.test_data_users_avail = [
    {userId: 1200,
     userLogin: 'administrator',
     userName: 'Администратор системы',
     available: true,
    },
    {userId: 1201,
     userLogin: 'test_user',
     userName: 'Тестовый пользователь',
     available: true,
    },
    {userId: 1203,
     userLogin: 'MishaV',
     userName: 'Волков Михаил',
     available: false,
    },
    {userId: 1400,
     userLogin: 'alex',
     userName: 'Саша',
     available: false,
    },
    {userId: 10,
     userLogin: 'anuta',
     userName: 'Рыжкова Анна Александровна',
     available: true,
    }
]


module.exports.test_data_users_list = [
    {
        userId: 1200,
        login: 'administrator',
        lastName: 'Администратор системы',
        firstName: '',
        isActive: true,
        chatAccessEnabled: false,
        chatVisibleName: '',
        chatGroupId: null, 
        userGroups: [3] 
    },
    {
        userId: 1201,
        login: 'test_user',
        lastName: 'Тестовый пользователь',
        firstName: '',        
        isActive: true,
        chatAccessEnabled: false,
        chatVisibleName: '',
        chatGroupId: null, 
        userGroups: [1] 
    },
    {
        userId: 1203,
        login: 'MishaV',
        lastName: 'Волков',
        firstName: 'Михаил',
        isActive: false,
        chatAccessEnabled: false,
        chatVisibleName: 'Миша Волков',
        chatGroupId: null, 
        userGroups: [1]
    },
    {
        userId: 1400,
        login: 'alex',
        lastName: 'Саша',
        firstName: '',
        isActive: false,
        chatAccessEnabled: true,
        chatVisibleName: 'Саша',
        chatGroupId: 100,
        userGroups: [1]
    },
    {
        userId: 10,
        login: 'anuta',
        lastName: 'Рыжкова',
        firstName: 'Анна Александровна',
        isActive: true,
        chatAccessEnabled: true,
        chatVisibleName: 'Рыжкова Анна',
        chatGroupId: 201,
        userGroups: [2, 3]
    }
]

module.exports.test_data_users_dictionaries = {
    chatGroups: [ 
        { 
            groupId: 100, 
            groupName: 'Клиенты'
        },
        { 
            groupId: 200, 
            groupName: 'Клиенты VIP'
        },
        { 
            groupId: 201, 
            groupName: 'Сотрудники'
        },
        { 
            groupId: 300, 
            groupName: 'Администраторы'
        }
    ], 
    userGroups: [
        {
            groupId: 1,
            groupName: 'Клиенты'
        },
        {
            groupId: 2,
            groupName: 'Клиентский менеджер'
        },
        {
            groupId: 3,
            groupName: 'Администрирование пользователей'
        }
    ]
};


