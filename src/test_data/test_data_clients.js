module.exports.test_data_clients = [
    {clientId: 100,
     clientName: 'ООО "Трансильвания"',
     isBuyer: true,
     isSupplier: false
    },
    {clientId: 200,
     clientName: 'ООО "ОЧЕНЬ ДЛИННОЕ НАЗВАНИЕ КЛИЕНТА и это еще не все что можно написать здесь"',
     isBuyer: true,
     isSupplier: true
    },
    {clientId: 300,
     clientName: 'ООО "ЛЮТИК"',
     isBuyer: false,
     isSupplier: false
    },
    {clientId: 400,
     clientName: 'LLC Metro Goldwyn Mayer (aka New Client)',
     isBuyer: false,
     isSupplier: false
    },
    {clientId: 500,
     clientName: 'ООО РОМАШКА',
     isBuyer: true,
     isSupplier: true
    },
    {clientId: 600,
     clientName: 'АО БАНК НАДЕЖНЫХ ИНВЕСТИЦИЙ',
     isBuyer: false,
     isSupplier: false
    },
    {clientId: 20,
     clientName: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s',
     isBuyer: true,
     isSupplier: false
    },
    {clientId: 800,
     clientName: 'ООО "БОТАНИКИ" (генерируем ошибку при сохранении)',
     isBuyer: false,
     isSupplier: false
    },
    {clientId: 801,
     clientName: 'Somecompany',
     isBuyer: true,
     isSupplier: false
    },
    {clientId: 802,
    clientName: '#Клиентбезсчетов',
     isBuyer: true,
     isSupplier: false
    }

];


module.exports.test_data_accounts = [
    {
        accountId: 1,
        clientId: 801,
        accountNumber: '407-USD-787611',
        accountName: 'Счет 407-USD-787611',
        accountCurrencyId: 1,
        accountCurrency: 'USD',
        accountStatus: 'opened',
        accountAmount: 289821.12, 
        dateOfOpen: '2018-08-29',
        dateOfClose: null,
        isBuyerAccount: true, 
        isSupplierAccount: false
    }, 
    {
        accountId: 2,
        clientId: 801,
        accountNumber: '407-RUB-601',
        accountName: 'Счет 407-RUB-601',
        accountCurrencyId: 2,
        accountCurrency: 'RUB',
        accountStatus: 'closed',
        accountAmount: 0, 
        dateOfOpen: '2018-01-01',
        dateOfClose: '2018-10-01',
        isBuyerAccount: true, 
        isSupplierAccount: false
    }, 
    {
        accountId: 3,
        clientId: 100,
        accountNumber: '407-EUR-009211',
        accountName: 'Счет 407-EUR-009211',
        accountCurrencyId: 3,
        accountCurrency: 'EUR',
        accountStatus: 'opened',
        accountAmount: 1000000, 
        dateOfOpen: '2018-02-01',
        dateOfClose: null,
        isBuyerAccount: true, 
        isSupplierAccount: false
    }, 
    {
        accountId: 4,
        clientId: 100,
        accountNumber: '407-RUB-009',
        accountName: 'Счет 407-RUB-009',
        accountCurrencyId: 2,
        accountCurrency: 'RUB',
        accountStatus: 'opened',
        accountAmount: 1023.20, 
        dateOfOpen: '2018-01-01',
        dateOfClose: null,
        isBuyerAccount: true, 
        isSupplierAccount: false
    }, 
    {
        accountId: 5,
        clientId: 100,
        accountNumber: '407-USD-009',
        accountName: 'Счет 407-USD-009',
        accountCurrencyId: 1,
        accountCurrency: 'USD',
        accountStatus: 'closed',
        accountAmount: 0.20, 
        dateOfOpen: '2018-08-21',
        dateOfClose: '2018-10-12',
        isBuyerAccount: true, 
        isSupplierAccount: false
    }, 
    {
        accountId: 6,
        clientId: 20,
        accountNumber: '407-USD-767211',
        accountName: 'Счет 407-USD-767211',
        accountCurrencyId: 1,
        accountCurrency: 'USD',
        accountStatus: 'closed',
        accountAmount: 0, 
        dateOfOpen: '2018-08-21',
        dateOfClose: '2018-10-12',
        isBuyerAccount: true, 
        isSupplierAccount: false
    },
    {
        accountId: 7,
        clientId: 20,
        accountNumber: '407-RUB-009',
        accountName: 'Счет 407-RUB-009',
        accountCurrencyId: 2,
        accountCurrency: 'RUB',
        accountStatus: 'opened',
        accountAmount: 100000000.02, 
        dateOfOpen: '2018-08-21',
        dateOfClose: null,
        isBuyerAccount: true, 
        isSupplierAccount: false
    }
];