module.exports.chatMessageInfo_data1 = function(chatHistoryId) {
    return (
        {
            chathistory_id: chatHistoryId,
            min_identifier: 6,
            max_identifier: 10,
            actual_time: '2018-09-30T18:48:04.118713+03:00',
            last_message_time: '2018-09-30T18:41:11+03:00',
            last_view_time: '2018-09-30T18:48:03+03:00',
            total_messages: 4,
            new_identifiers: [],
            my_user_id: 12,
            chat_user_id: [
                12,
                103
            ],
            chat_user_name: [
                'test_staff_01 aka Анна Рыжкова',
                'Tonya Elliot (Visible Name)'
            ]
        }
    );
}

module.exports.chatMessageContent_data1 = function (chatHistoryId) {
    
    return JSON.parse('[ \
    {\
        "id": 6,\
        "created": "2018-05-24T11:29:54+03:00",\
        "modified": "2018-05-24T11:29:54+03:00",\
        "message_text": "Привет!\\r\\nПришли пожалуйста фотку!",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": null,\
        "real_filename": null,\
        "user_id": 103\
    },\
    {\
        "id": 7,\
        "created": "2018-05-24T11:30:14+03:00",\
        "modified": "2018-05-24T11:30:14+03:00",\
        "message_text": "Инструкция",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 103\
    },\
    {\
        "id": 9,\
        "created": "2018-09-30T18:40:49+03:00",\
        "modified": "2018-09-30T18:40:49+03:00",\
        "message_text": "Получил это",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": null,\
        "real_filename": null,\
        "user_id": 2\
    },\
    {\
        "id": 10,\
        "created": "2018-09-30T18:41:11+03:00",\
        "modified": "' + (new Date().toJSON()) + '",\
        "message_text": "А это Анюта",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 2,\
        "real_filename": "7y4w7LJC9lg.jpg",\
        "user_id": 2\
    }\
]')};

module.exports.chatMessageInfo_data2 = function(chatHistoryId) {
    return (
        {
            chathistory_id: chatHistoryId,
            min_identifier: 6,
            max_identifier: 100,
            actual_time: '2018-09-30T18:48:04.118713+03:00',
            last_message_time: '2018-09-30T18:41:11+03:00',
            last_view_time: '2018-09-30T18:48:03+03:00',
            total_messages: 4,
            new_identifiers: [],
            my_user_id: 12,
            chat_user_id: [
                12,
                103
            ],
            chat_user_name: [
                'Тестовый собеседник',
                'Tonya Elliot (Visible Name)'
            ]
        }
    );
}

module.exports.chatMessageContent_data2 = function (chatHistoryId) {
    
    return JSON.parse('[ \
    {\
        "id": 6,\
        "created": "2018-05-24T11:29:54+03:00",\
        "modified": "2018-05-24T11:29:54+03:00",\
        "message_text": "Привет!\\r\\nПришли пожалуйста фотку!",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": null,\
        "real_filename": null,\
        "user_id": 103\
    },\
    {\
        "id": 7,\
        "created": "2018-05-24T11:30:14+03:00",\
        "modified": "2018-05-24T11:30:14+03:00",\
        "message_text": "Инструкция",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 103\
    },\
    {\
        "id": 9,\
        "created": "2018-09-30T18:40:49+03:00",\
        "modified": "2018-09-30T18:40:49+03:00",\
        "message_text": "Получил это",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": null,\
        "real_filename": null,\
        "user_id": 12\
    },\
    {\
        "id": 10,\
        "created": "2018-09-30T18:41:11+03:00",\
        "modified": "2018-09-30T18:41:11+03:00",\
        "message_text": "А это Анюта",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 2,\
        "real_filename": "7y4w7LJC9lg.jpg",\
        "user_id": 12\
    },\
    {\
        "id": 12,\
        "created": "2018-10-01T12:41:11+03:00",\
        "message_text": "Более новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 4,\
        "real_filename": "Самая ржачная картинка.jpg",\
        "user_id": 12\
    },\
    {\
        "id": 15,\
        "created": "2018-10-01T12:43:14+03:00",\
        "modified": "2018-10-24T12:43:14+03:00",\
        "message_text": "Инструкция",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 103\
    },\
    {\
        "id": 20,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 22,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 24,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 26,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 28,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 30,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 31,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 32,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "2018-10-24T13:13:14+03:00",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    },\
    {\
        "id": 35,\
        "created": "2018-10-01T13:13:14+03:00",\
        "modified": "' + (new Date().toJSON()) + '",\
        "message_text": "Новое сообщение",\
        "chat_history_id": ' + chatHistoryId + ',\
        "message_file_id": 1,\
        "real_filename": "Инструкция пользователя ЛК ТП основная 2017_10_09 v1_2_5.pdf",\
        "user_id": 12\
    }\
]')};


