var { getCurrentDate, getShiftDays, getDateStr } = require('./test_data_functions');
var immutable = require('immutable');
var Map = immutable.Map;

const operationStatuses = {
    activeOperation: {
        statusId: '5D50218A945F480E9C4665B7C37510BA',
        statusDescription: 'Действующая'
    },
    inactiveOperation: {
        statusId: '81E8E42BB1024E0683F1A6790B8A1E5B',
        statusDescription: 'Не действующая'
    },
    
}

const transferOperationType = '32DE629C3E4F440FA9A320622602C8F2',
      supplyOperationType = '474082881D4047EA9A3E8546BA45FCFA',
      settlementOperationType = 'DDFB366FD5A04471A0EB18886E557C50',
      simpleOperationType = 'AB59D801D95347E1B775F6DE48DA573C'


const operationTypes = [
    {
        orderType: transferOperationType,
        orderTypeDescription: 'Перевод'
    },
    {
        orderType: supplyOperationType,
        orderTypeDescription: 'Поставка товара'
    },
    {
        orderType: settlementOperationType,
        orderTypeDescription: 'Расчеты с клиентом'
    },
    {
        orderType: simpleOperationType,
        orderTypeDescription: 'Простая операция'
    }
];                                

const operationTypesMap = function () {
    let result = Map();
    operationTypes.forEach(value => {
        result = result.set(value.orderType, value.orderTypeDescription)
    })
    return result;
}

const operationTypesById = operationTypesMap()


module.exports.operations_data_for_order_1 = function() {
    return (
        [
            {
                operationId: 100,
                operationNumber: '98291',
                operationStatusId: operationStatuses.activeOperation.statusId,
                operationStatusDescription: operationStatuses.activeOperation.statusDescription,
                operationDate: getDateStr(getCurrentDate()),
                operationType: supplyOperationType,
                operationTypeDescription: operationTypesById.get(supplyOperationType),
                operationCurrencyId: 2,
                operationCurrencyCode: 'USD',
                clientId: 1,
                clientName: 'ООО Трансильвания',
                operationSum: 200123.12
            },
            {
                operationId: 101,
                operationNumber: '1',
                operationStatusId: operationStatuses.activeOperation.statusId,
                operationStatusDescription: operationStatuses.activeOperation.statusDescription,
                operationDate: getDateStr(getShiftDays(1)),
                operationType: transferOperationType,
                operationTypeDescription: operationTypesById.get(transferOperationType),
                operationCurrencyId: 1,
                operationCurrencyCode: 'RUB',
                clientId: 3,
                clientName: 'LLC Metro GolDen Mayer',
                operationSum: 0.99
            },
            {
                operationId: 103,
                operationNumber: '72661',
                operationStatusId: operationStatuses.inactiveOperation.statusId,
                operationStatusDescription: operationStatuses.inactiveOperation.statusDescription,
                operationDate: getDateStr(getShiftDays(3)),
                operationType: settlementOperationType,
                operationTypeDescription: operationTypesById.get(settlementOperationType),
                operationCurrencyId: 3,
                operationCurrencyCode: 'EUR',
                clientId: null,
                clientName: null,
                operationSum: 19920021.01
            },
        ]
    );
}


module.exports.operationsList_data1 = function() {
    return [
        {
            operationId: 100, 
            operationNumber: '762612',
            operationDate: getDateStr(getShiftDays(0)),
            status: operationStatuses.inactiveOperation.statusId,
            operationStatusDescription: operationStatuses.inactiveOperation.statusDescription,
            clients: [
                {
                    operationClientId: 2001,
                    operationClientName: 'ООО ПРОВИНЦИЯ'
                }
            ],
            operationSum: 0.00,
            operationCurrencyCodeId: 1, 
            operationCurrencyCode: 'RUB',
            operationType: settlementOperationType,
            operationTypeDescription: operationTypesById.get(settlementOperationType)
        },
        {
            operationId: 200, 
            operationNumber: '000001',
            operationDate: getDateStr(getShiftDays(0)),
            status: operationStatuses.inactiveOperation.statusId,
            operationStatusDescription: operationStatuses.inactiveOperation.statusDescription,
            clients: [
                {
                    operationClientId: 2001,
                    operationClientName: 'ООО ПРОВИНЦИЯ'
                },
                {
                    operationClientId: 2002,
                    operationClientName: 'ООО "Длинное предлинное название клиента"'
                }
            ],
            operationSum: 200000.14,
            operationCurrencyCodeId: 1, 
            operationCurrencyCode: 'RUB',
            operationType: transferOperationType,
            operationTypeDescription: operationTypesById.get(transferOperationType)
        },
        {
            operationId: 300, 
            operationNumber: '000002',
            operationDate: getDateStr(getShiftDays(10)),
            status: operationStatuses.inactiveOperation.statusId,
            operationStatusDescription: operationStatuses.inactiveOperation.statusDescription,
            operationSum: 500000.55,
            operationCurrencyCodeId: 2, 
            operationCurrencyCode: 'USD',
            operationType: settlementOperationType,
            operationTypeDescription: operationTypesById.get(settlementOperationType)
        },
    ]
}

module.exports.operationsList_data2 = function() {
    return [
        {
            operationId: 200, 
            operationNumber: '762612',
            operationDate: getDateStr(getShiftDays(0)),
            status: operationStatuses.inactiveOperation.statusId,
            operationStatusDescription: operationStatuses.inactiveOperation.statusDescription,
            operationSum: 0.00,
            operationCurrencyId: 1, 
            operationCurrencyCode: 'RUB',
            operationType: transferOperationType,
            operationTypeDescription: operationTypesById.get(settlementOperationType),
            transferData: { 
                buyerOrderId: 8772,
                buyerOrderInfo: {
                    orderClientId: 900,
                    orderNumber: 'NUMBER', 
                    orderDate: getDateStr(getShiftDays(0))
                },
                buyerClientId: 900, 
                buyerClientName: 'Баранкин и партнеры', 
                buyerAccountId: 790, 
                buyerAccountNumber: '407-RUB-827012',
                buyerPaymentRequisitId: 854421, 
                buyerPaymentRequisitInfo: { 
                    payRequisitClientId: 900,
                    payReqTaxNumber: '098987626710', 
                    payReqOrgName: 'ИП Лохматкина Вероника Павловна',
                    payReqAccountNumber: '40802810290091992009',
                    payReqBankName: 'МЕТАЛЛИНВЕСТБАНК АО'
                },
                supplierOrderId: 82991,
                supplierOrderInfo: {
                    orderClientId: 200,
                    orderNumber: '012021',
                    orderDate: getDateStr(getShiftDays(100)), 
                },                
                supplierClientId: 10, 
                supplierClientName: 'ООО Трансильвания Львовская',
                supplierAccountId: 100,
                supplierAccountNumber: '801-RUB-928819',
                supplierPaymentRequisitId: 892812,               
                supplierPaymentRequisitInfo: {
                        payRequisitClientId: 10, 
                        payReqTaxNumber: '7765662711',
                        payReqOrgName: 'Общество с ограниченной ответственностью "Длинное наименование клиента"',
                        payReqAccountNumber: '00000000000000000000',
                        payReqBankName: 'ПАО СБЕРБАНК РОССИИ'
                },
                paymentDocumentNumber: '875621',
                paymentDocumentDate: getDateStr(getShiftDays(3)),
                paymentDocumentPurpose: 'Оплата по счету № 1 от 10.12.2018 за поставку продуктов питания',
            },
            supplyData: null,
            settlementData: null,
            simpleOperationData: null       
        },
        {
            operationId: 300, 
            operationNumber: '000001',
            operationDate: getDateStr(getShiftDays(0)),
            status: operationStatuses.inactiveOperation.statusId,
            operationStatusDescription: operationStatuses.inactiveOperation.statusDescription,
            operationSum: 200000.14,
            operationCurrencyId: 1, 
            operationCurrencyCode: 'RUB',
            operationType: transferOperationType,
            operationTypeDescription: operationTypesById.get(transferOperationType)
        }
    ]
}

module.exports.operationsPickerAccount_data1 = function() {
    return ([
        // buy order type 
        {   accountId: 78727718,
            accountNumber: '407-RUB-786261',
            accountCurrencyId: 1,
            accountCurrencyCode: 'RUB',
            clientId: 109981,
            clientName: 'ООО Трансильвания',
            accountType: 'buyerAccount',
            accountName: 'Счет ООО Трансильвания, покупатель'
        }, 
        {   accountId: 10,
            accountNumber: '407-USD-909281',
            accountCurrencyId: 2,
            accountCurrencyCode: 'USD',
            clientId: 76261,
            clientName: 'ООО Очень длинное наименование клиента для отображения в списке',
            accountType: 'buyerAccount',
            accountName: 'Счет Очень длинное наименование клиента для отображения в списке, покупатель'
        }, 
        {   accountId: 101,
            accountNumber: '407-UAH-28821',
            accountCurrencyId: 2091,
            accountCurrencyCode: 'UAH',
            clientId: 191,
            clientName: 'Акционерное общество "Хлопцы из Запорожья"',
            accountType: 'buyerAccount',
            accountName: 'Счет Акционерное общество "Хлопцы из Запорожья", покупатель'
        },
        {   accountId: 10299313321,
            accountNumber: '407-USD-228821',
            accountCurrencyId: 2,
            accountCurrencyCode: 'USD',
            clientId: 109981,
            clientName: 'ООО Трансильвания',
            accountType: 'buyerAccount',
            accountName: 'Счет ООО Трансильвания, покупатель'
        },
        // supply order type
        {   accountId: 882711103,
            accountNumber: '801-USD-998122',
            accountCurrencyId: 2,
            accountCurrencyCode: 'USD',
            clientId: 10998122,
            clientName: 'ООО Трансильвания',
            accountType: 'supplierAccount',
            accountName: 'Счет ООО Трансильвания, поставщик'
        }, 
        {   accountId: 102,
            accountNumber: '801-RUB-998121',
            accountCurrencyId: 1,
            accountCurrencyCode: 'RUB',
            clientId: 10998121,
            clientName: 'ООО Трансильвания',
            accountType: 'supplierAccount',
            accountName: 'Счет ООО Трансильвания, поставщик, счет для альтернативных расчетов'
        }, 
        {   accountId: 103,
            accountNumber: '801-EUR-000009',
            accountCurrencyId: 978,
            accountCurrencyCode: 'EUR',
            clientId: 109981,
            clientName: 'ООО Трансильвания',
            accountType: 'supplierAccount',
            accountName: 'Счет ООО Трансильвания, поставщик, счет для альтернативных расчетов, номер 2'
        }, 
        {   accountId: 104,
            accountNumber: '801-USD-100001',
            accountCurrencyId: 2,
            accountCurrencyCode: 'USD',
            clientId: 897,
            clientName: 'LLC Metro Goldwyn Mayer',
            accountType: 'supplierAccount',
            accountName: 'Счет LLC Metro Goldwyn Mayer, поставщик, валюта USD'
        }, 
        {   accountId: 105,
            accountNumber: '801-USD-777001',
            accountCurrencyId: 2,
            accountCurrencyCode: 'USD',
            clientId: 777,
            clientName: 'ООО Роснефть',
            accountType: 'supplierAccount',
            accountName: 'Счет ООО Роснефть, поставщик, валюта USD'
        }, 
        {   accountId: 106,
            accountNumber: '801-RUB-909281',
            accountCurrencyId: 1,
            accountCurrencyCode: 'RUB',
            clientId: 67291,
            clientName: 'ООО КОММУНИСТЫ против КАПИТАЛИСТОВ',
            accountType: 'supplierAccount',
            accountName: 'ООО КОММУНИСТЫ против КАПИТАЛИСТОВ, поставщик, валюта RUB'
        }, 
        {   accountId: 107,
            accountNumber: '407-TRY-100001',
            accountCurrencyId: 949,
            accountCurrencyCode: 'TRY',
            clientId: 897,
            clientName: 'LLC Metro Goldwyn Mayer',
            accountType: 'supplierAccount',
            accountName: 'LLC Metro Goldwyn Mayer, поставщик, валюта TRY'
        }, 
        {   accountId: 8112108,
            accountNumber: '407-USD-728112',
            accountCurrencyId: 2,
            accountCurrencyCode: 'USD',
            clientId: 728112,
            clientName: 'ООО Трансильвания для ДРАКУЛЫ',
            accountType: 'supplierAccount',
            accountName: 'Счет ООО Трансильвания для ДРАКУЛЫ, поставщик'
        }, 

    ])
}
