var apiChatList_data1 = () => JSON.parse('[ \
     {\
        "chat_id": 890,\
        "chat_history_id": 1780,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: pittslawrence aka Edward Hodge",\
        "user_id": 2,\
        "o_user_id": 4,\
        "o_user_name": "pittslawrence aka Edward Hodge",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 12\
    },\
    {\
        "chat_id": 893,\
        "chat_history_id": 1785,\
        "last_message_time": "2018-08-29T17:00:01",\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: tiffany47 aka Jessica Cummings",\
        "user_id": 2,\
        "o_user_id": 10,\
        "o_user_name": "tiffany47 aka Jessica Cummings (Earlier)",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 886,\
        "chat_history_id": 1772,\
        "last_message_time": "2018-09-01T18:00:01",\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: josephhernandez aka Dan Tucker",\
        "user_id": 2,\
        "o_user_id": 23,\
        "o_user_name": "josephhernandez aka Dan Tucker (Later)",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 126\
    },\
    {\
        "chat_id": 100,\
        "chat_history_id": 100,\
        "last_message_time": "' + new Date(new Date() - 3600000).toJSON() + '",\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: joel04 aka Gina Davis",\
        "user_id": 2,\
        "o_user_id": 25,\
        "o_user_name": "Тестовые данные с докачкой",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 895,\
        "chat_history_id": 1789,\
        "last_message_time": "' + (new Date()).toJSON() + '",\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: xpaul aka John Mcgrath",\
        "user_id": 2,\
        "o_user_id": 28,\
        "o_user_name": "xpaul aka John Mcgrath",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 892,\
        "chat_history_id": 1784,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: teresa55 aka Cassandra Smith",\
        "user_id": 2,\
        "o_user_id": 33,\
        "o_user_name": "teresa55 aka Cassandra Smith",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 142,\
        "chat_history_id": 284,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: anthonyfields aka Lonnie Camacho",\
        "user_id": 2,\
        "o_user_id": 35,\
        "o_user_name": "anthonyfields aka Lonnie Camacho",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 441,\
        "chat_history_id": 882,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: chapmanjessica aka Joseph Jarvis",\
        "user_id": 2,\
        "o_user_id": 56,\
        "o_user_name": "chapmanjessica aka Joseph Jarvis",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 891,\
        "chat_history_id": 1782,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: shawnbradley aka Valerie Fisher",\
        "user_id": 2,\
        "o_user_id": 57,\
        "o_user_name": "shawnbradley aka Valerie Fisher",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 889,\
        "chat_history_id": 1778,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: paulmedina aka Randall Yoder",\
        "user_id": 2,\
        "o_user_id": 58,\
        "o_user_name": "paulmedina aka Randall Yoder",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 240,\
        "chat_history_id": 480,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: bbarrera aka Elizabeth Shields",\
        "user_id": 2,\
        "o_user_id": 60,\
        "o_user_name": "bbarrera aka Elizabeth Shields",\
        "o_group_id": 1,\
        "o_group_name": "Сотрудники",\
        "o_css_group": "rose-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 873,\
        "chat_history_id": 1746,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: hannahguerra aka John Francis",\
        "user_id": 2,\
        "o_user_id": 7,\
        "o_user_name": "hannahguerra aka John Francis",\
        "o_group_id": 2,\
        "o_group_name": "Клиенты",\
        "o_css_group": "blue-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 881,\
        "chat_history_id": 1762,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: riosjasmine aka Makayla Becker",\
        "user_id": 2,\
        "o_user_id": 12,\
        "o_user_name": "riosjasmine aka Makayla Becker",\
        "o_group_id": 2,\
        "o_group_name": "Клиенты",\
        "o_css_group": "blue-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
        "chat_id": 878,\
        "chat_history_id": 1756,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: mitchellmorgan aka Michael Wilson",\
        "user_id": 2,\
        "o_user_id": 14,\
        "o_user_name": "mitchellmorgan aka Michael Wilson",\
        "o_group_id": 2,\
        "o_group_name": "Клиенты",\
        "o_css_group": "blue-background",\
        "o_icon": null,\
        "unread_messages": 0\
    },\
    {\
       "chat_id": 182,\
        "chat_history_id": 364,\
        "last_message_time": null,\
        "multichat": false,\
        "last_view": null,\
        "visible_to_user": true,\
        "visible_name": "Chat with: baileyjeff aka Elizabeth Griffin",\
        "user_id": 2,\
        "o_user_id": 29,\
        "o_user_name": "baileyjeff aka Elizabeth Griffin",\
        "o_group_id": 2,\
        "o_group_name": "Клиенты",\
        "o_css_group": "blue-background",\
        "o_icon": null,\
        "unread_messages": 0\
    } \
    ]'
);

module.exports.apiChatList_data1 = apiChatList_data1; 