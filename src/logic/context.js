// Context rules functions 
import { fillIfEmptyMap, getCurrentDate } from '../core/core_functions';
import { Map, OrderedMap, List } from 'immutable';
import { allTypes }  from '../constants/operations_constants';
import { filterInitialContent } from '../report/report_defs';


const saveCurrentContext = function(appContext, newWindow) {
    /* save current App context and return new <context> part */ 
    let context = appContext;
    let chatContext = context.get('chatContext');
    if (chatContext.get('isActive')) {
        context = context.set('chatContext', chatContext.set('isActive', false)); // close chat window 
    }
    if (newWindow != 'chat') {
        // Can do additional saving here, if we need to 
        context = context.set('activeWindow', undefined); // simply make active window set to 'undefined'
    }
    return context;
}

const setNewContext = function(savedContext, newWindow, newContext, chatClose = false) {
    /* set new application context */ 
    let context = savedContext;
    if (newWindow == 'chat') {
        // activate chat window
        let chatContext = context.get('chatContext');
        let chatHistoryId = newContext.get('chatHistoryId')
        context = context.set('chatContext', chatContext.set('isActive', true)
                                                        .set('chatHistoryId', chatHistoryId)); 

    }
    else {
        const windowContext = savedContext.get('windowContext'); // old windowContext content 
        // now we get saved context for new Window 
        const newWindowOldContext = windowContext.get(newWindow);
        
        
        const newWindowNewContext = chatClose // if chatClose, we simply copy old values from window context
                                    ? newWindowOldContext 
                                    : setWindowContext(newWindow, newContext, chatClose, savedContext);
        // set new active Window context
        context = context.set('windowContext',
                               windowContext.set(newWindow, 
                                               ((Map.isMap(newWindowOldContext)) ? newWindowOldContext : fillIfEmptyMap()).merge(newWindowNewContext)
                               ))
                          .set('activeWindow', newWindow)
        
    }   
    return context;
}

const setWindowContext = function(newWindow, newWindowContext, chatClose, savedContext) {
    /* fix window context here */ 
    // console.log('New window context: ', newWindow);
    switch (newWindow) {
        case 'clientEdit': {
            const windowContent = newWindowContext.get('windowContent');
            const clientId = windowContent.get('clientId');
            const windowTitle = clientId ? 'Информация о клиенте' : 'Новый клиент';
        
            // clear fields for proper rendering 
            return  newWindowContext.set('windowTitle', windowTitle)
                                    .set('windowContent', 
                                            windowContent.merge({
                                                    clientName: '', 
                                                    isBuyer: false,
                                                    isSupplier: false,
                                                    clientPaymentRequisits: List(),
                                                    clientAccounts: List(),
                                                    clientUsers: List()
                                                })     
                                    );
        }
        case 'paymentRequsitEdit': {
            const windowContent = newWindowContext.get('windowContent');
            const requisitId = windowContent.get('requisitId'),
                  clientId = windowContent.get('clientId'),
                  clientName = windowContent.get('clientName');

            const windowTitle = requisitId 
                                ? 'Платежный реквизит клиента' 
                                : 'Новый платежный реквизит клиента';
        
            // clear fields for proper rendering 
            return  newWindowContext.set('windowTitle', windowTitle)
                                    .set('windowContent', 
                                            windowContent.merge({
                                                            requisitId: requisitId,
                                                            clientId: clientId,
                                                            clientName: clientName,
                                                            orgName: '',
                                                            orgTaxNumber: '',
                                                            orgTaxReasonCode: '',
                                                            accountNumber: '',
                                                            bankName: '',
                                                            bankCode: '',
                                                            bankCorrAccNumber: '',
                                                            enabled: true
                                                        })     
                                            );

        }
        case 'clientEditAddUser': {
            const windowContent = newWindowContext.get('windowContent');
            
            // clear fields for proper rendering 
            return  newWindowContext.set('windowContent', 
                                            windowContent.merge({ usersList: List() })     
                                    );

        }
        case 'orderEdit': {
            const windowContent = newWindowContext.get('windowContent');
            
            // clear pickerData for proper rendering
            return  newWindowContext.set('windowContent', 
                                            windowContent.merge({ accountsPickerData: List(),
                                                                  linkedOrdersPickerData: List(),
                                                                  linkedOrdersToRemove: List(),
                                                                  linkedOrdersToAdd: List() })
                                    );
        }
        case 'operationEdit': {
            const windowContent = newWindowContext.get('windowContent');
            
            return newWindowContext.set('windowContent', 
                                            windowContent.merge({})
                                    );
        }
        case 'statement': {

            const oldWindowContent = savedContext.get('windowContext').get('statement').get('windowContent'),
                  windowContent = newWindowContext.get('windowContent');  
            
            return newWindowContext.set('windowContent', 
                                        oldWindowContent.merge(windowContent)  // save old content here 
                                    );
        }
        default: return newWindowContext;
    }
}

const initialWindowContext = Map({
    clients: Map({
        windowTitle: 'Счета клиентов',
        windowFilter: Map({
            filterDescription: OrderedMap({
                name: Map({
                    caption: 'Фильтр по имени',
                    type: 'input',
                    id: '',
                    withClearBtn: true
                }),
                status: Map({
                    caption: 'Статус',
                    type: 'select',
                    selectValues: OrderedMap({
                        opened: { title: 'Открытые' },
                        allAccounts: { title: 'Все счета' }
                    })
                }),
                refresh: Map({
                    type: 'button',
                    buttonText: 'Обновить',
                    action: 'formRefresh'
                }),
                newClient: Map({
                    type: 'button',
                    buttonText: 'Создать клиента',
                    action: 'createClient'
                })
            }),
            filterValues: Map({
                'clients-filter__name': '',
                'clients-filter__acc-status': 'acc_status_all'
            })
        })
    }),
    clientEdit: Map({
        windowTitle: 'Новый клиент',
        windowFilter: Map(),
        windowContent: Map({
            clientId: null,
            clientName: '',
            isBuyer: false,
            isSupplier: false,
            clientPaymentRequisits: List(),
            clientAccounts: List(),
            clientUsers: List()
        })
    }),
    paymentRequsitEdit: Map({
        windowTitle: 'Новый платежный реквизит клиента',
        windowFilter: Map(),
        windowContent: Map({
            requisitId: null,
            clientId: null,
            clientName: '',
            orgName: '',
            orgTaxNumber: '',
            orgTaxReasonCode: '',
            accountNumber: '',
            bankName: '',
            bankCode: '',
            bankCorrAccNumber: '',
            enabled: true
        })
    }),
    clientEditAddUser: Map({
        windowTitle: 'Добавление пользователя к клиенту',
        windowFilter: Map({
            filterValues: Map({
                'clients-adduser-filter__name': '',
                'clients-adduser-filter__query-user-state': 'not-linked'
            })
        }),
        windowContent: Map({
            clientId: null,
            clientName: '',
            usersList: List()
        })
    }),
    ordersList: Map({
        windowTitle: 'Ордеры',
        windowFilter: Map({
            filterValues: Map({
                'orders-filter__period': 'today',
                'orders-filter__start-date': getCurrentDate(),
                'orders-filter__end-date': getCurrentDate(),
                'orders-filter__status': 'all_statuses',
                'orders-filter__name': ''
            })
        }),
        windowContent: Map({})
    }),
    orderEdit: Map({
        windowTitle: 'Ордер',
        windowFilter: Map(),
        windowContent: Map({
            orderId: undefined,
            orderIsEdited: true,
            accountsPickerData: List(),
            linkedOrdersPickerData: List(),
            linkedOrdersToRemove: List(),
            linkedOrdersToAdd: List()
        })
    }),
    operationsList: Map({
        windowTitle: 'Операции',
        windowFilter: Map({
            filterValues: Map({
                'operations-filter__period': 'today',
                'operations-filter__start-date': getCurrentDate(),
                'operations-filter__end-date': getCurrentDate(),
                'operations-filter__type': allTypes,
                'operations-filter__name': ''
            })
        }),
        windowContent: Map({})
    }),
    operationEdit: Map({
        windowTitle: 'Новая операция',
        windowFilter: Map(),
        windowContent: Map({
             operationId: null,
             operationIsEdited: true
        })
    }),
    statement: Map({
        windowTitle: 'Выписка по счету',
        windowFilter: Map(),
        windowContent: Map({
            accountId: null,
            statementData: Map(),
            accountNumber: '',
            accountName: '',
            clientId: null,
            clientName: '',
            statementStartDate: getCurrentDate(),
            statementEndDate: getCurrentDate(),
            dataIsRetrieving: true,
            accountPickerData: List()
        })
    }),
    reportsList: Map({
        windowTitle: 'Отчеты',
        windowFilter: Map({
            filterValues: Map({
                'reports-filter__name': ''
            })
        }),
        windowContent: Map({})
    }),
    report: Map({
        windowTitle: '',
        windowFilter: filterInitialContent(),
        windowContent: Map({
            reportId: null
        })
    }),
    usersList: Map({
        windowTitle: 'Управление пользователями',
        windowFilter: Map({
            filterValues: Map({
                'users-filter__name': ''
            })
        }),
        windowContent: Map({
            usersListData: List()
        })
    }),
    userEdit: Map({
        windowTitle: 'Новый пользователь',
        windowFilter: Map(),
        windowContent: Map({
            userId: null
        })
    })
})

function findElementInfoById(filterContext, elementId) {
    /* return elementInfo by its DOM Id  */
    const matchedKey = filterContext.findKey(value => (value.get('id') == elementId));
    if (matchedKey) {
        return { 
            matchedKey: matchedKey, 
            elementContext: filterContext.get(matchedKey)
        };
    }
    else return undefined;
}

function extractAndSetContent(action, state, windowName) {
    
    /* Get window content value and reduce it to appContext 
       responseData should be of Map type with windowContent structure 
       for example: 
           Map({
               clientId: 400
           })

           this will bind to 
           appContext:
              windowContext:
                 <windowName>:
                     windowContent:
                         ClientId: 400  
    */ 
    const { responseData } = action.payload;
    const data = state.data;
    const appContext = data.get('appContext');
    const windowContext = appContext.get('windowContext');
    const currentWindowContext = windowContext.get(windowName);
    const currentWindowContent = currentWindowContext.get('windowContent');

    return Object.assign({},
                            state, 
                            {data: data.set('appContext', 
                                    appContext.set('windowContext',
                                        windowContext.set(windowName,  
                                            currentWindowContext.set('windowContent',
                                               currentWindowContent.merge(
                                                   Map(responseData)
                                               )
                                           )
                                        )   
                                    )
                            )});
}


export {extractAndSetContent,
        findElementInfoById,
        initialWindowContext, 
        saveCurrentContext, 
        setNewContext };