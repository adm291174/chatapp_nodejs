import '@babel/polyfill'

import jQuery from 'jquery';
// import Popper from 'popper.js';
// import 'popper.js/dist/popper.js';
// import 'bootstrap/dist/js/bootstrap.bundle.min';

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import configureStore from './store/configureStore'

import { getCookie } from './core/core_functions'

import { onBasePageLoad } from './core/handler_functions'

const store = configureStore()

console.log('session_id Cookie:', getCookie('session_id'));
console.log('jQuery version:', jQuery.fn.jquery);
render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('id_app-root')
)

jQuery( document ).ready(onBasePageLoad(store.dispatch));

/* document.getElementById('root').innerHTML = 'Привет, я готов опять.'
module.hot.accept() */
