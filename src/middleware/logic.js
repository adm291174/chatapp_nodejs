import { createLogic } from 'redux-logic';
import axios from 'axios';
import { convertChatList } from '../core/objects_chatlist';
import { logicChatInfoGet, logicChatMessageSave } from './chat-logic';
import { logicDataProcessingErrors,
         logicDataProcessingMessaged } from './error-logic';
import { logicClientListGet, logicClientGet, 
         logicClientEditSave, logicClientGetUsersForAdd,
         logicClientSetClientUser } from './clients-logic';
import { contextActivateLogic, 
         contextProcessChatClose, 
         contextWindowTitleSet } from './context-logic';
import { logicPayReqGet, logicPayReqSave } from './payreq-logic';
import { logicProcessMassFilter } from './filter-logic';
import { logicOrdersListGet, 
         orderCommissionCalc, 
         orderEditClientTypeSet,
         logicOrderEditAccountsGet,
         logicOrderDataGet,
         logicOrderDataSave,
         logicOrderEditLOGet,
         logicOrderExecuteAction } from './orders-logic';

import { logicOperationsListGet,
         logicOperationsAccPickerGet,
         logicOperationsPayReqPickerGet,
         logicOperationsOrdersPickerGet,
         logicOperationDataGet,
         logicOperationDataSave } from './operations-logic';

import { logicStatementAccPickerGet,
         logicStatementGet } from './statement-logic';

import { logicReportDataGet } from './reports-logic';

import { postConfig } from '../constants/logic_constants';

import { logicProcessRefreshStatus } from './refresh-logic';

import { logicUsersListGet, 
         logicUsersDictionaries,
         logicUsersGetUser,
         logicUsersSaveUser } from './users-logic';


// Get data for ChatList 
const logicChatListGet = createLogic({
    type: 'GET_CHATLIST_INFO_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'DATA_СHATLIST_INFO',
        failType: 'DATA_CHATLIST_GET_FAILED'
  },
  process() {
    return axios.get('/api/chats')
      .then(resp => convertChatList(resp.data));
  }
});

const logicUserInfoGet = createLogic({
    type: 'GET_USER_INFO_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'DATA_USER_INFO',
        failType: 'DATA_USER_INFO_FAILED'
  },
  //process({ getState, action }) {
  process() {
    return axios.get('/api/v2/get_user_info')
      .then(resp => resp.data);
  }
});

function logout() {
    setTimeout(() => { window.location.reload(true); }, 500);
}

const logicLogout = createLogic({
    type: 'PROCESS_LOGOUT',
    latest: true,

    process() {
        return axios.post('/accounts/logout/',
                      null,
                      postConfig)
        .then(() => {
            logout()
        })
        .catch(() => {
            logout()  
        }) 
  }
});


function redirect(uriPath) {
    setTimeout(() => {  
        const { protocol, host } = window.location;
        window.location = protocol + '//' + host + uriPath;
    }, 500);    
}

const logicRedirect = createLogic({
    // Redirect application to new location
    type: 'PROCESS_REDIRECT',
    latest: true,

    process({ action }, ) {
        const uriPath = action.payload;
        return new Promise(function(resolve) {
            redirect(uriPath);
            resolve();
        }).catch(() => {
            redirect(uriPath);            
        })

    }
});

const logicDownloadFile = createLogic({
    // download report file 
    type: 'LOAD_REPORT_FILE',
    latest: true,
    process({ action }, dispatch, done) {
        console.log(action.payload);
        const { reportFileName } = action.payload,
              uri = '/download/files/' + reportFileName + '/';
         


        return axios.head(uri, 
                          null, 
                          postConfig).then(() => {
                    const elementId = 'id_file-dowload-' + reportFileName;
                    let form = document.createElement('form');
                    form.setAttribute('action', uri);
                    form.id = elementId;
                    document.body.appendChild(form);
                    form.submit();
                    document.body.removeChild(form);
                    dispatch({
                        type: 'REPORT_FILE_GET_DONE',
                        payload: null
                    });
                    done();
               }).catch((err) => {
                    dispatch({
                        type: 'REPORT_FILE_GET_FAILED',
                        payload: err,
                        error: true
                    });
                    console.log('File download failed');
                    done();
                   
        });
        
    }
});

export default [
                  contextActivateLogic,
                  contextProcessChatClose,
                  logicChatListGet, 
                  logicUserInfoGet,
                  logicChatInfoGet,
                  logicChatMessageSave, 
                  logicDataProcessingErrors,
                  logicClientEditSave,
                  logicClientGet,
                  logicClientGetUsersForAdd,
                  logicClientListGet,
                  logicClientSetClientUser,
                  logicPayReqGet,
                  logicPayReqSave,
                  logicLogout,
                  logicProcessMassFilter,
                  logicOrdersListGet,
                  orderCommissionCalc,
                  orderEditClientTypeSet,
                  logicOrderEditAccountsGet,
                  logicOrderDataGet,
                  logicOrderDataSave,
                  logicOrderEditLOGet,
                  logicOrderExecuteAction,
                  contextWindowTitleSet,
                  logicOperationsListGet,
                  logicOperationsAccPickerGet,
                  logicOperationsPayReqPickerGet,
                  logicOperationsOrdersPickerGet,
                  logicOperationDataGet,
                  logicOperationDataSave,
                  logicStatementAccPickerGet,
                  logicStatementGet,
                  logicRedirect,
                  logicDataProcessingMessaged,
                  logicReportDataGet,
                  logicDownloadFile,
                  logicProcessRefreshStatus,
                  logicUsersListGet,
                  logicUsersDictionaries,
                  logicUsersGetUser,
                  logicUsersSaveUser
               ];