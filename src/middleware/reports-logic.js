import { createLogic } from 'redux-logic';
import axios from 'axios';
import { postReportConfig } from '../constants/logic_constants';
import { customReportTransform } from '../report/report_transform_data';


const logicReportDataGet = createLogic({
    type: 'GET_REPORTS_DATA',
    latest: false,
    processOptions: { 
        dispatchReturn: false        
    },
    process({ action }, dispatch, done) {
        const { reportId, reportParams, reportOutput } = action.payload;
        return axios.post('/api/v2/reports/get_report',
                          {
                              reportId: reportId,
                              reportParams: reportParams,
                              reportOutput: reportOutput
                          }, 
                          postReportConfig)            // return axios record set 
                .then(({ data } ) => {
                    const { reportData, reportFileName} = data;
                    if (reportOutput == 'json') {
                        // return report data in REPORTS_DATA
                        dispatch({
                            type: 'REPORTS_DATA',
                            payload: {
                                reportId: reportId,
                                reportData: customReportTransform(reportId, reportData)
                            }
                        })
                    }
                    else {  // generetae action LOAD_REPORT_FILE
                        dispatch({
                            type: 'LOAD_REPORT_FILE',
                            payload: {
                                reportFileName: reportFileName
                            }
                        });
                    }
                    done();
                })
                .catch(err => {
                    dispatch({
                        type: 'REPORTS_DATA_GET_FAILED',
                        payload: err,
                        error: true
                    });
                    done();
                })

    }
});


export { logicReportDataGet }