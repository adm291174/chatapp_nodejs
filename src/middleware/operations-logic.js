/* orders data flow logic */ 

import { createLogic } from 'redux-logic';
import axios from 'axios';
import { putConfig, postConfig } from '../constants/logic_constants';
import { List, Map } from 'immutable';
import { getDateStrForQuery, floatPrettyPrint, 
         getDateFromStr, getDateStr } from '../core/core_functions';
import { fillEmptyTransferData, 
         fillEmptySupplyData, 
         fillEmptySettlementData,
         fillEmptySimpleOperationData } from '../core/operations_functions';


const logicOperationsListGet = createLogic({
    type: 'GET_OPERATIONS_LIST_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'OPERATIONS_LIST_DATA',
        failType: 'OPERATIONS_LIST_DATA_GET_FAILED'
    },
    process({ action }) {
        const { startDate, endDate, operationType } = action.payload;
        return axios.put('/api/v2/operations/list',
                         {startDate: startDate,
                          endDate: endDate, 
                          operationType: operationType}, 
                         putConfig)
                .then((operationsData) => {
                    return {
                        operationsList: operationsData.data
                    }
                });
    }
});


const logicOperationsAccPickerGet = createLogic({
    type: 'GET_OPERATION_PICKER_DATA_ACCS',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'OPERATIONS_PICKER_DATA_ACCS',
        failType: 'OPERATIONS_PICKER_DATA_ACCS_GET_FAILED'
    },
    process({ action }) {
        const { currencyId, accountType, pickerName } = action.payload;
        return axios.put('/api/v2/operations/get_acc_picker_data',
                         {currencyId: currencyId ? currencyId : null,
                          accountType: accountType}, 
                         putConfig)
                .then((queryData) => {
                    return {
                        pickerName: pickerName,
                        data: processAccountsPickerData(queryData.data)
                    }
                });
    }
});

const processAccountsPickerData = function(data) {
    return List(data.map( value => {
        return Map(value);
    }));
}

const logicOperationsPayReqPickerGet = createLogic({
    type: 'GET_OPERATION_PICKER_DATA_PAYREQS',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'OPERATIONS_PICKER_DATA_PAYREQS',
        failType: 'OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED'
    },
    process({ action }) {
        const { clientId, pickerName } = action.payload;
        return axios.put('/api/v2/operations/get_payreq_picker_data',
                         {clientId: clientId}, 
                         putConfig)
                .then((queryData) => {
                    return {
                        pickerName: pickerName,
                        data: processPayReqsPickerData(queryData.data)
                    }
                });
    }
});

const processPayReqsPickerData = function(data) {
    return List(data.map( ({requisitId, orgTaxNumber, accountNumber, orgName, bankName})  => {
        const payReqDescription = {requisitId, orgTaxNumber, accountNumber, orgName, bankName};
        return Map( {requisitId, orgTaxNumber, accountNumber, orgName, bankName, payReqDescription} );
    }));
}

const logicOperationsOrdersPickerGet = createLogic({
    type: 'GET_OPERATION_PICKER_DATA_ORDERS',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'OPERATIONS_PICKER_DATA_ORDERS',
        failType: 'OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED'
    },
    process({ action }) {
        const { accountId, orderType, startDate, endDate, linkedOrderId, pickerName } = action.payload;
        return axios.put('/api/v2/operations/get_orders_picker_data',
                         { accountId, 
                           orderType, 
                           startDate: getDateStrForQuery(startDate), 
                           endDate: getDateStrForQuery(endDate), 
                           linkedOrderId: (linkedOrderId !== undefined) ? linkedOrderId : null },
                         putConfig)
                .then((queryData) => {
                    return {
                        pickerName: pickerName,
                        data: processOrdersPickerData(queryData.data)
                    }
                });
    }
});

const processOrdersPickerData = function(data) {
    return List(data.map( ({ orderId, orderDate, orderNumber, orderSum, 
                             orderCurrencyId, orderCurrencyCode }) => {
        const orderSumForPrint = floatPrettyPrint(orderSum) + ' ' + orderCurrencyCode,
              orderDateDt = getDateFromStr(orderDate),
              orderInfo = 'Ордер № ' + orderNumber + ' от ' + getDateStr(orderDateDt) ;
        return Map( { orderId, orderDate: orderDateDt, orderNumber, orderSum, 
                      orderCurrencyId, orderCurrencyCode, orderInfo, orderSumForPrint } );
    }));
}

const logicOperationDataGet = createLogic({
    type: 'GET_OPERATION_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'OPERATION_DATA',
        failType: 'OPERATION_DATA_GET_FAILED'
    },
    process({ action }) {
        const { operationId } = action.payload;
        return axios.put('/api/v2/operations/get_operation',
                         { operationId }, 
                         putConfig)
                .then((operationData) => {
                    return processOperationData(operationData);
                });
    }
});

const processOperationData = function(operationData) {
    const data = operationData.data;

    const currencyInfo = {
             operationCurrencyId: data.operationCurrencyId,
             operationCurrencyCode: data.operationCurrencyCode
          };
          

    return Map({
        operationDataId: data.operationId,
        operationType: data.operationType,
        operationTypeDescription: data.operationTypeDescription,
        operationNumber: data.operationNumber,
        operationDate: getDateFromStr(data.operationDate),
        operationSum: data.operationSum,
        operationStatus: data.operationStatus,
        transferData: fillTransferData(data.transferData, currencyInfo),
        supplyData: fillSupplyData(data.supplyData, currencyInfo),
        settlementData: fillSettlementData(data.settlementData, currencyInfo),
        simpleOperationData: fillSimpleOperationData(data.simpleOperationData, currencyInfo) 

    });
}

const fillTransferData = function(transferData, currencyInfo) {
    return (transferData)
        ? Map({
            buyerOrderId: transferData.buyerOrderId,
            buyerOrderInfo: transferData.buyerOrderId ? 
               Map({
                 orderClientId: transferData.buyerOrderInfo.orderClientId,
                 orderNumber: transferData.buyerOrderInfo.orderNumber,
                 orderDate: getDateFromStr(transferData.buyerOrderInfo.orderDate)
               }) : Map({ orderClientId: null, orderNumber: null, orderDate: null}),
            buyerClientId: transferData.buyerClientId,
            buyerClientName: transferData.buyerClientName,
            buyerAccountId: transferData.buyerAccountId,
            buyerAccountNumber: transferData.buyerAccountNumber,
            buyerPaymentRequisitId: transferData.buyerPaymentRequisitId,
            buyerPaymentRequisitInfo: transferData.buyerPaymentRequisitId 
               ? Map({  
                    payRequisitClientId: transferData.buyerPaymentRequisitInfo.payRequisitClientId,
                    payReqTaxNumber: transferData.buyerPaymentRequisitInfo.payReqTaxNumber,
                    payReqOrgName: transferData.buyerPaymentRequisitInfo.payReqOrgName,
                    payReqAccountNumber: transferData.buyerPaymentRequisitInfo.payReqAccountNumber,
                    payReqBankName: transferData.buyerPaymentRequisitInfo.payReqBankName
                 }) : 
                 Map({ payRequisitClientId: null,
                       payReqTaxNumber: null,
                       payReqOrgName: null,
                       payReqAccountNumber: null, 
                       payReqBankName: null
                     }),
            supplierOrderId: transferData.supplierOrderId,
            supplierOrderInfo: transferData.supplierOrderId ? 
               Map({
                 orderClientId: transferData.supplierOrderInfo.orderClientId,
                 orderNumber: transferData.supplierOrderInfo.orderNumber,
                 orderDate: getDateFromStr(transferData.supplierOrderInfo.orderDate)
               }) : Map({ orderClientId: null, orderNumber: null, orderDate: null}),
            supplierClientId: transferData.supplierClientId,
            supplierClientName: transferData.supplierClientName,
            supplierAccountId: transferData.supplierAccountId,
            supplierAccountNumber: transferData.supplierAccountNumber,
            supplierPaymentRequisitId: transferData.supplierPaymentRequisitId,
            supplierPaymentRequisitInfo: transferData.supplierPaymentRequisitId 
               ? Map({  
                    payRequisitClientId: transferData.supplierPaymentRequisitInfo.payRequisitClientId,
                    payReqTaxNumber: transferData.supplierPaymentRequisitInfo.payReqTaxNumber,
                    payReqOrgName: transferData.supplierPaymentRequisitInfo.payReqOrgName,
                    payReqAccountNumber: transferData.supplierPaymentRequisitInfo.payReqAccountNumber,
                    payReqBankName: transferData.supplierPaymentRequisitInfo.payReqBankName
                 }) : 
                 Map({ payRequisitClientId: null,
                       payReqTaxNumber: null,
                       payReqOrgName: null,
                       payReqAccountNumber: null, 
                       payReqBankName: null
                     }),
            paymentDocumentNumber: transferData.paymentDocumentNumber,
            paymentDocumentDate: getDateFromStr(transferData.paymentDocumentDate),
            paymentDocumentPurpose: transferData.paymentDocumentPurpose,
            operationCurrencyId: currencyInfo.operationCurrencyId,
            operationCurrencyCode: currencyInfo.operationCurrencyCode
        })
        : fillEmptyTransferData();
}

const fillSupplyData = function(supplyData, currencyInfo) {
    return (supplyData) 
        ? Map({
        supplierOrderId: supplyData.supplierOrderId,
        supplierOrderInfo: Map({
            orderClientId: supplyData.supplierOrderInfo.orderClientId,
            orderNumber: supplyData.supplierOrderInfo.orderNumber,
            orderDate: getDateFromStr(supplyData.supplierOrderInfo.orderDate)
        }),
        supplierClientId: supplyData.supplierClientId,
        supplierClientName: supplyData.supplierClientName,
        supplierAccountId: supplyData.supplierAccountId,
        supplierAccountNumber: supplyData.supplierAccountNumber,
        supplyMoneyPart: Map({
            operationPartType: supplyData.supplyMoneyPart.operationPartType,
            operationPartAccountId: supplyData.supplyMoneyPart.operationPartAccountId,
            operationPartAccountNumber: supplyData.supplyMoneyPart.operationPartAccountNumber,
            operationPartAccountName: supplyData.supplyMoneyPart.operationPartAccountName,
            operationPartSum: supplyData.supplyMoneyPart.operationPartSum,
            operationPartPurpose: supplyData.supplyMoneyPart.operationPartPurpose
        }),
        supplyCommissionPart: Map({
            operationPartType: supplyData.supplyCommissionPart.operationPartType,
            operationPartAccountId: supplyData.supplyCommissionPart.operationPartAccountId,
            operationPartAccountNumber: supplyData.supplyCommissionPart.operationPartAccountNumber,
            operationPartAccountName: supplyData.supplyCommissionPart.operationPartAccountName,
            operationPartSum: supplyData.supplyCommissionPart.operationPartSum,
            operationPartPurpose: supplyData.supplyCommissionPart.operationPartPurpose
        }),
        operationCurrencyId: currencyInfo.operationCurrencyId,
        operationCurrencyCode: currencyInfo.operationCurrencyCode})
        : fillEmptySupplyData();
}

const fillSettlementData = function(settlementData, currencyInfo) {
    return (settlementData) 
        ? Map({
        buyerOrderId: settlementData.buyerOrderId,
        buyerOrderInfo: Map({
            orderClientId: settlementData.buyerOrderInfo.orderClientId,
            orderNumber: settlementData.buyerOrderInfo.orderNumber,
            orderDate: getDateFromStr(settlementData.buyerOrderInfo.orderDate)
        }),
        buyerClientId: settlementData.buyerClientId,
        buyerClientName: settlementData.buyerClientName,
        buyerAccountId: settlementData.buyerAccountId,
        buyerAccountNumber: settlementData.buyerAccountNumber,
        settlementMoneyPart: Map({
            operationPartType: settlementData.settlementMoneyPart.operationPartType,
            operationPartAccountId: settlementData.settlementMoneyPart.operationPartAccountId,
            operationPartAccountNumber: settlementData.settlementMoneyPart.operationPartAccountNumber,
            operationPartAccountName: settlementData.settlementMoneyPart.operationPartAccountName,
            operationPartSum: settlementData.settlementMoneyPart.operationPartSum,
            operationPartPurpose: settlementData.settlementMoneyPart.operationPartPurpose
        }),
        settlementCommissionPart: Map({
            operationPartType: settlementData.settlementCommissionPart.operationPartType,
            operationPartAccountId: settlementData.settlementCommissionPart.operationPartAccountId,
            operationPartAccountNumber: settlementData.settlementCommissionPart.operationPartAccountNumber,
            operationPartAccountName: settlementData.settlementCommissionPart.operationPartAccountName,
            operationPartSum: settlementData.settlementCommissionPart.operationPartSum,
            operationPartPurpose: settlementData.settlementCommissionPart.operationPartPurpose
        }),
        operationCurrencyId: currencyInfo.operationCurrencyId,
        operationCurrencyCode: currencyInfo.operationCurrencyCode})
        : fillEmptySettlementData();
}

const fillSimpleOperationData = function(simpleOperationData, currencyInfo) {
    return (simpleOperationData) 
       ? Map({
        debetOperationPart: Map({
            operationPartType: simpleOperationData.debetOperationPart.operationPartType,
            accountId: simpleOperationData.debetOperationPart.accountId,
            accountNumber: simpleOperationData.debetOperationPart.accountNumber,
            accountName: simpleOperationData.debetOperationPart.accountName,
            clientId: simpleOperationData.debetOperationPart.clientId,
            clientName: simpleOperationData.debetOperationPart.clientName
        }),
        creditOperationPart: Map({
            operationPartType: simpleOperationData.creditOperationPart.operationPartType,
            accountId: simpleOperationData.creditOperationPart.accountId,
            accountNumber: simpleOperationData.creditOperationPart.accountNumber,
            accountName: simpleOperationData.creditOperationPart.accountName,
            clientId: simpleOperationData.creditOperationPart.clientId,
            clientName: simpleOperationData.creditOperationPart.clientName
        }),
        operationPurpose: simpleOperationData.operationPurpose,
        operationCurrencyId: currencyInfo.operationCurrencyId,
        operationCurrencyCode: currencyInfo.operationCurrencyCode
       })
       : fillEmptySimpleOperationData();
} 

const logicOperationDataSave = createLogic({
    type: 'SAVE_OPERATION_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'APP_CONTEXT_SET',
        failType: 'OPERATION_DATA_SAVE_FAILED'
    },
    process({ action }) {
        const requestData = action.payload;
        return axios.post('/api/v2/operations/save_operation',
                         requestData, 
                         postConfig)
                .then((operationSaveData) => {
                    return {
                        window: 'operationEdit',
                        context: Map({
                            windowContent: Map({
                               operationId: operationSaveData.data.operationId,
                               operationIsEdited: false
                            })
                        })    
                    };
                });
    }
});



export { logicOperationsListGet, logicOperationsAccPickerGet, 
         logicOperationsPayReqPickerGet, logicOperationsOrdersPickerGet,
         logicOperationDataGet, logicOperationDataSave } 