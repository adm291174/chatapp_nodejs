import { createLogic } from 'redux-logic';
import axios from 'axios';
import { putConfig } from '../constants/logic_constants';
import { List, Map } from 'immutable';
import { getDateStrForQuery, getDateFromStr } from '../core/core_functions';


const logicStatementAccPickerGet = createLogic({
    type: 'GET_STATEMENT_PICKER_DATA_ACCS',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'WINDOW_DATA_CHANGED',
        failType: 'STATEMENT_PICKER_DATA_ACCS_GET_FAILED'
    },
    process() {
        return axios.get('/api/v2/statement/get_acc_picker_data',
                         {}, 
                         putConfig)
                .then((queryData) => {
                    return {
                        windowName: 'statement',
                        fieldName: 'accountPickerData',
                        fieldValue: processAccountsPickerData(queryData.data)
                    }
                });
    }
});

const processAccountsPickerData = function(data) {
    return List(data.map( value => {
        return Map(value);
    }));
}

const logicStatementGet = createLogic({
    type: 'GET_STATEMENT_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'WINDOW_DATA_MASS_CHANGED',
        failType: 'STATEMENT_DATA_GET_FAILED'
    },
    process({ action }) {
        const { accountId, startDate, endDate } = action.payload;
        return axios.put('/api/v2/statement/get_statement_data',
                         {accountId: accountId,
                          startDate: getDateStrForQuery(startDate),
                          endDate: getDateStrForQuery(endDate)},
                         putConfig)
                .then((queryData) => {
                    return {
                        windowName: 'statement',
                        windowContent: Map({
                            statementData: processStatementData(queryData.data),
                            dataIsRetrieving: false
                        })
                    }
                });
    }
});

const processStatementData = function(data) {
    const statementRecords = List(
        data.statementRecords.map( value => {
            const operations = List(value.operations.map(record => {
                return Map({
                    operationSum: record.operationSum,
                    mcOperationSum: record.mcOperationSum,
                    formattedSum: record.formattedSum,
                    mcFormattedSum: record.mcFormattedSum,
                    operationPurpose: record.operationPurpose
                })
            }))
            return Map({
                recordDate: getDateFromStr(value.recordDate),
                startAmount: value.startAmount,
                mcStartAmount: value.mcStartAmount,
                endAmount: value.endAmount,
                mcEndAmount: value.mcEndAmount,
                operations: operations
                
            })
        })
    );
    
    return Map({
        accountId: data.accountId,
        accountNumber: data.accountNumber,
        accountName: data.accountName,
        startDate: getDateFromStr(data.startDate),
        endDate: getDateFromStr(data.endDate),
        currencyId: data.currencyId,
        currencyIsoCode: data.currencyIsoCode,
        startAmount: data.startAmount,
        mcStartAmount: data.mcStartAmount,
        endAmount: data.endAmount,
        mcEndAmount: data.mcEndAmount,
        statementRecords: statementRecords
    })
}

export { logicStatementAccPickerGet,
         logicStatementGet }