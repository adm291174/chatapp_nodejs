/* orders data flow logic */ 

import { createLogic } from 'redux-logic';
import axios from 'axios';
import { putConfig, postConfig } from '../constants/logic_constants';

import { roundAlt, getDateFromStr, getDateStrForQuery, floatPrettyPrint } from '../core/core_functions';
import { orderTypesById } from '../constants/order_constants';

import { List, Map } from 'immutable';


const logicOrdersListGet = createLogic({
    type: 'GET_ORDERS_LIST_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'ORDERS_LIST_DATA',
        failType: 'ORDERS_LIST_DATA_GET_FAILED'
    },
    process({ action }) {
        const { startDate, endDate, status } = action.payload;
        return axios.put('/api/v2/orders/list',
                         {startDate: startDate,
                          endDate: endDate, 
                          status: status}, 
                         putConfig)            // return axios record set 
                .then((ordersData) => {
                    return {
                        ordersList: ordersData.data
                    }
                });
    }
});

const logicOrderDataGet = createLogic({
    type: 'GET_ORDER_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'ORDER_DATA',
        failType: 'ORDER_DATA_GET_FAILED'
    },
    process({ action }) {
        const { orderId, 
                getOrderBodyData, 
                getOrderAdditionalData, 
                getOrderActions } = action.payload;

        let actionsList = [];
        
        const actionSetOrderDataId = new Promise((resolve) => {
            resolve({ orderDataName: 'orderDataId', orderDataValue: orderId })
        });
        
        const actionOrderBodyGet = () => {
            return (
                axios.put('/api/v2/orders/get_order_data',
                         { orderId: orderId }, 
                         putConfig)            // return axios record set 
                .then((orderData) => {
                    const { orderNumber, orderStatusId, orderStatusDescription,
                            orderDate, orderType, orderTypeDescription, accountId,
                            accountNumber, accountCurrencyId, accountCurrencyCode,
                            clientId, clientName, orderSum, commissionType,
                            commissionDescription, commissionPercent,
                            commissionSum, secondarySum, canBeEdited } = orderData.data;

                    
                    return {
                        orderDataName: 'orderBodyData', 
                        orderDataValue: Map({
                            orderNumber: orderNumber,
                            orderType: orderType,
                            orderTypeDescription: orderTypeDescription,
                            orderStatusId: orderStatusId,
                            orderStatusDescription: orderStatusDescription,
                            orderDate: getDateFromStr(orderDate),
                            orderAccountId: accountId,
                            orderAccountCurrencyId: accountCurrencyId,
                            orderAccountCurrencyCode: accountCurrencyCode,
                            orderAccountNumber: accountNumber,
                            orderClientId: clientId,
                            orderClientName: clientName,
                            orderSum: orderSum,
                            orderCommissionType: commissionType, 
                            orderCommissionDescription: commissionDescription,
                            orderCommissionPercent: commissionPercent,
                            orderCommissionSum: commissionSum, 
                            orderSecondarySum: secondarySum,
                            canBeEdited: canBeEdited
                        })

                    }
                })
            );
        };
        const actionLinkedOrdersGet = () => {
            return (
            axios.put('/api/v2/orders/get_order_linked_orders',
                         { orderId: orderId }, 
                         putConfig)
                .then((linkedOrdersData) => {

                    return {
                        orderDataName: 'orderLinkedOrders', 
                        orderDataValue: List(
                            linkedOrdersData.data.map(value => {
                                const { orderNumber, orderStatusId, orderStatusDescription, 
                                        orderDate, orderType, orderTypeDescription, accountCurrencyId, 
                                        accountCurrencyCode, clientId, clientName, orderSum,
                                        orderAccountId, orderAccountNumber } = value,
                                        linkedOrderId = value.orderId;
                                
                                return Map({
                                    orderId: linkedOrderId,
                                    orderNumber: orderNumber, 
                                    orderStatusId: orderStatusId,
                                    orderStatusDescription: orderStatusDescription,
                                    orderType: orderType,
                                    orderTypeDescription: orderTypeDescription,
                                    orderDate: getDateFromStr(orderDate),
                                    orderAccountCurrencyId: accountCurrencyId,
                                    orderAccountCurrencyCode: accountCurrencyCode,
                                    orderSum: orderSum,
                                    orderClientId: clientId,
                                    orderClientName: clientName,
                                    orderAccountId: orderAccountId,
                                    orderAccountNumber: orderAccountNumber
                                })
                            })
                        )
                    }
                })
            )
        }

        const actionLinkedOperationsGet = () => {
            return (
                axios.put('/api/v2/orders/get_order_linked_operations',
                         { orderId: orderId }, 
                         putConfig)
                .then((linkedOperationsData) => {

                    return {
                        orderDataName: 'orderLinkedOperations', 
                        orderDataValue: List(
                            linkedOperationsData.data.map(value => {
                                const { operationId, operationNumber, operationType, 
                                        operationTypeDescription, operationDate, 
                                        operationCurrencyCode,
                                        operationSum, 
                                        clientId, clientName,
                                        operationStatusId, operationStatusDescription } = value;

                                return Map({
                                    operationId: operationId,
                                    operationNumber: operationNumber, 
                                    operationType: operationType,
                                    operationTypeDescription: operationTypeDescription,
                                    operationDate: getDateFromStr(operationDate),
                                    operationCurrencyCode: operationCurrencyCode,
                                    operationSum: operationSum,
                                    clientId: clientId,
                                    clientName: clientName,
                                    operationStatusId: operationStatusId,
                                    operationStatusDescription: operationStatusDescription

                                })
                            })
                        )
                    }
                })
            )
        }
        
        const actionOrderActionsGet = () => {
            return (
                axios.put('/api/v2/orders/get_order_actions',
                         { orderId: orderId }, 
                         putConfig)
                .then((orderActionsData) => {

                    return {
                        orderDataName: 'orderActions', 
                        orderDataValue: List(
                            orderActionsData.data.map(value => {
                                const { actionId, actionDescription, actionSortOrder } = value;

                                return Map({
                                    actionId: actionId,
                                    actionDescription: actionDescription, 
                                    actionSortOrder: actionSortOrder
                                })
                            })
                        )
                    }
                })
            )
        }
        
        actionsList.push(actionSetOrderDataId);
        (getOrderBodyData == true) && actionsList.push(actionOrderBodyGet());
        (getOrderAdditionalData == true) && actionsList.push(actionLinkedOrdersGet());
        (getOrderAdditionalData == true) && actionsList.push(actionLinkedOperationsGet());
        (getOrderActions == true) && actionsList.push(actionOrderActionsGet());
        return Promise.all(actionsList);
    }
});


const logicOrderDataSave = createLogic({
    type: 'ORDER_DATA_SAVE',
    latest: false,
    processOptions: { 
        dispatchReturn: false        
    },
    process({ action }, dispatch, done) {
        const { data, postAction } = action.payload;
        return axios.post('/api/v2/orders/save_order_data',
                         data, 
                         postConfig)            // return axios record set 
                .then((ordersResultData) => {
                    if (postAction == 'exitOrder') {
                        // close order window and switch to orders list 
                        dispatch({
                            type: 'APP_CONTEXT_SET',
                            payload: {
                                window: 'ordersList',
                                context: Map()
                            }
                        });
                        dispatch(
                        {
                            type: 'WINDOW_TITLE_SET',
                            payload: {
                                windowName: 'orderEdit',
                                windowTitle: ''
                            }
                        });
                    }
                    else {
                        dispatch ({
                            type: 'APP_CONTEXT_SET',
                            payload: {
                                window: 'orderEdit',
                                context: Map({
                                    windowContent: Map({
                                        orderId: ordersResultData.data.orderId,
                                        orderIsEdited: false
                                    })
                                })
                            }
                        });
                    }
                    done();
                })
                .catch(err => {
                    dispatch({
                        type: 'ORDER_DATA_SAVE_FAILED',
                        payload: err,
                        error: true
                    });
                    done();
                })

    }
});

const orderCommissionCalc = createLogic({
    type: 'ORDER_EDIT_DATA_CHANGED',
    latest: true,
    process({ getState, action }, dispatch, done) {
        const { fieldName, fieldValue} = action.payload;
        switch (fieldName) {
            case 'orderCommissionPercent': 
            case 'orderSum': {
                const ordersData = getState().get('orders').data.get('orderData'),
                      orderBodyData = ordersData.get('orderBodyData');

                const orderSum = (fieldName == 'orderSum') ? fieldValue : orderBodyData.get('orderSum');
                const orderCommissionPercent = (fieldName == 'orderSum') 
                                              ? orderBodyData.get('orderCommissionPercent') 
                                              : fieldValue;

                const commissionSum = roundAlt(orderSum * orderCommissionPercent / 100, 2);

                if (commissionSum == 0 || commissionSum) {
                    dispatch( {
                        type: 'ORDER_EDIT_DATA_CHANGED',
                        payload: {
                            fieldName: 'orderCommissionSum',
                            fieldValue: commissionSum
                        }
                    });

                }

                break;
            }
            case 'orderCommissionSum': {
                const ordersData = getState().get('orders').data.get('orderData'),
                      orderBodyData = ordersData.get('orderBodyData');

                const orderSum = orderBodyData.get('orderSum');
                const secondarySum = roundAlt(orderSum - fieldValue);
                if (secondarySum == 0 || secondarySum) {
                    dispatch( {
                        type: 'ORDER_EDIT_DATA_CHANGED',
                        payload: {
                            fieldName: 'orderSecondarySum',
                            fieldValue: secondarySum
                        }
                    });
                }
                break;
            }
        }
        done();
    }
});

const orderEditClientTypeSet = createLogic({
    type: 'ORDER_EDIT_DATA_CHANGED',
    latest: false,
    process({ action }, dispatch, done) {
        const { fieldName, fieldValue} = action.payload;
        switch (fieldName) {
            case 'orderType': {
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderTypeDescription',
                        fieldValue: orderTypesById.get(fieldValue)
                    }
                });
                // Clear accounts and clients data
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderAccountId',
                        fieldValue: null
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderAccountNumber',
                        fieldValue: null
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderClientId',
                        fieldValue: null
                    }
                });
                dispatch( {
                    type: 'ORDER_EDIT_DATA_CHANGED',
                    payload: {
                        fieldName: 'orderClientName',
                        fieldValue: null
                    }
                });
                break;
            }
        }
        done();
    }
});

const logicOrderEditAccountsGet = createLogic({
    type: 'GET_ORDER_EDIT_PICKER_DATA_ACCS',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'ORDER_EDIT_PICKER_DATA',
        failType: 'ORDER_EDIT_PICKER_DATA_ACCS_GET_FAILED'
    },
    process({ action }) {
        const { orderType } = action.payload;
        return axios.put('/api/v2/orders/get_acc_picker_data',
                         { orderType: orderType }, 
                         putConfig)             
                .then((orderPickerData) => {
                    return {
                        responseData: {
                            accountsPickerData: List(
                                orderPickerData.data.map( value => {
                                    return Map({
                                        accountId: value.accountId,
                                        accountNumber: value.accountNumber,
                                        accountCurrencyId: value.accountCurrencyId,
                                        accountCurrencyCode: value.accountCurrencyCode,
                                        clientId: value.clientId,
                                        clientName: value.clientName,
                                        orderType: value.orderType,
                                        orderTypeDescription: value.orderTypeDescription
                                    })
                                })
                            )
                        }
                    }
                });
    }
});


const logicOrderEditLOGet = createLogic({
    type: 'GET_ORDER_EDIT_PICKER_DATA_LO',
    latest: false,
    processOptions: { 
        dispatchReturn: true,
        successType: 'ORDER_EDIT_PICKER_DATA',
        failType: 'ORDER_EDIT_PICKER_LO_DATA_GET_FAILED'
    },
    process({ action }) {
        const { orderType, startDate, endDate, onlyNotLinked } = action.payload;
        return axios.put('/api/v2/orders/get_lo_picker_data',
                         { orderType: orderType,
                           startDate: getDateStrForQuery(startDate), 
                           endDate: getDateStrForQuery(endDate),
                           onlyNotLinked: onlyNotLinked }, 
                         putConfig)             
                .then((orderPickerData) => {
                    return {
                        responseData: {
                            linkedOrdersPickerData: List(
                                orderPickerData.data.map( value => {
                                    return Map({
                                        orderId: value.orderId,
                                        orderNumber: value.orderNumber,
                                        orderStatusId: value.orderStatusId,
                                        orderStatusDescription: value.orderStatusDescription,
                                        orderType: value.orderType,
                                        orderTypeDescription: value.orderTypeDescription,
                                        orderDate: getDateFromStr(value.orderDate),
                                        orderAccountCurrencyId: value.orderCurrencyId,
                                        orderAccountCurrencyCode: value.orderCurrencyCode,
                                        orderSum: value.orderSum,
                                        orderClientId: value.clientId,
                                        orderClientName: value.clientName,
                                        orderSumForPrint: floatPrettyPrint(value.orderSum) + ' ' + value.orderCurrencyCode
                                    })
                                })
                            )
                        }
                    }
                });
    }
});

const logicOrderExecuteAction = createLogic({
    type: 'ORDER_PROCESS_ACTION',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'APP_CONTEXT_SET',
        failType: 'ORDER_PROCESS_ACTION_FAILED'
    },
    process({ action }) {
        const { orderId, actionId } = action.payload;
        return axios.post('/api/v2/orders/execute_action',
                         { orderId: orderId,
                           actionId: actionId },
                         postConfig)
                .then(() => ({
                        window: 'orderEdit',
                        context: Map({
                            windowContent: Map({
                                orderId: orderId,
                                orderIsEdited: false
                            })
                        })
                    })
            );
    }
});

export { logicOrdersListGet, 
         orderCommissionCalc, 
         logicOrderEditAccountsGet, 
         orderEditClientTypeSet,
         logicOrderDataGet,
         logicOrderDataSave,
         logicOrderEditLOGet,
         logicOrderExecuteAction } 