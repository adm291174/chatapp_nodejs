/* payment requisits data flow logic */ 

import { createLogic } from 'redux-logic';
import axios from 'axios';
import { putConfig, postConfig } from '../constants/logic_constants';
import { Map } from 'immutable';


const logicPayReqGet = createLogic({
    type: 'GET_PAYREQ_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'PAYREQ_DATA',
        failType: 'PAYREQ_DATA_GET_FAILED'
    },
    process({ action }) {
        // determine what kind of action process 
        const { requisitId } = action.payload;
        // const lastUpdateTime = queryData.last_time;
        return axios.put('/api/v2/clients/get_pay_req',
                         {requisitId: requisitId}, 
                         putConfig)            // return axios record set 
                .then((requisitsData) => {
                    return {
                        responseData: requisitsData.data
                    }    
                });
    }
});

const logicPayReqSave = createLogic({
    type: 'PAYREQ_DATA_SAVE',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'APP_CONTEXT_SET',
        failType: 'PAYREQ_DATA_SAVE_FAILED'
    },
    process({ action }) {
        // determine what kind of action process 
        const { requisitId, clientId, orgTaxNumber, orgTaxReasonCode, orgName,
                accountNumber, bankCode, bankName, bankCorrAccNumber, enabled } = action.payload;

        // const lastUpdateTime = queryData.last_time;
        return axios.post('/api/v2/clients/save_pay_req',
                         {requisitId: requisitId,
                          clientId: clientId, 
                          orgTaxNumber: orgTaxNumber, 
                          orgTaxReasonCode: orgTaxReasonCode, 
                          orgName: orgName,
                          accountNumber: accountNumber,
                          bankCode: bankCode,
                          bankName: bankName,
                          bankCorrAccNumber: bankCorrAccNumber,
                          enabled: enabled}, 
                         postConfig)            // return axios record set 
                .then(() => ({
                        window: 'clientEdit',
                        context: Map({
                            windowContent: Map({
                                clientId: clientId
                            })
                        })    
                      }));
    }
});

export { logicPayReqGet, logicPayReqSave }
