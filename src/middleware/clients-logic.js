// Clients logic. We use separate file to reduce complexity 
import { createLogic } from 'redux-logic';
import axios from 'axios';
import { Map, List } from 'immutable';
import { putConfig, postConfig  } from '../constants/logic_constants';
import { getDateFromJSON } from '../core/core_functions';


const logicClientListGet = createLogic({
    type: 'GET_CLIENTS_LIST_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'CLIENTS_LIST_DATA',
        failType: 'CLIENTS_LIST_DATA_GET_FAILED'
    },
    process({ action }) {
        // determine what kind of action process 
        const { filterText } = action.payload;
        // const lastUpdateTime = queryData.last_time;
        return axios.put('/api/v2/clients/list',
                         {filterText: filterText}, 
                         putConfig)            // return axios record set 
                .then(processClientsList)  // return chatInfo, newIdentfiers 
                .then(([clientsList, clientIds]) => {
                    
                    // check whether we have data in messageList, if nothing there - get bunch of last messages 
                    if (clientIds.length > 0) {
                        const queryData = {'clients': clientIds};

                        return (Promise.all([ clientsList, axios.put('/api/v2/clients/accounts',
                                                queryData,
                                                putConfig)] ))
                    }
                    else 
                        return (Promise.all([ clientsList, {data: []}]))
                })
                .then(([clientsList, responseData]) => {
                    const accountsDataList = responseData.data;
                    let result = Map();
                    accountsDataList.forEach(accountInfo => {
                        const clientId = accountInfo.clientId;
                        const clientRecord = result.get(clientId);
                        if (clientRecord) {
                            result = result.set(clientId, clientRecord.push(Map(accountInfo)))
                        }
                        else {
                            result = result.set(clientId, List([Map(accountInfo)]));
                        }
                    })

                    return {  
                        clientsInfo: clientsList,
                        accountsInfo: result
                    }; 
                })
            
    }
});

function processClientsList(responseData) {
    // get response data and returns sorted List with data and next query data 
    const clientIds = responseData.data.map(clientDataItem => clientDataItem.clientId);
    const clientsList = List(responseData.data.map(clientDataItem => Map(clientDataItem))).sortBy( value => value.get('clientName').toLowerCase());
    return [clientsList, clientIds]
}

const logicClientGet = createLogic({
    type: 'GET_CLIENT_DATA',
    latest: false,
    process({ action }, dispatch, done) {
        const { clientId } = action.payload;
        if (clientId) {
            return axios.put('/api/v2/clients/get_info',
                         {clientId: clientId}, 
                         putConfig)            
                .then(responseData => {
                    return prepareClientInfoAction(responseData, clientId)
                })  // return chatInfo, newIdentfiers 
                .then(clientInfoData => {
                    dispatch(clientInfoData);
                    return axios.put('/api/v2/clients/get_pay_reqs_list',
                         {clientId: clientId}, 
                         putConfig)            
                })
                .then(responseData => {
                    return prepareClientPayReqAction(responseData, clientId)
                })
                .then(payReqInfo => {
                    dispatch(payReqInfo);
                    return axios.put('/api/v2/clients/accounts',
                         {clients: [clientId]}, 
                         putConfig)            
                })
                .then(responseData => {
                    return prepareClientAccountAction(responseData, clientId)
                })
                .then(accountsInfo => {
                    dispatch(accountsInfo);
                    return axios.put('/api/v2/clients/get_users',
                         {clientId: clientId,
                          availableList: false}, 
                         putConfig)            
                })
                .then(responseData => {
                    return prepareUsersAction(responseData, clientId)
                })
                .then(usersInfo => {
                    dispatch(usersInfo);
                    done();                    
                })
                .catch(err => {
                    dispatch({
                        type: 'CLIENT_DATA_GET_FAILED',
                        payload: err,
                        error: true
                    });
                    done();
                })

        }
        done();
    }
});

function prepareClientInfoAction(responseData, clientId) {
    const clientInfo = responseData.data;
    return {
        type: 'CLIENT_MAIN_DATA',
        payload: {
            clientId: clientId,
            windowContent: Map(clientInfo)
        }    
    } 
}

function prepareClientPayReqAction(responseData, clientId) {
    const payReqInfo = responseData.data;
    return {
        type: 'CLIENT_PAYREQ_DATA',
        payload: {
            clientId: clientId,
            windowContent: Map({
                clientPaymentRequisits: List(payReqInfo.map(paymentRequisit => 
                    Map({
                        requisitId: paymentRequisit.requisitId,
                        clientId: paymentRequisit.clientId,
                        orgTaxNumber: paymentRequisit.orgTaxNumber,
                        orgTaxReasonCode: paymentRequisit.orgTaxReasonCode,
                        orgName: paymentRequisit.orgName,
                        accountNumber: paymentRequisit.accountNumber,
                        bankCode: paymentRequisit.bankCode,
                        bankName: paymentRequisit.bankName,
                        bankCorrAccNumber: paymentRequisit.bankCorrAccNumber,
                        enabled: paymentRequisit.enabled
                    })
                ))
            })
        }
    }        
}

function prepareClientAccountAction(responseData, clientId) {
    const clientAccountInfo = responseData.data;
    
    return {
        type: 'CLIENT_ACCOUNT_DATA',
        payload: {
            clientId: clientId,
            windowContent: Map({
                clientAccounts: List(clientAccountInfo.map(accountInfo => Map({
                    accountId: + accountInfo.accountId,
                    clientId: + accountInfo.clientId,
                    accountNumber: accountInfo.accountNumber,
                    accountCurrencyId: accountInfo.accountCurrencyId,
                    accountCurrency: accountInfo.accountCurrency,
                    accountStatus: accountInfo.accountStatus,
                    accountAmount: accountInfo.accountAmount,
                    dateOfOpen: getDateFromJSON(accountInfo.dateOfOpen),
                    dateOfClose: getDateFromJSON(accountInfo.dateOfClose),
                    isBuyerAccount: accountInfo.isBuyerAccount,
                    isSupplierAccount: accountInfo.isSupplierAccount
                })))
            })
        }
    }
}

function prepareUsersAction(responseData, clientId) {
    const clientUsersInfo = responseData.data;
    return {
        type: 'CLIENT_USERS_DATA',
        payload: {
            clientId: clientId,
            windowContent: Map({
                clientUsers: List(clientUsersInfo.map(userInfo => Map({
                    userId: userInfo.userId,
                    userLogin: userInfo.userLogin,
                    userName: userInfo.userName
                })))
            }
            )    
        }
    }
}

const logicClientEditSave = createLogic({
    type: 'CLIENT_DATA_SAVE',
    latest: true,
    process({ action }, dispatch, done ) {
        // determine what kind of action process 
        const { clientId, clientName, isBuyer, isSupplier, closeForm } = action.payload;

        // const lastUpdateTime = queryData.last_time;
        return axios.post('/api/v2/clients/save_info',
                        {clientId: clientId, 
                        clientName: clientName, 
                        isBuyer: isBuyer,
                        isSupplier: isSupplier}, 
                        postConfig)            // return axios record set 
                     .then((responseData) => {
                            if (closeForm) {
                                dispatch({
                                    type: 'APP_CONTEXT_SET',
                                    payload: {
                                        window: 'clients',
                                        context: Map({})
                                    }
                                });
                            }
                            else {
                                dispatch({
                                    type: 'GET_CLIENT_DATA',
                                    payload: {
                                        clientId: responseData.data.clientId
                                    }
                                });
                            }
                            done();
                     })
                     .catch(err => {
                            dispatch({
                                type: 'CLIENT_DATA_SAVE_FAILED',
                                payload: err,
                                error: true
                            });
                            done();
                    })
    }
});

const logicClientGetUsersForAdd = createLogic({
    type: 'GET_CLIENT_USERS_FORADD',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'CLIENT_USERS_AVAILABLE_DATA',
        failType: 'CLIENT_USERS_AVAILABLE_GET_FAILED'
    },
    process({ action }) {
        // determine what kind of action process 
        const { clientId, onlyAvailable } = action.payload;
        // const lastUpdateTime = queryData.last_time;
        return axios.put('/api/v2/clients/get_users_for_addition',
                         {clientId: clientId,
                          onlyAvailable: onlyAvailable}, 
                         putConfig)            // return axios record set 
                .then(processAvailableUsersList)  // return usersList 
    }
});

const processAvailableUsersList = function(responseData) {
    const availableUsersList = responseData.data;
    return ({ responseData: {
                  usersList: List(availableUsersList.map(userInfo => Map({
                        userId: userInfo.userId,
                        userLogin: userInfo.userLogin,
                        userName: userInfo.userName
                  })))
              }
           })
}

const logicClientSetClientUser = createLogic({
    type: 'CLIENT_ADDUSER_SAVE',
    latest: true,
    process({ action }, dispatch, done) {
        // determine what kind of action process 
        const { clientId, userId, enable, callContext } = action.payload;
        return axios.post('/api/v2/clients/set_user',
                         { clientId: clientId,
                           userId: userId,
                           enable: enable }, 
                         postConfig)            // return axios record set 
                .then( () => {
                    switch (callContext) {
                        case 'addUser': { // switch to client edit form 
                            dispatch({
                                type: 'APP_CONTEXT_SET', 
                                payload: {
                                    window: 'clientEdit',
                                    context: Map({
                                        windowContent: Map({
                                            clientId: clientId
                                        })
                                    })
                            }});
                            break; 
                        }
                        case 'removeUser': { // refresh clientEdit data 
                            dispatch({
                                type: 'GET_CLIENT_DATA',
                                payload: {
                                    clientId: clientId
                                }
                            });
                            break;
                        }
                    }
                    done();
                })
                .catch(err => {
                    dispatch({
                        type: 'CLIENT_ADDUSER_SAVE_FAILED',
                        payload: err,
                        error: true
                    });
                    done();
                }) 
    }
});



export { logicClientListGet, 
         logicClientGet,
         logicClientEditSave,
         logicClientGetUsersForAdd,
         logicClientSetClientUser }