// Chat logic. We use separate file to reduce complexity 
import { createLogic } from 'redux-logic';
import { escapeHtml } from '../core/core_functions';
import { errorIdentifiers, 
         codeOperationDescriptions,
         mapTypeToIdentifiers,
         mapTypeToContext } from '../constants/error_constants';
import axios from 'axios';


/* process following events here: 

    CHAT_INFO_DATA_GET_FAILED - error of getting chat information 
    CHAT_MESSAGE_SAVE_FAILED - error of saving new chat message 
    DATA_CHATLIST_GET_FAILED - error of getting list of chats 
    CLIENT_DATA_GET_FAILED - error of getting client data (for editing)
    PAYREQ_DATA_GET_FAILED - error of getting payment requisit
    PAYREQ_DATA_SAVE_FAILED - error of saving payment requisit 
    CLIENT_DATA_SAVE_FAILED - error of saving clients data
    CLIENT_USERS_AVAILABLE_GET_FAILED - error of getting available users
    CLIENT_ADDUSER_SAVE_FAILED - error of saving user binding to client 
    ORDERS_LIST_DATA_GET_FAILED - error of getting orders list  
    ORDER_EDIT_PICKER_DATA_GET_FAILED - error of getting accounts for order
    ORDER_EDIT_PICKER_LO_DATA_GET_FAILED - error of getting linked orders info for order 
    ORDER_DATA_GET_FAILED - error of getting orders data 
    ORDER_DATA_SAVE_FAILED - error of saving order data (Body)
    ORDER_PROCESS_ACTION_FAILED - error of order action process 
    OPERATIONS_LIST_DATA_GET_FAILED - error of retrieving operations list 
    OPERATIONS_PICKER_DATA_ACCS_GET_FAILED - error of retrieving operations picker (Accounts)
    OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED - error of retrieving operations picker (Payment requisits)
    OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED - error of retrieving operations picker (Orders)
    OPERATION_DATA_GET_FAILED - error of operation retrieving
    OPERATION_DATA_SAVE_FAILED - error of operation saving 
    STATEMENT_PICKER_DATA_ACCS_GET_FAILED - error of getting accounts list for statement 
    STATEMENT_DATA_GET_FAILED - error of getting statement data 
    REPORTS_DATA_GET_FAILED - error of reports get 
    REPORT_FILE_GET_FAILED - error of getting report file 
    USERS_LIST_DATA_GET_FAILED - error of getting users list 
    USERS_DICTIONARY_DATA_GET_FAILED - error of getting users dictionaries
    'USER_DATA_GET_FAILED' - error of getting user data (editUser),
    'USER_DATA_SAVE_FAILED' - error of saving user data (editUser)
 
*/

function getConcealTime(concealDelay) {
    /* Function for getting unix board time for errors hiding 
       compare this time with current time and conceal record  
       pass milliseconds here */ 
    return Date.now() + concealDelay 
}


const generateWindowMsg = function(type, status, message, response) {
    const operationDesc = codeOperationDescriptions.hasOwnProperty(type) ? 
                          codeOperationDescriptions[type] : 'Не определено';

    const responseDetail = response && response.data && response.data.detail;
    let detailMessage = ''
    if (responseDetail) {
        detailMessage = ', детализация: ' + escapeHtml(responseDetail);
    }
    if (response == undefined) {
        detailMessage = ', детализация: ' + escapeHtml(message);
    }

    switch (status) {
        case 400: {
            return ('Неверные данные в запросе, обратитесь к системному администратору (400). Операция: ' + operationDesc);
        }
        case 403: {
            return ('Отказано в доступе (403). Операция: ' + operationDesc);
        }
        case 404: {
            return ('Требуемый ресурс не найден (404). Операция: ' + operationDesc);
        }
        case 500: {
            return ('Внутренняя ошибка сервера, обратитесь к системному администратору (500). Операция: ' 
                    + operationDesc
                    + detailMessage);
        }
        default: {
            return ('Ошибка при выполнении операции, ' + operationDesc + detailMessage);
        }
    }
}

const getErrorIdentfierType = function(type) {
    return  mapTypeToIdentifiers.hasOwnProperty(type) ? mapTypeToIdentifiers[type] : errorIdentifiers.commonError;
};

const getContext = function(type) {
    return  mapTypeToContext.hasOwnProperty(type) ? mapTypeToContext[type] : '(' + type + ')';
};

const messageCategories = {
    error: errorIdentifiers.messageError,
    alert: errorIdentifiers.messageAlert,
    info: errorIdentifiers.messageInfo,
    success: errorIdentifiers.messageSuccess
}

var MessageId = 0; 

var getErrorMessageId = function() {
    return (++MessageId).toString();
};

const logicDataProcessingErrors = createLogic({
    type: ['CHAT_INFO_DATA_GET_FAILED', 
           'CHAT_MESSAGE_SAVE_FAILED', 
           'DATA_CHATLIST_GET_FAILED',
           'CLIENT_DATA_GET_FAILED',
           'PAYREQ_DATA_GET_FAILED',
           'PAYREQ_DATA_SAVE_FAILED',
           'CLIENT_DATA_SAVE_FAILED',
           'CLIENT_USERS_AVAILABLE_GET_FAILED',
           'CLIENT_ADDUSER_SAVE_FAILED',
           'CLIENTS_LIST_DATA_GET_FAILED',
           'ORDERS_LIST_DATA_GET_FAILED',
           'ORDER_EDIT_PICKER_DATA_ACCS_GET_FAILED',
           'ORDER_DATA_GET_FAILED',
           'ORDER_DATA_SAVE_FAILED',
           'ORDER_EDIT_PICKER_LO_DATA_GET_FAILED',
           'ORDER_PROCESS_ACTION_FAILED',
           'OPERATIONS_LIST_DATA_GET_FAILED',
           'OPERATIONS_PICKER_DATA_ACCS_GET_FAILED',
           'OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED',
           'OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED',
           'OPERATION_DATA_GET_FAILED',
           'OPERATION_DATA_SAVE_FAILED',
           'STATEMENT_PICKER_DATA_ACCS_GET_FAILED',
           'STATEMENT_DATA_GET_FAILED',
           'REPORTS_DATA_GET_FAILED',
           'REPORT_FILE_GET_FAILED',
           'USERS_LIST_DATA_GET_FAILED',
           'USERS_DICTIONARY_DATA_GET_FAILED',
           'USER_DATA_GET_FAILED',
           'USER_DATA_SAVE_FAILED'
           ],
    latest: false,
    processOptions: { 
           dispatchReturn: false
        },  
    process({ action }, dispatch, done) {
    // determine type of event to process 
        const { type, payload } = action;
        
        switch (type) {
            case 'CHAT_INFO_DATA_GET_FAILED':
            case 'DATA_CHATLIST_GET_FAILED':            
            case 'CHAT_MESSAGE_SAVE_FAILED': 
            case 'CLIENT_DATA_GET_FAILED': 
            case 'PAYREQ_DATA_GET_FAILED':
            case 'PAYREQ_DATA_SAVE_FAILED':
            case 'CLIENT_DATA_SAVE_FAILED':
            case 'CLIENT_USERS_AVAILABLE_GET_FAILED':
            case 'CLIENT_ADDUSER_SAVE_FAILED':
            case 'CLIENTS_LIST_DATA_GET_FAILED':
            case 'ORDERS_LIST_DATA_GET_FAILED':
            case 'ORDER_EDIT_PICKER_DATA_ACCS_GET_FAILED':
            case 'ORDER_DATA_GET_FAILED':
            case 'ORDER_DATA_SAVE_FAILED':
            case 'ORDER_EDIT_PICKER_LO_DATA_GET_FAILED':
            case 'ORDER_PROCESS_ACTION_FAILED':
            case 'OPERATIONS_LIST_DATA_GET_FAILED':
            case 'OPERATIONS_PICKER_DATA_ACCS_GET_FAILED':
            case 'OPERATIONS_PICKER_DATA_PAYREQS_GET_FAILED':
            case 'OPERATIONS_PICKER_DATA_ORDERS_GET_FAILED':
            case 'OPERATION_DATA_GET_FAILED':
            case 'OPERATION_DATA_SAVE_FAILED': 
            case 'STATEMENT_PICKER_DATA_ACCS_GET_FAILED':
            case 'STATEMENT_DATA_GET_FAILED':
            case 'REPORTS_DATA_GET_FAILED':
            case 'REPORT_FILE_GET_FAILED':
            case 'USERS_LIST_DATA_GET_FAILED':
            case 'USERS_DICTIONARY_DATA_GET_FAILED':
            case 'USER_DATA_GET_FAILED':
            case 'USER_DATA_SAVE_FAILED':
            {
                const { response, message, request } = payload;
                const status = response ? response.status : 'Информация отсутствует';
                const context =  getContext(type);
                const errorType = getErrorIdentfierType(type);
                const messageId = errorType.oneInstance ? errorType.guid : errorType.guid + '-' + getErrorMessageId(),
                      concealDelay = errorType.concealDelay;
                const windowMsg = generateWindowMsg(type, status, message, response);
                dispatch( { 
                            type: 'ERROR_MESSAGE_ADD', 
                            payload: {
                                       messageId: messageId, 
                                       errorMessage: windowMsg, 
                                       errorCategory: errorType.categoryId,
                                       concealTime: concealDelay ? getConcealTime(concealDelay) : null
                            }
                          } 
                        );
                done();
                console.log('axios: return status = ' + status + 
                            ', context = ' + context + 
                            ', message: ' + message + 
                            ', URI: ' + (request ? request.responseURL : ''));
                break;
            }
            default: {
                done();
            }
        }
    }
});

const logicDataProcessingMessaged = createLogic({
    type: ['PROCESS_SYSTEM_STATUS'],
    latest: true,
    processOptions: { 
           dispatchReturn: false
        },  
    process({ action }, dispatch, done) {
        const { type } = action;
        switch (type) {
            case 'PROCESS_SYSTEM_STATUS':
            {   
                const uniqueId = (+ new Date()).toString();
                return axios.get('/api/v2/status/get?timestamp=' + uniqueId,
                        {})
                       .then((responseData) => {
                            if (responseData.data) {
                                const { systemStatus, needLogin, messages } = responseData.data;
                                if (systemStatus) {
                                    dispatch({
                                        type: 'SYSTEM_STATUS',
                                        payload: systemStatus
                                    });
                                    if (systemStatus == 'work' && needLogin) {
                                        console.log('User need to perform login');
                                    }
                                } 
                                
                                messages && messages.forEach(value => {
                                    
                                    const errorCategory = messageCategories[value.level],
                                          message = value.message,
                                          concealDelay = value.concealDelay ? 
                                                         value.concealDelay : 
                                                         errorCategory.concealDelay;
                                    const messageId = errorCategory.oneInstance ? 
                                                      errorCategory.guid : 
                                                      errorCategory.guid + '-' + getErrorMessageId();
                
                                           
                                    dispatch({
                                        type: 'ERROR_MESSAGE_ADD', 
                                        payload: {
                                            messageId: messageId, 
                                            errorMessage: message, 
                                            errorCategory: errorCategory.categoryId,
                                            concealTime: concealDelay ? getConcealTime(concealDelay) : null
                                        }
                                    });
                                })
                                done();
                            }
                        }) 
                        .catch(err => {
                             console.log(err);
                             done() 
                        });                
            }
        }
        done();
    }
});



export { logicDataProcessingErrors, 
         logicDataProcessingMessaged };