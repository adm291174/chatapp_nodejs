import { createLogic } from 'redux-logic';
import { Map, List } from 'immutable';

import { ordersListStatusQueryParams } from '../constants/logic_constants';
import { getDateStrForQuery } from '../core/core_functions';
import { emptyOrderBodyData } from '../constants/order_constants';

import { allTypes } from '../constants/operations_constants';
import { fillEmptyOperationData } from '../core/operations_functions';
import { fillEmptyUserEditData } from '../core/user_functions';

import { reportDefs } from '../report/report_defs';


/* context change logic */ 

const contextActivateLogic = createLogic({
    type: 'APP_CONTEXT_SET',
    latest: false,
    processOptions: { 
        dispatchReturn: false
    },
    process({ getState, action }, dispatch, done) {
        // get current context 
        const appContext = getState().get('root').data.get('appContext'), 
              newWindow = action.payload.window;
        
        const { chatClose } = action.payload;

        if (newWindow != 'chat') {
            // application context changed - new window 
            const windowContext = appContext.get('windowContext').get(newWindow);

            if (!chatClose) { // after closing chat we don't refresh context window 
                switch (newWindow) {
                    case 'clients': { // client list activated 
                        const filterText = windowContext.get('windowFilter')
                                                        .get('filterValues')
                                                        .get('clients-filter__name');
                        dispatch({
                            type: 'GET_CLIENTS_LIST_DATA',
                            payload: {
                                filterText: filterText
                            }
                        });
                        break;     
                    }
                    case 'clientEdit': { // client list activated 
                        const clientId = windowContext.get('windowContent').get('clientId');
                        if (clientId) {
                            dispatch({
                                type: 'GET_CLIENT_DATA',
                                payload: {
                                    clientId: clientId
                                }
                            });
                        }
                        break;
                    }       
                    case 'paymentRequsitEdit': { // reuqisit edit form activated 
                        const requisitId = windowContext.get('windowContent').get('requisitId');
                        if (requisitId) {
                            dispatch({
                                type: 'GET_PAYREQ_DATA',
                                payload: {
                                    requisitId: requisitId
                                }
                            });
                        }
                        break;
                    }       
                    case 'ordersList': { // 
                        const filterValues = windowContext.get('windowFilter').get('filterValues');

                        if (filterValues) {
                            const startDate = filterValues.get('orders-filter__start-date'),
                                  endDate = filterValues.get('orders-filter__end-date'),
                                  status = filterValues.get('orders-filter__status');

                            const ordersStatusForQuery = ordersListStatusQueryParams[status];

                            dispatch({
                                type: 'GET_ORDERS_LIST_DATA',
                                payload: {
                                    'startDate': getDateStrForQuery(startDate),
                                    'endDate': getDateStrForQuery(endDate),
                                    'status': ordersStatusForQuery ? ordersStatusForQuery : []
                                }
                            });
                        }
                        break;
                    }
                    case 'orderEdit': {
                        const windowContent = windowContext.get('windowContent');
                        const orderId = windowContent.get('orderId'),
                              orderIsEdited = windowContent.get('orderIsEdited');
                        
                        if (orderId) {
                            // clear orders visual data and request data from database 
                            dispatch({
                                type: 'ORDER_DATA',
                                payload: [
                                    {
                                        orderDataName: 'orderDataId',
                                        orderDataValue: null
                                    },
                                    {
                                        orderDataName: 'orderBodyData',
                                        orderDataValue: emptyOrderBodyData
                                    },
                                    {
                                        orderDataName: 'orderLinkedOrders',
                                        orderDataValue: List()
                                    },
                                    {
                                        orderDataName: 'orderLinkedOperations',
                                        orderDataValue: List()
                                    },
                                    {
                                        orderDataName: 'orderLinkedOperations',
                                        orderActions: List()
                                    }
                                ]
                            });
                            dispatch({
                                type: 'GET_ORDER_DATA',
                                payload: {
                                    orderId: orderId,
                                    getOrderBodyData: true,
                                    getOrderAdditionalData: true,
                                    getOrderActions: !orderIsEdited
                                }
                            })                            
                        }
                        else {
                            // clear orders visual data, and merge it with payload data
                            const orderBodyData = emptyOrderBodyData
                                    .merge(action.payload.orderBodyData);
                            dispatch({
                                type: 'ORDER_DATA',
                                payload: [
                                    {
                                        orderDataName: 'orderDataId',
                                        orderDataValue: null
                                    },
                                    {
                                        orderDataName: 'orderBodyData',
                                        orderDataValue: orderBodyData
                                    },
                                    {
                                        orderDataName: 'orderLinkedOrders',
                                        orderDataValue: List()
                                    },
                                    {
                                        orderDataName: 'orderLinkedOperations',
                                        orderDataValue: List()
                                    },
                                    {
                                        orderDataName: 'orderLinkedOperations',
                                        orderActions: List()
                                    },

                                ]
                            });
                            
                        }
                        dispatch({
                            type: 'PROCESS_SYSTEM_STATUS',
                            payload: null
                        })
                        break;
                    }
                    case 'operationsList': { 
                        const filterValues = windowContext.get('windowFilter').get('filterValues');

                        if (filterValues) {
                            const startDate = filterValues.get('operations-filter__start-date'),
                                  endDate = filterValues.get('operations-filter__end-date'),
                                  operationType = filterValues.get('operations-filter__type');

                            dispatch({
                                type: 'GET_OPERATIONS_LIST_DATA',
                                payload: {
                                    'startDate': getDateStrForQuery(startDate),
                                    'endDate': getDateStrForQuery(endDate),
                                    'operationType': ( operationType == allTypes
                                                       ? null : operationType )
                                }
                            });
                        }
                        break;
                    }
                    case 'operationEdit': {
                        const windowContent = windowContext.get('windowContent');
                        const operationId = windowContent.get('operationId')
                              // operationIsEdited = windowContent.get('operationIsEdited');

                        if (operationId) {
                            // clear operations visual data and request data from database 
                            dispatch({
                                type: 'OPERATION_DATA',
                                payload: fillEmptyOperationData()
                            });
                            dispatch({
                                type: 'GET_OPERATION_DATA',
                                payload: {
                                    operationId: operationId                                    
                                }
                            })                            
                        }
                        else {
                            // clear operations visual data, and merge it with payload data

                            const { operationData } = action.payload;

                            dispatch({
                                type: 'OPERATION_DATA',
                                payload: fillEmptyOperationData().merge(
                                    (operationData && !operationData.equals(Map())) ? operationData : Map())
                            });
                        }
                        break;
                    }
                    case 'usersList': {
                        // get users list data 
                        dispatch ({
                            type: 'GET_USERS_LIST_DATA',
                            payload: {}
                        });
                        break;
                    }
                    case 'userEdit': {
                        const windowContent = windowContext.get('windowContent');
                        const userId = windowContent.get('userId')

                        if (userId) {
                            dispatch({
                                type: 'USER_DATA_MASS_CHANGE',
                                payload: fillEmptyUserEditData()
                            });
                            dispatch({
                                type: 'WINDOW_TITLE_SET',
                                payload: {
                                    windowName: 'userEdit',
                                    windowTitle: 'Редактирование пользователя'
                                }
                            });
                            dispatch({
                                type: 'GET_USER_DATA',
                                payload: {
                                    userId: userId
                                }
                            })                            
                        }
                        else {
                            const { userData } = action.payload,
                                  newUserData = userData ? 
                                                userData.merge({ userDataId: null }) :
                                                Map({ userDataId: null });
                             
                            dispatch({
                                type: 'WINDOW_TITLE_SET',
                                payload: {
                                    windowName: 'userEdit',
                                    windowTitle: 'Новый пользователь'
                                }
                            });

                            dispatch({
                                type: 'USER_DATA_MASS_CHANGE',
                                payload: fillEmptyUserEditData().merge(newUserData)
                            });
                        }
                        break;
                    }
                }
            }
            else {
                /* set window title after chat close */ 
                switch (newWindow) {
                    case 'operationEdit': {
                        dispatch({
                            type: 'OPERATION_EDIT_TITLE_SET',
                            payload: Map()
                        });
                        break;
                    }   
                    case 'orderEdit': { 
                        dispatch({
                            type: 'ORDER_EDIT_TITLE_SET',
                            payload: []
                        })
                        break;
                    }
                }
            }
            switch (newWindow) {
                /* unconditional processing */
                case 'report': {
                    const windowContent = windowContext.get('windowContent'),
                          reportId = windowContent.get('reportId'),
                          reportName = reportDefs(reportId).windowTitle;
                    
                    dispatch({
                        type: 'WINDOW_TITLE_SET',
                        payload: {
                            windowName: 'report',
                            windowTitle: reportName
                        }
                    })
                    break;
                }
            }
        }
        

        done();
    }
});

const contextProcessChatClose = createLogic({
    type: 'CHAT_WINDOW_CLOSE',
    latest: false,
    process({ getState }, dispatch, done) {
        // get current context 
        const appContext = getState().get('root').data.get('appContext'), 
              windowName = appContext.get('activeWindow'),
              chatState = appContext.get('chatContext');
        
        if (chatState.get('isActive') && windowName) {
            dispatch ({
                type: 'APP_CONTEXT_SET',
                payload: {
                    window: windowName,
                    context: Map(),
                    chatClose: true,
                }
            })
        }
        done();
    }
});

const contextWindowTitleSet = createLogic({
    type: ['ORDER_DATA', 
           'ORDER_EDIT_TITLE_SET', 
           'OPERATION_DATA', 
           'OPERATION_EDIT_TITLE_SET'],
    latest: false,
    process({ getState, action }, dispatch, done) {
        switch (action.type) {
            case 'ORDER_DATA':
            case 'ORDER_EDIT_TITLE_SET': {
                const appContext = getState().get('root').data.get('appContext'), 
                      windowContext = appContext.get('windowContext').get('orderEdit'),
                      windowContent = windowContext.get('windowContent'),
                      orderId = windowContent.get('orderId');

                const orderDataRecords = getState().get('orders').data.get('orderData'),
                      orderDataId = orderDataRecords.get('orderDataId'),
                      orderBodyData = orderDataRecords.get('orderBodyData');

                if (orderBodyData) {
                    let windowTitle = 'Ордер';  // set empty title if we fetching data 
                    if (!(orderId && !orderDataId)) {
                        // set window title depends on order number 
                        const orderNumber = orderBodyData.get('orderNumber');
                        windowTitle = (orderNumber.length) ? 'Ордер № ' + orderNumber : 'Новый ордер';
                    } 
                    dispatch(
                        {
                            type: 'WINDOW_TITLE_SET',
                            payload: {
                                windowName: 'orderEdit',
                                windowTitle: windowTitle
                                    
                            }
                        }
                    );
                }
                break;
            }
            case 'OPERATION_DATA': 
            case 'OPERATION_EDIT_TITLE_SET': {
                const appContext = getState().get('root').data.get('appContext'), 
                      windowContext = appContext.get('windowContext').get('operationEdit'),
                      windowContent = windowContext.get('windowContent'),
                      operationId = windowContent.get('operationId');

                const operationDataRecords = getState().get('operations').data.get('operationData'), 
                      //action.payload,
                      operationDataId = operationDataRecords.get('operationDataId');

                let windowTitle = 'Операция';  // set empty title if we fetching data 
                if (operationId) {
                    if (operationDataId) {
                        // set window title depends on order number 
                        const operationNumber = operationDataRecords.get('operationNumber');
                        windowTitle = 'Операция № ' + operationNumber;
                    } 
                }
                else windowTitle = 'Новая операция';
                dispatch(
                    {
                        type: 'WINDOW_TITLE_SET',
                        payload: {
                            windowName: 'operationEdit',
                            windowTitle: windowTitle
                                
                        }
                    }
                );    
                break;
            }
            
        }
        done();
    }
});


export { contextActivateLogic, contextProcessChatClose, contextWindowTitleSet }; 