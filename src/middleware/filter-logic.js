/* Coherence filter change logic 
   Process 'FILTER_DATA_CHANGED' action 
       and dispatch 'FILTER_MASS_SET' action with new filter values 
*/ 

import { createLogic } from 'redux-logic';
import { getCurrentDate } from '../core/core_functions';
import { Map } from 'immutable';


const selectValues = function (periodDescription) {
    /* function for determing start and end dates 
       depending on period description 
    */ 
    const today = getCurrentDate();
    switch (periodDescription) {
        case 'today': {
            return [ today, today ];
        }
        case 'this_week': {
            const dayOfWeek = today.getDay();
            const shiftDays = dayOfWeek ? dayOfWeek - 1 : 7;
            return [ new Date((+ today) - 3600000 * 24 * shiftDays),  today ]
        }
        case 'this_month': {
            return [
                new Date(today.getFullYear(),today.getMonth(), 1, 0, 0, 0), today
            ]
        }        
    }
} 

const logicProcessMassFilter = createLogic({
    type: 'FILTER_DATA_CHANGED',
    latest: false,
    processOptions: { 
           dispatchReturn: false
        },  
    process({ action }, dispatch, done) {
    // determine type of event to process 
        const { windowName, filterElementName, filterData } = action.payload;

        if (windowName == 'ordersList' || windowName == 'operationsList') {
            if (filterElementName == 'orders-filter__period' || 
                filterElementName == 'operations-filter__period') {
                
                switch (filterData) {
                    case 'today': 
                    case 'this_week': 
                    case 'this_month': {
                        const [ startDate, endDate ] = selectValues(filterData);
                        let filterValues = null;
                        if (filterElementName == 'orders-filter__period') {
                            filterValues = Map({
                                   'orders-filter__start-date': startDate,
                                   'orders-filter__end-date': endDate
                            });
                        }
                        else {
                            if (filterElementName == 'operations-filter__period') {
                                filterValues = Map({
                                    'operations-filter__start-date': startDate,
                                    'operations-filter__end-date': endDate
                                });
                            }
                        }
                        dispatch( { 
                            type: 'FILTER_MASS_SET', 
                            payload: {
                                windowName: windowName,
                                filterValues: filterValues
                            }
                        } );
                    }
                }  
            }
        }
        done();
    }
});

export { logicProcessMassFilter }