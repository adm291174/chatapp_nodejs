import { createLogic } from 'redux-logic';
import axios from 'axios';
import { getConfig, putConfig, postConfig } from '../constants/logic_constants';
import { Map, List } from 'immutable';


const logicUsersListGet = createLogic({
    type: 'GET_USERS_LIST_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'USERS_LIST_DATA',
        failType: 'USERS_LIST_DATA_GET_FAILED'
    },
    process() {
        // determine what kind of action process 
        return axios.get('/api/v2/users/get',
                         {},
                         getConfig)            // return axios record set 
                .then((result) => {
                    return {  // return data for processWindowDataChanged processing 
                        windowName: 'usersList', 
                        fieldName: 'usersListData',
                        fieldValue: transformUsersList(result.data)
                    }    
                });
    }
});


const logicUsersDictionaries = createLogic({
    type: 'GET_USERS_DICTIONARY_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'USERS_DICTIONARY_DATA',
        failType: 'USERS_DICTIONARY_DATA_GET_FAILED'
    },
    process() {
        // determine what kind of action process 
        return axios.get('/api/v2/users/get_params',
                         {},
                         getConfig)            // return axios record set 
                .then((result) => {
                    const { chatGroups, userGroups } = result.data;
                    return {
                        chatGroups: chatGroups ? chatGroups : [],
                        userGroups: userGroups ? userGroups : []
                    };
                });
    }
});


const logicUsersGetUser = createLogic({
    type: 'GET_USER_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'USER_DATA',
        failType: 'USER_DATA_GET_FAILED'
    },
    process({ action }) {
        // determine what kind of action process 
        const { userId } = action.payload;
        return axios.put('/api/v2/users/get_user',
                         { userId },
                         putConfig)            // return axios record set 
                .then((result) => {
                    return result.data;
                });
    }
});


const logicUsersSaveUser = createLogic({
    type: 'SAVE_USER_DATA',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'APP_CONTEXT_SET',
        failType: 'USER_DATA_SAVE_FAILED'
    },
    process({ action }) {
        // determine what kind of action process 
        const payload = action.payload;
        return axios.post('/api/v2/users/save_user',
                         payload,
                         postConfig) // return axios record set 
                .then((result) => {
                    return {
                        window: 'userEdit',
                        context: Map({
                            windowContent: Map({
                                userId: result.data.userId
                            })
                        })
                    };
                });
    }
});


const transformUsersList = function(usersList) {
    return List(usersList.map( userRecord => {
        return Map({
            userId: userRecord.userId,
            login: userRecord.login,
            userName: userRecord.userName,
            isActive: userRecord.isActive
        })
    }));
}

export { logicUsersListGet, 
         logicUsersDictionaries, 
         logicUsersGetUser,
         logicUsersSaveUser };