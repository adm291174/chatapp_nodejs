import { createLogic } from 'redux-logic';

const logicProcessRefreshStatus = createLogic({
    type: ['GET_REPORTS_DATA',
           'REPORTS_DATA',
           'REPORTS_DATA_GET_FAILED',
           'REPORT_FILE_GET_DONE',
           'REPORT_FILE_GET_FAILED',
           'GET_ORDERS_LIST_DATA',
           'ORDERS_LIST_DATA',
           'ORDERS_LIST_DATA_GET_FAILED',
           'GET_ORDER_DATA',
           'ORDER_DATA',
           'ORDER_DATA_GET_FAILED',
           'GET_CLIENTS_LIST_DATA',
           'CLIENTS_LIST_DATA',
           'CLIENTS_LIST_DATA_GET_FAILED',
           'GET_CLIENT_DATA',
           'CLIENT_USERS_DATA',
           'CLIENT_DATA_GET_FAILED',
           'CLIENT_DATA_SAVE',
           'CLIENT_DATA_SAVE_FAILED',
           'ORDER_PROCESS_ACTION',
           'ORDER_PROCESS_ACTION_FAILED',
           'GET_OPERATIONS_LIST_DATA',
           'OPERATIONS_LIST_DATA',
           'OPERATIONS_LIST_DATA_GET_FAILED',
           'GET_OPERATION_DATA',
           'OPERATION_DATA',
           'OPERATION_DATA_GET_FAILED', 
           'SAVE_OPERATION_DATA',
           'OPERATION_DATA_SAVE_FAILED', 
           'GET_STATEMENT_DATA',
           'STATEMENT_DATA_GET_FAILED',
           'WINDOW_DATA_MASS_CHANGED',
           'GET_PAYREQ_DATA',
           'PAYREQ_DATA_SAVE',
           'PAYREQ_DATA',
           'PAYREQ_DATA_GET_FAILED',
           'PAYREQ_DATA_SAVE_FAILED',
           'GET_CLIENT_USERS_FORADD',
           'CLIENT_USERS_AVAILABLE_DATA',
           'CLIENT_USERS_AVAILABLE_GET_FAILED',
           'CLIENT_ADDUSER_SAVE',
           'CLIENT_ADDUSER_SAVE_FAILED',
           'USER_DATA',
           'USER_DATA_GET_FAILED',
           'GET_USER_DATA',
           'SAVE_USER_DATA',
           'USER_DATA_SAVE_FAILED'
           ],
    latest: false,
    processOptions: { 
           dispatchReturn: false
        },  
    process({ action }, dispatch, done) {
    // determine type of event to process 
        const { type } = action;
        
        let isRefreshData = false; 

        switch (type) { 
            // Set refresh flag ON 
            case 'GET_REPORTS_DATA': 
            case 'GET_ORDERS_LIST_DATA':
            case 'GET_CLIENTS_LIST_DATA':
            case 'GET_CLIENT_DATA':
            case 'CLIENT_DATA_SAVE':
            case 'ORDER_PROCESS_ACTION':
            case 'GET_OPERATIONS_LIST_DATA':
            case 'GET_OPERATION_DATA':
            case 'SAVE_OPERATION_DATA':
            case 'GET_STATEMENT_DATA':
            case 'GET_PAYREQ_DATA':
            case 'PAYREQ_DATA_SAVE':
            case 'GET_CLIENT_USERS_FORADD':
            case 'CLIENT_ADDUSER_SAVE':
            case 'GET_USER_DATA':
            case 'SAVE_USER_DATA':           
            {
                isRefreshData = true;
                break;
            }
            // Set refresh flag OFF
            case 'GET_ORDER_DATA': {
                const { getOrderBodyData, 
                        getOrderAdditionalData } = action.payload;
                
                // refresh only if not only getOrderActions passed 
                isRefreshData = getOrderBodyData && getOrderAdditionalData; 
                break;
            }
            
            case 'REPORTS_DATA':
            case 'REPORT_FILE_GET_DONE':
            case 'REPORTS_DATA_GET_FAILED': 
            case 'ORDERS_LIST_DATA': 
            case 'ORDERS_LIST_DATA_GET_FAILED':
            case 'ORDER_DATA':
            case 'ORDER_DATA_GET_FAILED':
            case 'CLIENTS_LIST_DATA':
            case 'CLIENTS_LIST_DATA_GET_FAILED': 
            case 'CLIENT_USERS_DATA':
            case 'CLIENT_DATA_GET_FAILED':
            case 'CLIENT_DATA_SAVE_FAILED':
            case 'ORDER_PROCESS_ACTION_FAILED':
            case 'OPERATIONS_LIST_DATA':
            case 'OPERATIONS_LIST_DATA_GET_FAILED':
            case 'OPERATION_DATA':
            case 'OPERATION_DATA_GET_FAILED':
            case 'OPERATION_DATA_SAVE_FAILED':
            case 'STATEMENT_DATA_GET_FAILED':
            case 'WINDOW_DATA_MASS_CHANGED':
            case 'PAYREQ_DATA':
            case 'PAYREQ_DATA_GET_FAILED':
            case 'PAYREQ_DATA_SAVE_FAILED':
            case 'CLIENT_ADDUSER_SAVE_FAILED':
            case 'CLIENT_USERS_AVAILABLE_DATA':
            case 'CLIENT_USERS_AVAILABLE_GET_FAILED':
            case 'REPORT_FILE_GET_FAILED':
            case 'USER_DATA':
            case 'USER_DATA_GET_FAILED':
            case 'USER_DATA_SAVE_FAILED': 
            { 
                isRefreshData = false;
                break;
            }
        }
        dispatch( { 
            type: 'SET_REFRESH_FLAG', 
            payload: {
                isRefreshing: isRefreshData
            }
        });
        done();
    }
});

export { logicProcessRefreshStatus }