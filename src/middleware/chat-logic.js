// Chat logic. We use separate file to reduce complexity 
import { createLogic } from 'redux-logic';
import axios from 'axios';
import { Map, List } from 'immutable';
import { getMessageElementId, escapeHtml, getDateFromJSON } from '../core/core_functions';
import { putConfig, postHtmlConfig, bunchMessagesCount } from '../constants/logic_constants';



/* We process different types of updates 
   
   type of updates is specified in <payload> attribute <action>

   payload values:
       chatHistoryId: id of chat history, 
       action: refresh, load_previous
       queryData : if set, use it in query 

   return both attributes to state:
       chatInfo 
       messageList - updated on every 

*/

const logicChatInfoGet = createLogic({
    type: 'CHAT_INFO_DATA_GET',
    latest: true,
    processOptions: { 
        dispatchReturn: true,
        successType: 'DATA_СHAT_INFO',
        failType: 'CHAT_INFO_DATA_GET_FAILED'
    },
    process({ getState, action }) {
        // determine what kind of action process 
        const { chatHistoryId, actionKind, queryData } = action.payload;
        // const lastUpdateTime = queryData.last_time;
        if (actionKind == 'refresh') {
            return axios.put('/api/messages/info',
                    queryData, 
                    putConfig)            // return axios record set 
                .then(processMessagesInfo)  // return chatInfo, newIdentfiers 
                .then((chatInfo) => {
                    const messagesCount = chatInfo.get('totalMessages');
                    const chatStateData = getState().get('chat').data.get(chatHistoryId);
                    
                    // check whether we have data in messageList, if nothing there - get bunch of last messages 
                    if (messagesCount && (!Map.isMap(chatStateData) || !(chatStateData.get('messageList').size > 0))) {
                        
                        const queryData = {
                            'chat_history_id': chatHistoryId,
                            'command': 'before',
                            'count': bunchMessagesCount,
                            'items': [ chatInfo.get('maxIdentifier') + 1 ]
                            };

                        return (Promise.all([ chatInfo, axios.put('/api/messages/content',
                                                queryData,
                                                putConfig)] ))
                    }
                    if (Map.isMap(chatStateData)) {
                        const messageList = chatStateData.get('messageList');    
                        const maxId = (messageList.size > 0) ? messageList.get(messageList.size-1).get('messageId') : 0;
                        console.log('get 2, maxId:', maxId);
                        const dbMaxId = chatInfo.get('maxIdentifier');
                        if (maxId < dbMaxId) {
                            const queryData = {
                                'chat_history_id': chatHistoryId,
                                'command': 'after',
                                'count': bunchMessagesCount,
                                'items': [ maxId ]
                                };

                            return (Promise.all([ chatInfo, axios.put('/api/messages/content',
                                                    queryData,
                                                    putConfig)]))
                        }
                    }

                    return Promise.all([ chatInfo, { data: []} ])
                    /* // Check whether we have some new elements
                    if (newIdentfiers.length) {
                        const queryData = {
                            'chat_history_id': chatHistoryId,
                            'command': 'items',
                            'items': newIdentfiers
                            };

                        return (chatInfo, axios.put('/api/messages/contents',
                                                queryData,
                                                putConfig))
                    } */

                })
                .then(([chatInfo, { data }]) => {
                    const chatStateData = getState().get('chat').data.get(chatHistoryId);
                    const stateMessageList = chatStateData ? chatStateData.get('messageList') : List();
                    let newMessagesList = processMessagesList(chatInfo, stateMessageList, data);
                    const oldChatInfo = chatStateData ? chatStateData.get('chatInfo') : Map();
                    
                    return {  
                            chatHistoryId: chatHistoryId,
                            chatInfo: oldChatInfo.merge(chatInfo), 
                            messageList: newMessagesList,
                            lastQueryTime: chatInfo.get('actualTime')
                            }; 
                })
        }         
        if (actionKind == 'get_before') {
            // get minimal value of messageId 
            const chatStateData = getState().get('chat').data.get(chatHistoryId);
            const chatInfo = chatStateData.get('chatInfo');
            const minIdentifier = action.payload.queryData.minIdentifier;
            
            if (minIdentifier) {
                const queryData = {
                            'chat_history_id': chatHistoryId,
                            'command': 'before',
                            'count': bunchMessagesCount,
                            'items': [ minIdentifier ]
                      };

                return axios.put('/api/messages/content',
                                  queryData,
                                  putConfig)
                            .then( ( {data} ) => {
                                const chatStateData = getState().get('chat').data.get(chatHistoryId);
                                const stateMessageList = chatStateData ? chatStateData.get('messageList') : List();
                                let newMessagesList = processMessagesList(chatInfo, stateMessageList, data);
                                
                                return {  
                                    chatHistoryId: chatHistoryId,
                                    messageList: newMessagesList                                
                                }; 
                            })
            }     
        }
    }   
});


var processMessagesInfo = function (responseData) {
    
    const data = responseData.data;
    const chatInfo = Map({
        chatHistoryId: +data.chathistory_id,
        minIdentifier: +data.min_identifier,
        maxIdentifier: +data.max_identifier,
        totalMessages: +data.total_messages,
        myUserId: +data.my_user_id,
        chatUsers: ((userIds, userNames) => {
            let result = Map();
            for (let i=0; i < userIds.length; i++) {
               result = result.set(userIds[i], userNames[i]);
            }   
            return result;
            })(data.chat_user_id, data.chat_user_name),
        actualTime: getDateFromJSON(data.actual_time),
        lastMessageTime: getDateFromJSON(data.last_message_time),
        lastViewTime: getDateFromJSON(data.last_view_time)        
    })
    //const newIdentfiers = data.new_identifiers;
    return chatInfo

}


var processMessagesList = function(chatInfo, stateMessageList, data) {
    
    const myUserId = chatInfo.get('myUserId');
    const chatUsers = chatInfo.get('chatUsers');
        
    const itemsList = List(
        data.map(item => Map({
            messageId: +item.id,
            chatHistoryId:+ item.chat_history_id,
            elementId: getMessageElementId(item.id),
            created: getDateFromJSON(item.created),
            modified: getDateFromJSON(item.modified),
            messageText: escapeHtml(item.message_text),
            messageFileId: item.message_file_id,
            messageFileName: escapeHtml(item.real_filename),
            messageAuthor: item.user_id,
            messageAuthorText: escapeHtml(chatUsers.get(item.user_id)),
            authorIsMe: (item.user_id == myUserId)
        }))
    );
    
    let _messageList = stateMessageList;

    itemsList.forEach(newValue => {
        let itemsIndex = _messageList.findIndex(mlValue => (mlValue.get('messageId')) == newValue.get('messageId'));
        if (itemsIndex != -1) {
           _messageList = _messageList.set(itemsIndex, newValue) 
        }
        else {
           _messageList = _messageList.push(newValue);
        }
    });

    return _messageList.sortBy(value => value.get('messageId'));

} 

const logicChatMessageSave = createLogic({
    type: 'CHAT_MESSAGE_SAVE',
    latest: false,
    processOptions: { 
        dispatchReturn: true
   },
   process({ action }) {
        /* also can get getState function as parameter */ 
        // determine what kind of action process 

        const { chatHistoryId, queryData } = action.payload;
        // const lastUpdateTime = queryData.last_time;
        return axios.post('/chat/' + chatHistoryId + '/',
                    queryData, 
                    postHtmlConfig)            // return axios record set 
                .then(data => ({type:  'CHAT_MESSAGE_SAVE_SUCCESS',
                                payload: data,
                                chatHistoryId: chatHistoryId,
                                error: false
                                }))

                .catch(err => ({type: 'CHAT_MESSAGE_SAVE_FAILED',
                                chatHistoryId: chatHistoryId,
                                payload: err,
                                error: true
                                }))


    }
});

export { logicChatInfoGet, logicChatMessageSave };