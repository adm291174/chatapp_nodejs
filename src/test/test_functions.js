var core_functions = require('../core/core_functions');

var floatPrettyPrintForInput = core_functions.floatPrettyPrintForInput,
    roundAlt = core_functions.roundAlt;


var assert = require('assert');

describe('Core function testing', function() {
  describe('floatPrettyPrintForInput', function() {
    it('should return correct values on function call', function() {
      assert.equal(floatPrettyPrintForInput(2.22), '2.22');
      assert.equal(floatPrettyPrintForInput(-2), '-2');
      assert.equal(floatPrettyPrintForInput(-2.52, 1), '-2.5');
      assert.equal(floatPrettyPrintForInput(129.25245, 3), '129.252');
    });
  }),
  describe('roundAlt', function() {
    it('should return correct values on function call', function() {
      assert.equal(roundAlt(2.225, 2), 2.23);
      assert.equal(roundAlt(122.78271, 4), 122.7827);
      assert.equal(roundAlt(0, 0), 0);
      assert.equal(roundAlt(-0.06, 1), -0.1);
      assert.equal(roundAlt(-10.562, 3), -10.562);
      assert.equal(roundAlt(1092881092.56239012, 5), 1092881092.56239);
    });
  });
});


