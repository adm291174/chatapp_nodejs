// import { createStore, applyMiddleware } from 'redux'
import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import { createLogicMiddleware } from 'redux-logic';
// custom logic here 
import usedLogic  from '../middleware/logic'

const deps = {};

const logicMiddleware = createLogicMiddleware(usedLogic, deps);

const middleware = applyMiddleware(
  logicMiddleware
);

const enhancer = middleware; 


// uncomment to log all middleware events 
 
/* logicMiddleware.monitor$.subscribe(
  x => console.log(x)
);  */ 

export default function configureStore(initialState) {
    const store = createStore(rootReducer, initialState, enhancer)
    
    if (module.hot) {
        module.hot.accept('../reducers', () => {
           const nextRootReducer = require('../reducers')
              store.replaceReducer(nextRootReducer)
        })
    } 
    
    return store
}