// Header component actions 

const actionClickChatListBtn = function(data) {
    // Expecting Map object with ready to paste data for <chatList> state
    return {
        type: 'CHAT_LIST_STATE_CHANGE',
        payload: data
    }
}

export { actionClickChatListBtn }; 