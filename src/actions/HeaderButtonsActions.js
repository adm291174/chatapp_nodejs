// Header Buttons actions 

const actionSubmitButton = function(data) {
    return {
        type: 'DOM_EVENT_BTN_CLICK',
        payload: {
            source: 'HEADER_COMPONENT',
            data: data
        }
    }
}

export { actionSubmitButton }; 