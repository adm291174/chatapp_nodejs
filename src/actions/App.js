// App actions 


export function actionAppSetUsersData(data) {
    // Stub function for testing user data delivery - remove it after API call creation 
    console.log('action DATA_USER_INFO creation');
    return {
        type: 'DATA_USER_INFO',
        payload: data
    }
}

export function actionGetChatListData() {
    console.log('action GET_CHATLIST_INFO_DATA creation');
    return {type: 'GET_CHATLIST_INFO_DATA',
                   payload: null}; 
}
            
