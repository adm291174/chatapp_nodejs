import { List, Map } from 'immutable';
import { ID_REPORT_BALANCE } from '../constants/report_constants';

function customReportTransform(reportId, reportData) {
    switch (reportId) {
        case ID_REPORT_BALANCE: {
            if (reportData) {
                const { active, passive, accounts } = reportData;
                const accountsData = accounts.map( baccValue => {
                    const { baccGroupId, baccGroupCode, 
                            baccGroupName, isActive, 
                            beginAmount, debitTurnover, 
                            creditTurnover, endAmount, accounts } = baccValue;
                    return Map({
                        baccGroupId: baccGroupId, 
                        baccGroupCode: baccGroupCode, 
                        baccGroupName: baccGroupName, 
                        isActive: isActive,
                        beginAmount: beginAmount,
                        debitTurnover: debitTurnover, 
                        creditTurnover: creditTurnover, 
                        endAmount: endAmount,
                        accounts: List(accounts.map( value => Map(value)))
                    }) 
                });
                return Map({
                    active: Map(active),
                    passive: Map(passive),
                    accounts: List(accountsData)
                });
            }
            else return null;
        }
    }
    return List(reportData);  // default behaviour 
}

export { customReportTransform }