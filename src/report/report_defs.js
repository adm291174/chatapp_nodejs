import { ID_REPORT_BALANCE } from '../constants/report_constants';
import { getCurrentDate, getDateStrForQuery } from '../core/core_functions';
import { Map } from 'immutable';



// Filter data function

function filterReportData(reportId, filterValues, reportDataWithId) {
    const dataReportId = reportDataWithId.get('reportId'),
          reportData = reportDataWithId.get('reportData');
    if (dataReportId == reportId) {
        return Map({
            reportId: reportId,
            reportData: reportData
        });  // default behaviour  
    }
    else {
        return Map({
            reportId: reportId,
            reportData: undefined
        });  // reportId mismatch  
    }
}


// Report filter fields definition 
function reportDefs(reportId) {
    switch (reportId) {
        case ID_REPORT_BALANCE: {
            return {
                windowTitle: 'Баланс за период',
                filterFields: [
                    {
                        caption: 'Фильтр по дате',
                        type: 'BLOCK',
                        id: 'id-report_date-block',
                        fields: [
                            {
                                caption: 'Дата начала',
                                name: 'report-filter__start-date',
                                nextElementId: 'id-report_end-date',
                                prevElementId: 'id-report_export-excel',
                                id: 'id-report_start-date',
                                type: 'DATEPICKER'
                            },
                            {
                                caption: 'Дата окончания',
                                name: 'report-filter__end-date',
                                nextElementId: 'id-report_get-report',
                                prevElementId: 'id-report_start-date',
                                id: 'id-report_end-date',
                                type: 'DATEPICKER'
                            }
                        ]
                    }                    
                ],
                filterButtons: [
                    {
                        caption: 'Получить',
                        id: 'id-report_get-report',
                        type: 'BUTTON'
                    },
                    {
                        caption: 'Экспортировать в Excel',
                        id: 'id-report_export-excel',
                        type: 'BUTTON'
                    }
                ],
                paramsExtractFunc: (filterValues) => {
                    return {
                        startDate: getDateStrForQuery(filterValues.get('report-filter__start-date')),
                        endDate: getDateStrForQuery(filterValues.get('report-filter__end-date'))
                    }
                }
            }
        }
    }
    return {
        windowTitle: 'Ошибка',
        filterFields: [],
        filterButtons: [] 
    }
}

function filterInitialContent() {
    return Map({
        identifiedFilterValues: 
            Map().set(
                ID_REPORT_BALANCE, 
                Map({
                    filterValues: Map({
                    'report-filter__start-date': getCurrentDate(),
                    'report-filter__end-date': getCurrentDate()
                })
                }))
    })
}

export { filterReportData,
         reportDefs,
         filterInitialContent }