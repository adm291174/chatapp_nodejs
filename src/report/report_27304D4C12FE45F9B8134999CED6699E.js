import React from 'react';
import { floatPrettyPrint } from '../core/core_functions';
/* 
    Render function for reportId = 27304D4C12FE45F9B8134999CED6699E (ID_REPORT_BALANCE)
    Balance Report 

*/

const renderAccounts = function(accountsData) {
    let result = [];

    accountsData.forEach(baccData => {
        const baccGroupCode = baccData.get('baccGroupCode'),
              baccGroupName = baccData.get('baccGroupName'),
              isActive = baccData.get('isActive'),
              beginAmount = floatPrettyPrint(baccData.get('beginAmount'), ' ', false),
              debitTurnover = floatPrettyPrint(baccData.get('debitTurnover'), ' ', false),
              creditTurnover = floatPrettyPrint(baccData.get('creditTurnover'), ' ', false),
              endAmount = floatPrettyPrint(baccData.get('endAmount'), ' ', false),
              accounts = baccData.get('accounts');

        result.push(<tr className='report-table__tr_border' key={ baccGroupCode + '-header' }>
                       <td className='report-table__td_noborder'>{ baccGroupCode + ' ' + 
                                                                   ( isActive ? '(А)' : '(П)') }</td>
                       <td className='report-table__td_noborder'>{ baccGroupName }</td>
                       <td/>
                       <td/>
                       <td/>
                       <td/>
                    </tr>);
        accounts.forEach(value => {
            const accountId = value.get('accountId'),
                  accountNumber = value.get('accountNumber'),
                  accountName = value.get('accountName'),
                  beginAmount = floatPrettyPrint(value.get('beginAmount'), ' ', false),
                  debitTurnover = floatPrettyPrint(value.get('debitTurnover'), ' ', false),
                  creditTurnover = floatPrettyPrint(value.get('creditTurnover'), ' ', false),
                  endAmount = floatPrettyPrint(value.get('endAmount'), ' ', false);

            result.push(<tr key={ accountId }> 
                            <td className='report-table__td_border'>{ accountNumber }</td>
                            <td className='report-table__td_border'>{ accountName }</td>
                            <td className='report-table__td_border 
                                           report-table__td_align-right'>{ beginAmount }</td>
                            <td className='report-table__td_border 
                                           report-table__td_align-right'>{ debitTurnover }</td>
                            <td className='report-table__td_border 
                                           report-table__td_align-right'>{ creditTurnover }</td>
                            <td className='report-table__td_border 
                                           report-table__td_align-right'>{ endAmount }</td>
                        </tr>);
        });
        result.push(<tr key={ baccGroupCode + '-footer' }>
                        <td className='report-table__td_noborder'>{ 'Итого по ' + baccGroupCode }</td>
                        <td></td>
                        <td className='report-table__td_border 
                                       report-table__td_align-right'>{ beginAmount }</td>
                        <td className='report-table__td_border report-table__td_align-right'>{ debitTurnover }</td>
                        <td className='report-table__td_border report-table__td_align-right'>{ creditTurnover }</td>
                        <td className='report-table__td_border report-table__td_align-right'>{ endAmount}</td>
                    </tr>);
    })

    return result;
}

const renderTotals = function(title, totalInfo) {
    const beginAmount = floatPrettyPrint(totalInfo.get('beginAmount'), ' ', false),
          debitTurnover = floatPrettyPrint(totalInfo.get('debitTurnover'), ' ', false),
          creditTurnover = floatPrettyPrint(totalInfo.get('creditTurnover'), ' ', false),
          endAmount = floatPrettyPrint(totalInfo.get('endAmount'), ' ', false);

    return(<tr key={ title }>
                <td className='report-table__td_noborder'>{ title }</td>
                <td></td>
                <td className='report-table__td_border report-table__td_align-right'>{ beginAmount }</td>
                <td className='report-table__td_border report-table__td_align-right'>{ debitTurnover }</td>
                <td className='report-table__td_border report-table__td_align-right'>{ creditTurnover }</td>
                <td className='report-table__td_border report-table__td_align-right'>{ endAmount}</td>
            </tr>);
}

const renderBalanceReport = function(reportData) {
    const active = reportData.get('active'),
          passive = reportData.get('passive'),
          accounts = reportData.get('accounts');
    let result = [];
    result.push(renderAccounts(accounts));
    result.push(renderTotals('Итого по активу:', active));
    result.push(renderTotals('Итого по пассиву:', passive));
    return (<div className='report-table'>
                <table>
                    <thead>
                        <tr>
                            <td className='report-table__th_border'>Номер счета</td>
                            <td className='report-table__th_border'>Наименование счета</td>
                            <td className='report-table__th_border'>Остаток на начало</td>
                            <td className='report-table__th_border'>Оборот по дебету</td>
                            <td className='report-table__th_border'>Оборот по кредиту</td>
                            <td className='report-table__th_border'>Остаток</td>                                
                        </tr>
                    </thead>
                    <tbody>
                      { result }
                    </tbody>                    
                </table>
    </div>); 
}



export default renderBalanceReport;