import renderBalanceReport from './report_27304D4C12FE45F9B8134999CED6699E';
import { ID_REPORT_BALANCE } from '../constants/report_constants';

function renderReportBody(reportDataWithId) {
    const reportId = reportDataWithId.get('reportId'),
          reportData = reportDataWithId.get('reportData');

    switch (reportId) {
        case ID_REPORT_BALANCE:
            return renderBalanceReport(reportData);
    }
    return <div className='main-block__body'>Данный отчет не поддерживается</div>
}

export { renderReportBody }