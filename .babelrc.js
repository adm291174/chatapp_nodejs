// This file is for Babel v/7 only 

module.exports = function (api) {
  api.cache(true);
  const presets = ['@babel/preset-env', '@babel/preset-react'] // поддержка ES2015, ES7 и JSX
  const plugins = [
    'react-hot-loader/babel',
    '@babel/plugin-syntax-dynamic-import',    
    '@babel/transform-runtime',     
    [
      '@babel/plugin-proposal-class-properties', {'spec': true}
    ],
    [
      '@babel/plugin-transform-block-scoping', {
        'throwIfClosureRequired': true
      }
    ]
  ]

  const ignore =  ['./lib', './src/test/', ];
  const sourceMaps = true;

  // const plugins = ["transform-runtime"] // without react-hot-loader

  return {
    presets,
    plugins,
    ignore,
    sourceMaps
  };
}



/* 

// Old .babelrc values: 
{
    "presets": ["env", "react"], 
    "plugins": ["transform-runtime", "react-hot-loader/babel"]
    // "plugins": ["transform-runtime"]
}

*/